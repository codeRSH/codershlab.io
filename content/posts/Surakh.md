---
public: true
title: सुराख
date: 2014-06-27
tags:
  - 30-day-challenge
categories: hindi
---

> **रूह में सुराख हो तो दर्द रिस्ता है धीरे धीरे,**  
> **रूह भीगती है दर्द में तो भूल जाते हैं सब नाते रिश्ते,**  
> **कोई समझता ही नहीं इस लाइलाज रोग को,**  
> **सिवाय उसके जिसकी रूह में खुद सुराख हो,**  
> **दर्द सूखता भी नहीं, बहना रुकता भी नहीं,**  
> **टूट जाता है परिंदा ये सुराख भरते भरते।**
> 
> **~रबी**


\[ When there is a void in the soul, the pain trickles slowly,  
When the soul drenches in pain, all the relations get forgotten,  
No one understands this incurable disease,  
Except the one whose own soul has a void in it,  
The pain doesn’t dry, neither does it stop bleeding,  
The bird breaks down trying to fill that void. \]
