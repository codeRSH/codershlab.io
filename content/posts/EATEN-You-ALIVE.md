---
public: true
title: EATEN you ALIVE
date: 2014-12-27
tags:
  - problem
  - haiku
categories: english
---

![ ](../images/eaten-you-alive.png)

> **If CANNIBALISM were ALLOWED,**  
> **I would have EATEN you ALIVE.**  
>   
> **Can’t think of another SOLUTION to the problem that’s YOU.**
> 
> **~RavS**
