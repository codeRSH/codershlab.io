---
public: true
title: अँगूठा - छाप
date: 2012-04-25
tags:
  - sarcasm
  - humor
categories: hindi
---

> **ना  कर  दिन  रात  इतने  एस.एम्.एस.  बंदया।**   
> **कहीं  लोग  तुझे  अँगूठा - छाप  ना  समझ  बैठें।** 
> 
> **~रबी** 

\[ Don’t type so many SMS, day and night, Buddy!  
Otherwise people might think you are a thumb-stamper\*!!  

_(\*One who doesn’t even know how to write.)_ \]
