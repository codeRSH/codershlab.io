---
public: true
title: अनकही बात
date: 2012-08-13
tags:
  - love
  - longing
  - regret
categories: hindi
---

> **एक बात थी कहने को,  
> जो अरमान बन रह गयी।**   
>   
> **दिल से तो निकली थी उफ़न के,  
> पर ज़ुबान बंद रह गयी।**   
>   
> **तुझको पाया तो औरों की क्या कहें,  
> खुद अपने हाथों की लकीरें दंग रह गयीं।**   
>   
> **कुछ इस कदर हारे हम तेरी आदतों से,  
> मेरी हर अदा तेरी शोख़ियों के आगे कम रह गयी।**   
>   
> **मगर…  
> ख़बर मिली रुख़सत होने की तेरी मुझे जब,  
> लहू की बूँदें रगों में जम रह गयीं।**   
>   
> **छीन लिया उस ख़ुदा ने तुझे मुझसे,  
> और वो अनकही बात दफ़न हो संग रह गयी…**
> 
> **~रबी**

  

\[ I wanted to say something,  
But I couldn’t say.  
  
It blasted from the heart,  
But lips didn’t sway.  
  
When you came in life, of others what can I say,  
My own luck couldn’t believe it.  
  
I lost to your habits so badly,  
All my style couldn’t match your flattery.  
  
But…  
The day I found you passed away,  
The blood in my veins froze.  
  
That God took you away from me,  
Those last words, I couldn’t say… \]
