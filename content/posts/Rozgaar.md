---
public: true
title: रोज़गार
date: 2012-03-08
tags:
  - unemployment
  - philosophical
categories: hindi
---

> **मेरी  तकलीफ  और  तुम्हारी  रोहानी  में  फर्क  सिर्फ़  इतना  था ,**  
> **मुझे  रोज़गार  न  मिला , तुम  बेरोज़गार  रह  गए।**
>
> **~रबी**


\[ There was only this much difference between my pain and your discomfort,  
That I couldn’t find work, and work couldn’t find you. \]
