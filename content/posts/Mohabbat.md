---
public: true
title: मोहब्बत
date: 2014-10-02
tags:
  - love
  - tragedy
categories: hindi
---

> **मैंने कहा खुदा से तूने सबको दी ,**  
> **थोड़ी मोहब्बत मेरे हिस्से भी लिख दे।**  
> **खुदा ने कहा तू बदनसीब है रबी ,**  
> **मोहब्बत तेरे मुकद्दर में नहीं।**  
>   
> **लोग चोट खाते हैं अक्सर इश्क में,**  
> **तुझे इश्क ही चोट देता रहेगा।**  
> **मत चाहना किसी को, भूल कर भी ,**  
> **जिसे चाहेगा, वो तुझसे दूर ही रहेगा।**  
>   
> **न कर मोहब्बत किसी से, वर्ना समझ ले ,**  
> **मोहब्बत तेरे पास तो रहेगी, पर तुझे कभी मिल न सकेगी।**  
>   
> **~रबी**


\[ I told God, you gave everyone,  
Please write a little love for me as well.  
God said, you are unlucky Rabi,  
Love is not in your fate.

People often get hurt in Love,  
The Love itself will keep hurting you,  
Do not even dare to like anyone,  
Whoever you would like, they will remain off from you.

Do not love anyone, else understand this,  
Your love would be near you always, but you will never get it. \]
