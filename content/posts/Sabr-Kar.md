---
public: true
title: सब्र कर
date: 2014-10-17
tags:
  - dedication
  - special
  - art
categories: hindi
---

![ ](../images/sabr-kar.png)

>**आग जला कर कोई भूल गया होगा सर्द रात में,  
>रात काली है, कोहरा घना है अभी भी,  
>उजली सुबह की चाह में अकेली बेठी है यहाँ,  
>थोड़ा सब्र कर सुबह आएगी तो सही।**
>
>**तू अकेली नहीं है, और भी हैं यहाँ,  
>हँसते खेलते जिनकी तिलमिला उठती है ज़िन्दगी,  
>सपने देखती है, सपनो से ही डरती है अपने,  
>थोड़ा सब्र कर, नींद अच्छी आएगी तो सही।**
>
>**चलते चलते थक गयी है तू, मालूम है,  
>दूर कहीं उड़ जाने की ख्वाइश है तेरी,  
>मन बहुत करता होगा नई ऊँचाइयाँ छूने का,  
>पर थोड़ा सा तो सब्र कर , पंख लगने दे तो सही।**
>
>**तारे तोड़ लेने को जी करता है तेरा ,  
>रहने दे उन्हें अम्बर में, गिनतियाँ कम न पड़ जाएँ किसी की,  
>लाना है तो अम्बर ही खींच ला ऊपर से,  
>या सब्र कर, वो खुद तेरे आँचल में सिमट आयेगा तो सही।**
>
>**~रबी**  


\[ Someone must have forgotten after lighting fire on a cold night,  
The night is black, the fog is still dense,  
You are sitting alone here, wishing for a sunny morning,  
Have a little patience, the morning will definitely come.

You are not alone, there are others too,  
Whose life has taken a turn all of sudden,  
You dream, and then you fear your own dreams,  
Have a little patience, a good night’s sleep will definitely come.

You are tired of walking, I know,  
You want to fly away far somewhere,  
You would be wishing to touch new heights,  
But have a little patience, let the wings grow first.

You want to pluck the stars,  
Leave them in the sky, somebody would fall short of counting,  
If you really want, then pull the whole sky downwards,  
Or have patience, it will itself come and fall in your lap. \]

_## Artwork after a long time. Thank you mate for the Sketch. I would take all the credit though. :) ##_
