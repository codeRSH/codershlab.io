---
public: true
title: बात
date: 2023-06-23
tags:
  - loneliness
categories: hindi
---

> **बात, करने को तड़पते हैं कई लोग,  
> बात, करो तो बात से बात निकलती है।  
> बात, ये नहीं कि दोस्त कितने हैं, मेरे दोस्त,  
> बात, ये है की उनसे दोस्ती कितनी है।**  
>
> **~रबी**  

\[ Many people yearn to talk.  
If you start a conversation, it leads to more conversation.  
The important thing is not how many friends you have, my friend.  
The important thing is how deep your friendship is with them. \]  
