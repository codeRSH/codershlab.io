---
public: true
title: प्यार  और  डर
date: 2012-01-30
tags:
  - friends
  - love
  - fear
categories: hindi
---

> **किसी  ने  मुझसे  कहा  था  कभी ,**   
> **इस  दिल  में  प्यार  और  डर  दोनों  एक  साथ  नहीं  रह  सकते।**   
>   
> **तो  करते  हैं  प्यार  तुमसे  अब  उतना  ही ,**  
> **जितना  पहले  तुमसे**  **डरा   करते  थे।** 
> 
> **~रबी** 

\[ Somebody someday told me,  
Love and fear can’t co-exist in this heart,  
  
So now I love you to the same extent,  
The extent to which I used to fear you. \]
