---
public: true
title: Lust of Insanity
date: 2014-12-07
tags:
  - happy
  - thoughts
categories: english
---

![ ](../images/lust-of-Insanity.png)

> **Baby I am the rock, I am the salt.**  
> **I am nickel, I am cobalt.**  
> **You are the catalyst of my affinity,**  
> **You are me-phile, my vanity,**  
> **You are my lust of insanity.**
> 
> **~RavS**
