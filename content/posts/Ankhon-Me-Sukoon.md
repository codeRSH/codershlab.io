---
public: true
title: आँखों  में  सुकून
date: 2012-03-09
tags:
  - hurt
  - guilt
categories: hindi
---

> **सुना  था ,  कुछ  पाने  के  लिए  है  कुछ  खोना  ही  पड़ता ,**  
> **पर  अपनों  के  लिए  पाते - पाते, अपनों  को  ही  खो  दूँ , चाहा ना  था।**   
> **वादे  तोड़े , दर्द  दिया , अपनी-ओ-अपनों  की  नज़रों  में  गिर  गया ,**   
> **आख़िर जीता , थक  हार  कर , पर  आँखों  में  सुकून  फिर  भी  ना  था।** 
> 
> **~रबी**


\[ I had heard that to achieve something, you need to lose something,  
But I never thought that while winning for my own ones, I will lose even them.  
I broke promises, I hurt them, I fell down in my and loved ones’ eyes,  
At last I won what I had set out for, but there was no peace in my eyes. \]
