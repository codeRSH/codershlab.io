---
public: true
title: Dear Friend, Thanks for your text
date: 2013-08-04
tags:
  - friendship
  - special
  - letter
categories: english
---

> **Dear friend, last night I got your text,**  
> **Where you wrote how, “I’m one of ya best”.**  
> **Well let me tell you first I appreciate that,**  
> **You took sometime out to say what you said.**  
>   
> **What I didn’t like is that you took a year,**  
> **To state something that should have been so obvious.**  
> **You could have told me this when we last met,**  
> **Or piggybacked it with your occasional “gn” messages.**  
> **Or left it unsaid, and I would have understood,**  
> **Because after all that’s what real friends do.**  
>   
> **But you had to choose Today from the calendar,**  
> **As if it was decided by some divine intervention.**  
> **So tell me what’s the difference between you and them,**  
> **Who send me the exact lines today that you just sent.**  
> **Or how do I differentiate myself from your other friends,**  
> **With whom you share wishes through mass messaging.**  
>   
> **Why didn’t you write all this, mate,**  
> **When the things in my life weren’t looking great.**  
> **When I desperately needed a confidant,**  
> **To express what was going in my mind.**  
> **When I even thought for a while to cut myself,**  
> **Only few were around when I needed some help.**  
>   
> **I realize you can’t take all of the blame,**  
> **Even my initiatives to forge our friendship have been lame.**  
> **But pal, understand that things haven’t been good off-late,**  
> **And sometimes I did try, but you didn’t reciprocate.**  
> **And let it be known here, that I am quite self-centered,**  
> **I only give that much in a relationship that I get back.**  
>   
> **I know in my heart, I’ve been a great friend,**  
> **To whoever that treated me more than an acquaintance.**  
> **But whenever I get cold shouldered, or treated like crap,**  
> **I throw them out of my life, in a finger’s snap.**  
> **And I pray to Dear God, it never comes to this,**  
> **But if I am forced to do that to you, I’ll be unapologetic.**  
>   
> **I know your intentions weren’t bad,**  
> **So I urge you, please don’t misunderstand.**  
> **Anyway, never mind, I got it, how I am your best,**  
> **Till next Friendship Day, thanks for your text.**
> 
> **~RavS**
 

## _It sucks when many of your ‘friends’ would remember you only on Friendship day. ##_
