---
public: true
title: फसाद
date: 2015-11-02
tags:
  - fight
  - love
categories: hindi
---

> **मेरे  दिल  में  क्या  छुपा  है, मेरे   मन  में  क्या  बसा  है,    
> अगर  वो  जान   जाता, मेरा  ईमान  पहचान  पाता।  
> जो  तकलीफ  है  उसे  मुझसे  , जो  फसाद  है  मुझे   उससे,  
> शायद  मैं  बता  पाता, शायद  वो  खुद  समझ  जाता।**
>
> **~रबी**


\[ What’s hiding in my heart, why’s residing in my mind,  
If he would have found out, he would have known my honesty,  
The pain that I have from him, the quarrel that I have with him,  
May be I might have told, may be he himself might have understood. \]
