---
public: true
title: नौकरशाही
date: 2012-02-12
tags:
  - life
  - friends
  - unemployment
categories: hindi
---

> **जब  बेकार  थे, तब  ही  ज़िन्दगी  ख़ुशनुमा थी।**    
> **उनकी  नौकरशाही  हमने  बेकार  कबूल  की …**
> 
> **~रबी**


\[ When I had no work, life was so good,  
I don’t know why I accepted to become their servant… \]
