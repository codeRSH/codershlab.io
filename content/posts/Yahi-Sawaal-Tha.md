---
public: true
title: यही सवाल था
date: 2014-01-09
tags:
  - dream
  - weird
  - special
categories: hindi
---

> **वो कोई तो जाना-पहचाना सा शख्स था,**   
> **जिसका आँखों में आज अक्स था।**  
> **कुछ धुंधली धुंधली सी उसकी परछाई थी,**  
> **अचानक ही अहातों में उतर आयी थी।**
> 
> **कुछ तो था जो वो कहना चाहता था,**  
> **पर मेरे इर्द गिर्द तो बस एक घहरा सन्नाटा था,**  
> **कानो तक पहुँचती उसकी सहमी सहमी सी साँसें थी,**  
> **छिपी कहीं जिनमे दम तोड़ चुकी कुछ बातें थी।**
> 
> **कुछ दूर बढे उसके मेरी तरफ कदम, फिर सिहर गए,**  
> **हाथ उठे कुछ दिखाने को, पर ठहर गए,**  
> **“पीछे मत देखना"… बस इतना कहा उसने फिर मुड़ गया,**   
> **नहीं आया वो साया लौटकर, एक बार जो उड़ गया,**   
> **धड़कने बढ़ चली, वहीं खड़ा रहा निस्तब्ध सा,**  
> **मन तो बहुत किया, पर मुड़कर नहीं देखा।**
> 
> **आँखें बंद कर सोने की बेईमानी सी कोशिश की ,**  
> **पर निगाहों में नींद नहीं, उसका ख़याल था,**  
> **कौन था वो, क्या चाहता था,**  
> **आज बस जेहन में यही सवाल था।**
> 
> **~रबी**


\[ He seemed very familiar,  
whose image was caged in my eyes,  
His shadow was a little hazy,  
which had come nearer all of a sudden.

There was something that he wanted to say,  
But there was just deep silence around me,  
Only a few afraid whispers reached my ears,  
Inside which were hidden some dead conversations.

His legs moved a little nearer, then shuddered,  
His hands rose to show something, but stopped,  
"Don’t look back”… He said this much and turned,  
He never came back, once he flew away unseen,  
The heart beats rose, I stood there shocked,  
I wanted to see behind, but decided not to.

I closed my eyes and tried to sleep,  
But there was no sleep in eyes, but his thoughts,  
Who was he, what did he want,  
Today that’s all I could think of. \]
