---
public: true
title: रोज़ मरना
date: 2023-06-24
tags:
  - sad
categories: hindi
---

> **कोई क्या ही समझ सकेगा उसका दर्द, रबी,  
> जिसे मरहम से पहले दर्द नोचना पड़ता है।  
> तुम खुशहाल जीने की नसीहत ना ही दो तो सही,  
> उसे ज़िंदा रहने के लिए रोज़ मरना पड़ता है।**
>
> **~रबी**

\[ Who can truly grasp the depth of his pain, Rabi,  
One who must scratch his wounds before applying balm?  
It's futile to offer him advice on living joyfully,  
For he must die every day just to remain alive. \]
