---
public: true
title: पाँव
date: 2014-07-11
tags:
  - 30-day-challenge
  - birthday
  - special
categories: hindi
---

![ ](../images/paanv.jpg)

> **कभी लाओ अपने पाँव हमारे आँगन में भी,**  
> **बहुत दिनों से गलियारे में हरियाली नहीं हुई है,**  
> **जहाँ जहाँ भी पड़ जाते हैं कदम आपके,**  
> **वो धूल भी सोने के दाम बेचीं जाती है।**
> 
> **पर कुछ बदकिस्मत नहीं पहचानते इन कदमो की कीमत को,**  
> **तो क्या पता इस बहाने पाँव की नमी मिल जाय हमारी ही सूखी जमी को,**  
> **अच्छा ही है, उम्मीद है, उम्मीद बंधे रहने दो,**  
> **और हाँ एक बात और… इन कदमो को, जन्मदिन मुबारक हो।**
> 
> **~रबी**


\[ Bring your feet in our yard also someday,  
The corridor hasn’t been green for a long time,  
Wherever you go, wherever you keep your feet,  
That dust is also sold at gold prices.  
  
But a few unlucky ones don’t recognize the value of these steps,  
So who knows, this way the moisture of these feet reach our dry ground as well,  
It’s better this way, there’s hope, let the hope remain,  
And yes one more thing … to these feet, Happy Birthday. \]
