---
public: true
title: अब कुछ बचा नहीं...
date: 2014-02-25
tags:
  - special
  - praise
categories: hindi
---

![ ](../images/ab-kuch-nahi-bacha.jpg)

> **किसी ने हूर, किसी ने नूर,**  
> **किसी ने परी, किसी ने जलपरी कहा होगा,**  
> **किसी के लिए मोहिनी, किसी के लिए अप्सरा,**  
> **कोई  कहे संजीवनी, किसी ने विष कन्या का नाम भी  लिया होगा,**  
> **अब क्या कहूँ इसके बाद तुम्हारे वास्ते,**  
> **मेरे कहने के लिए अब कुछ  बचा नहीं।**  
>   
> **किसी को हुई होगी चेहरे को चाँद समझने की गलतफहमी,**  
> **कोई माथे को सूरज समझ बैठा होगा,**  
> **किसी ने झुकी पलकों में आयतें ढूंढी होंगी,**  
> **कोई जुल्फों को ही नमाज समझ बैठा होगा,**  
> **किसी को हंसी से ख़ुशी नसीब हुई होगी,**  
> **कोई उसी हंसी से जल मरा होगा,**  
> **अब क्या ढूँढू  खुली किताब में अपने वास्ते,**  
> **मेरे ढूंढने के लिए **अब** कुछ बचा नहीं।**  
>  
> **किसी ने होठों पर नगमे लिखे होंगे,**  
> **किसी ने अदाओं पर बयाने-हश्र किया होगा,**  
> **किसी ने कसीदे लिखे होंगे लौंग पर,**  
> **कई गीतों में बालियों का ज़िक्र हुआ होगा,**  
> **सैकड़ों रुबाइयों लिख डाली गयी होंगी बस गालों पर,**  
> **किसी कविता की खुश्बू को आँखों ने इत्र दिया होगा,**  
> **अब क्या ही लिखूं  सबके  बाद तुम्हारे वास्ते,**  
> **मेरे लिखने के लिए **अब** कुछ बचा नहीं।**  
>  
> **क्या पता कितनी आँखों से नींद उडी होगी,**  
> **किसे इल्म कितने अंधों ने देखना शुरू कर दिया होगा,**  
> **क्या खबर कितने क़त्ल हुए होंगे दिन भर,**  
> **कौन जाने कोई मरता जी गया होगा,**  
> **अब क्या सोचूं इसके बाद तुम्हारे वास्ते,**  
> **मेरे सोचने के लिए भी अब कुछ बचा नहीं।**
> 
> **~रबी**


\[ A Beauty for someone, a glimmer for someone,  
A fairy for someone, a mermaid for someone,  
Someone calls you siren, others a nymph,  
Some call you life-giver, some refer you as a girl of poison ;  
Now what shall I say about you, after all that,  
There’s nothing left for me to say.  
  
Someone would have mistook your face as a moon,  
Someone would have mistook your forehead as sun,  
Someone would have tried to discover verses in eyelids,  
Someone would have mistook your curls as prayers,  
Your laugh would have made a few happy,  
Others would have burnt to death by it,  
Now what shall I search in this open book for myself,  
There’s nothing left for me to find.  
  
Someone would have written melody on your lips,  
Someone would have told his story stung by your charms,  
Someone would have written ballads on your nose ring,  
Many of the songs would have discussed your earrings,  
Hundreds of lines would have been written only on your cheeks,  
Your eyes would have given perfume to a poem,  
Now what shall I write for you, after all that,  
There’s nothing left for me to write.  
  
Sleep would have disappeared from so many eyes,  
Who knows how many blinds would have started to see,  
No one knows how many people would have murdered each other for you,  
Who knows a dying person would have gotten well,  
Now what shall I think about you, after all that,  
There is nothing left for me to think even. \]
