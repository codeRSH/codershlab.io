---
public: true
title: Cold Freaking Night
date: 2013-04-04
tags:
  - life
  - philosophical
categories: english
---

> **As I walked through a long dark tunnel,**  
> **At the other end I saw a glimmer of light.**  
> **I started running towards it in hope,**  
> **That finally now, everything’s going to be alright.**  
> **It was nothing, but mirage of disappointment,**  
> **That too in the middle of a cold freaking night.**
>
> **~RavS**
