---
public: true
title: NOT ME
date: 2014-08-12
tags:
  - life
  - me
categories: english
---

> **My whole life has been a merry-go-round of distancing people and craving their intimacy,**
>
> **While I am not busy hating the world, what I seek from it is a little empathy,**  
> **Whining about burdensome relations but keeping them close, clutched firmly,**  
> **A paradox in the man who loves being alone but always fears being lonely.**
>
> **It’s a constant fight between trying to love people all the while hating their guts,**  
> **An unending see-saw between wishing people happiness and trying not to be jealous,**  
> **Sometimes everything in life is so crystal clear, sometimes nothing at all makes sense,**  
> **A struggle to reach the doors of bliss, only to find the keys missing in the end.**
>
> **To be bad or to be good, to chuckle or to brood,**  
> **It does get tiring sometimes to always second guess your every move.**  
> **Sometimes I wonder if I am a part of some sick Universe’s grand experiment.**  
> **May be it’s its trick to find out, without breaking, how much a man’s back can be bent.**
>
> **To be in constant state of flux, not knowing a single thing.**  
> **To wage a new war everyday, life sure is interesting, to say the least.**  
> **But Who signed up for this life in the first place?**  
> **Not me. Not me. Not me. Not me.**
>
> **~RavS**

_## Am I revealing a little too much? Anyway, I liked this composition; I think it’s pretty tightly crafted. Classic yet elegant. I suppose I am getting better as a lyrics writer :) ##_
