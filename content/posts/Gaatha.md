---
public: true
title: गाथा
date: 2014-07-18
tags:
  - 30-day-challenge
categories: hindi
---

> **गाथा इस बात से नहीं बनती की तुम्हारे जीवन में क्या हुआ,**  
> **गाथा इस बात से बनती है की तुमने जीवन में क्या किया।**  
> **गाथा कोई इसलिए याद नहीं रखता की तुम्हें बस सुख ही सुख मिला,**  
> **गाथा इसलिए याद रखी जाती है की तुमने दुखों पर फतह कैसे किया।**  
> **गाथा बनानी ही है तो दूसरों की गाथा का हिस्सा बनो,**  
> **तुम्हारी गाथा खुद-ब-खुद बन जायेगी।**
> 
> **~रबी**

  
\[ The saga does not originate from what happened in your life,  
The saga originates from what you do in life.  
Nobody remembers the story in which you got so much happiness,  
The story is remembered as how did you triumph over the sufferings.  
If you really want to write your story, be a part of others’ saga,  
Your story will write itself automatically. \]
