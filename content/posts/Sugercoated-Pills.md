---
public: true
title: Sugercoated Pills
date: 2018-08-13
tags:
  - poem
categories: english
---

![ ](../images/sugarcoated-pills.jpg)

> **Aren’t poems  
> just sugarcoated bitter pills  
> from one patient to another.**  
>  
> **~RavS**  


_## Poems aren’t for everyone, and its quite evident from how a few people scorn from it. However, there’s another aspect to it. I feel to truly understand a poem you have to be able to ‘feel’ it and to be able to feel a poem you would have to go through a similar experience sometime in your life. ##_
