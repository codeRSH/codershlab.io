---
public: true
title: डर
date: 2014-12-02
tags:
  - fear
  - anger
  - request
categories: hindi
---

> **डर इस बात का नहीं कि हमें डर लगता है,**  
> **डर है तो बस कि कहीं हम डरना ना छोड़ दें।**  
> **डर लगता है अंदर जो सुलगती आग है,**  
> **डर लगता है कहीं और ना भड़क उठे।**  
> **खुद घुट कर मर जाएँ तो ज्यादा रंज नहीं,**  
> **डर लगता है कहीं दुनिया को राख ना कर बैठें।**
> 
> **~रबी**

  
\[ I don’t fear that I am afraid,  
I fear that I might stop being afraid.  
I fear the fire that’s smoldering inside,  
I fear it might get erupted.  
I won’t regret if I die choking of it,  
I fear I might end up burning the world with it. \]
