---
public: true
title: Li'l Soldier
date: 2019-03-25
tags:
  - separation
  - pain
categories: english
---

![ ](../images/lil-soldier.jpg)

> **Do not hold on, Li'l soldier,  
> Let these people go.  
> Your clutches will not stop them,  
> from going, just make it a Li'l slow.**
>
> **The more you resist, Li'l soldier,  
> The more the fights will grow,  
> Eventually they’ll leave, I promise,  
> And you’ll be left with Li'l blisters to show.**
>
> **And what’s even there to stop, Li'l soldier,  
> You can’t stop time, when that time is already gone,  
> You can’t stop from breaking, what’s already broken from storm,  
> And you can’t be Li'l right, now, when you’re already wholly wrong.**
>
> **So here is what you do, Li'l soldier,  
> Put up a brave face, put on a fake smile,  
> Tell them you are happy, alright,  
> As they start a Li'l new phase of their life.**
>
> **~RavS**


_## How you feel when someone leaves. ##_
