---
public: true
title: जलन
date: 2014-06-25
tags:
  - 30-day-challenge
categories: hindi
---

> **पता नहीं तुझसे इतनी जलन क्यों होती है,**  
> **तुझे देखता हूँ खुश तो एक चुभन सी होती है,**  
> **ये हैरान करने वाली बात नहीं तो और क्या है,**  
> **क्योंकि तू परेशान हो तो भी एक घुटन सी होती है।**
> 
> **शायद मुझे जलन तुझसे नहीं, ना तेरी ख़ुशी, ना तेरी हंसी-ओ-हालात से है,**  
> **तेरी ख़ुशी में शरीक नहीं मैं, शायद जलन मुझे इस बात से है,**  
> **कोई और तेरी ख़ुशी का जरिया है, ये मुझे बर्दाश्त नहीं,**  
> **और तुझे इस बात की कतई जलन नहीं, जलन मुझे इस बात से है।**
> 
> **~रबी**


\[ Don’t know why I feel so jealous of you,  
When I see you glad, I needle pierces through my heart,  
And this is really amazing,  
Because when I see you upset, then also I feel suffocation inside.

Maybe I do not envy you, or your happiness, or your laughter,  
Probably what irritates me is that I am not part of your happiness,  
Someone else be the source of your happiness, I can not stand it,  
And you don’t feel anything, that’s what irritates me. \]
