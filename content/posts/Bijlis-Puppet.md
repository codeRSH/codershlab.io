---
public: true
title: Bijli's Puppet
date: 2013-06-23
tags:
  - power
  - problems
  - special
categories: english
---

> **It’s a story not a ‘long ago’,**  
> **There was a boy, let’s name him Arlo.**  
>   
> **He didn’t have a lot of friends,**  
> **But he never cried.**  
> **He knew he had Bijli\* 24 Seven,**  
> **To keep him occupied.**  
>   
> **But apparently Bijli had other plans in store,**  
> **Soon came those terrible summer nights,**  
> **When Bijli would start to elope,**  
> **Without Arlo getting even notified.**  
>   
> **Poor Arlo. He thought Bijli was all his,**  
> **After all he had spent a lot to get her.**  
> **But, what he didn’t know was this,**  
> **Bijli had a huge crush on his neighbor.**  
>   
> **Every night he tussled and tossed in his bed,**  
> **Till the time his body would drown in his own sweat.**  
> **Seconds felt like minutes, minutes turned into hours.**  
> **Every time he hoped Bijli would return sooner.**  
>   
> **But Bijli would stay away all night,**  
> **And he couldn’t do anything to make it alright.**  
> **He was so used to her company now,**  
> **Life had become unimaginable without.**  
> **Every moment he felt so ‘power-less’,**  
> **He was now nothing, but Bijli’s puppet.**  
>   
> **Yes it’s a sad story,**  
> **But it’s so very true.**  
> **Never let anyone, anything,**  
> **Become so indispensable for you.**
> 
> **~RavS**

\*Bijli = Electricity
