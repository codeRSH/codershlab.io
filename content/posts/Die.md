---
public: true
title: Die
date: 2020-07-16
tags:
  - special
  - fear
  - love
categories: english
---
![](../images/die.png)

> **What I am afraid of more than dying…  
> is the thought of living long enough…  
> to see you die…**  
>
> **~RavS**

_## Are you not afraid of living long enough to see your loved ones die? ##_
