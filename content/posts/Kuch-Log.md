---
public: true
title: कुछ  लोग
date: 2017-09-11
tags:
  - frustration
categories: hindi
---

> **वो  हाथ  तो  पकड़ते  हैं, मगर  साथ  नहीं।  
> वो  मिन्नतें  तो  सुनते  हैं  , पर  गुहार  नहीं।  
> वो  निवाला  तो  देते  हैं, मगर  इलाज  नहीं।  
> कुछ  लोग  मदद  करते  हैं,  कुछ  लोग  नहीं।**
>
> **वो  प्यार  तो  करते  हैं, मगर  मजबूरियों  के  चलते।  
> वो  वादे  तो  करते  हैं, पर  सब  झूठे  हैं  निकलते।  
> वो  विश्वास  तो  करते  हैं, मगर  सहूलियत  के  हिसाब  से।  
> मैं  किससे  उम्मीद  करूँ, और  किससे  नहीं।**
>
> **वो  मेरा  इस्तेमाल  करते  हैं, मगर  क्या  ये  गलत  है ?  
> वो  दिखावे  का  ख्याल  रखते  हैं, पर  क्या  ये  गलत  है ?  
> वो  मुझसे  बेजां  मलाल  रखते  हैं  , मगर  क्या  ये  गलत  है ?  
> कुछ  लोग  समझते  हैं  रबी, कुछ  लोग  नहीं।**
>
> **~रबी**


\[ They hold hands, but don’t support.  
They hear requests, but never listen,  
They give food, but not the cure.  
Some people help, some people don’t.

They do love, but out of compulsions.  
They promise, but they all prove false.  
They do believe, but according to convenience,  
Whom do I expect, and whom not.

They use me, but is that wrong?  
They show false care, but is that wrong?  
They are always complaining, but is that wrong?  
Some people understand Rabi, some people don’t. \]

_## Mind dump. ##_
