---
public: true
title: लौट आओ, बस एक बार
date: 2012-07-12
tags:
  - main-blog 
  - special 
  - sad
categories: hindi
---

> **सालों  बीत गए,  
> लेकिन आज भी,  
> कभी कभी दर्द उठता है,  
> कभी कभी चुभन होती है,  
> जब अचानक तुम्हारी याद आती है।** 
> 
> **लोग कहते हैं,  
> समय बड़ा बलवान होता है,  
> सारे घाव भर देता है,  
> शायद सच भी है,  
> क्योंकि अब खून नहीं रिसता,  
> अब आँसू नहीं निकलते।** 
> 
> **पर दर्द तो आज भी होता,  
> साँसों में तड़प तो आज भी होती है,  
> काश बाहर के घाव नहीं भरते,  
> काश आँखें नहीं सूखती,  
> शायद कोई ये तकलीफ देख पाता,  
> शायद कोई दर्द कम कर पाता।** 
> 
> **तुम्हारे जाने का गम नहीं,  
> जाना तो आखिर सभी को है,  
> गिला बस इतना है तुमसे,  
> तुम चली गयीं, और मुझे इत्तेला भी ना किया,  
> या शायद... शायद बतलाया भी हो कभी,  
> शायद मैं बदनसीब ही समझ ना सका।** 
> 
> **हँसता हूँ किस्मत पर अपनी कभी कभी,  
> इतने साल लगा दिए तुम्हें समझने में,  
> और जब तक समझा,  
> तुमने रुकना मुनासिब नहीं समझा,  
> तुम रहती थी तो जलन होती थी,  
> और  आज नहीं हो तो चुभन होती है।** 
> 
> **तुम्हें जाना था, तुम चली गयीं,  
> तुम्हें रोकने का शायद हक भी मुझे ना था,  
> पर यादें तो तुम अपनी यही छोड़ गयीं,  
> कहो ये हक तुम्हें किसने दिया?  
> अब तुम ही बताओ, इन यादों का मैं करूँ,  
> ये यादें मुझे जीने नहीं देतीं।** 
> 
> **धीरे धीरे ही सही, तुम बिन जीना सीख रहा हूँ,  
> घुट घुट ही सही, सांसे लेना सीख रहा हूँ,  
> जो खुशियाँ तुम्हें ना दे पाया,  
> जिस सुकून के लिए तुम तडपती रही,  
> वो अब अपनों को देना सीख रहा हूँ।**
> 
> **लौट आओ, बस एक बार,  
> अगर हो सके तो,  
> गलती मेरी क्या थी, मुझे बता दो।  
> अरे गलत तो तुम हो,  
> तुम कभी अकेली नहीं थी,  
> अकेला तो तुम मुझे कर गयीं।** 
> 
> **~रबी**


\[ Years have passed,  
But even today,  
Sometimes the pain rises,  
Sometimes there's a sting,  
When I suddenly remember you. 

People say,  
Time is very powerful,  
It heals all wounds,  
Perhaps it's true,  
Because now there's no bleeding,  
Now tears don't fall. 

But the pain is still there,  
The agony in my breath is still there,  
I wish the external wounds didn't heal,  
I wish my eyes didn't dry up,  
Maybe someone could see this suffering,  
Maybe someone could lessen the pain. 

I don't grieve your departure,  
Everyone has to go eventually,  
My only complaint is with you,  
You left, and didn't even inform me,  
Or maybe... maybe you did tell me sometime,  
Maybe I was unlucky to not understand. 

I laugh at my fate sometimes,  
I spent so many years trying to understand you,  
And by the time I did,  
You didn't think it appropriate to stay,  
When you were here, I felt jealousy,  
And now that you're gone, I feel a constant sting.  

You had to go, you left,  
Perhaps I didn't even have the right to stop you,  
But you left your memories here,  
Tell me, who gave you this right?  
Now you tell me, what am I to do with these memories,  
These memories don't let me live.  

Slowly but surely, I'm learning to live without you,  
Still gasping for breath, I'm learning to breathe,  
The happiness I couldn't give you,  
The peace you yearned for,  
I'm now learning to give to my loved ones.  

Come back, just once,  
If possible,  
Tell me what my mistake was.  
Actually, you're the one at fault,  
You were never alone,  
You're the one who made me alone.  \]
