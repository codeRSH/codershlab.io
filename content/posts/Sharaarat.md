---
public: true
title: शरारत
date: 2014-07-01
tags:
  - 30-day-challenge
categories: hindi
---

> **जो बात अनकही रह गई,**  
> **है खबर मुझको भी,**  
> **है खबर तुमको भी,**  
> **हैरान परेशान रहना सब कुछ कुछ जानकार भी,**  
> **शरारत करने लगी हैं आपकी नादानियां भी।**
> 
> **~रबी**


\[ What was left untold,  
I also know that,  
You know it too,  
To remain bewildered while knowing everything,  
Your innocence have started playing prank too. \]
