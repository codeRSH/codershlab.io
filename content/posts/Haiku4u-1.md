---
public: true
title: Haiku4U - 1
date: 2019-04-01
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-1.jpg)

> **Old reasons left behind.  
> We find new reasons,  
> to live n’ love.**
>
> **~RavS**


_## Some of the reasons for which I started loving you with have been left behind now. Old perceptions are getting broken. But new ones are being made. I see you in new light. More imperfect than I ever thought. But also worth loving more than I ever thought.  ##_
