---
public: true
title: अब उतर भी आओ ज़मीं पर।
date: 2014-09-03
tags:
  - special
  - song
  - dedication
categories: hindi
---

> **धरती पर जश्न होगा,**  
> **हर कोई तब मग्न होगा,**   
> **बजेगा मदहोशी का संगीत ,**   
> **खुद थिरकने का मन होगा…**   
> **बंजर मिट्टी को बूँद मिलेगी,**  
> **रुक गई बयार चल पड़ेगी,**  
> **कुछ टहनियाँ उदास पड़ी हैं,**  
> **तुम्हे देख फिर झूम उठेंगी…**   
> **बस…**   
> **अब उतर भी आओ ज़मीं पर।**   
> **अब उतर भी आओ ज़मीं पर।**
> 
> **रह गयी तुम्हारे बिना,**  
> **कुछ पंक्तियाँ लफ़्ज़ों बिना,**  
> **कुछ आँखें दीदार को,**  
> **कुछ होंठ मुस्कुराये बिना…**   
> **कुछ बैठे हैं आस में,**  
> **तुमसे मिलने की ताक में,**  
> **शायद मन्नत मिल ही जाय,**  
> **कुछ ऐसे ही एहसास में…**   
> **बस…**   
> **अब उतर भी आओ ज़मीं पर।**   
> **अब उतर भी आओ ज़मीं पर।**
> 
> **~रबी**

  
\[ There will be celebrations on earth,   
Everyone then would be fain,  
There will trance music aloud,   
And dance will be on everyone’s mind…   
Barren soil will get a drop,   
The stopped winds will start blowing again,   
There are some twigs which are sad,  
They will start dancing with pleasure seeing you…   
That’s it …   
Please come on the ground now.   
Please come on the ground now.

They remained uncomplete without you,   
Some lines without words,   
Some eyes without your glimpse,   
Some lips without smiling…  
There are some sitting around,   
Waiting to meet you,   
May be their prayers get answered ,   
With such thoughts with them …   
That’s it …   
Please come on the ground now.   
Please come on the ground now. \]

_## This is one my special compositions. I tried to keep the number of words to 4-5 in each line (except the ending). May be will develop this into a song. ##_
