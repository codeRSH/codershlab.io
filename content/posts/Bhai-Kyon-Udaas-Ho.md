---
public: true
title: भाई क्यों उदास हो
date: 2014-05-10
tags:
  - sister
  - separation
  - feelings
categories: hindi
---

> **जो दिल में बस्ते हों वो आँखों में उतर आएं तो,**  
> **जो आँखों में तैरते हों वो दामन में छलक आएं तो,**  
> **जो दामन में सिमटे हों वो हाथों से निकल जाएँ तो,**  
> **नहीं पूछना चाहिए… भाई क्यों उदास हो।**
> 
> **~रबी**


\[ Those who live in heart, if they come in the eyes,  
Those who float in eyes, if they spill on the shoulders,  
Those who stay on shoulders, if they leave the hands,  
You should never ask…. why are you sad bro. \]

_## Met my sister today after one and half year. For two and half hours. Some bittersweet moments. ##_
