---
public: true
title: Close Enough
date: 2018-01-02
tags:
  - friends
  - memories
categories: english
---

![ ](../images/close-enough.jpg)

> **Umpteen things hurt.  
> Prime among them is the realization  
> that even with same persons  
> ‎and same surrounding,  
> ‎you would never be able to  
> ‎recreate the memories  
> ‎that have been spent and lost forever.**  
>   
> **But sometimes,  
> close enough is good enough.**  
>   
> **~RavS**  


_## A Great start to a great year. Believe. ##_  
