---
public: true
title: Go Play
date: 2014-11-26
tags:
  - life
  - free-verse
categories: english
---

![ ](../images/go-play.png)

> **Listen, life is a board of chess.**  
> **And while rules of the game remain simple,**  
> **You lose time and again against more experienced players.**
>
> **But that does not make you a loser, that makes you a rookie.**  
> **And you know what the advantage of being a rookie is?**  
> **That despite all the mistakes, blunders, miscalculations and mess-ups,**  
> **She still has the audacity to stand up yet again and try just one more time.**
>
> **So here are your Pawns, Knights, Rooks, Bishops, King and the Queen.**  
> **The battle field is set.**  
> **The opponent is fierce but vincible.**  
> **This game called life is long and complex.**  
> **But you will win.**  
> **If you stay long enough.**  
> **We are waiting to root for you.**  
> **Go play.**
> 
> **~RavS**
