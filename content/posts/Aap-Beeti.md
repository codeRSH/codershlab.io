---
public: true
title: आप-बीती
date: 2016-12-11
tags:
  - philosophical
  - sad
categories: hindi
---

> **मैं  कौन  था,  
> अब  क्या  हो  गया  हूँ।    
> अपने  अक्स  में  अक्सर,  
> अतीत  ढूंढता  हूँ।    
> एक  आह  है  अंदर  घुटी  सी,  
> एक  चाह  दिल  में  कुछ  दबी  सी।  
> चला  जा  रहा  हूँ  कहाँ,  
> मुझको  जाना  है  कहाँ,  
> कहाँ  छोड़  आये  तुम  मुझे,  
> ये  ज़िन्दगी  देके, खुदखुशी  सी।**  
> 
> **कहते  हैं  वो  कुछ  लोग,  
> मैं  बदल  गया  हूँ।    
> कुछ  कागज़ी  टुकड़ो  पे,  
> रूह  बेच  उठा  हूँ।    
> वो  ना समझेंगे  कभी,  
> आलम -ए -दिल -ए-बेताबियाँ  मेरी।    
> करें  किस्से  फ़रियाद  मगर,  
> करें  कैसे  फ़रियाद  मगर,  
> रुस्वा  रहती  है  अब  जो,  
> मुझसे  मेरी  शायरी  भी।**  
> 
> **अटकलें  लगाते  हैं  ऐय्याश,    
> मैं  जो  चुप  रहता  हूँ।    
> जानकार  गलत  समझ  लेते  हैं,  
> मैं  जो  कुछ  कहता  हूँ।    
> वो  अपने  हैं, कहाँ  गलती  उनकी,  
> गैर  मैं  हूँ, ये  गलती  मेरी  ही।  
> बस  आप-बीती,  
> बस  आप-बीती।  
> कुछ  ना  कहता  था, रबी,  
> इसके  सिवा, इससे  पहले  भी।**
> 
> **~रबी**


\[ Who I was,  
Now what have I become.  
In my reflection often,  
I search for my past.  
There is a pain inside strangled,  
There is a desire in heart muffled.  
Where am I going,  
Where do I have to go.  
Where did you leave me,  
Giving me this life, like suicide.

There are some people who say,  
I’ve changed.  
For some pieces on paper,  
I have sold my soul.  
They will never understand it,  
The state of anxiety of my heart.  
But whom do I complain,  
But how  do I complain,  
When everyone is angry with me,  
Even my own poetry.

Those well-offs keep guessing,  
When I remain quiet.  
They knowingly misunderstand,  
Whenever I say something.  
They are my own, how can they be at fault,  
I am ‘separate’, so it’s my own fault only.  
Just my own story,  
Just my own story.  
Rabi never used to say anything,  
Beside this, even before this. \]
