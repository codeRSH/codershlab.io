---
public: true
title: कुछ नहीं, मैं ठीक हूँ...
date: 2012-11-11
tags:
  - pain
  - grief
categories: hindi
---

> **ना मिटा शिकन अपने चेहरे से,  
> तेरी आँखें तेरा हाल-ए-बयाँ करती हैं।    
> करते रहो पर्दा ऊपर के ज़ख्मो पर जितना भी,  
> अन्दर लगी चोटें कहाँ छुपती हैं?**  
>   
> **होठों पर हँसी लाने की क्या ज़रुरत ,  
> पलकों पर जमी नमी साफ़ दिखती है।  
> और उसपर ये कहना, “_कुछ नहीं, मैं ठीक हूँ..._”,  
> ठीक न होने की हद और क्या हो सकती है?**
> 
> **~रबी**

  

\[ Do not erase the uneasiness from your face,  
Your eyes are telling the whole story about your state.  
You can cover the wounds on your exterior,  
But how will you cover the internal bruises?  
  
What’s the use of smiling from the lips,  
When I can see the mist on your lashes,  
And on top of that you insisting, “_Nothing, I am fine…”_,  
What else can be the limit to know, that you are not? \]
