---
public: true
title: "बहुत दिन हुए, कुछ नया नहीं लिखा तुमने..."
date: 2014-06-05
tags:
  - special
  - dedication
  - gratitude
categories: hindi
---

> **आज फुर्सत में बैठे,**  
> **यूं ही याद आया मुझे,**  
> **बहुत दिन हुए, कुछ नया नहीं लिखा तुमने…**
> 
> **उम्मीद है, कलम तुम्हारी अभी टूटी नहीं होगी,**  
> **लहू मिले आंसुओं की स्याही अभी सूखी नहीं होगी,**  
> **उम्मीद है अभी भी तुम आयतें लिखते होगे किसी के लिए,**  
> **उम्मीद है तुमसे तुम्हारी नज़्म अभी रूठी नहीं होगी।**
> 
> **पता नहीं तुम्हे याद है भी या नहीं,**  
> **कई दिन पहले तुमसे एक फरमाइश किया करता था अक्सर,**  
> **शायद तुम भूल गए, शायद तुम उसे पूरी करना चाहते ही नहीं,**  
> **चलो कोई बात नहीं,**  
> **लिखना ना छोड़ देना मगर,**  
> **पूरी ना कर सके फरमाइश अगर।**
> 
> **क्योंकि तुम्हारी बातों में खुद को कहीं ढूंढता हूँ,**  
> **तुम्हारे अफ़्सानो में खोकर खुद से जा मिलता हूँ,**  
> **तुम्हारी लिखी ग़ज़लों में कुछ तो बात है,**  
> **की वो हर बात उसमे होती है, जो हर बात मैं खुद से कहना चाहता हूँ।**
> 
> **आज उन्ही ग़ज़लों के सुर्ख पन्ने पलटते हुए ,**  
> **बस यूं ही याद आया मुझे,**  
> **बहुत दिन हुए, कुछ नया नहीं लिखा तुमने…**
> 
> **~रबी**


\[ Today while I was sitting alone,  
The thought came in mind,  
It’s been a while, you haven’t written anything new…

I hope, your pen hasn’t broken yet,  
The ink of blood and tears hasn’t dried yet,  
I hope, you still write verses for someone,  
I hope your poetry hasn’t left you yet.

I don’t know if you still remember or not,  
I used to ask you to write something for me,  
May be you forgot, may be you don’t want to fulfill my wish,  
But that’s alright,  
But hey you don’t stop writing alright,  
If you can’t fulfill it.

Because I search for myself in your words,  
I lose and find myself in your stories,  
There is definitely something in your poetry,  
That I find everything in it, that I want to say to myself.

Today while looking at the crumbled pages of your poetry,  
This thought came in mind,  
It’s been a while, you haven’t written anything new…\]

_## This poem can be dedicated to so many people who keep demanding “more” whenever I stop. Thank you everyone. Most of what I write will remain utter garbage, but some of them will turn out to be likeable. I will keep searching for those gems. I will keep trying. Thank you. ##_
