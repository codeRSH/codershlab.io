---
public: true
title: Haiku - 6
date: 2019-04-06
tags:
  - haiku4u
categories: english
---

![ ](../images/h4u-6.jpg)

> **Unfulfilled promises.  
> Not broken.  
> They’re due.**
>
> **~RavS**


_## I keep on making promises. But for a lot of reasons (or excuses), I am not able to fulfill them. It doesn’t mean I have forgotten them. They are just (over-)due, but I will fulfill them all eventually. All I ask from you is patience, and a belief in me. ##_
