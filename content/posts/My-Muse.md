---
public: true
title: "My Muse"
date: 2014-07-28
tags:
  - muse
  - creation
  - meta
categories: english
---

![ ](../images/my-muse.jpg)

> **You ask me, _do it_.**  
> **I say, no.**  
> **You grunt, _Do It Now!_**   
> **I scream, Noooo.**  
> **And then without warning, you take an iron bat,**  
> **And hit me, over and over, at my guts,**  
> **No please… No…**  
> _**I am sorry, but it’s necessary,**_  
> **And then you hit harder,**  
> **Right between the forehead,**  
> **Arggghhhhh…**  
> **I fall down. Head first,**  
> **Inspiration trickling down.**  
> **From head, through neck, to chest.**  
>   
> **You take a brush, soak it in my crimson,**  
> **And put the first stroke on a blank canvas,**  
> **Then you look at me and groan,**  
> _**There. I started.**_  
> _**Now go complete what’s on your mind.**_  
> **I mumble, but… but I can’t.**  
> **I am not capable enough,**  
> **I am not talented enough,**  
> **What if they hate it,**  
> **What if they laugh at me?**  
>   
> _**Who are they to judge?**_  
> _**They don’t know the resistance you fought,**_  
> _**They don’t know the dip from which you came out.**_  
> _**The pain you felt all day long,**_  
> _**The adversities you had to overcome.**_  
> _**It’s me who was there,**_  
> _**When you felt empty.**_  
> _**It was me who made you walk,**_  
> _**When you were crippling.**_  
> _**So you do it for the ones who love you,**_  
> _**You do it for the ones who support you,**_  
> _**You do it not for the glory but for the pride.**_  
> _**You do it for me, if there’s no one else.**_   
>   
> **With a pounding head and fractured guts,**  
> **I get up with a brush in my fingers,**  
> **I can barely stand, but I have to keep drawing.**  
> **The process is slow, it’s incredibly tiring.**  
> **I don’t know if I would ever cross the finish line.**  
> **But, You scream and scream in my head,**  
> _**This is who you are, you can not forget.**_  
> _**If not for your work, who will you be?**_  
> _**If not for your art, how will you define yourself,**_  
> _**Your work made you, how can you leave it?**_  
>   
> **I take cognizance of your talk,**  
> **And bleed and bleed and bleed on the canvas,**  
> **Till I have something to show for the efforts,**  
> **And create something that others could marvel.**  
> **I share it with the world and they sigh in awe.**  
> **And as they compete to congratulate,**  
> **For a piece that’s so unflawed.**  
> **I look at you, hiding at a distance,**  
> **Behind the silver colored curtains.**  
> **You smile and I whisper thank you,**  
> **The secret ingredient, my muse.**
> 
> **~RavS**


_## I read this book “The War of Art” by Steven Pressfield recently where he attributed a part of his work to ‘The Muse’ which he equated to be like The Angels. This piece is a dedication to the process of creating. The roadblocks we hit and how our muse can help us, almost force us out of this huge crater. The picture of 'Angel’ taken from the blog of my friend Skand. We can even go meta and dedicate this poem to the above art piece and its creator. ##_
