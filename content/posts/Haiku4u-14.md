---
public: true
title: Haiku4U - 14
date: 2019-04-14
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-14.jpg)

> **Without you,  
> I might survive.  
> But I wanna be alive.**
>
> **~RavS**


_## Hearing this may hurt you. But I do believe we can survive without each other. Life never stops for anyone or anything._

_But who wants to just ‘survive’ in life. I want to feel alive and actually ‘live’ this fickle life. ##_
