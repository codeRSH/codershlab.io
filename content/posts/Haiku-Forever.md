---
public: true
title: Haiku - Forever
date: 2019-04-30
tags:
  - haiku
  - love
categories: english
---

![ ](../images/haiku-forever.jpg)

> **Let yesterday be gone.  
> Today be mine,  
> Tomorrow and forever.**
>
> **~RavS**


_## I just wish, we could have met earlier. We could have gotten so much more time to understand each other and make our relationship even stronger._

_But I can’t do anything about what’s gone. I hope to have today and a lot of tomorrows._

_I am gonna make the most of what’s left, with you. ##_
