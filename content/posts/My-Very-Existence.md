---
public: true
title: My Very Existence
date: 2012-05-29
tags:
  - romantic
  - separation
categories: english
---

> **To separate you from myself,  
> Tell me how’s that even possible?  
> But I will do if you insist,  
> Just tell me one thing first,  
> That where do I end and where you begin?  
> When you are the extension of my very existence…**
> 
> **~RavS**
