---
public: true
title: नामुराद
date: 2012-05-19
tags:
  - hurt
  - betrayal
categories: hindi
---

> **वो तबाह कर गए हमें, हमारा आशियाना, हमारा शहर,    
> फिर भी इस नामुराद दिल से उनके लिए सिर्फ दुआ ही निकलती है।**
> 
> **~रबी**

  

\[ She destroyed me, my home, my city,  
Still only good wishes come out for her, from this damned heart. \]
