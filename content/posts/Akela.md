---
public: true
title: अकेला
date: 2020-06-14
tags:
  - death
  - tribute
  - special
categories: hindi
---

> **उसकी  यादें, उसकी चाहत,  
> उसके ख्वाब, उसकी हसरत,  
> उसकी हस्ती, उसके वादे,  
> कुछ कर जाने के इरादे,  
> माँ की गोद, पत्नी का आँचल,  
> लोरी को ताकती बेटी का काजल,  
> दर्द का बोझ उठाये कंधे,  
> गले में उलझे पोशीदा फंदे।  
> …हम्म  
> सब मरते हैं साथ में,  
> वो ‘अकेला’ मरते हुए भी अकेला नहीं होता।**
>
> **~रबी**


\[ His memories, his desires,  
His dreams, his longings,  
His personality, his promises,  
Intentions to do something,  
Mother’s lap, wife’s scarf,  
Mascara of little daughter waiting for lullaby,  
Shoulders bearing the pain,  
An invisible noose stuck in the neck.  
… Hmm  
Everything dies with him,  
He is not alone even while dying 'alone’. \]

_## My Tribute to Sushant Singh Rajput. I used to like him as an actor. Wish all this controversy and mud-slinging around his death wouldn’t happen. ##_
