---
public: true
title: पहले  क्यों  नहीं  आये  ज़िन्दगी  में
date: 2015-05-13
tags:
  - complaint
  - romantic
  - fear
categories: hindi
---

> **अब  तक  इतना  क्यों  रुलाया  तुमने,  
> पहले  क्यों  नहीं  आये  ज़िन्दगी  में।    
> हाँ  नहीं  ढूँढ  पाया  मैं  तुम्हें,  
> पर  मैं  तो  खड़ा  था  यहीं।    
> तुम  ही  आ  जाते  मुझे  लेने।**
> 
> **हर  वक़्त  सोचता  हूँ, और  कैसे  मैं  हँसाऊं  तुम्हें,  
> गुज़रा  वक़्त  जो  नहीं  मिला, वो  कैसे  वापस  लाऊँ  फिर  मैं  ,  
> जानती  हो  ना,  तुम्हारी  हँसी  से, मेरे  कितने  घाव  भर  जाते  हैं,  
> एक  ले  दो  मेरा  नाम, प्यार  से, मेरे  बहुत  से  दर्द  मिट  जाते  हैं।    
> फिर  अब  तक  इतना  क्यों  तड़पाया  तुमने,  
> बोलो  ना,  पहले  क्यों  नहीं  आये  ज़िन्दगी  में ?**    
> 
> **चलो  छोड़ो, बहुत  करीं  बातें  बीते  कल  की,  
> और  जो  अभी  आया   भी  नहीं  उस  पल  की।    
> चलो  ना  कुछ  देर  तो  अभी  की  बातें  करते  हैं,  
> ना  कल  अपना  था,  ना  कल  शायद  होगा,  
> चलो  ना  आज  को  ही  अपना  कर  लेते  हैं।**  
> 
> **चलो  ना  मिट्टी  में  कुछ  आड़ी  तिरछी  लकीरें  खींचते  हैं।    
> चलो  ना  ठंडी  सडकों  पर  नंगे  पाँव, यूँ  ही  निकल  पड़ते  हैं,  
> चलो  न  आसमान   की  छत्त  से, आज  सभी  तारे  चुन  लेते  हैं।    
> चलो  ना …  
> कल  तो  शायद   तुम्हें  जाना  ही  है।    
> मेरे  पास  रेत  से  बस  ये  कुछ  पल  बचे  हैं,  
> वो  भी  धीरे-धीरे  हाथ  से  फिसल  रहे  हैं।**  
> 
> **पता  नहीं  इतना  क्यों  सताया  तुमने,  
> पता  नहीं  पहले  क्यों  नहीं  आये  ज़िन्दगी  में।  
> पर  अब  जो  आये  हो, दो  पल  ही  सही,  
> कुछ  देर  तो  इत्मिनान  से  ठहरो  ज़िन्दगी  में।**
> 
> **~रबी**  

\[ Why did you make me cry so much till now,  
Why didn’t you come in life earlier?  
Yes I couldn’t find you,  
But I was standing here,  
You could have come to take me.

I think all the time, how to make you laugh,  
The time that’s gone, how can I bring it back.  
You know na, your laugh heals so many of my wounds,  
If you take my name once, a lot of my pain goes away.  
Why did you agonize me so much then,  
Tell me, why didn’t you come in life earlier?

OK leave it, enough talk about the past,  
And the moments which haven’t even arrived yet.  
Come on, let’s talk about Now, for a while,  
Neither yesterday was ours, nor tomorrow might be,  
Come one, let’s make today ours.

Come on, let’s make some lines in the sand,  
Come on, let’s walk bare foot on cold paths,  
Come on, let’s pluck the stars from the ceiling of sky,  
Come on…  
Tomorrow you have to go probably,  
I have just some moments left like these sand grains,  
They are also slipping slowly from my hand.

I don’t know why you teased me so much,  
I don’t know why you didn’t come in life earlier.  
But now that you have come, let it be for two moments,  
Stand still with me for a while. \]
