---
public: true
title: Haiku4U - 22
date: 2019-04-22
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-22.jpg)

> **Hold on (to my hand)  
> Chin up (and smile)  
> (together) we’ll endure.**
> 
> **~RavS**


_## It’s not easy.  The hard ships. It breaks us down. People will be negative. We will fight on our own, but we will fight each other._  
  
_But in all that I wish we don’t forget to keep our heads up in pride of what we are doing and remember to smile as we endure and surpass everything together. Things might get ugly. Just hold on to me. ##_  
