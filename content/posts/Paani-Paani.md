---
public: true
title: पानी-पानी 
date: 2012-03-12
tags:
  - romantic
categories: hindi
---

![ ](../images/paani-paani.jpg)

> **मत  आया  करो  सामने,  
> के  प्यार  उमड़  पड़ता  है।  
> किसी  दिन  बह  गए  इसमें,  
> तो  पानी-पानी  हो  जाओगे।** 
> 
> **~रबी**


\[ Please don’t come in front of eyes,  
as love starts brimming over.  
If you get washed away in it someday,  
then you will feel ashamed in public. \]
