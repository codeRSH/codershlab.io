---
public: true
title: मेरी आँखों में
date: 2012-11-02
tags:
  - karvachauth
  - special
categories: hindi
---

> **क्यों करती हो इंतज़ार उस चाँद के निकलने का,  
> क्या मेरी आँखों में अपना रुख़ तुम्हें दिखाई नहीं देता?**
> 
> **~रबी**


\[ Why are you waiting for that moon to come out,  
Can’t you see your face in my eyes? \]
