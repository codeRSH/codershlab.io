---
public: true
title: शायद आज कोई दूर जा रहा है...
date: 2013-12-20
tags:
  - separation
  - farewell
  - life
categories: hindi
---

> **होठ तो कुछ नहीं कहते,**  
> **पर आँखों में बंद एक सैलाब है,**  
> **रोकने को हाथ नहीं उठते,**  
> **पर उंगलियां मुट्ठियों में कैद करने को बेताब हैं,**  
> **लौट आने का झूठा वादा भी साथ है,**  
> **जाने क्यों फिर दिल ये घबरा रहा है,**  
> **शायद आज कोई दूर जा रहा है…**
> 
> **कुछ बातें जो अनकही सी रह गयीं,**  
> **कुछ यादें जो जल्दी में खो गयीं,**  
> **कुछ लम्हे जो अधूरे से रह गए,**  
> **कुछ वादे जो पूरे न कर सके,**  
> **मुसाफिर, उन्हें एक बार मुड़कर तो देख ले,**  
> **खामोश सीने में दिल ये कराह रहा है,**  
> **शायद आज कोई दूर जा रहा है…**
> 
> **कुछ दिन याद रखेंगे,**  
> **फिर भुला देंगे,**  
> **कुछ दिन आँसूं बहेंगे,**  
> **फिर सुखा देंगे,**  
> **ये तो दस्तूर है ज़िन्दगी का,**  
> **लोग तो आते ही हैं जाने के लिए,**  
> **दिल बेचारा धड़कनो को समझा रहा है,**  
> **शायद आज कोई दूर जा रहा है…**
> 
> **~रबी**


\[ Lips don’t say anything,  
But there’s a volcano in eyes,  
Hands don’t rise to stop,  
But fingers want to catch hold of them,  
There is a false promise to come back as well,  
I don’t know why the heart is so tensed then,  
May be someone is going tonight…

Some things that couldn’t be said,  
Some memories that got lost somewhere,  
Some moments that remained incomplete,  
Some promises that couldn’t be met,  
Hey traveller, please look at them once,  
The heart wails in the silent chest,  
May be someone is going tonight…

Will remember for some days,  
Then will forget forever,  
Will cry a few tears,  
Then will let them dry,  
That’s how life works,  
People come only to go away,  
Little heart tries to console the beats,  
May be someone is going tonight…\]
