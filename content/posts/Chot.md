---
public: true
title: चोट
date: 2012-04-01
tags:
  - witty
  - humor
categories: hindi
---

> **चोट  तुम  मुझे  दो  या  मैं  तुम्हें,  
> दर्द  हमेशा  मुझे  ही  होता  है …  
> तुम  मुझे  बस इतना  बता  दो,  
> अगर  तुमसे  हो  सके  तो …  
> क्या  तुम  भूतनी  की  हो ?  :p**
> 
> **~रबी**


\[ Whether you hurt me or I hurt you,  
Why do only I feel the pain?  
  
Please tell, if you can…  
Are you a witch's daughter? :P  \]
