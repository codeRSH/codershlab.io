---
public: true
title: फ़रियाद
date: 2015-08-05
tags:
  - philosophical
categories: hindi
---

> **क्यों  बैठा  रहे  कि  आके  मेरी  कोई  फ़रियाद  सुने,  
> कभी  देखा  है  किसी  फरियादी  की  कोई  फ़रियाद  सुने ?  
> ये  ज़माना  बहुत  मतलबी  हो  चला  है  रबी,  
> खुद  करना  है  कर  गुज़र, भूलकर  की  मेरी कोई  फ़रियाद  सुने।** 
> 
> **~रबी**  

  
\[ Why do you keep waiting that someone will come and listen to you?  
Have you ever seen someone getting heard nowadays?  
This world has become so self-centred Rabi,  
Just do it, if you want to, rather than waiting for someone to help you do it. \]
