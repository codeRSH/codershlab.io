---
public: true
title: एक सपना
date: 2012-08-05
tags:
  - dream
  - romantic
  - wish
categories: hindi
---

> **चलो आज मिलकर एक सपना बुनें,  
> जिसमे सिर्फ तुम रहो, और मुझे पनाह मिले,  
> जहाँ और किसी का जिक्र न हो,  
> जहां और किसी से इल्तिजा न रहे।**    
>   
> **गम का जिसमे निशाँ ना हो,  
> आँसुओं को जिसमे जगह ना मिले,  
> अगर कुछ रहे तो बस एक सुकूँ,  
> ये यकीन, मैं तेरी,  तू मेरी नींदों में रहे।**     
>   
> **एक सपना जो असलियत सा लगे,  
> और हकीकत बुरा ख्वाब महसूस हो मुझे,  
> जिसको देखूं तो जागने की तमन्ना न रहे,  
> चलो आज मिलकर एक सपना बुनें।**  
> 
> **~रबी**

  

\[ Come, let’s weave a dream,  
In which only you exist, and I get a place,  
Where no one else is ever mentioned,  
Where no one else is ever craved.  
  
Where there is no sign of sadness,  
Where there is no place for tears,  
If something does remain, let it be bliss,  
And this belief, that you live in mine and I live in your sleep.  
  
A dream which feels like reality,  
And the truth feels like a nightmare,  
A dream which never lets me wake up,  
Come let’s weave such a dream. \]
