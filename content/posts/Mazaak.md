---
public: true
title: मज़ाक
date: 2012-01-31
tags:
  - friends
  - humor
categories: hindi
---

> **सच  कहता  हूँ  दोस्त ,**  
> **हँसी  तो  बहुत  आती  है ,**  
> **तेरी  लिखी  एक-एक  बात  पर।**   
>
> **मैं  दुआ  करता  हूँ ,**   
> **मेरे  अलावा  भी  कोई  हँस दिया  करे ,**   
> **झूठा  ही  सही ,**   
> **तेरी  इन  करामात  पर।**   
>
> **तेरी  हस्ती  कोई  मामूली  नहीं ,**   
> **गर्व  होता  है  हर  पल ,**   
> **खुदा की  ऐसी  क़ायनात  पर।**   
>
> **खैर  ऐसे  ओछे  मज़ाक  करने  की  ज़रूरत  नहीं ,**   
> **कुछ  अपने  बारे  में  ही  लिख  दिया  कर।**   
> **क्योंकि  तेरी  ज़िन्दगी  खुद  ही  लगती  है,**  
> **एक  छोटा  सा  मज़ाक  भर !**  
>
> **~रबी** 


\[ I tell you the truth mate,  
I laugh at each and every word of yours.  

I pray,  
That someone else could also laugh,  
Even if just a pretend-laughter,  
On these deeds of yours.  

Your existence on this earth is not mediocre,  
I feel proud,  
On this creation of God.  

Anyways, no need to make such idiotic jokes,  
You can always write about yourself instead,  
As your life itself is,  
A little joke for us. \]
