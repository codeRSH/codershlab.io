---
public: true
title: Closed Off
date: 2014-04-14
tags:
  - free-verse
  - aloof
categories: english
---

> **And yet I fear my own,**  
> **I fear their rejection of my affection,**  
> **I fear being seen as vulnerable.**  
> **So I build a hard shell around.**  
> **A cocoon of safety.**  
> **Rendering me invincible and alone.**  
> **And I walk amongst the masses,**  
> **Invisible and insignificant.**  
> **Incapable of empathy.**  
> **Apathetic of emotions.**  
> **I risk being seen aloof.**  
> **Because to feel what they feel,**  
> **I will have to break open my heart.**  
> **Leaving me fragile and unarmed.**  
> **Giving them a chance to bruise.**  
> **And that risk I can not take.**
> 
> **~RavS**


_## Tried free verse after a long long time. ##_
