---
public: true
title: Companionship 
date: 2012-01-20
tags:
  - friends
  - separation
categories: english
---

![ ](../images/companionship.jpg)

> **The air is the same, and so is luminosity,**  
> **The energy is the same, and so is curiosity,**  
> **But… I can feel something is amiss,**  
> **Yes.. Yes, it’s your companionship…** 
> 
> **~RavS**
