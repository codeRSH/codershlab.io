---
public: true
title: रियासत
date: 2014-07-22
tags:
  - 30-day-challenge
categories: hindi
---

> **दादा से सीखा तपते शरीर से भी दिन रात कैसे मेहनत लेते हैं,**  
> **दादी से सीखा अपाहिज होकर भी परिवार पर कैसे बरकत करते हैं,**  
> **बाप ने सिखाया कैसे अपनों की ख़ुशी के लिए खुद से बेपरवाह रहते हैं,**  
> **माँ ने सिखाया कैसे थोड़े से में भी बिन कुछ बोले निर्वाह करते हैं।**  
> **बस मुझे रियासत में यही मिला है,**  
> **इससे ज्यादा की ऐ खुदा कोई चाहत भी नहीं …**
> 
> **~रबी**

  
\[ Learned from Grandfather how to take work day and night, even from the burning body,  
Learned from Grandmother, how to bless the family even when you are bedridden,  
Father taught how to sacrifice your own happiness for the sake of your loved ones,  
Mother taught how to live with little without uttering a word,  
That’s all what I have got in Inheritance;  
And no, that’s enough. I don’t need anything else… \]
