---
public: true
title: Haiku4U - 15
date: 2019-04-15
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-15.jpg)

> **Our love is hard  
> My Love.  
> ‘cause it’s supposed to be.**
>
> **~RavS**


_## This is an ugly fact that we aren’t going to have an easy Love life. We are going to have differences, and we will have to overcome a lot of difficulties._

_But I think that would make our love how it’s supposed to be. That would make us special. ##_
