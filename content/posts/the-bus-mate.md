---
public: true
title: The Bus Mate
date: 2025-01-21
tags:
  - friend
  - separation
categories: english
---


> **Hi X\*! How are you?  
> I hope your new life is treating you good.  
> Now it's ok that me  You don't know.  
> And, there's no rhyme or reason to write this,  
> Except the fact that Your friend is My friend also.**  
> 
> **You see I got to know you left our company recently,  
> And also the company of our common friend, more importantly.  
> She has taken your name quite a few times, ever since.  
> She claims, here, you were her best friend,  
> And she does remain sad, without a doubt ,  
> The degree it hurts without you, only she can tell.**  
> 
> **If I am not wrong , you were "The Bus Mate" ,  
> And not just a Friend, a support and a guide as well.  
> The fact that to have lunch with you,  
> She would leave everyone in her group,  
> That obviously does mean something.  
> You left a lacuna in her lunch time as you left,  
> Which nowadays she is trying hard to fill.**  
> 
> **You had your reasons, you had to leave,  
> Friendship should never become an obstacle, I agree.  
> But I hope  you miss your friend as much as she misses you.  
> Contacts are worth a dime, but a real friend? It can't be valued.  
> I try hard not to preach but please heed this unsolicited advice.  
> Treasure your friend 'cause her treasury has you firmly locked inside.** 
> 
> **I know the value of a genuine  friend,  
> ' cause I've lost a few over time,  
> The reasons were many,  
> as there always are.  
> But not caring enough was primary,  
> and it hurts, it hurts a lot.  
> So I wouldn't want you two to go through that, too.  
> Well that's it. I hope will get to hear more about you.  
> Ohh.. And in case you are wondering, I am fine. Thank you. :)**  
> 
> **~RavS**

\*X: Name concealed. X stands for Ex (Bus Mate).  

_## This poem was addressed to a friend of a friend. Our common friend was very sad when she left, and so I thought I might just intervene to help them keep their friendship alive. Life overtook and the friendship wasn't same later, but I am glad I tried. ##_
