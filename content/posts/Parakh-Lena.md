---
public: true
title: परख लेना
date: 2012-08-06
tags:
  - friendship
  - loyality
categories: hindi
---

> **अगर बेज़ार हो वहम मेरी दोस्ती पे तुम्हें,  
> आँखों में झाँक मेरी सच्चाई परख लेना।**    
>   
> **अगर न आ सकूँ नज़रों के सामने मैं तुम्हारी,  
> तुम गुफ़्तगू कर मेरी वफ़ाई परख लेना।**    
>   
> **अगर न सुन पाओ मेरी आवाज़ भी एक तलब पर तुम,  
> फ़क़त तुम पर लिखी मेरी रुबाई परख लेना।**  
> 
> **~रबी**


\[ If you get angry and suspect my friendship,  
Look into my eyes and test my truthfulness.  
  
If I can’t come in front of your eyes for some reason,  
You listen to my voice and test my loyalty.  
  
And if you don’t hear my voice on calling once,  
Read and test my poems written just for you. \]
