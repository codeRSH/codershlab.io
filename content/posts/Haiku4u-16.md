---
public: true
title: Haiku4U - 16
date: 2019-04-16
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-16.jpg)

> **May be I’m not worthy.  
> But my love,  
> deserves no one but you.**
>
> **~RavS**


_## I have this feeling in my head somewhere that somehow I am not supposed to be the one you should love. I am sorry._

_But you know what, at the same time, I am so proud (even arrogant) of my love that I would never want anyone else to get it. ##_
