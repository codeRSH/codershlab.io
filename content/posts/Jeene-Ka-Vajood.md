---
public: true
title: जीने का वजूद
date: 2012-10-09
tags:
  - life
  - pain
categories: hindi
---

> **छिड़क लेता हूँ नमक कभी कभी ख़ुद के ही ज़ख़्मों पर,  
> अब तो  दर्द के एहसास में ही जीने का वजूद मिलता है।**
> 
> **~रबी**


\[ I rub salt on my own wounds sometimes,  
Now, I feel alive only when I shriek in pain. \]
