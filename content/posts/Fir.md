---
public: true
title: फिर
date: 2012-09-21
tags:
  - pain
  - life
categories: hindi
---

> **फिर  तेरी  बातों  में  वही  कशिश  नज़र  आती  है।**    
> **फिर  तेरी  आँखों   में  वही  तपिश  नज़र  आती  है।**    
> **फिर  तेरे  चेहरे  पे  वही  चमक  नज़र  आती  है।**    
> **फिर  तेरे  कदमों   में  वही  खनक   नज़र  आती  है।**   
> 
> **….बता  मुलाज़िम, आज  अपने  दर्द  कहाँ  दफना  आया  है ?**
> 
> **~रबी**


\[ I can see that attraction in your voice, again.  
I can see that flame in your eyes, again.  
I can see that sheen in your face, again.   
I can see that jingle as you walk, again.

… Tell me servant, where did you bury your pain today? \]
