---
public: true
title: जुगनी
date: 2014-07-15
tags:
  - 30-day-challenge
  - punjabi
  - special
categories: hindi
---

> **की केवान जुगनी को अस्सी वी,**  
> **जुगनी कुज समझती सी नहीं,**  
> **जे कहाँ लड़ना है अज्ज मैनूं,**  
> **ते लड़े विन रुक्दी वी नहीं।**  
> **जुगनी नु जे ख़ुराफ़ात है,**  
> **जे वि करो, कदो छिपदी सी नहीं,**  
> **कित्ती वार बोलया न कर, न कर जुगनी,**  
> **बिन किये गलती जुगनी, हाय, कुज सिखदी सी नहीं।**
> 
> **~रबी**

  
\[ What shall I say to Jugni,  
Jugni doesn’t understand anything,  
If I say I want to fight today,  
Then she won’t stop before fighting.  
The mishief that’s inside Jugni,  
Whatever you don’t, it won’t hide,  
How many times did I say, don’t do it, don’t do it Jugni.  
But without making mistakes, alas, Jugni doesn’t learn anything  \]
