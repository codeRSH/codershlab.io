---
public: true
title: मौत कुत्ते की
date: 2012-02-15
tags:
  - life
  - humor
  - witty
  - friends
categories: hindi
---

![ ](../images/maut-kutte-ki.jpg)

> **अच्छा ख़ासा टाइम-पास होता था, हमें भी क्या सनक चढ़ी,**  
> **मज़ाक - मज़ाक में कमिटमेंट कर लिया उनसे, गलती हमने बहुत बड़ी की।**    
> **लेकिन अब गलती की सज़ा तो हमें मिलनी ही थी,**  
> **ज़िन्दगी हमारी गधों सी हो गयी, और आखिर में मौत कुत्ते की मिली…**
> 
> **~रबी**


\[ Time used to pass so easily, I don’t know what madness got over me,  
I committed to them jokingly, it was such a big mistake.  
But now I had to pay for my mistakes,  
My life became ass-like, and in the end like a stray dog, I died. \]
