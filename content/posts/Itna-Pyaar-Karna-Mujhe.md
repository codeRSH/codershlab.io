---
public: true
title: इतना  प्यार  करना  मुझे
date: 2019-07-16
tags:
  - anger
  - love
categories: hindi
---

> **ना  भी  कर  पाऊँ  बात  तुमसे  किसी  मोड़  पर  ,  
> तुम  मेरी  खामोशियाँ  तो  पहचानती  हो  न ?  
> कभी  रोकना उन्हें, और पूछना  उनको,  
> क्यों  खामोशियाँ  मेरी  यूँ  बदहवास  हैं।  
> क्यों  जुबान मेरी  लावारिस  सी,  
> क्यों  आवाज आज  बेआवाज  है।  
> शायद  मेरी  खामोशियों  में कहीं  दबा  मिल  जाऊँ मैं  तुम्हें …**
>
> **ना  भी  सो  पाऊँ  मैं  कभी  रात  भर,  
> तुम  मेरी  बेचैनियाँ  तो  समझती  हो  न ?  
> अपने  तकिये  तले  सुला लेना  मेरी  बेचैनियों  को,  
> अपना  आँचल  ओढ़ा  करके  रात  में।  
> थपकियाँ   लगा  देना  उन्हें  , हलकी-हलकी  आवाज  में।  
> और  सोके  उठें  तो  करना  जिरह  उनसे  ,  
> क्यों  बेचैनियों  को  चैन  नहीं  आज  है।  
> शायद  जो  मैं  खुद  भी  नहीं  जानता, वो  बेचैनियाँ  बता  जाएँ  तुम्हें …**
>
> **जो  लगे  तुम्हे  तन्हा, मैं  नाराज़  रहूँ  अगर,  
> तुम  मेरी  नाराज़गियों  से  लड़  सकती  हो  न ?  
> लड़ना मेरी  नाराज़गियों  से  अपनी  तन्हाइयों  की  खातिर,  
> और  मना  कर  मुझे,  खुद रूठ  जाना  फिर।  
> वही  दीवानगी  रखना  गुस्से  में  भी,  
> जिस  शिद्दत  से  तुमने  मुझे  चाहा  है ।  
> बस  ना  देना  तन्हाईयाँ  बदले  में  ,  
> तुम्हारी  नाराजगियाँ  मुझे  फिर  भी  गवारा  हैं।  
> मेरी  तन्हाईयाँ  भी  नाराज़  हो  जाएँ  मुझसे, तुम  इतना  प्यार  करना  मुझे …**
>
> **~रबी**


\[ Even if I am not able to talk to you at some point,  
You still recognize my silence, right?  
Stop it some time, and ask it,  
Why my silence is so restless.  
Why do my words feel unclaimed,  
Why my voice is voiceless today.  
May be you will find me buried somewhere in my silence…

If am not able to sleep whole night,  
You understand my discomfort, don’t you?  
Let my restlessness sleep under your pillow,  
By covering it with the sheet of your skin,  
Pat them a little with a slow sound.  
And when they wake up, then talk to them,  
Why my restlessness can’t rest today.  
Maybe what I don’t even know myself, my restlessness might tell you that…

If you ever feel lonely, if I am angry with you,  
You can fight my resentment, right?  
Fight with my resentment for the sake of your loneliness,  
And get angry after consoling me.  
Keeping the same madness in your anger,  
With that passion that you love me.  
Just don’t give me loneliness in return,  
I will accept your resentments still.  
My loneliness also should get angry with me, you love me so much… \]


_## It’s not easy for me to be OK once I get angry. This piece is more of a cry for help. ##_
