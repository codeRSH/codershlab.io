---
public: true
title: My Protagonist
date: 2014-12-18
tags:
  - saara
  - free-verse
categories: english
---

![ ](../images/my-protagonist.png)

> **Is it wrong if he doesn’t want to see a single speck in your eyes?**  
> **Is it improper if he can’t bear even a slight quiver in your voice?**  
> **Is it unfair if he wants to protect you all the time?**  
> **Is it asking for too much, if he wants**  
> **to share the pensive part of your life?**
>
> **If it is, then for him, I sincerely apologize.**  
> **You see. My protagonist,**  
> **He doesn’t know any other way to live,**  
> **and love.**
> 
> **~RavS**
