---
public: true
title: सांस एक आखिरी
date: 2014-09-21
tags:
  - dangerous
  - love
categories: hindi
---

> **जब रह जायेगी सांस एक आखिरी,**  
> **आधी तुझे दूंगा, आधी खुद रख लूंगा,**  
> **डर लगे तुझे सोते हुए तो,**  
> **नींद अपनी भी दे दिया करूँगा।**  
> **मुश्किलें आएँगी तो हाथ पकड़ लेना,**  
> **हर मुश्किलों से मेहफ़ूज़ तुझे रखूँगा।**  
> **ढूंढना नहीं पड़ेगा कभी,**  
> **जब जब पलकें उठेंगी, तेरे सामने ही मिलूंगा।**  
> **चाहे खुद को गिरवी रख सही,**  
> **तेरी हर ख्वाइश मैं पूरी करूँगा।**
> 
> **बस न जाना छोड़ कर,**  
> **सच कहता हूँ इस बार,**  
> **तुझे तुझसे छीन लूंगा ,**  
> **नोच डालूँगा तुझे हर कतरा कतरा,**  
> **या मार ही दूंगा,**  
> **या खुद मर मिटूँगा।**
> 
> **~रबी**


\[ When the last breath will remain,  
I will give the half to you, half I will keep myself,  
When you’ll feel afraid to sleep,  
I will give away my sleep as well.  
When difficulties come grab my hand,  
I will keep you safe from all difficulties,  
You would never have to find me,  
Whenever you will raise your eyelids, I’ll be in front,  
Even if I have to sell myself for it,  
I will fulfill all your wishes.

Just do not go away,  
I mean it this time,  
I will snatch you from you,  
I’ll claw each strand of you,  
Either I would kill you,  
Or end up killing myself. \]
