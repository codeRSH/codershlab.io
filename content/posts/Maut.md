---
public: true
title: मौत
date: 2012-12-29
tags:
  - dedication
  - death
  - special
categories: hindi
---

> **बेमौत की मौत, ये मौत आई है मुझे,  
> जो मौत के काबिल हैं, ना मौत आई है उन्हें।    
> जब मौत को चिल्लाती थी, ना मौत आई तब मुझे,  
> अब ज़िन्दगी को लड़ने लगी, तो मौत आई है बेवजह।    
> और मौत आई भी, तो देखो मौत आई किस समय,  
> जब माँ भी कहने लगी कि बेटी, ना मौत आएगी  तुझे।    
> खैर, मौत तो आनी ही थी, मलाल मौत का नहीं है,  
> बस बेमौत की मौत, ये मौत आई है मुझे।**
> 
> **~रबी**


\[ Without a cause, this death has come to take me,  
While those who deserve death, are still alive there,  
When I cried for the death, the death didn’t come then,  
Now when I started fighting for life, the death has come to take me,  
And look at the timing for the death to come,  
When even mom started saying, baby nothing will happen,  
Anyway, death had to come sometime, I don’t regret dying,  
It’s just that without a cause, this death has come to take me. \]
