---
public: true
title: Haiku4U - 25
date: 2019-04-25
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-25.jpg)

> **Would this love be different?  
> If I were you.  
> And you were me.**
>
> **~RavS**


_## Sometimes I wonder, would we behave differently, would we be living  a different relationship, had you been in my position and I in yours?_

_That certainly would have been interesting to see. ##_
