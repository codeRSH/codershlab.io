---
public: true
title: पुकारा
date: 2011-12-24
tags:
  - romantic
  - friendship
categories: hindi
---

> **तूने  इतनी  बार  पुकारा  मुझे ,**  
> **कि  मुझे  अपने  ही  नाम  से  मोहब्बत  हो  गयी !**
> 
> **~रबी**


\[ You called me so many times,  
That I fell in love with my own name! \]
