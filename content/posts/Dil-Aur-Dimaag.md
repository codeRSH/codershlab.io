---
public: true
title: दिल  और  दिमाग
date: 2012-01-27
tags:
  - confusion
  - life
  - witty
categories: hindi
---

> **अब  दिल  की  सुने,  या  दिमाग  की,**  
> **आखिर  दिल  और  दिमाग  दोनों  ही  अपने  हैं,**  
> **चलो  इस  बार  दिल  पर  ही  भरोसा  करके  देखते  हैं,**  
> **बात  नहीं  बनी, तो  कह  देंगे…**  
> **दिल  के  पास  दिमाग  नहीं  था।**  
> 
> **~रबी** 


\[ Now should I listen to my heart or my brain,  
After all, heart and brain both are mine,  
Alright, let’s believe my heart this time,  
If it doesn’t turn out to be right,  
then we can always say…  
That heart didn’t have a brain to think! \]
