---
public: true
title: Moments Left Behind
date: 2013-05-22
tags:
  - love
  - lost
categories: english
---

> **Agreed it hurts a lot,**  
> **to have loved and lost,**  
> **But only when you lose do you discover,**  
> **How much love were you  capable of.**
> 
> **Yes it would have been much easier,**  
> **To not have loved in the first place,**  
> **But then you wouldn’t have a story to tell,**  
> **I am afraid, you wouldn’t have lived at all.**
> 
> **Only those can truly love,**  
> **Who have the courage to live without.**  
> **They can take away your object of affection,**  
> **The moments left behind, still count.**
>
> **~RavS**
