---
public: true
title: तुझे माफ़ किया
date: 2012-06-20
tags:
  - god
  - hurt
  - anger
categories: hindi
---

> **जन्म तूने मुझे दिया,  
> जा तूने मुझे ख़रीद लिया,  
> अब ज़लील कर, तू नीलाम कर,  
> ना परवाह जो अंजाम कर,  
> रहम कर, बस इतना सा परवरदिगार,  
> ज़िन्दगी ना देना अगली बार।**  
>   
> **जा खुदा… इस बार मैंने तुझे माफ़ किया…**
> 
> **~रबी**

  
\[ You gave me birth,  
Yes, now you own me forever,  
Now insult me or auction me,  
I don’t care what you do to me,  
But do a favor, if you can,  
Don’t give me life next time.  
  
Rejoice God… I forgive you in this lifetime. \]
