---
public: true
title: गुरेज़
date: 2011-12-09
tags:
  - sufi
  - love
categories: hindi
---

> **ज़र्रा  ज़र्रा  वीरान  हो  गया  है  मेरा,  
> तेरे  आँचल  में  पनाह  लेने  दे ,   
> तू  कर  इबादत  उस  खुदा  की, मुझे  गुरेज़  नहीं,  
> मुझे  तो  तेरा  बिस्मिल  होने  दे …**
> 
> **~रबी**


\[ Every shred of mine is deserted now,  
Let me take shelter in your lap,  
You keep on praying to that God, I won’t complain,  
But let me become your disciple instead… \]
