---
public: true
title:  मैं कश्ती हूँ
date: 2020-05-16
tags: 
 - inspiration
 - special
categories: hindi
---

> **मैं कश्ती हूँ, मैं बहूँगा,  
> थोड़े जल की बस ज़रूरत है।  
> मैं लफ्ज़ हूँ, मैं रहूँगा,  
> चढ़ने की लबों पे ये कुव्वत है।**  
>   
> **जो देखे थे तूने सपने,  
> वो ख़्वाब अब भी पाले हूँ।  
> तेरी आँखों से गिरे कतरे,  
> मैं अब भी उन्हें संभाले हूँ।**  
>   
> **मैं सुबह हूँ, मैं लौटूँगा,  
> बस रात ढलने की ही देरी है।  
> मैं आग हूँ, मैं देहकूँगा।  
> चिंगारी लगने की बारी मेरी है।**  
>   
> **ये ज़ंजीर मुझको काटे हैं,  
> मुझे इनसे तू कट जाने दे।  
> मैं हल्का सा ही जी लूँगा,  
> फिर चाहे पूरा मर जाने दे।**  
>   
> **मैं ख़ुदा हूँ, मैं ख़ुदी का,  
> मुझमे अब भी इतनी ज़ुर्रत है।  
> मैं कश्ती हूँ, मैं बहूँगा,  
> थोड़े जल की बस ज़रूरत है।**  
>   
> **~रबी**    


\[ I’m a boat, I’ll float,  
Just some water is needed.  
I’m a word, I’ll stay,  
I’ve the power to be on everyone’s lips.  
  
The dreams that you saw,  
I still have those dreams.  
The tears that dropped from your eyes,  
I’ve still kept them safe.  
  
I’m morning, I’ll return,  
Just wait for the night to end.  
I’m a fire, I’ll spread,  
It’s my turn to spark.  
  
These chains bite me,  
Let me cut off from them.  
I’ll live a little bit,  
Then let me die completely.  
  
I’m God of my own,  
I still have so much audacity.  
I’m a boat, I’ll float,  
Just some water is needed. \]  
  

_## Written after being inspired from hearing lyrics of a good Pakistani Rock Band Auz. ##_
