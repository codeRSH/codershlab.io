---
public: true
title: तारीफ़ -ए-काबिल
date: 2012-06-04
tags:
  - hurt
  - sad
categories: hindi
---

> **मुझसे  फ़साद  क्या  है  तू  मुझे  बता,  
> मैं  हूँ  तेरा  बिस्मिल  अभी  भी।**  
> 
> **अपनों  को  मेरे, तकलीफ़  दे  मुझे  सजा  देना,  
> ऐ  ख़ुदा,  
> तेरी  ये  अदा  तारीफ़ -ए-काबिल  नहीं।**  
> 
> **~रबी**  

  
\[ Tell me what’s your problem with me,  
I am still your disciple,  
  
But to punish me by hurting my loved ones,  
Oh God,  
This style of yours isn’t praise-worthy. \]
