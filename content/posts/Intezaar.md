---
public: true
title: इंतज़ार
date: 2014-02-28
tags:
  - hiatus
categories: hindi
---

> **मैं गुज़रा लम्हा नहीं, मैं लौट आउंगा, ऐतबार करो।**  
> **ना भी आ सका जो अगर, तो तुम चले आना, मैं इंतज़ार करूँगा।**
> 
> **~रबी**


\[ I am not a moment that’s gone, I will be back, believe me.  
But even if I couldn’t, you come there, I’ll wait for you. \]
