---
public: true
title: Tonight
date: 2011-11-25
tags:
  - work-stress
categories: english
---

> **Tonight’s the night when I beat the insomnia,**  
> **No stress, no anxiety, no tension, no mania.**  
> **Tonight’s the night, when I get the long lost sleep.**  
> **Tonight’s the night when I rest… In peace.** 
> 
> **~RavS**
