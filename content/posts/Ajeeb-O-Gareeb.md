---
public: true
title: अजीब-ओ-गरीब
date: 2015-03-24
tags:
  - life-partner
  - difficulties
categories: hindi
---

> **जो  कहना  है  कहा  नहीं  जाता,  
> तुम  अजीब-ओ-गरीब  बातें  करते  हो।    
> जो  मैं  पूरी  कर  नहीं  सकता,  
> न  जाने  कैसी-कैसी  फरियादें  करते  हो।  
> क्या  सुनूँ, क्या  समझूँ  मैं,  
> तुम  क्या  कहते, क्या  छुपा  कर  रखते  हो।    
> न  जाने   क्यों  लगता  है  तुम्हें, दवा  नहीं  मैं,  
> इसलिए  अपना  मर्ज़  दबा  कर  रखते  हो।    
> छोड़ो, जब  दूर  जाते  जाते  थक  जाओ, तो  लौट  आना,  
> मैं  कहाँ  मिलूंगा, इसका  पता  तो  तुम  रखते  हो।**
>
> **~रबी**


\[ What you want to say, you don’t say,  
Instead you do these weird talks.  
The things that I can’t solve,  
Those are the kind of problems you forward.  
What should I listen, and what should I understand,  
God knows what you say and what you conceal.  
I don’t know why you think I am not the medicine,  
So you keep your disease pressed from me.  
Anyways, when you get tired of going away, just come back,  
You already know where to find me. \]
