---
public: true
title: आँसुओं
date: 2012-04-12
tags:
  - sad
  - separation
categories: hindi
---

> **आज दिल कर रहा है रोने को,**  
> **आँसुओं, कुछ देर मुझसे दोस्ती कर लो…**
> 
> **~रबी** 

\[ Today this heart wants to cry,  
Tears, befriend me at least for today… \]
