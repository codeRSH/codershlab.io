---
public: true
title: बदनामी
date: 2014-02-10
tags:
  - philosophical
categories: hindi
---

> **हम तो फिरते थे छुप छुप के बदनामी के डर से,**  
> **और बदनामी है कि हमसे ज्यादा मशहूर हो गई।**
> 
> **~रबी**


\[ I used to remain obscure, to avoid infamy,  
But look! My infamy has become more popular than me. \]
