---
public: true
title: रिश्ता
date: 2014-02-27
tags:
  - relationship
categories: hindi
---

> **एक डोर है रिश्ता तेरा मेरा,**  
> **जो खींच ले तो तर गया, जो छोड़ दे तो मर गया,**  
> **जो ढील दी तो असर गया, जो कस ले तो किधर गया,**  
> **जो बाँध ले तो हूँ तेरा, और जो काट दे तो बिखर गया।**
> 
> **~रबी**


\[ Our relationship is like a thread,  
If you pull me with it I will be saved, if you leave it I will die,  
If you loosen it then your influence will be gone, if you tighten it then where will I go?  
If you tie me with it, I will be yours, and if you cut me off it I will be shattered. \]
