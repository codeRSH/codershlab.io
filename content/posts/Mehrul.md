---
public: true
title: मेहरुल
date: 2012-07-06
tags:
  - romantic
categories: hindi
---

> **वाह मेहरुल! खेल ये तुमने अच्छा ही खेला,  
> मुझसे अपना सब कुछ हार गयीं, बस एक मुझे ही जीत लिया!**
> 
> **~रबी**
  

\[ Great Mehrul! You played such a clever game,  
You lost everything to me,but made sure you finally win me! \]
