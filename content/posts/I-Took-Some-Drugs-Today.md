---
public: true
title: I took some drugs today
date: 2014-08-07
tags:
  - song
  - special
  - first
  - drugs
categories: english
---

> **I was drowning in my own mucous.**  
> **Fluids flowing from nose, mouth ‘n ears,**  
> **Saliva in and saliva out,**  
> **Puking in and Puking out,**  
> **The windpipe was stinging like hell,**  
> **Like that kid taking tequila shots felt.**  
>   
> **Some powdered substance and a syringe,**  
> **Some through mouth and Some through veins.**  
> **I took some drugs today,**  
> **It wasn’t pretty but I had to take.**  
>   
>   
> **I couldn’t eat, I couldn’t sleep.**  
> **Shivering like a weathered leaf,**  
> **Spiking headache was unbearable,**  
> **The back was broken beyond recognizable.**  
> **The legs said we can no longer hold on ,**  
> **I was burning like a pre-heated oven,**  
> **More para- and more analgesic,**  
> **Couldn’t take more lest I would have killed.**  
>   
> **So, at last… Some powdered substance and a syringe,**  
> **Some through mouth and Some through veins.**  
> **I took some drugs today,**  
> **It wasn’t pretty but I had to take.**  
>   
> 
> **A gone case, floored like a bread crumb,**  
> **But drugs are working, now I feel numb,**  
> **Can’t tell whether it’s dusk or dawn,**  
> **But drugs are working, the pain is gone,**  
> **It’s addictive, I might turn a junk-ay,**  
> **But drugs are working, I’ll live another day.**  
>   
> **Yeah… Some powdered substance and a syringe,**  
> **Some through mouth and Some through veins.**  
> **I took some drugs today,**  
> **It wasn’t pretty but I had to take.**
> 
> **~RavS**


_## I had to take a sick leave today, which meant a record of no-sick-leave in 3 years got broken._  
  
_Worse, I was forced to visit a physician today which meant another record of not-visiting-a-doctor for 3-4 years was broken._  
  
_It’s a sad, pathetic, forgettable day._  
  
_So what did I do lying in bed most of the day? I took some “drugs” today. And made a song out of it. Initially thought to make it more gross by going more graphic details, but here’s the censored version :). ##_
