---
public: true
title: Haiku4U - 19
date: 2019-04-19
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-19.jpg)

> **I don’t feel it sometimes.  
> But it’s there.  
> Love.**
>
> **~RavS**


_## If I say I am in love with you 24x7, 365 days, then I would be lying. I don’t feel it sometimes. Instead, I feel anger, frustration. But through all of that, I am always aware, that love hasn’t faded. It’s there. And it will come back. Eventually.  ##_
