---
public: true
title: मुस्कुराने (Reprise)
date: 2014-09-24
tags:
  - reprise
  - song
categories: hindi
---

> **तुम हाँ कहो न कहो, तुम ना भी मत कहना,**  
> **जैसे भी हो भोले भाले, वैसे ही रहना।**  
> **मुस्कुराने की वजह तुम हो,**  
> **गुनगुनाने की वजह तुम हो।**  
> **जिया जाए न जाए न जाए ना ,**  
> **ओ रे पिया रे।**
> 
> **एक बार तो कहो, छीन लायेंगे हम,**  
> **रूखा सूखा जैसे भी हो, खा ही लेंगे संग।**  
> **\[ एक बार तो कहो, छीन लायेंगे तुम्हे,**  
> **रूखा ही हो, सूखा ही हो, खिला तो देंगे तुम्हें। \]**  
> **जिया जाए न जाए न जाए ना,**  
> **ओ रे पिया रे।**
> 
> **जो गया कल उसे, बदल नहीं सकते,**  
> **आने वाले कल में तुझ बिन, रह भी नहीं सकते।**  
> **मुस्कुराने की वजह तुम हो,**  
> **गुनगुनाने की वजह तुम हो।**  
> **जिया जाए न जाए न जाए ना ,**  
> **ओ रे पिया रे।**
> 
> **~रबी**


\[ Whether you say yes or not, please don’t say no,  
Howsoever you are, innocent, nascent, just remain the same.  
You’re the reason I smile,  
You’re the reason I hum,  
I can’t live, can’t live without you,  
Oh my Love.

  
Just say it once, I’ll snatch you away,   
Howsoever it is, sweet or salty, we will manage to eat,  
\[ Just say it once, I’ll snatch you away;  
Be it sweet, be it salty, I’ll feed you somehow, I promise. \]  
I can’t live, can’t live without you,  
Oh my Love.

  
I cannot change your yesterday, true,  
But I can’t imagine my future without you too.  
You’re the reason I smile,  
You’re the reason I hum,  
I can’t live, can’t live without you,  
Oh my Love. \]

_## Started writing alternative lines as well inside compositions. Part of evolving as a writer :) ##_
