---
public: true
title: बिटिया
date: 2017-10-14
tags:
  - parents
  - special
  - friend
categories: hindi
---

> **प्यारे पापा,  
> कभी  कंधे  पे  खेलती  थी  आपके,  
> आज  कंधे  तक  आ  जाती  हूँ।  
> फिर  भी  कुछ  कह  देते  हो  जोर  से,  
> मैं  अब  भी  डर  जाती  हूँ।**  
>   
> **हो  जाता  है  आपको  ज़ुखाम  भी  ,  
> घर  सारा  सर  पे  उठा  लेती  हूँ।  
> कर  पाऊं  आपकी, कुछ  ख्वाहिशें  पूरी  ,  
> थोड़ा  बहुत  इसलिए  कमा  लेती  हूँ।**  
>   
> **बेटा  तो  नहीं  मैं  आपका, आपने  पर,  
> बेटे  से  कभी  खराब  ना  समझा।  
> आगे  भी  करने  देना मुझे बेटी  बन  कर  ,  
> पापा, हर  काम  जो  करता  आपका  बेटा।**  
>   
>   
> **प्यारी  मम्मा,  
> मैं  बहुत  नाराज़  हूँ  आपसे  ,  
> जो  आप  मेरा  कहा  नहीं  मानती।  
> मुझे  कितनी  फ़िक्र  रहती  है  आपकी,  
> ये  शायद  आप  भी  नहीं  जानती।**  
>   
> **सबसे  ज्यादा  लड़ती  हूँ  आपसे  ,  
> आप  ना जाने   कैसे  सेह  जाती  हो।  
> मैं  रो  देती  हूँ  अक्सर  इसीलिए,  
> आप  जो  कभी  मुझसे  रूठ  जाती  हो।**  
>   
> **जब  कभी  खिलाफ  भी  हुई  है  दुनिया  ,  
> तब  भी  आपने  मेरा  साथ  ही  दिया।  
> माँ  कभी  आपको  दुःख  दिए  किसी  ने,  
> कसम  से  लड़  जायेगी  आपकी  बिटिया।** 
>
> **~रबी**


\[ Dear Dad,  
I used to play on your shoulder once,  
I have grown up to your shoulder now,  
Still when you say something loudly,  
I get scared sometimes.

Even if you get cold,  
I break all hell at home,  
To be able to fulfill some of your wishes,  
That’s why I earn a little bit.

I am not your Son, but you,  
Never considered lesser than a son.  
Please let me do all that as a daughter,  
Papa, what a son would have done for you.  

  

Dear Mumma,  
I am very angry with you,  
That you never heed my advice.  
I’m so worried about you,  
You probably don’t even know.  

I fight with you the most,  
I don’t know how you bear me.  
That’s why I cry sometimes,  
When you get angry with me.

Whenever the world was against me,  
You supported me even then.  
Mother, if someone hurt you,  
I swear, I will fight them all. \]

  

_## Written for a friend on her parents’ anniversary. ##_
