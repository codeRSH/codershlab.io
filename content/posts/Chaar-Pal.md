---
public: true
title: चार पल
date: 2013-05-16
tags:
  - life
  - philosophical
categories: hindi
---

> **चार पल की ज़िन्दगी मिली थी उधार की,**  
> **एक पल गलती  करने, दूजी दोहराने में गुज़ार दी ,**  
> **दो आखरी लम्हें जो बचे भी मुश्किल से ,**  
> **उन्हें हमने अफ़सोस करने में बेकार की।**
> 
> **~रबी**


\[ We only got four moments to be alive,  
We spent one to make mistakes, the other one to repeat,  
And when only 2 more moments were left in our life,  
We wasted them to regret the mistakes we made. \]
