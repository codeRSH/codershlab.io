---
public: true
title:  माँ  तुझे  मेरी  याद  तो  आएगी  ना ?
date: 2018-05-16
tags: 
 - mother
 - bonding
 - special
categories: hindi
---

> **अपना  आँचल  मेरे  सर  से  हटा  कर,  
> कंधे  पर  रखा  'बोझ’ बता  कर,  
> किसी  और  को  मेरा  जिम्मा  थमा  कर,  
> मुझे  भूल  तो  नहीं  जाएगी  ना ?  
> माँ  तुझे  मेरी  याद  तो  आएगी  ना ?**  
>   
> **क्यों  बेटियां  ही  दूर  जाती  हैं  माँ ?  
> उम्र  भर  साथ  नहीं  रह  सकती  मैं, क्यों  ना ?  
> बदन  तप  रहा  होगा, या  होगा  दर्द  सर  में,  
> तू  मुझे  अब  भी  अपना  हाल  तो  बताएगी  ना?  
> माँ  तुझे  मेरी  याद  तो  आएगी  ना ?**  
>   
> **मुझे  दूर  जाना  होगा  तुझसे, जानती  हूँ।  
> मैं  अब  किसी  और  की  अमानत  हूँ, मानती  हूँ।  
> पर  वादा कर, कभी कहीं से  माँ  … कहके पुकारूँ,  
> तू  मेरी  आवाज  सुनके  दौड़ी  आएगी  ना?  
> माँ  तुझे  मेरी  याद  तो  आएगी  ना ?**  
>   
> **पता  है,  अब  खुदसे  सुबह  उठना  होगा,  
> अपना  ख्याल  अब  मुझे  खुद  ही  रखना  होगा,  
> पर  मैं  रोउंगी तुझे याद कर, जब अँधेरा होगा,  
> तेरे  कलेजे  में  हिचकियाँ  तो  आएगी  ना?  
> माँ  तुझे  मेरी  याद  तो  आएगी  ना ?**  
>   
> **मेरे  बिन, पापा  का  ख़याल  रखना,  
> अकेले  ही  अब, परिवार  संभाल  रखना,  
> सब  खुशियां  अब  छोटी  को  डाल  देना,  
> पर  कुछ  मेरे  हिस्से  भी  बचाएगी  ना?  
> माँ  तुझे  मेरी  याद  तो  आएगी  ना ?**  
>   
> **~रबी**  

  

\[ With your scarf removed from my head,  
Stating the ‘burden’ placed on the shoulder,  
To give someone else my job,  
Will you not forget me?  
Mother, will you miss me?  
  
Why do only daughters go away mother?  
I can’t I live with you forever?  
The body will be unwell, or your will have pain in the head,  
Will you still tell me your situation?  
Mother, will you miss me?  
  
I have to go away from you, I know.  
I am now someone else’s belonging, I agree.  
But promise me, If I ever call you “Maa”… from somewhere,  
Will you come running hearing my voice?  
Mother, will you miss me?  
  
I know, now I have to get up in the morning by yourself,  
Now I only will have to take care of myself,  
But when I will cry remembering, in the dark,  
Will there be hiccups in your chest?  
Mother, will you miss me?  
  
Take care of my dear father,  
Now taking care of family alone,  
Give all the happiness now to the younger one,  
But will you save my share too?  
Mother, will you miss me? \]  
  

_## Some bonds are truly special. A tribute to mother-daughter relationship. ##_
