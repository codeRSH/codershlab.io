---
public: true
title: Cause no one is coming tonight.
date: 2013-12-10
tags:
  - loneliness
categories: english
---

> **Throw off the dinner,**   
> **Pull off the blinders,**  
> **Close the doors and turn off the lights,**  
> **‘Cause no one is coming tonight.**
> 
> **Lie down and hug yourself,**  
> **Pull out a pillow and scream for help,**  
> **Lick your own wounds and put off the fight,**  
> **'Cause no one is coming tonight.**
> 
> **Stop praying, No one will hear,**  
> **Let the showers hide your tears,**  
> **Quit longing for footsteps outside,**  
> **'Cause no one is coming tonight**
> 
> **Remember them, abuse them,**  
> **Beg them, accuse them,**  
> **It won’t matter even if you die,**  
> **'Cause no one is coming tonight.**
>
> **~RavS**


_## Some nights, I feel so lonely, I start to doubt if my life has any purpose. ##_
