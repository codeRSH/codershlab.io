---
public: true
title: ए नूर
date: 2013-04-18
tags:
  - feelings
  - sad
categories: hindi
---

> **क्यों हैं मायूसी छाई** **,**  
> **क्यों लाल हैं तेरी आँखें,**  
> **क्यों है उदास तू,**  
> **बस इतना बता दे मुझे।**  
>   
> **तेरी उदासी देख तड़पूँ**   
> **ये भी कहाँ है सही,**  
> **लेकिन कुछ पूछूं भी तो कैसे**  
> **तुझसे कोई रिश्ता भी तो है नहीं।**  
>   
> **छुपा मत दर्द अपने आँचल से,**  
> **एक एहसान तू कर मुझपे,**  
> **तू खुशियाँ नहीं बाँट  सकता मुझसे,**  
> **अपने सारे गम ही मेरे कर दे।**  
>   
> **अजब है तू भी खुदा,**  
> **की जिसे देख कर मैं मुस्कुराता था**   
> **तूने उसे ही आज रुला दिया?**   
> **जिसको देखने के लिए तरसतीं थीं आँखें मेरी,**  
> **उसी की आँखों को आज भीगा दिया?**  
>   
> **मेरी जिंदगी तो वैसे ही अँधेरा है,**  
> **बस समझ इतना तू समझ ले ,**  
> **क्या पता ढूंढ लूं अपनी ज़िन्दगी भी,**  
> **जो कर सकूं ज़रा रौशनी तेरी ज़िन्दगी में।**   
>   
> **और फिर जब तक ये उदासी का ग्रहण रहेगा,**  
> **ए नूर, तेरे चेहरे का चाँद पूरा कैसे दिखेगा?**
> 
> **~रबी**


\[ Why are you so helpless?  
Why your eyes are red?  
Why do you look so sad?  
Just tell me everything for once.  
  
To suffer seeing your sadness,  
It’s not right for me, is it?  
But how do I ask you anything,  
There is no relationship between us.  
  
Don’t try to hide away your pain,  
Would you do me a favor?  
If you can’t share the happy moments,  
At least give me all your sadness.  
  
Nobody can understand you God,  
that who was my source of happiness,  
You made the same person cry?  
Whom my eyes used to crave to see,  
You brought tears in theirs?  
  
My life is already full of darkness,  
Just try and understand this,  
I might be able to discover my own life,  
If I could bring a little light in yours.  
  
And well… Till this eclipse of sorrow doesn’t fade,  
How would I be able to see that moon face. \]
