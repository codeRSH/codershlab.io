---
public: true
title: जुनूं-ए-इश्क़ 
date: 2012-02-11
tags:
  - love
  - friends
categories: hindi
---

![ ](../images/junoon-e-ishq.jpg)

> **कभी मशवरे पर हमारे भी गौर कर लिया करो फ़ैज़ाबादी,  
> तुमसे वक़्फ़ियत हमारी बहुत पुरानी है।**  
> 
> **ये जो जुनूं-ए-इश्क़ तुम पर चढ़ा है नया नया,  
> नादानी है तुम्हारी, छोड़ दो।  
> वर्ना फ़ना हो जाओगे।**
> 
> **~रबी**


\[ Listen to even our advice also, sometimes, oh Faizabadi,  
Our relationship with you is very old.

This madness of love that has engulfed you nowadays,  
It’s just immaturity of yours, leave it.  
Else you will get perished forever. \]
