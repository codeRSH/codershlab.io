---
public: true
title: In your vicinity
date: 2015-01-05
tags:
  - free-verse
  - special
categories: english
---

![ ](../images/In-your-vicinity.png)

> **Every time I am in your vicinity, I catch a new condition.**
>
> **Here is what you have made me :**  
> **Philophobic because of you.**  
> **Cingulomanic for you.**  
> **Somniphobic through you.**  
> **Insomnia, paranoia, hyperventilation… you have taught me.**
>
> **Your cold might be common but the shiver that you have given me; it’s not common. It’s rare.**
>
> **You are the cause and you are the cure of my problem..**
>
> **Alas they couldn’t find the cause and cure of your condition.**
> 
> **~RavS**
