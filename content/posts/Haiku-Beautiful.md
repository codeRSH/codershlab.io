---
public: true
title: Haiku - Beautiful
date: 2014-12-05
tags:
  - haiku
  - romantic
  - death
categories: english
---

![ ](../images/beautiful-death.png)

> **All I want is a beautiful death**  
> **and someone to mourn**  
> **when I am dead.**
> 
> **~RavS**
