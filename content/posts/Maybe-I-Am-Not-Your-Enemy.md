---
public: true
title: May be I am not your enemy
date: 2013-10-03
tags:
  - friend
  - philosophical
categories: english
---

> **Strip me - not just of my clothes,  
> My skin, my flesh, my bones.  
> Look straight into my bare soul,  
> Is it not mirror image of your own?**
> 
> **Then what makes you think I am,  
> Any different than who you are?  
> Look closer and you will find,  
> My conscience bears the same invisible scars.**
> 
> **Yes, may be the journeys are different,  
> Our destinations are all the same,  
> Stop wearing these shades of prejudiced ignorance,  
> May be I am not your enemy, may be… a friend.**
> 
> **~RavS**

_## Many times we make artificial differences between us, thinking of each other based on color, caste, race, religion even gender. So, our initial reaction is to form a negative perception for an ‘outsider’. But, inside we all want the same thing, a little bit of love. So, why the hate? ##_
