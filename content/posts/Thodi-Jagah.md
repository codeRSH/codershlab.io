---
public: true
title: थोड़ी जगह
date: 2017-03-26
tags:
  - love
  - philosophical
categories: hindi
---

![ ](../images/thodi-jagah.png)

> **ना बिछाओ  इतने  गलीचे, ना रखो  इतनी  कुर्सियां  मेरे  आँगन  में।    
> थोड़ी  जगह मुझे घर में प्यार का  गुलिस्ताँ  उगाने  को  भी  चाहिए।**
>
> **~रबी**


\[ Do not put so many carpets and chairs in my lawn,  
I want to grow a little bit of love in the house. \]
