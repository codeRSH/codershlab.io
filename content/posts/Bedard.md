---
public: true
title: बेदर्द
date: 2014-03-16
tags:
  - heartless
categories: hindi
---

> **हमें “बेदर्दी” की ज़िल्लत न दो, हम “बा-दर्द” कहलाते हैं,**  
> **बेदर्द तो वो होते हैं, जो ता-उम्र बे-दर्द रह जाते हैं।**
> 
> **~रबी**


\[ Don’t call me heartless, I am “hurt-full”,  
Heartless are those, who remain “hurt-less” their whole life. \]

  
_## 2 liners are always my favorite. Fastest to write. But writing something clever/witty is always the challenge. Thanks Akshita. ##_
