---
public: true
title: ना दोस्त, ना साथी
date: 2012-10-29
tags:
  - friendship
  - journey
categories: hindi
---

> **एक रोज़ पीछे मुड़कर देखा, कोई दिखाई ना दिया,  
> ना दोस्त, ना साथी, ना मित्र, ना सखा।    
> फिर किसी ने धीरे से कंधे पर हाथ रख कहा,  
> _“भाई मंज़िल आगे है। अब चलें क्या?”_**
> 
> **~रबी**

  

\[ One day, I turned around and could see no one,  
No friend, no companion, no mate, no confidant.  
Then someone gently tapped over my shoulder, and said,  
_“Bro, our destination is ahead. Shall we get going now?"_ \]
