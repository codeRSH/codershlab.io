---
public: true
title: नफ़रत
date: 2014-04-02
tags:
  - love
  - hatred
  - irony
categories: hindi
---

> **वो करती थी प्यार से प्यार और नफ़रत से नफ़रत कभी,**  
> **आज उसे प्यार से नफ़रत और नफ़रत से प्यार हो गया।**  
> **वो जो समझाया करता था, ना कर किसी से इतना प्यार पगली,**  
> **वो खुद किसी की नफ़रत का शिकार हो गया।**
> 
> **~रबी**


\[ She used to love the Love and hate the Hatred,  
Today she hates the Love and is in love with Hatred,  
And he who used to advise her, don’t love anyone so much idiot,  
He himself became a victim of someone’s hatred. \]
