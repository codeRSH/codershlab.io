---
public: true
title: Happy Valentine Day
date: 2018-02-14
tags:
  - love
  - valentine
  - special
categories: english
---

![ ](../images/happy-valentine-day.jpg)

> **I think a lot about the subject of love. Not just the romantic kind, but love of all kinds.. Brotherly, motherly, friendly…**
>
> **There are people who want someone to love them. Someone who can take away their feeling of being ‘lonely’. Someone to take care of them, to talk to them and be a base for their self worth.**
>
> **Then there are some other kind of people, people like yours truly. More than someone to love them, they want someone whom they can love wholeheartedly. They hope their love would be acknowledged and respected, and at least not rejected or ridiculed.**
>
> **I feel both of these are selfish forms of love. Because both these forms expect something in return. But what happens when that expectation is not met?**
>
> **Love in its truest form is the suspension of this 'Self’. When our love doesn’t depend on the reaction of the other person, when we love for the sake of it and not to get something back in return, that’s when we truly love.**
>
> **I hope we all get to experience that love. At least temporarily. At least once.**
>
> **Happy Valentine’s Day (whatever that means).**
>
> **~RavS**


_## Needs no explanation. ##_
