---
public: true
title: Creative
date: 2014-11-16
tags:
  - free-verse
  - creative
  - special
  - milestone
  - anniversary
categories: english
---

![ ](../images/creative.jpg)

> **You set yourself ablaze,**  
> **Light up the abyss,**  
> **Highlight your majestic nightmares.**  
> **Volunteer to dive into the black hole of ignorance,**  
> **To bring out the pearls of gay reminiscence.**  
>
> **You delight, you surprise, and then you break the heart.**  
> **You use your tears as ink to write another invisible story on the walls of consciousness.**  
> **You construct your memoir through the countless hickies on everyone you embrace.**  
> **You endure the labour of inertia and give birth to a new love everyday.**   
>
> **Then you go to sleep knowing that you left the Universe shaken and stirred.**  
> **And it would never be the same again.**  
>
> **Yes baby, you are creative.**
> 
> **~RavS**

_## Today is the 3rd anniversary of this blog, and #250 is here!!_

_And from here I try something new. A new style of poetry I recently discovered. Free verse written on a vintage textured paper with old fonts. Looks beautiful to me. :) ##_
