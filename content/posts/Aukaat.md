---
public: true
title: औकात
date: 2016-05-02
tags:
  - dare
categories: hindi
---

> **तू  है,  
> तो  जो  है,  
> कर  जा।    
> वर्ना  खैर  कोई  बात  नहीं।    
> तेरी खिलाफत  कर  कुछ  कर  गुज़रने की,  
> वैसे  भी  मेरी  कोई  औकात  नहीं।**
>
> **~रबी**


\[ If you are there,  
then whatever it is,  
you do it.  
Otherwise, it’s alright.  
To go against you and do something,  
I anyway don’t have that stature, in me. \]
