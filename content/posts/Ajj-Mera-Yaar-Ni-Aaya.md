---
public: true
title: अज्ज मेरा यार नि आया
date: 2013-09-11
tags:
  - friend
  - longing
  - first
  - special
  - punjabi
categories: hindi
---

> **मैं करदा रवां इंतज़ार,  
> पर अज्ज मेरा यार नि आया।**
> 
> **मैं पट्ट देखां, देखां कई वार,  
> के अज्ज मेरा यार नि आया।**
>   
> **कि कित्ता मैं गल कोई खार,  
> जे अज्ज मेरा यार नि आया।**
> 
> **लगदा ओंनू देखे दिन हुए हज़ार,  
> ना अज्ज मेरा यार नि आया।**
> 
> **आजा, अब ते कंवली होया यार,  
> क्यों अज्ज तू मेरे यार नि आया।**
> 
> **मेनू सौ उस दी, मैं छड्ड देना संसार,  
> जो अज्ज मेरा यार नि आया।**
> 
> **~रबी**

  

\[ I kept waiting for,  
but today my friend didn’t come.

I saw, saw the doorsteps umpteen times,  
still my friend didn’t come.

Did I say something harsh to him,  
that my friend didn’t come.

It seems it’s been ages seeing him,  
no, today my friend didn’t come.

Com'n, I will go mad like this,  
why mate, why you didn’t come.

I swear, I will leave this world,  
if today my friend doesn’t come. \]


\## _I don’t know if it happens with you, but whenever my best friend doesn’t join me at work (or school/college), it feels like something is missing in life. The brain keeps asking why didn’t he come today? ##_
