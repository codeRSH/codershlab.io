---
public: true
title: Same set of words
date: 2015-01-14
tags:
  - special
  - ink-on-paper
categories: english
---

![ ](../images/same-set-of-words.jpg)

> **You and I have exactly the same set of words.**  
> **But I know why you are able to paint them so much better than me.**  
> **Because you crush your emotions inside the cavities of your letters.**  
> **You have endured far too much than I could possibly imagine myself to do.**
>
> **I merely write.**  
> **You vomit, you bleed, you sweat, you ejaculate, you lactate,**  
> **You piss, you excrete.**  
> **You don’t attempt to hide all the ugliness under the guile of saccharine words.**  
> **Some people hate you for it.**  
> **Not everyone likes to read about the nakedness of humanity.**  
> **Some of us want the stories of puppies and fairy tales.**  
> **You instead peel your skin and show your bones.**  
> **For them to agnize that under all our inferiority and superiority complexes,**  
> **we are all the same, after all.**
>
> **I can’t match you.**  
> **I don’t even try to.**
>
> **~RavS**
