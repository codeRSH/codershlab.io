---
public: true
title: Job Well Done...
date: 2012-03-04
tags:
  - main-blog
  - inspiration 
categories: english
---

> **As I opened my eyes,  
> the head still pounding,  
> consciousness coming and going,  
> the mouth tasting like vomit,  
> the veins trying to bust into pieces,  
> breath playing hard to catch up,  
> hands paralyzed, legs feeling crippled.**
> 
> **As I lay there in pool of own blood and sweat,  
> clothes torn, soiled, hair ruffled,  
> I saw the faces of my fam and friends,  
> the pride in their eyes, the smile on their faces.  
> At that moment, at that very moment,  
> the only noise that was impeding between the ears,  
> was of my own conscience.  
> It was shouting, voice mixed with relief and excitement,  
> "Job well done, Job well done, Job well done"...**
>
> **~RavS**
