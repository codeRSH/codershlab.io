---
public: true
title: Lady Luck
date: 2011-11-24
tags:
  - work
categories: english
---

> **The body feels fragile and the mind feels weak,**  
> **The spirits are down, prospects of success seem bleak,**  
> **Oh! lady luck, if you can hear me somehow,**  
> **The time for you to smile is now…**
> 
> **~RavS**
