---
public: true
title: खुशगवार से चेहरे
date: 2017-05-02
tags:
  - death
  - philosophical
categories: hindi
---

> **ना  उड़ाओ  हँसी  मौत  की  भरी  महफ़िल  में  यारों,  
> ये  खुशगवार  से  चेहरे, ना जाने  कितने  करीबी  हैं।**
>
> **~रबी**


\[ Do not make fun of death in front of everyone, friends,  
You never know these happy faces might be so close to it. \]

_## Just saying we never know what the other person might be suffering from. So gotta be careful about saying something that may offend them. ##_
