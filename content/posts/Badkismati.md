---
public: true
title: बदकिस्मती
date: 2014-10-22
tags:
  - dedication
  - badluck
categories: hindi
---

> **एक सहर उठी थी आज,**  
> **कुछ गुलाबी सी, कुछ लाल सी,**  
> **और दिन की तरह नहीं थी आज,**  
> **कुछ अलग थी, एक चमक थी,**  
> **जैसे बहुत दिनों बाद उठी थी आज,**  
> **कुछ कहती ताजगी, कुछ महक भी।**  
>   
> **पर मैं उठना ही भूल गया आज,**  
> **हाय बदकिस्मती, हाय बदकिस्मती।**
> 
> **~रबी**


\[ A morning woke up today,  
A little pinkish, a little red,  
She wasn’t the same like everyday,  
A little different, there was a spark,  
Like she woke up after a lot of days today,  
Something says the freshness, a little fragrance.

But I forgot to wake up today,  
O Bad Luck, My Bad Luck. \]
