---
public: true
title: Bloodied and Battered
date: 2014-11-17
tags:
  - dark
  - dedication
  - special
categories: english
---

> **I am in that kinda mood today,**  
> **I want to harm you so much,**  
> **Suck the oxygen out of your  air-tract,**  
> **Bring you to the knees begging for breaths,**  
> **I will laugh, as you choke to death,**  
> **But I won’t let  you die, just yet.**  
> **There’s so much I will do to you,**  
> **You would curse the one you were born to.**  
> **Run away, run away ,**  
> **You’ll get killed today.**
> 
> **I will smell the shallow breath under your nose,**  
> **Feel the uneasiness boiling inside your navel,**  
> **See if you shriek with agony or pleasure ,**  
> **As I stab my teeth on your shoulders,**  
> **Or pierce your thighs with little needles,**  
> **Or hit your ribcage hard with a sledge hammer.**  
> **Wow.. And I wouldn’t even have started.**  
> **Run away, run away,**  
> **You’ll get killed today.**
> 
> **Skin you to the bones,**  
> **And eat away your  limbs,**  
> **Lick the tears rolling off your cheeks,**  
> **You’d be frightened, but won’t be able to scream,**  
> **And then gauge your eyes, from the duct,**  
> **And throw them in fire to burn,**  
> **There’d be tears of blood rolling off now,**  
> **And I will lick them as well.**  
> **Run away, Run away ,**  
> **You’ll get killed today.**
> 
> **You won’t be able to hide from me,**  
> **Wherever you want to run away.**  
> **I will paint your body with endless scars,**  
> **So deep, they will never conceal or fade,**  
> **They will be the reminiscence,**  
> **Of my dreadful presence,**  
> **I will pump in you, pain so much,**  
> **You will shudder even at slightest of my touch.**  
> **Run away, run away,**  
> **You’ll get killed today.**
> 
> **~RavS**

_## Probably my darkest composition yet, written on a request. ##_
