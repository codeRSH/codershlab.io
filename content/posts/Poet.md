---
public: true
title: Poet
date: 2014-12-22
tags:
  - prose
  - poet
categories: english
---

![ ](../images/poet.png)

> **I always thought being a poet meant having an extraordinary vocabulary, an incredible imagination and a knack of finding the right words for the right situation. But now I know, it’s all bollocks. To be a poet, at least in my eyes, you just need two things - a madly vulnerable heart and a courage to rip it apart.**
>
> **~RavS**
