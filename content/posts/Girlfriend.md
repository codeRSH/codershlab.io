---
public: true
title: गर्लफ्रेंड 
date: 2012-02-14
tags:
  - witty
  - valentine
  - special
categories: hindi
---

![ ](../images/girlfriend.jpg)

> **हमें  जलाने  की  कोशिश  मत  कर  दुनिया, ये  ज़ाया है।    
> गर्लफ्रेंड  तो  हम  जब  मर्ज़ी, बना  सकते  हैं।    
> हम  तो  बस  उसके  इंतज़ार  में  हैं, जो  हमें  चाहे,  
> वर्ना  हम  तो  बहुतों  को  चाहते  हैं … ;)**
>  
> **~रबी**


\[ Don’t try to tease us, you cruel world,  
We can have a girlfriend whenever we want.  
We are just awaiting someone who likes us,  
Otherwise we like way too many of them! \]
