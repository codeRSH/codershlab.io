---
public: true
title: Haiku4U - 4
date: 2019-04-04
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-4.jpg)

> **let’s embrace difficulties,  
> they’ll define  
> the story, that’s us.**
>
> **~RavS**


_## I don’t like difficult moments, neither do you. But if everything were to be so easy. If I were to get you in the first glance,  if our life path  were to be a linear line of progress, would it all be worth going through? Hardly, I believe._

_Let’s be prepared for more difficult moments, they will define who we are. And they will be the moments worth remembering the most. Trust me.  ##_
