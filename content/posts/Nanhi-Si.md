---
public: true
title: नन्ही सी
date: 2014-12-21
tags:
  - special
categories: hindi
---

> **नन्ही सी तो हो तुम मेरे सामने,**  
> **पर मैं इतना भी बड़ा नहीं,**  
> **की तुम मुझे ओढ़ कर,**  
> **दुनिया से खुद को बचा लोगी।**  
>   
> **~रबी**


\[ You are so tiny, my baby,  
but I am not that huge even,  
that you can wrap me around,  
and save yourself from the world. \]
