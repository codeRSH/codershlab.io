---
public: true
title: मेहुल
date: 2018-09-11
tags:
  - rain
  - romantic
categories: hindi
---

![ ](../images/mehul.png)

> **वो लफ्ज़ वो तराने,  
>   कागज पर लिखे गाने,  
>     तुझे हंसाने के बहाने  
>       कहीं संभाले हैं ना मेहुल।**  
>   
>  **नाम तेरा सोंधा सा,  
>      मेरे होटों पर रुकता,  
>        जाते जाते लौट आता,  
>           क्यों है ना मेहुल।**  
>   
>   **पानी में तेरी छपकियां,  
>      रात सोते मेरी थपकियां,  
>         लोरी सुनते सुनते झपकियां,  
>            याद आती हैं ना मेहुल।**  
>    
>  **पुलिंदे अपनी तारीफ के,  
>    छत के किनारे बैठ के,  
>     हर शाम बंधवाना मुझसे,  
>        भुलाए नहीं हैं ना मेहुल।**  
>      
> **वह दिन चले गए,  
>   ना रही अब वो रातें,  
>     मैं बदल गया साथ मौसम के,  
>       तू मगर वही है ना मेहुल।**  
>   
> **\*मेहुल = बारिश**  
>   
> **~रबी**  

  
\[ Those words, those hymns,  
  Songs written on paper,  
    Excuses to make you laugh  
      You have kept them safe, right Mehul?  
  
 Your name like a fragrance,  
     Stops on my lips,  
       Comes back while leaving,  
          Isn’t it right Mehul?  
  
  Your splashes in water,  
     My dabs while sleeping at night  
        Little naps while listening to lullabies,  
           You Miss them, right Mehul?  
   
 The trumpets of praise,  
   Sitting on the edge of the roof,  
    Making me sing every evening,  
       You have not forgotten, right Mehul?  
     
Those days are gone,  
  Now those nights are not there,  
    I changed with the weather,  
      But you are the same, right Mehul?  
  
\*Mehul = Rain \]  
  
  
_## Written for those lovely cool evenings of monsoon. ##_
