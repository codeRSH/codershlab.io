---
public: true
title: रुखसती
date: 2014-07-06
tags:
  - 30-day-challenge
categories: hindi
---

> **रुखसत होती है तेरा दामन पकडे खलिश भी और खुशी भी,**  
> **रुखसत होती है तेरे आँचल में मेरी चीख भी मेरी हंसी भी ,**  
> **पता नहीं तेरी रुखसती में आज ये आँखों का झरना कौन रुख ले,**  
> **पता नहीं इनमे नमकीन पानी खुशी के गिरे या गम के बहे।**
> 
> **~रबी**

  
\[ Holding the tip of your clothes, both the pain and happiness depart,  
Inside your scarf, both my screams, my laugh depart,  
Don’t know in your departure, which stance will this cascade of eyes take, Don’t know if the salty water will fall, of joy, or will flow, of sorrow. \]
