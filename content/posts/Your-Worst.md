---
public: true
title: Your Worst
date: 2015-01-15
tags:
  - free-verse
  - challenge
categories: english
---

![ ](../images/your-worst.png)

> **The Mirror lies, and so does the world,  
> The blemishes on your face aren’t there.  
> Look at your reflections in my eyes,  
> Do you find a single trace of ugliness?**
>
> **You are as beautiful as the glacial cone,  
> A sparkling delicate crystal for me to adore,  
> Sure, sometimes there will be frost on you,  
> And sure sometimes my light won’t reach you,  
> But this dust on you is not your identity,  
> And you never needed my shine to be world’s envy.**
>
> **I challenge you. Bring out what you think is your worst.  
> People who can see you through, will still watch in awe.**
>
> **~RavS**
