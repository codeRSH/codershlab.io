---
public: true
title: सहर
date: 2014-07-05
tags:
  - 30-day-challenge
categories: hindi
---

> **जब रात में माँ घुड़की नहीं लोरियाँ सुनाएगी,**  
> **कोई सपना बुरा ना होगा, नींद अच्छी ही आएगी।**  
> **जब उम्मीद खुशियों को ऊँगली पकड़ खींच लाएगी।**  
> **जिंदगी भी पीछे पीछे लौट ही आएगी।**  
> **स्याही घुली हो अम्बर में कई रातों से चाहे कितनी भी,**  
> **इस शहर में सहर कभी तो आएगी।**
> 
> **~रबी**

  
\[ When mother won’t give scoldings but sing lullabies,  
Any dream won’t be a nightmare, sleep will be fine.  
When hope will bring back happiness, by the grip of its fingers,  
Life will also come back following.  
No matter for how many nights the sky has been pitch dark,  
This city will see the morning one day. \]
