---
public: true
title: Haiku4U - 8
date: 2019-04-08
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-8.jpg)

> **Don’t let the dreams die.  
> We’ll guard them  
> Till we are alive.**
>
> **~RavS**


_## We saw a dream. Together. That was the easy part. The hard part is to protect it when the shrapnel of reality tries to smash that dream. Because no one, Absolutely no one in this world would ever understand what that dream means to us._

_I can’t protect that dream alone. I need your help. Will you?  ##_
