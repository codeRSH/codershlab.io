---
public: true
title: सितार
date: 2014-07-20
tags:
  - 30-day-challenge
categories: hindi
---

> **जो रोक दे वक्त को, कि एक लम्हा सांस ले सकूँ,**  
> **जो थाम ले सांस को, कि हांफता दिल कुछ ले सके सुकूँ,**  
> **बंद पड़ जाए धड़कने जिससे, कि कुछ तो मद्धम हो दौड़ता खून,**  
> **क्या तुम सितार को ऐसा कोई साज दे सकते हो ?**
> 
> **~रबी**


\[ Which can stop the time, so that I can take a moment to breath,  
Which can hold the breath, so that the breathless heart can get some peace,  
Which can make the heart stop beating, so that the blood can slow down at least,  
Can you strum such a note from your Sitar? \]
