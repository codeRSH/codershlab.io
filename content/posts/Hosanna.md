---
public: true
title: होसाना
date: 2012-01-20
tags:
  - faaltu
  - spoof
categories: hindi
---

> **आँखें  अगर  होतीं  मेरी  भैंस  की  दो ,**  
> **एक  तुम्हें  दे  देता  निकालने  को।**  
>   
> **होसाना… होसाना… होसाना…**  
> 
> **किडनी  अगर  होतीं  जो  मेरी  बॉडी  में  दो ,**  
> **एक  तो  दे  ही  देता  तुम्हे  डायलिसिस  करने  को।**  
>   
> **होसाना… होसाना… होसाना…**
> 
> **~रबी**


\[ If my buffalo would have had two eyes,  
I would have given you one to pull out.  
  
Hosanna… Hosanna… Hosanna…  
  
If my body would have had two kidneys,  
I would have given you one to do dialysis.  
  
Hosanna… Hosanna… Hosanna… \]


_## Hosanna seems to be a “holy” word. This was written as a spoof to a song, and the intention is not to hurt anyone’s sentiments. ##_
