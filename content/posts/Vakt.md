---
public: true
title: वक्त
date: 2014-02-23
tags:
  - separation
categories: hindi
---

> **बातें तो बहुत थीं,**  
> **मंज़र भी सही बहुत था,**  
> **कह कर रुला देने का ,**  
> **मकसद भी जिगर में कहीं बहुत था,**  
> **बस ना थी मेरे पास वो सब कह पाने की हिम्मत,**  
> **और ना उसके पास सुन पाने का वक्त ही बहुत था।**
> 
> **~रबी**


\[ There was a lot to say,  
And it was a perfect time for it,  
To make them cry, by telling all that,  
I had this wish also deep inside,  
But what I didn’t have was the courage to say it,  
And what they didn’t have was the time to hear it. \]
