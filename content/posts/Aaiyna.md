---
public: true
title: आईना
date: 2014-06-26
tags:
  - 30-day-challenge
categories: hindi
---

> **एक दिन दिल सीने से निकाल कर रख दिया आईने के सामने,**  
> **बोला, देख। देख कितना बदसूरत है तू।**  
> **क्यों करता है फिर मोहब्बत किसी से,**  
> **मैं हूँ न तेरे पास, फिर तुझे और किसी की जरूरत है क्यों?**  
> **दिल ने आईना बड़े गौर से देखा,**  
> **बोला, बदसूरत सही मैं, पर पाक हूँ।**  
>   
> **तुम मेरे साथ हो पर मैं तुम्हारा ना रहा,**  
> **मैं तुम में धड़कता तो हूँ,**  
> **पर दर्द मुझमे अब तुम्हारा ना रहा,**  
> **और आईने में देखने की बात जाया ही करते हो तुम,**  
> **मेरी बंदगी तो मोहब्बत से है बस, मोहब्बत करने वाला कभी हमारा ना रहा।**
> 
> **~रबी**


\[ One day, I ripped out the heart from my chest and put it down in front of the mirror,  
I said, see, see how ugly you are.  
Then why do you even try to love someone,  
I am there with you no, then why do you need anyone else?  
Heart looked carefully in the mirror,  
Then said, yeah I am ugly but am still pure.

You are with me, but I am yours no more,  
I still beat inside you,  
But my pain is yours no more,  
And why do you even ask me to look in the mirror,  
My loyality is only towards love, the lover has never been ours. \]
