---
public: true
title: तन्हाई
date: 2013-12-13
tags:
  - relationship
  - separation
categories: hindi
---

> **हमारे कूचे को मुड़कर न देखना, ये उनकी रज़ा थी,**  
> **उनकी गली में पाँव न रख सकें, ये हमारी सजा थी,**  
> **ता-उम्र तन्हाई में काट देना किसे मंजूर था मगर,**  
> **कुछ उनकी, कुछ हमारी वजह थी।**
> 
> **~रबी**


\[ They won’t come to my area, it was their wish,  
I can’t set foot in their area, it was my punishment,  
Who wanted to live a life of this kind of loneliness,  
But they had some reasons, and I had some reasons. \]
