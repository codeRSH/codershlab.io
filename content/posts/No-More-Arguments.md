---
public: true
title: No More Arguments
date: 2014-11-21
tags:
  - posessive
  - free-verse
categories: english
---

![ ](../images/no-more-arguments.jpg)

> **You are mine.**
>
> **Every thought, every word, every deed, every setback, every consequence,**  
> **all the flaws, all the insecurities, all the fears, all the inhibitions,**  
> **the tiniest of wishes, the smallest of desires, the scariest of dreams, the ugliest of nightmares,**  
> **each laugh on your lips, each glitter in your pupils, each strand fallen from your eyelids,**  
> **your heartbreaks, your heartaches, your love, your hatred,**  
> **past, present, future and thereafter.**  
> **It’s all mine. Everything.**  
> **Today, tomorrow, forever. And ever.**
>
> **You are unshareable, understand this.**  
> **I don’t want to argue about it anymore.**
>
> **~RavS**
