---
public: true
title: What will you do with the thorn in your flesh?
date: 2013-11-12
tags:
  - life
  - difficulties
  - bittersweet
categories: english
---

> **A thorn in my flesh,**  
> **It hurts every day.**  
> **The more I want to walk away,**  
> **The more it gets in the way.**  
> **A constant reminder of my past,**  
> **The stuffs I did, the mistakes I made.**
> 
> **So deep, it now touches the bone,**  
> **I can take it out, and move on.**  
> **But, it could kill me to remove the thorn,**  
> **From the extreme agony and blood loss.**  
> **Not even sure, if I want to part from it,**  
> **After all it’s a part of me now.**
> 
> **Everyday I fight this battle, from dusk till dawn,**  
> **Should I learn to live with the thorn,**  
> **Piercing the body every waking minute?**  
> **Or, risk my life and test my spirit,**  
> **Pull it out, and set myself free?**
> 
> **You can’t live with it, you can’t live without.**  
> **What will you do with the thorn in your flesh?**
> 
> **~RavS**

  

## _Are there some memories in your life which are really difficult to remember, but at the same time are linked to something good as well? ##_
