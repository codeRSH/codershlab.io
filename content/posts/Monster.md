---
public: true
title: Monster
date: 2017-06-25
tags:
  - nightmares
  - fear
categories: english
---

![ ](../images/monster.png)

> **I am afraid to close my eyes tonight.  
> A monster is waiting for me to drown.**  
>   
> **I gasped for breath and saw your face,  
> All I could utter is… “Mom!”.**  
>   
> **I don’t know if it’s pent up anxiousness,  
> Or do I really feel so alone.**  
>   
> **But I’ll be keeping the door unlocked,  
> Hoping that you will come.**  
>   
> **~RavS**


_## Wrote it on an nightmare filled night. ##_  
