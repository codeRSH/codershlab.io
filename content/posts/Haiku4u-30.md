---
public: true
title: Haiku4U - 30
date: 2019-04-30
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-30.jpg)

> **Unknown the heights I’ll go,  
> The depths I’ll plunge,  
> All to get you.**
>
> **~RavS**


_## I have done whatever I had in me all to get you. And even I don’t know how far I can go until I get you. Just saying. ##_
