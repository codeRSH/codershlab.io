---
public: true
title: सजनी
date: 2014-07-31
tags:
  - song
  - special
  - sajani
categories: hindi
---

> **सजनी देखो, मेरी एक बात सुनो,**  
> **कबसे नहीं बोली तुम, कुछ बात तो करो,**  
> **जो भी बात हुई, छोटी बड़ी कुछ भी,**  
> **जाने दो, तुम ऐसे तो ना रूठो।**
> 
> **सजनी, तुम्हे तो पता ही है, मैं कैसा हूँ,**  
> **छोटी छोटी बात पर बिफर पड़ता हूँ।**  
> **पर तुम तो वो धागा हो ना, जो बांधे रखता है,**  
> **तुम टूटो तो मैं भी बिखर पड़ता हूँ।**
> 
> **दुखता मुझे भी है सजनी, जो चोट लगती है तुमको ,**  
> **छलनी होता है कलेजा मेरा भी, ठेस लगती है जब तुम्हारे दिल को,**  
> **बहुत गुस्सा हो ना, मैं जानता हूँ।**  
> **क्या पता तुम्हे अच्छा लगे, चलो थोड़ा सा मुझे आज मार ही लो।**
> 
> **सजनी अच्छा चलो अब तुम ही कहो,**  
> **क्या करूँ जो भूल जाओ मेरी खता को,**  
> **न कुछ कह सकता, न सुन सकता तुम्हें,**  
> **दूर से बस थोड़ी सी देर तुम्हे देख तो लेने दो।**
> 
> **सजनी तुम्हारी ये बात बहुत गलत है,**  
> **तुम तो रूठी हो, तुम्हे पता है मेरी क्या हालत है ?**  
> **तुम जुदा हुई तो दुनिया ने भी साथ छोड़ दिया,**  
> **तन्हाई नहीं लेने देती साँसे, इतनी भी नहीं राहत है।**
> 
> **सजनी बहुत हुआ, अब सहा नहीं जाता ,**  
> **कब तक रूठोगी अब जिया नहीं जाता।**  
> **ऐसा करोगी तो मर ही जाऊँगा, सच में ,**  
> **तुम्हारे बिना तो अब वैसे भी रहा नहीं जाता।**
> 
> **~रबी**


\[ Sajani Look, listen to me,  
You haven’t spoken for so long, please talk to me,  
Whatever the issue was, major or minor,  
Let it go, You don’t get angry like this, please.

Sajani, you know how I’m like,  
I get mad on very little things.  
But you are that thread, which binds me,  
When you break down, I get shattered too.

Sajani it pains me too when you get hurt,  
My guts get shattered when your heart gets scarred,  
You are very angry na, I know.  
OK, who knows you might feel good, just beat me a little today.

Sajani, OK you only tell.  
What shall I do that you forget my sins,  
Can not say anything, can not hear you,  
OK, at least let me look at you for a while.

Sajani, this is very wrong,  
You’re upset, but you know what my condition is?  
You got separated, so the world also left me,  
Loneliness does not let me take breaths, there’s not even so much relief.

Sajani that’s it, I can not bear it now,  
How long will you stay angry, I can’t live anymore now.  
If you stay like this, I will die, I swear,  
I can’t live without you anyway now. \]
