---
public: true
title: Haiku4U - 2
date: 2019-04-02
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-2.jpg)

> **I blinked and you came.  
> Won’t blink again,  
> lest you’ll be gone.**
>
> **~RavS**


_##  You came in life in a whisker, before I could even finish blinking my eyes. And that’s why I need to be extra careful. The moment I take my eyes off you, someone might come and snatch you away. I won’t rest until I get you.  ##_
