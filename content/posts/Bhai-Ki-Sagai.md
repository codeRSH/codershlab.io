---
public: true
title: भाई की सगाई 
date: 2014-05-24
tags:
  - engagement
  - humor
  - special
  - faaltu
categories: hindi
---

> **आप आये मेरे भाई की सगाई में क्या जरूरत थी,**  
> **खुद तो आये ही पूरा परिवार लाने की भी आपने जुर्रत की,**  
> **साला अब क्या खाने के पैसे तुम्हारे पडोसी आके देंगे,**  
> **खुद तो ठूस कर निकल लोगे, आधे जने अब भूखे मरेंगे।**
> 
> **यार सगाई पर बुलाने का तो सिर्फ रिवाज है,**  
> **इसका मतलब ये तो नहीं आ धमको सही में, अगर नहीं कोई काम काज है ?**  
> **मैं तो सोच ही रहा था तुम्हारी मनहूस शकल न देखनी पड़े,**  
> **भगवान करे तुम्हारे भाई की शादी हो तो गली के कुत्ते खाने पे पहले ही टूट पड़े।**
> 
> **~रबी**


\[ You came to my brother’s engagement what was the necessity?  
You not only came yourself you also brought your whole God-damn family,  
The hell, will your neighbors come and give the money for all the food you will eat?  
You will stuff your bloody mouth, and others will now starve to death.

My friend, to invite people on engagements is just a formal tradition,  
It doesn’t mean that you actually come to the engagement, if you don’t have any work,  
I was hoping that I don’t get to see your ugly face,  
But now I pray to God, when your brother gets engaged, all the street dogs eat up the food from your plates. \]

_## Katy’s elder brother is getting engaged soon. He asked me to write a few lines to be printed on the engagement card. This is what I came up with. He liked it a lot. But for some reason he is not printing it on the cards :) ##_
