---
public: true
title: Haiku - Bleed
date: 2014-10-27
tags:
  - haiku
  - creation
categories: english
---

> **Bleed a little,**  
> **From anywhere,**  
> **Let it drip on me.**
> 
> **~RavS**
