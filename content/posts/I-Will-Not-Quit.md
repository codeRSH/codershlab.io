---
public: true
title: I Will Not Quit
date: 2012-07-01
tags:
  - girl
  - special
  - dedication
  - tears
categories: english
---

> **Needed the tears to make me stronger,  
> A little bump on head to make it clearer,**  
>   
> **I am not a structure of mortar and iron,  
> I am not sissy if I cry, I am just human.**  
>   
> **A girl. With flesh, bones and emotions,  
> I also get hurt like every other person.**  
>   
> **Blessed are those who can cry & move on,  
> Some unfortunate ones don’t even have tears to befriend.**  
>   
> **So, I will cry, I will wail.  
> But that’s just about it.  
> Come hell or high water,  
> I. Will. Not. Quit.**
> 
> **~RavS**
