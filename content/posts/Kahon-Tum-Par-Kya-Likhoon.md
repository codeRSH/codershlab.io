---
public: true
title: कहो तुम पर क्या लिखूं ?
date: 2014-09-28
tags:
  - dedication
categories: hindi
---

![ ](../images/kaho-tum-par-kya-likhoon.jpg)

> **ये जो तुम रोज रोज कहा करती हो,**  
> **हमारे लिए भी कुछ लिखा करो।**  
> **तुम्हे पता है कितना मुश्किल होता है,**  
> **जब इस बात का ही कुछ पता न हो,**  
> **की किस किस अदा पर लिखूं,**  
> **किस किस बात को रहने दूँ।**
> 
> **चलो ठीक है, बोलो कहाँ से शुरू करूँ ?**  
> **क्या इन बालों को किसी नागिन की अंगड़ाई कह दूँ ?**  
> **या इन्हे किसी दिल-फैंक शायर की स्याही कह दूँ ?**  
> **अहह… हद है। ऐसा भी कोई कहता है भला ?**  
> **काली रात से बालों को देखकर, मुंह से कुछ और कभी निकलता है भला ?**
> 
> **अच्छा, तो क्या इस हंसी को सूरज की रोशनी कह दूँ ?**  
> **इन आँखों को शराब की नदी कह दूँ ?**  
> **इन पलकों को फूलों की झड़ी कह दूँ ?**  
> **होटों को कली या पंखड़ी कह दूँ ?**  
> **कहो तो गले को सुराही कह दूँ ?**  
> **कहो तो हाथों को सागर की गहराई कह दूँ ?**  
> **तुम कहो तो सही की क्या कह दूँ,**  
> **जो कहो वो वैसा ही बयाँ कर दूँ।**
> 
> **अच्छा, दुनिया को क्या आज ये ही बता दूँ,**  
> **की जो कमर पर तुम्हारी बल पड़ें,**  
> **दीवाने यहां से वहां उछल पड़ें,**  
> **तुम जाओ जहां जहां पर,**  
> **इशकजादे पीछे पीछे चल पड़ें,**  
> **तुम मुस्कुरा दो किसी एक को देखकर,**  
> **दिलजले बाकी अपने आप ही जल मरें,**  
> **और कहीं तुम कर लो बात किसी से रुक कर,**  
> **हाय… वो तो आशिक़ों के पैरों तले ही कुचल मरें।**
> 
> **शायद तुम खुश हो जाओगी,**  
> **अगर ऐसा ही कुछ लिख दूँ,**  
> **पर तुम्हे ज्यादा पता होगा,**  
> **तुम्हे क्या बुरा क्या अच्छा लगता होता,**  
> **क्या फायदा जो पसंद ना आये अगर कुछ लिखूं,**  
> **इसलिए तुमसे ही पूछ लेता हूँ,**  
> **बोलो तुम पर क्या लिखूं ?**  
> **कहो तुम पर क्या लिखूं ?**
> 
> **~रबी**


\[ This thing that you ask everyday,  
That please write something for me as well.  
You know how difficult it is,   
When one doesn’t even know,  
That from where he should begin,  
And what all things he should skip.

Okay, tell me where should I begin from?  
Should I call these hair a stretch of a serpent ?  
Or call them the ink of a romantic poet ?  
Ahh … how stupid. Does anyone ever say like this?  
After looking at hair like pitch blank night, does anything else ever leaves  mouth ?

Ok, so should I call this laughter sunlight?  
Should I call these eyes a river of wine?  
Should I call these eyelids a bunch of flowers ?  
Should I call these lips a bud or a petal?  
Tell me, if I should call this throat a pot of clay?  
Tell me if I should describe these hands as the depths of ocean?  
Just tell me what I should say,  
I will say just as it is.

OK, should I tell the world at least this,  
That when you move your waist,  
The crazies fall here and there,  
Wherever you go,  
The fans follow you trailing behind,  
If you smile at someone,  
The others die out of jealousy,  
And if by chance, you ever stop and talk to someone,  
God… They get crushed under the feet of your lovers.

May be you’ll get happy,  
If I write something similar to this,  
But you would know better,  
What feels good to you, what feels bad,  
What purpose will it serve, if I write and you don’t like,  
So let me ask you only,  
Say what should I write on you?  
Tell what should I write on you? \]
