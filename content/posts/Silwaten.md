---
public: true
title: सिलवटें
date: 2014-07-08
tags:
  - 30-day-challenge
categories: hindi
---

> **जो कर सके हाल-ए-बयान,**  
> **तेरे कांच के अर्श का,**  
> **मेरे मिट्टी के फर्श का,**  
> **तेरे बढते मर्ज़ का,**  
> **मेरे बहते दर्द का,**  
> **नमी तेरी नज़र का,**  
> **कमी मेरे हमदर्द का,**  
> **कहाँ से लाऊं सिलवटें इतनी …**
> 
> **~रबी**

  
\[ Which could describe clearly,  
About your glass ceilings,  
About my soiled floor,  
About your increasing disease,  
About the pain running in my guts,  
About the moisture in your eyes,  
About the lack of my soulmate,  
From where should I get so many wrinkles… \]
