---
public: true
title: Trail
date: 2014-12-01
tags:
  - path
  - free-verse
categories: english
---

![ ](../images/trail.png)

> **I don’t want to leave a trail behind,**  
> **As we walk alone from nowhere to nowhere.**  
> **I fear I might have to descend it back home,**  
> **And you won’t be returning with me.**
>
> **Let me get erased with you, please.**
>
> **~RavS**
