---
public: true
title: सनी भाभी
date: 2014-05-13
tags:
  - birthday
  - faaltu
categories: hindi
---

> **मेरी प्यारी सनी भाभी,**  
> **क्या पता था जन्मदिन आता है आपका भी,**  
> **मुझे तो लगा था आपने दुनिया में अवतार लिया,**  
> **भगवान न मिले तो दुनिआ ने आपको ही अपना लिया।**
> 
> **लेकिन याद करो, आपकी कदर सबसे पहले भाई ने ही जानी थी,**  
> **जो आपको खुद नहीं पता था, आपकी प्रतिभा भाई ने ही पहचानी थी,**  
> **उम्मीद रहेगी आप भाई को, भाई आपको खुश रखे,**  
> **वर्ना हम तो हैं ही… लाइन मारने के लिए !**
> 
> **~रबी**

  
\[ My Dear Sunny Bhabhi,  
Didn’t know even you also have a birthday,  
I thought you took avatar over earth,  
When people didn’t find God, they made you their own.

But remember, it was bro who understood your value first,  
When even you didn’t know, bro recognized your talent  
So, I hope bro and you will keep each other happy,  
Otherwise… we are always there to spew cheap pick up lines on you! \]

  
_## Sunny Leone’s birthday! She has really become a source of joy and amusement for me and a lot of friends (and in a totally non-sexual way). ##_
