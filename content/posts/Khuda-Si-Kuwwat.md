---
public: true
title: ख़ुदा सी कुव्वत
date: 2012-08-15
tags:
  - life
  - god
  - anger
categories: hindi
---

> **सर झुकाने आया हूँ, सर झुका के जाऊँगा,  
> लहू की प्यास हो ‘गर तुझे, हर कतरा चढ़ा के जाऊँगा,  
> बंदा हूँ मैं जात से, बंदगी फितरत मेरी,  
> है ख़ुदा सी कुव्वत तुझमें, तो आ रोक ले मुझे…**
> 
> **~रबी**


\[ I came here to bow my head, I will do that,  
If you are thirsty, I will give every drop of my blood,  
I am a disciple from caste, bowing to you is my habit,  
If you really have God like ability, come stop me… \]
