---
public: true
title: She
date: 2014-12-10
tags:
  - special
  - dedication
categories: english
---

![ ](../images/she.png)

> **she calls herself earth, fire & water,**  
> **and i silently laugh at her innocent stupidity.**  
> **she doesn’t understand that she isn’t limited to just these elemental entities,**  
>            **she goes beyond that.**  
> **she is a universe in herself.**  
> **a universe that’s constantly expanding through the cosmos,**  
> **encompassing the dimensions way outside the capabilities of human imagination.**  
> **if i were to explore her, i would need a billion lives just to unwrap a tiny portion of her intricacies.**
>
> **she isn’t aware of her own potential.**
>
> **but that’s alright,**  
> **she has time.**  
> **she will find out**  
> **the infinite possibilities that she is.**  
>                 **one day she will.**
>
> **~RavS**
