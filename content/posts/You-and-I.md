---
public: true
title: You and I
date: 2015-01-03
tags:
  - free-verse
  - crossroads
categories: english
---

![ ](../images/you-and-i.png)

> **You and I.**  
> **You and I.**  
> **We.**  
> **Are meant to be so much more.**  
> **Look around.**  
> **We are in middle of the cross section of an infinite roads.**  
> **Forget about where you came from.**  
> **Forget about where I met you.**  
> **Hold my hands.**  
> **And point your fingers at any road you like.**  
> **Let’s walk.**  
> **And find ourselves on that path.**
> 
> **~RavS**
