---
public: true
title: हसरत
date: 2012-05-10
tags:
  - sufi
  - love
categories: hindi
---

> **हसरत  है… चाहूँ  मैं  तुझे  इतना  कि …**  
> **मुझे  तेरी  चाहत  की  कभी  ज़रूरत ही  ना  पड़े।**
>
> **~रबी**


\[ I wish… that I love you so much that…  
I never need you to love me back. \]
