---
public: true
title: इन्द्र-धनुष
date: 2014-07-10
tags:
  - 30-day-challenge
categories: hindi
---

> **जब जीवन में केवल दुःख की ही बरसात हो,**  
> **ना रवि का ही कहीं निकट होने का आभास हो।**  
> **निश्चय ही बहुत कठिन है, किन्तु हो सके तो,**  
> **मन मस्तिक में बात मेरी ये बाँध लो।**  
> **सुख का इन्द्र-धनुष समीप ही है ,**  
> **बस रुकना नहीं तुम, ऐ मित्र तुम बढते चलो।**
> 
> **~रबी**

  
\[ When there is just rain of sorrow in life everywhere,  
Neither the sun is perceived to be somewhere near.  
Indeed it is very difficult, but if possible,  
Try and contemplate this in your mind,  
The rainbow of relief is somewhere nearby,  
But don’t you dare stop, my friend, just keep walking. \]
