---
public: true
title: "गुन्छा कोई मेरे नाम कर दिया"
date: 2014-06-10
tags:
  - first
  - special
  - song
  - reprise
categories: hindi
---

> **गुन्छा कोई मेरे नाम कर दिया,**  
> **साकी ने फिर से मेरा जाम भर दिया…**
> 
> **लेते रहे नाम उनका रह रह कर,**  
> **और वो हैं की हमको ही बदनाम कर दिया,**  
> **गुन्छा कोई मेरे नाम कर दिया…**
> 
> **महफिलें जो कहती थी मासूम हैं बहुत,**  
> **एक हंसी भर से वहां क़त्ल-ए-आम कर दिया,**  
> **साकी ने फिर से मेरा जाम भर दिया…**
> 
> **ऐसे जले विरहा में उनके लिए,**  
> **उस शमा ने हमको परवान कर दिया,**  
> **गुन्छा कोई मेरे नाम कर दिया…**
> 
> **गुन्छा कोई मेरे नाम कर दिया,**  
> **साकी ने फिर से मेरा जाम भर दिया…**
> 
> **~रबी**


\[ A bunch of flowers were given on my name,  
The tender filled by glass again…

I kept taking her name again and again,  
And she defamed me in front of every one,  
A bunch of flowers were given on my name,

The gatherings which used to say she is very innocent,  
She murdered everyone with her single smile,  
The tender filled by glass again…

I burned in separation in such a way,  
I became a moth getting burned in flame,  
A bunch of flowers were given on my name,

  
A bunch of flowers were given on my name,  
The tender filled by glass again…\]

  
_## Whenever I REALLY like a song, either I want to make a spoof out of it or write a reprise version. This one is my first ‘official’ reprise version of a song. “Guncha koi” is a small Ghazal from the movie “Main, Meri Patni aur Woh”. Mine is a humble attempt to write a reprise version of it, keeping the tone and theme the same. The English translation is very crude since the song itself is a little complex in words. ##_
