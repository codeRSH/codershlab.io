---
public: true
title: मनाओ दिवाली ऐसी
date: 2012-11-13
tags:
  - diwali
  - special
  - faaltu
categories: hindi
---

> **खाओ इतनी मिठाई,  
> कि तुम्हें मधुमेह हो जाए।    
> घर जाओ दूसरों के इतने,  
> कि लोग भगाने लग जाएँ।    
> चलाओ इतने पटाख़े,  
> कि सारा मोहल्ला जल जाए।    
> इस बार भैंस-की मनाओ दिवाली ऐसी,  
> कि तुम्हारा दिवाला निकल जाए !**
> 
> **~रबी**

  

\[ Eat so many sweets,  
That you start suffering from diabetes.  
Go to others’ homes so many times,  
That they start kicking you out.  
Burst so many crackers,  
That whole neighborhood gets burned down.  
This time, swear-to-Buffalo, celebrate Diwali in such a way,  
That you become bankrupt in a day! \]
