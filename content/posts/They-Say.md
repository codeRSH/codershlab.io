---
public: true
title: They Say
date: 2013-04-10
tags:
  - life
  - philosophical
categories: english
---

> **They say there is light at the end of the tunnel.**   
> **But how do I ascertain my tunnel is not circular?**   
>   
> **They say the dawn is at the horizon,**   
> **When the night starts getting darker,**   
> **But what if the dawn has already come for others,**   
> **Only I cannot see it any further?**   
>   
> **They say to hold on, because it eventually gets better,**   
> **But what if I am doomed to go through this forever?**   
> **What if I am not the destiny’s favorite child after all,**  
> **What if I am destined to die a dreamer?**
>
> **~RavS** 
