---
public: true
title: ज़मीन-ओ-आसमां
date: 2013-03-08
tags:
  - life
  - tragedy
categories: hindi
---

> **जिसे चाहो वो नहीं मिलता,  
> जब चाहो तो नहीं मिलता,  
> मैं चाहूँ जो, नहीं मिलता,  
> और जो चाहूँ सो नहीं मिलता।**  
>   
> **कहा था तुमने,  
> किसी को भी ज़मीन-ओ-आसमां नहीं मिलता,  
> मुझे तो कहीं भी दोनों का ही निशाँ नहीं मिलता।**
> 
> **~रबी**


\[ Whom you want, you never get,  
When you want, you never get,  
If I want, I will never get,  
And the one that I want, I will never get.  
  
You had said,  
Nobody gets everything,  
But nowhere do I see anything, for me. \]  
