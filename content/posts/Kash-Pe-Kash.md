---
public: true
title: कश पे कश
date: 2012-12-08
tags:
  - smoker
  - pain
categories: hindi
---

> **कश पे कश मैं लगाता गया…  
> हर तकलीफ़, हर दर्द, धुंए में उड़ाता गया।    
> “ये बस आख़िरी है”,  कह -कहकर ख़ुद को समझाता गया,  
> कुछ इस तरह एक और, फिर एक और मैं सुलगाता गया…**
> 
> **धीरे धीरे अपनों को अपने से दूर मैं भगाता गया,  
> एक तड़पाती मौत को करीब, और करीब, मैं बुलाता गया।    
> पर एक बार मरना मंज़ूर, हर रोज़ मरना गंवारा ना गया,  
> इसीलिए कश पे कश, कश पे कश मैं लगाता गया…**
> 
> **~रबी**  

\[ I kept on smoking one, then another one…  
Every problem, every pain, I smoked them out from inside,  
‘I swear, this one is my last’, I kept on saying to myself,  
And this way, kept on smoking one, then another one…  
  
Slowly and slowly, I kept on distancing my loved ones,  
And kept on asking the death to come near once.  
But I was ready to die for once and all, I could not bear to die little by little everyday,  
And that is why, I kept on smoking, then smoking another one. \]
