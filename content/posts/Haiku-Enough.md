---
public: true
title: Haiku - Enough
date: 2016-04-06
tags:
  - haiku
  - love
categories: english
---

![ ](../images/haiku-enough.jpg)

> **Don’t need to show    
> anything to anyone.  
> Enough we are among ourselves.**
>
> **~RavS**


_##  Of course, people are going to judge us.  Some of them might want us to fail. They will ask questions, they will make sarcastic comments. But we don’t need to prove anything to anyone. We don’t need to show them anything. You are me, and I am you. That is enough. ##_
