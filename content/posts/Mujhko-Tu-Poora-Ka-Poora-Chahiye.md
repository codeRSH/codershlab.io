---
public: true
title: मुझे तू पूरा का पूरा चाहिए
date: 2014-08-18
tags:
  - song
categories: hindi
---

> **क्या तूने कहा, किसे खबर किसको पता ,**  
> **मैं तो तेरी आवाज ही सुनता रह गया …**
> 
> **पुतला था, जाने कहाँ से आया तू, जान मुझमे फूंक दी,**  
> **तूने क्या असर किया, मचल पड़ी सांस भी उखड़ी उखड़ी,**  
> **धीरे से क्या ऐसा कहा, न गलत पता न कुछ सही,**  
> **तुझे देखने को एक नज़र, रात ने भी सुबह कर दी,**  
> **बाँट ले दुनिया नफरत तेरी चाहे जितनी भी,**  
> **पर प्यार तो मुझे तेरा सारा चाहिए ,**  
> **न आधा चाहिए न अधूरा चाहिए,**  
> **मुझे तू पूरा का पूरा चाहिए।**  
> **चाहिए… मुझे तू पूरा का पूरा चाहिए।**
> 
> **गीत अधूरा अधूरा सा था, तेरा साज मिला तो पूरा हो गया ,**  
> **अब क्या ख़ुशी क्या गम मेरा, हिस्सा सब में तेरा हो गया,**  
> **तू जेहन में क्या बसा, आहिस्ता आहिस्ता मैं तेरा हो गया ,**  
> **फिर कैसे होने दूँ किसी और का, जो तू भी मेरा हो गया,**  
> **क्या करूंगा जग जो संग चलता भी रहा,**  
> **मुझे तो बस एक तेरे होने का सहारा चाहिए,**  
> **मुझे तू पूरा का पूरा चाहिए।**  
> **चाहिए… मुझे तू पूरा का पूरा चाहिए।**
> 
> **~रबी**

  
\[ What you said, Who knows who could tell,  
I was just listening to your voice …

I was a statue, I don’t know from you came, you breathed life into me,  
What magic did you do over me, even the shallow breaths have become full,  
What did you say so softly, I don’t know what’s wrong and right anymore,  
To have a glance of you, the night also made it a morning,  
I don’t care if the world shares all your hatred,  
But I want all of your love,  
Neither half, nor incomplete,  
I want the whole of you.  
Yes… I want the whole of you.

The song was unfinished, when you gave your melody it became complete,  
Now what’s my happiness and what’s my sorrow, in everything you have taken your share,  
Since the moment you came in my heart, I have gradually become thine,  
Then how should I let you become someone else’s, when you also have become mine,  
What will I do if the world moves with me,  
I just want the belief that you are with me,  
I want the whole of you.  
Yes… I want the whole of you. \]
