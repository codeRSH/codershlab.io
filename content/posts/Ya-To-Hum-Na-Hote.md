---
public: true
title: या तो हम ना होते
date: 2020-08-28
tags:
  - pain
  - frustration
  - family
categories: hindi
---

> **या तो हम ना होते, या तो वो ना होते।  
> या होते भी अगर, तो ये हालात ना होते।  
> मैं थक गया हूँ लड़ लड़कर,  
> काश… हम अपनों से ही बर्बाद ना होते।**  
>   
> **जो  बोला है करो,  
> जो होता है सहो,  
> चुप रहो !  
> दो पल सुकून चाहना गुनाह है यहाँ,  
> काश… हम पर ऐसे इल्ज़ामात ना होते।**  
>   
> **घुटते रहने दो इसे, जो भी है अंदर,  
> चाहे कितना ही गहरा हो पीठ का खंजर।  
> हम मर भी जाएँ तो किसे क्या ही फ़िक्र है,  
> काश … रिश्ते इस तरह ख़ैरात ना होते।**  
>   
> **~रबी**


\[ Either we wouldn’t have been alive, or they wouldn’t have been alive.  
Or even if we were both there, these circumstances wouldn’t have happened.  
I’m tired of fighting,  
I wish … we wouldn’t have been ruined by our own people.  
  
Do what is told,  
Whatever happens, bear it,  
Keep quiet !  
It is a crime to wish a few moments of peace here,  
I wish … we would not have been accused like this.  
  
Let it ruin you, whatever is inside,  
No matter how deep is the dagger in the back.  
Even if I were to die, who would care about it,  
I wish … relationships would not have become alms like this.  \]  


_## Written at one of the low moments of life. ##_
