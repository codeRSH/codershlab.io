---
public: true
title: Haiku4U - 18
date: 2019-04-18
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-18.jpg)

> **A lot to change in us.  
> But  
> Lot more needs unchanged.**
>
> **~RavS**


_## If there is anything that I have realized over the years, it’s that we will have to work doubly hard if we want to live a happy life together. A lot of our habits are totally opposite of each other. If we don’t change a little for each other, we would not be able to sustain._

_But at the very elementary level. I love both of us. And I would never want our basic nature to ever change.  ##_
