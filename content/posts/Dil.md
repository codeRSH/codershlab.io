---
public: true
title: दिल
date: 2012-07-27
tags:
  - pain
categories: hindi
---

> **दिल लगा के हमने देखा,  
> दिल पर इस कदर लगी,  
> अब दिल लगे ना लगे,  
> फिर कभी दिल्लगी नहीं।**  
> 
> **~रबी**
  

\[ I tried giving my heart,  
So much it got hurt,  
Howsoever path3tic my life remains,  
I will never try it again. \]
