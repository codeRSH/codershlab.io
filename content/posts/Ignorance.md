---
public: true
title: Ignorance
date: 2014-04-19
tags:
  - light
  - friendship
categories: english
---

> **Fear not my friend,**  
> **Let the path be scary and dark,**  
> **You shine light on my ignorance,**  
> **Let me lighten yours.**
>
> **~RavS**


_## Quite old. Forgot to post it earlier :) ##_
