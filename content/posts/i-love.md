---
public: true
title: I Love
date: 2025-01-07
tags:
  - love
  - romantic
categories: english
---

> **I love her white pearls,  
> I love the long locks of that girl,  
> I  love her potential double chin,  
> I love the touch of  her soft skin,  
> I love her warm soothing laughter,  
> That alone made me fall for her,  
> I love her nightly melodic whispers,  
> That makes me crave her even more.**  
> 
> **I love her maddening craziness,  
> I love all her hugs which I am yet to get,  
> I love it when she wants to be pampered like a baby,  
> I really love it when I am able to make her happy,  
> I love to write for her, I love to draw for her,  
> I love to wait for her, I love to stare at her,  
> And one day when she would become my teacher,  
> I would love to cook for her, and feed her with my fingers.**  
> 
> **And you know what?  
> This is just the start.  
> Dozens of things about her, I haven't even touched,  
> And there are  a million more things left,  
> For her to show me,  
> and for me to fall in love.**  
> 
> **~RavS** 


_## Starting 2025 with a series of personal poems which were written for 'private read' but declassifying them as quite some time has passed now. This one is a a sweet little poem about the Girl I Loved.. I mean Love ;) ##_
