---
public: true
title: मेहरम
date: 2019-10-16
tags:
  - love
  - belonging
categories: hindi
---

![ ](../images/mehram.png)

> **खुदको  मेहरम  कहने  दे,  
> तेरा  साया  बनके  रहने  दे।    
> जो  आग  लगी  है  कलेजे  में,  
> इसे  सींच  ना, थोड़ा  जलने  दे।**  
>   
> **कुछ  लोग  नसीहत  देते  हैं,  
> ना काबिल  तेरे  लिए,  कहते  हैं।  
> क्यों  रोके  हैं  उनका  मुँह  फिर  तू,    
> आज  इस  बात  का  इल्म  भी  करने  दे।**  
>   
> **जिसे  मेरे  होने  का  अफ़सोस  ना  हो,  
> ना  जाने  कब  मिलेगा  शख्स  वो।  
> उधारी  सही, तू  फकत  सुकून  तो  दे,  
> यहाँ  बस नोचने  वाले  रेह्न्दे  ने।**    
>   
> **सही  है, या  न  जाने  गलत  है  तू,  
> अच्छी  या  बुरी, मेरी  आदत  है तू।  
> रखले  गिरवी  मेरी  खुशियां  मेहरम,  
> बदले  में  गम  तेरे  सहने  दे।**  
>   
> **खुदको  मेहरम  कहने  दे,  
> तेरा  साया  बनके  रहने  दे।**   
>   
> **~रबी**  
>   
> **\*मेहरम = बहुत करीबी**
  
  
\[ Let me call myself your own,  
Let me remain your shadow forever.  
The fire in my mind and heart,  
Don’t water it, let it burn a little.  
  
Some people advise me,  
I don’t deserve you, they tell me,  
Why do you try to shut their mouth then,  
Let me also believe this truth.  
  
The one who doesn’t feel sorry for me,  
I don’t know when I will feel that person.  
Let it be temporary at least give me some peace,  
Here only people who tear apart are found.    
  
I don’t know if you are right or wrong,  
Good or bad, you are my habit now.  
Keep my happiness with you, O loved one,  
Let me bear your sadness in return.  
  
Let me call myself your own,  
Let me remain your shadow forever. \]  


_## When you long for someone to call your own. When you want to belong. ##_
