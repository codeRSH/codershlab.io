---
public: true
title: मैं फिर भी ठीक हूँ
date: 2014-10-04
tags:
  - life
  - philosophical
  - special
categories: hindi
---

> **ज़िन्दगी ऐसा लगता है ठहर सी गयी गई है,**  
> **मैं आगे निकल गया वो पीछे कहीं रह गयी है ,**  
> **बहुत कोशिश की, कि ज़िन्दगी से मुलाकात तो हो,**  
> **पर ज़िन्दगी तो लगता है जैसे मर सी गयी है।**  
> **इश्तेहार दिया, ज़िन्दगी जहां भी हो लौट आओ,**  
> **पर ज़िन्दगी है की ना ज़िंदा ना मुर्दा मिलती है।**  
> **पर मैं फिर भी ठीक हूँ।**
> 
> **ऐसा नहीं है की मैं हँसता नहीं हूँ,**  
> **ऐसा नहीं है की मैं खुश रहता नहीं हूँ,**  
> **रोज सुबह ज़िंदा उठता तो मैं अब भी हूँ,**  
> **रोज अपने आप से ही हार जाना दुखता तो है,**  
> **फिर भी हर शाम खुद से लड़ता तो मैं अब भी हूँ,**  
> **खुद से नाराज, रब से नाराज, सब से नाराज हूँ।**  
> **पर मैं फिर भी ठीक हूँ।**
> 
> **अब सबसे नाराज़ रहना किसे अच्छा लगता है,**  
> **अपने हाल को बेहाल कहना किसे अच्छा लगता है,**  
> **शरम तो आती है अपनी बदनसीबी को सरे-आम करने में,**  
> **जब आंसूं सूख जाएँ तो सोचो कैसा लगता है।**  
> **अब हर वक्त झूठ भी तो नहीं बोला जाता,**  
> **इसलिए कभी कभी सच भी कह देता हूँ।**  
> **पर मैं फिर भी ठीक हूँ।**
> 
> **मुझे फ़िक्र हैं, तो बस तेरी हैं,**  
> **तूने दुनिया अभी नहीं देखी हैं,**  
> **तू नहीं जानती कैसे अक्सर लोग घाव कुरेदते हैं,**  
> **तू नहीं जानती अक्सर लोग जीते जी मार देते हैं ,**  
> **गफलत का धुंआ तो तेरी ओर भी सुलगता होगा,**  
> **तुझे थोड़ी ख़ुशी मिले, बस यही दुआ करता हूँ,**  
> **मैं तो फिर भी ठीक हूँ।**
> 
> **~रबी**


\[ Life seems to have come to a stand still,  
I moved forward, and it got left behind,  
I tried so much to meet life some day,  
But life seems to have become dead.  
I gave adverts, life please come back wherever you are,  
But I still can’t find life either dead nor alive.  
But I’m still fine.

It’s not like I don’t laugh,  
It’s not like I’m not happy,  
Every morning I still get up alive,  
It hurts to lose from one’s own self,  
Yet I still fight myself every evening,  
Angry with myself, angry with God, angry with all.  
But I’m still fine.

Now who likes to be angry with everyone,  
Who likes describe his sufferings,  
Yes I feel ashamed to announce my misfortunes,  
Imagine how it feels when tears dry up.  
It’s not possible to lie all the time,  
So sometimes I tell the truth.  
But I’m still fine.

I only fear about you,  
You haven’t seen the world yet,  
You do not know how people like to rip the wounds,  
You do not know how often people kill alive,  
The smoke of negligence would be burning on your side as well,  
You get a little happiness, that’s all pray,  
Don’t worry, I’m fine. \]
