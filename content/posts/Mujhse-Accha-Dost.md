---
public: true
title: मुझसे अच्छा दोस्त
date: 2012-12-14
tags:
  - friendship
  - friends
categories: hindi
---

> **दोस्ती फ़क़त इस हद तक निभानी है मुझे,  
> मुझसे अच्छा दोस्त, तू किसी और को साबित ना कर सके।    
> और अगर कर भी जाए तू ये गुस्ताखी,  
> तेरे ‘सच’ पर किसी को यकीं  **ना** हो सके।**
> 
> **~रबी**


\[ I want to take my friendship only till that limit,  
A friend better than me, you won’t be able to prove.  
And if by chance, you try and do this sin,  
Nobody shall believe in your 'truth’. \]
