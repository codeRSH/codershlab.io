---
public: true
title: इश्क़
date: 2014-05-18
tags:
  - love
categories: hindi
---

> **इश्क़ में अक्सर ऐसा होता है,**  
> **कई राधा मीरा हो जाती हैं।**
> 
> **~रबी**


\[ It usually happens in love,  
A lot of Radhas (lovers) become Meeras (devotees). \]

_## Some say they have heard this line somewhere. If that’s true please do tell and I will remove this one from the blog. I believe in ‘stealing’ ideas but not plagiarism. ##_
