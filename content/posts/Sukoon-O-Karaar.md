---
public: true
title: सुकून-ओ-करार
date: 2014-04-01
tags:
  - romantic
  - collaboration
  - special
categories: hindi
---

> **है मिसरी सा, कानो में घुलता एक-एक लफ्ज़ तेरे प्यार का,**  
> **लगे चौदवी का चाँद भी बेनूर, जो हो दीदार मेरे यार का,**  
> **खनक है चूड़ी जैसी, उसकी कांच की बातों में,**  
> **कभी गुजरे हो वो जिस ओर से, हम बैठे रहते हैं उन्ही राहों पे,**  
> **जिसके ख़त्म हो जाने से आ जाए यकीन सुकून-ओ-करार का,**  
> **मुझे रहता है इंतज़ार उसके ऐसे किसी इंतज़ार का।**
> 
> **~रबी**


\[ Like sugar, her words melt in my ears,  
Even that glorious moon fades, when my love shows her face,  
When she talks, it sounds like the tinkling of her bangles,  
The way she took once, I keep sitting on that path waiting for her,  
At the end of which I get the belief of relief and peace,  
I wait to get such an opportunity to wait for her. \]

  
_## This marks the 150th post on this blog! And fittingly I have tried something new in this one. For the first time, I have attempted to completed someone else’s lines. Thanks Akshita! ##_
