---
public: true
title: Musings in the ICU
date: 2013-08-26
tags:
  - life
  - death
categories: english
---

> **I can stay another day,  
> But what’s the point.  
> I’ll have to fight yet again,  
> Just to keep myself alive.**  
>   
> **I am just so tired,  
> From this battle to survive,  
> Make it easy for me God,  
> Either let me, or, take my life.**  
>   
> **Being a hostage on bed,  
> For countless endless days,  
> Don’t I deserve a little rest,  
> A little resting in peace?**  
>   
> **All around, I see a sea of faces,  
> All sympathizing at my state.  
> It makes me want to die even more,  
> Never wanted anyone pitying my fate.**  
>   
> **Doctor can’t tell if I have years,  
> Or months or weeks or days.  
> They continue to tear me apart, piece by piece,  
> All they do and can do is to sedate  
> Me, to buy time to know for sure,  
> The date to let them finally cremate.**  
>   
> **Mom is desperate, brother’s in pain,  
> The blood dripping through my veins.  
> All these alien things attached to my body,  
> All I can do is stare at the ceiling.**  
>   
> **I don’t know what’s right and wrong anymore,  
> I don’t know what will happen, or will not,  
> If I am on this Earth no-more,  
> You know, I just want the ordeal to stop.**
> 
> **~RavS**


_## Chronicling the plight of a colleague in ICU. ##_
