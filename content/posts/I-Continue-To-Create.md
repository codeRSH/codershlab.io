---
public: true
title: I continue to create
date: 2014-12-06
tags:
  - free-verse
  - writing
  - purpose
categories: english
---

![ ](../images/i-continue-to-create.png)

> **I look at all these words written by masters,**  
> **And they depress me more than they inspire.**  
> **They fill me with self doubts.**  
> **Would I ever be able to write something even half as good as this?**  
> **Would someone ever love me even half as much as them?**
>
> **And what’s the point of writing?**  
> **Everything that was ever to be said has already been said.**  
> **Everything I ever write from today onwards would be a form of plagiarism.**  
> **No idea is original. Not a single one.**  
> **So why even try?**  
> **Why keep excruciating yourself only to feel like a fraud in the end.**
>
> **But then, isn’t it egoistic to think only about yourself?**  
> **I write not just for myself but for others’ sake too.**  
> **Yes, everything to be said, has been said, but not in my style.**  
> **Yes, I might never be loved, but at least I am unquestionably liked.**  
> **That means Something.**
>
> **So I continue to create.**  
> **Until I won’t be able to.**  
> **So should you.**
> 
> **~RavS**
