---
public: true
title: Empathy
date: 2018-06-12
tags:
  - love
  - pain
  - longing
categories: english
---

> **I wish you suffer a lot.  
> So you know what I feel,  
> So you don’t pity me,  
> So you love me in that pain.**
>
> **Call me selfish, but,  
> I don’t need your sympathy,  
> I want your empathy.  
> If you can.  
> Please.**
>
> **~RavS**

_## Slight, but in a way enormous difference between sympathy and empathy. ##_
