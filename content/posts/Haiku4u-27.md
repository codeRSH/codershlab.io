---
public: true
title: Haiku4U - 27
date: 2019-04-27
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-27.jpg)

> **In the deepest of dark,  
> I’ll find the path  
> that leads to you.**
>
> **~RavS**


_## I sometimes feel lost in trying to understand you. The path isn’t clear, through which to reach your heart and to your soul. But I am always determined to reach and connect with you, no matter what. After all, it’s important, for both of us. ##_
