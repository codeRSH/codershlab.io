---
public: true
title: जल्लाद
date: 2013-02-23
tags:
  - terrorism
categories: hindi
---

> **वो मारते हैं मासूमों को,  
> और फिर कहते हैं ये हमारा जिहाद है,  
> वो अपनों का ही खून कर खुश होते हैं,  
> ये किस तरह की जीत, ये कैसा फसाद है?**  
>   
> **मैं नहीं मानता उस अल्लाह, उस भगवान् को,  
> जिसे अपने ही बन्दों के लहू की प्यास है,  
> परवाह नहीं, फिर अगर कहो तुम काफिर मुझको,  
> इंसान हो इंसानों को उधेने वाला, नहीं बनाना जल्लाद है।**
> 
> **~रबी**


\[ They kill the innocents,  
And then say, it’s our holy war,  
They get happy murdering their own,  
What’s this fight, what kind of win?  
  
I do not believe in such kind of Gods,  
Who want the blood of their own desciples,  
And I don’t care, if you call me an ‘atheist’,  
I don’t want to become a grave digger for others. \]
