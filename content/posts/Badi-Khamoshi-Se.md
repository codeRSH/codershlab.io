---
public: true
title: बड़ी  ख़ामोशी  से
date: 2017-02-17
tags:
  - love
  - special
categories: hindi
---

> **बड़ी  ख़ामोशी  से  मैं  तुझे  प्यार  करता  था  ,  
> ना  धड़कनें  जानती  थीं, ना दिल  को  पता  था।  
> हाँ इतनी  ख़ामोशी  से  मैं  तुझे  प्यार  करता  था  ,  
> ना  धड़कनें  जानती  थीं, ना दिल  को  पता  था।**
>
> **पता है, जब  मैं  और  कलम  अकेले  होते  थे  ,  
> अक्सर  तेरे  बातें  ही  किया  करते  थे।  
> तुझे  याद  करते  कलेजा  भर  आता  था  ,  
> जो  मैं  कह   नहीं  पाता  था  पन्नो  पे  ,  
> वो  पगला  यूँ  ही  गुदगुदा  जाता  था।  
> कलम  की  सुगबुगाहट  पन्ने  बता  ना  दें  कहीं,  
> कुछ  ख्याल  इसीलिए  दबा  के  रखता  था।  
> ना  धड़कनें  जानती  थीं, ना दिल  को  पता  था।**
>
> **जो  मेरा  नहीं  उसे  पाने  की  कोशिश  भी  आखिर  क्यों  हो ?  
> बिन  कोशिश  पर  गैर  मान  लेना  भी  सही  क्यों  हो ?  
> हूक  उठती  थी  रुक-रुक  के  अंदर  कभी-कभी,  
> बता  दूँ  तुझे, और  जान  ले  ये  दुनिया  सारी  सभी।  
> नहीं  आज  नहीं  कल, मैं  किया  करता  था,  
> नज़र   न  लग  जाए  कहीं, बहुत  डरा  करता  था।  
> बड़ी  ख़ामोशी  से  इसलिए  मैं तुझे  प्यार  करता  था।**
>
> **जो  जाऊँ  दूर  तो  खुद  से  नफरत  होती  थी,  
> जो  आऊँ  पास  तो  तुझ  से  हसरत  होती  थी।    
> जो  नहीं  है  उसे  पाने  की  मुख़्तसर  कोशिश  में,  
> जो  है  पास, बिछड़  जाने  की  जप्त  हर  वक्त  होती  थी।    
> खुद  से  भी  छुपके  इसलिए  मैं  तुझे  प्यार  करता  था,  
> ना  धड़कनें  जानती  थीं, ना दिल  को  पता  था।**
>
> **~रबी**


\[ I used to love you with such great silence,  
Neither pulse knew, nor heart came to know.  
Yes, I used to love you with such great silence,  
Neither pulse knew, nor heart came to know.

You know, when I used to be alone with my pen,  
Often we used to talk about you.  
We used to get emotional remembering you,  
Things that I could never say on the pages,  
That mad pen used to write them so easily.  
Lest the pages make the murmurs of the pen, public,  
I used to suppress some of my thoughts.  
Neither pulse knew, nor heart came to know them.

The one who isn’t mine, why should I even try to get them?  
But why should I assume they can’t be mine, without even trying?  
I used to get anxious sometimes,  
That I should tell you and let the world come to know too.  
Not today, I promise tomorrow; I used to tell myself,  
Let bad luck not come near us, I used to be so afraid.  
That’s why I used to love you with such great silence.

If I go away, I used to hate myself,  
If I come close, I used to desire from you.  
While trying to get what’s not mine yet,  
I used to be fearful of losing what’s with me already.  
That’s why I used to love you even hiding it from myself,  
Neither pulse knew, nor heart came to know. \]
