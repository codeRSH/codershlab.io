---
public: true
title: Not looking to impress...
date: 2014-11-29
tags:
  - prose
  - connection
categories: english
---

![ ](../images/not-looking-to-impress.png)

> **I am not looking to impress you,**  
> **I have already tried that so many times,**  
> **with so many people,**  
> **and failed so utterly miserably.**  
> **What I strive for instead and will keep striving,**  
> **is to hit a string in your heart, to say something, to create something which when you notice you unequivocally gasp and mumble,**  
> **“yeah that’s right, that’s right. That’s what I feel deep inside.”**  
>   
> **And if that day is not today, probably it will come tomorrow or 20 years after. I can’t predict, but rest assured until that day comes, I will keep trying everyday I wake up, again and again and again. That’s the only thing I can do. Isn’t it?**
>
> **~RavS**
