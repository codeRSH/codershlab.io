---
public: true
title: Don't Backstab
date: 2018-03-12
tags:
  - backstab
  - hurt
  - heartbreak
  - depression
categories: english
---

![ ](../images/dont-backstab.png)

> **I wish today I close my eyes,  
> Not to open it ever again.  
> Let this be my last good night.  
> Lest my dreams would go in vain.**
>
> **Don’t wake me up brother,  
> Let me sleep long enough.  
> I swear, it’s nothing I’m just tired.  
> I just had a day, that’s been rough.**
>
> **I started in search of love,  
> All I got was terms & conditions.  
> Now I am just bitter and cold,  
> To even be myself, I need permission.**
>
> **I don’t want love anymore,  
> I just want to rest in peace now.  
> I know you can’t bring me that either,  
> So try not to disturb me somehow.**
>
> **The games that you play,  
> I don’t know what to say,  
> Don’t backstab me, I deserve better,  
> Your loss ain’t my win,  
> I’ve always thought of greater.**
>
> **~RavS**


_## Wrote this on an extremely depressing night. When you are emotional, you get extra creative.  ##_
