---
public: true
title: कश्ती
date: 2014-06-30
tags:
  - 30-day-challenge
categories: hindi
---

> **बारिशें हों तुम पर कभी जो इनायतों की,**  
> **कुछ दुआएं रोक लेना मेरे लिए भी,**  
> **कश्ती बना कर तुम अपनी हथेलियों की।**
> 
> **~रबी**

  
\[ If ever the favors rain on you,  
Please save a few drops of prayers for me too,  
Making a boat of your palm.  \]
