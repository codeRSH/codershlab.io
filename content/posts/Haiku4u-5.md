---
public: true
title: Haiku4U - 5
date: 2019-04-05
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-5.jpg)

> **What would I be without you?  
> Lungs gasping for breaths,  
> Heart screaming for blood.**
>
> **~RavS**


_## Sometimes we fight and then we don’t talk. I know it’s hard for you. But it’s doubly hard for me too. I also feel, I am losing those days when we don’t love and care for each other._

_And then I shiver to think. What would happen, if you were snatched away from me. ##_
