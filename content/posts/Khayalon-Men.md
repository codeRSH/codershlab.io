---
public: true
title: ख़यालों में
date: 2012-04-19
tags:
  - romantic
categories: hindi
---

> **ना आया  करो  यूँ  ही  ख़यालों  में  कभी  भी ,**  
> **मुझे  अपनी  हर  मुस्कराहट  का  हिसाब  देना  पड़ता  है।** 
> 
> **~रबी** 

\[ Do not come in thoughts, at just any random time,  
I have to explain each smile of mine to others… \]
