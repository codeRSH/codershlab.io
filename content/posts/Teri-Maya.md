---
public: true
title: तेरी माया
date: 2012-03-29
tags:
  - hunger
  - irony
categories: hindi
---

![ ](../images/teri-maaya.jpg)

> **ये  तेरी  माया  है  खुदा , ये  तू  ही  है  जानता ,**  
> **हाथ  में  जिसके  निवाला  था, उसका  पेट  था  भरा ,**  
> **और  पेट  जिसका खाली  था, उसका  हाथ  भी  खाली  रह  गया।**  
> 
> **~रबी**


\[ It’s your guile God, only you know it,  
That the one whose hands were full had his stomach full,  
And the one whose stomach was empty remained empty handed. \]
