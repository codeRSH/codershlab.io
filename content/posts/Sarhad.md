---
public: true
title: सरहद
date: 2014-07-16
tags:
  - 30-day-challenge
categories: hindi
---

> **सरहदें तो बहुत हैं,**  
> **कुछ दिखती हैं, कुछ दिखती नहीं।**  
> **सरहदें तो कबसे हैं,**  
> **कुछ महसूस होती हैं, कुछ चुभती नहीं।**  
> **तुम अपनी कहो, किस सरहद को लांघ कर आये हो,**  
> **कोई अपना नहीं कहता मुझे तो,**  
> **मेरे लिए सरहदें मायने रखती नहीं।**
> 
> **~रबी** 

  
\[ There are always many boundaries,  
Some are visible, some invisible.  
There have always been many boundaries,  
Some feel, some don’t sting.  
You tell about yourself, which border did you cross to come here,  
Nobody calls me their own so,  
To me, boundaries don’t matter. \]
