---
public: true
title: Conditional Love
date: 2014-12-20
tags:
  - free-verse
  - love
categories: english
---

![ ](../images/conditional-love.png)

> **Unconditional love is like distilled water.**  
> **Perfect in concept but impractical in reality.**  
> **And what use is that love unless it is adultered by a pinch of possessiveness, jealousy, expectations. insecurity, dread, anger, cravings…**  
> 
> **Let’s leave unconditional love for the Gods and the Immortal Fables.**  
> **Let’s anticipate a little bit in love and let’s be disappointed when that doesn’t happen.**  
> **What’s wrong in conditional love?**  
> **What’s wrong?**
> 
> **~RavS**
