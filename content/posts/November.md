---
public: true
title: November
date: 2018-11-12
tags:
  - dedication
  - special
  - love
categories: english
---

> **When the dust would have settled and the sky lit blue,  
> When the memories have faded but not the vows we took,  
> When you would have left just now, and the time had stalled.  
> I will wait for you, for as long as it takes, in the November’s cold.**
>
> **~RavS**

_## Written for a friend on their anniversary :) ##._
