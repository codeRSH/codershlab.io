---
public: true
title: दग़ा
date: 2012-01-28
tags:
  - friendship
  - betrayal
categories: hindi
---

> **तुम्हें  तो  लगा  होगा , तुमने  'दोस्त’ पर  दया  की ,**  
> **मैं  कहता  हूँ, तुमने  मेरी  दोस्ती  से  ही  दग़ा की।**  
> **अरे  अगर  मारना  ही  था  तो  पूरा  ही  मार  देते ,**  
> **मुझे  अधमरा  कर  छोड़ने  की  जरूरत  क्या  थी ?**
> 
> **~रबी**


\[ You would have thought, you did a favor to a ‘friend’,  
I say, you betrayed my friendship,  
If you wanted to kill me, then you should have killed me fully,  
why did you leave me half dead? \]
