---
public: true
title: क्यों बैठी हो उदास
date: 2012-08-14
tags:
  - romantic
categories: hindi
---

> **क्यों बैठी हो उदास,  
> चलो तुम्हें मैं एक काम नया देता हूँ।**  
>   
> **तुम पहनो नए-नए लिबास,  
> मैं दाद नयी देता हूँ।**  
>   
> **तुम करो नयी शिकायतें,  
> मैं फ़साद नए लेता हूँ।**  
>   
> **तुम गुनगुनाओ नयी पंक्तियाँ,  
> मैं साज़ नए छेड़ता हूँ।**  
>   
> **तुम बनाओ ख़्वाबों के पुलिंदे नए,  
> मैं साथ उनपे चलता हूँ।**  
>   
> **ना बैठोगी फिर कभी यूँ उदास,  
> चलो एक काम तुम्हें ऐसा देता हूँ।**  
> 
> **~रबी**

  

\[ Why are you sitting alone,  
Come let me give you a little work.  
  
You wear new dresses,  
I will devise new praises.  
  
You make new complaints,  
I will start new brawls.  
  
You hum new rhymes,  
I will find new tunes.  
  
You construct new bridges of dreams,  
I will walk on them with you.  
  
You will never sit alone like this again,  
Come let me give you such a work. \]
