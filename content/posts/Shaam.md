---
public: true
title: शाम
date: 2019-11-17
tags:
  - love
  - life
  - waiting
categories: hindi
---

![ ](../images/shaam.png)

> **चलो  तुम्हे  एक  बात  बताता  हूँ  कल  की,  
> मेरे  सपने  में  थी, एक  शाम  अजीब  सी,  
> जहाँ  तुम्हे  सुकून  मिले  मेरी  ख़ामोशी  से,  
> और  तुम्हारी  साँसे  महके, मुझे  मदहोशी  दे।**  
>   
> **वहां  तू  राह  भी  थी, हमराही  भी,  
> कितना  भी  दूर  जाऊं, तेरे  करीब  रहता  था,  
> तू  ख़ुशी  भी  थी, नसीब  भी,  
> इसलिए  मैं  खुद  को  खुशनसीब  कहता  था।**    
>   
> **पर  गम  ना  कर  अगर  ये  सपना ही  था  सच  नहीं,  
> तू  दिन तो  जीले  शिद्दत  से, आएगी  ऐसी  शाम  भी,  
> हर  दिन  सपनो  सा  लगे  इस  बात  पर  चलो  ज़ोर  दें,  
> क्यों  हसरत  सजा  बैठे  रहें  एक  दिन  के  अंजाम  की।**    
>   
> **वैसे भी मेरा  प्यार  किसी  एक  दिन  का  मोहताजी  नहीं,  
> मैं  इबादत  तो  करता  हूँ, पर हूँ  नमाज़ी  नहीं,  
> तुम  ये  ना सोचो  की  चाँद  चला  गया  कल  का,  
> तुम  ये  सोचो  आज  सूरज  उगने  को  क्यों  राज़ी  नहीं।**  
>   
> **और  तेरे  सिवा  इस  फरेबी  दुनिया  में  किसके  पास  जाऊँ  मैं,  
> सब  तो  यहाँ  मतलबी  या  ग़लतफहमी  का  शिकार  हैं,  
> सपनो  सी  आके  मिल  जाओ  किसी  शाम  को, ऐ  ज़िन्दगी,  
> मुझे  एक   अरसे   से  तुम्हे  जीने  का  इंतज़ार  है।**  
>   
> **~रबी**  


\[ OK let me tell you something about yesterday,  
I saw a dream, and in that an odd evening,  
Where you could be comforted by my silence,  
And the fragrance of your breathe would make me glow.  
  
There you were the path, and also a co-traveler,  
Howsoever far I would go, I would find myself near to you.  
You were the happiness, and fortune too,  
That’s why I used to call myself lucky.  
  
But do not sigh, if this was just a dream, not realty,  
You live this day with your full heart, that evening will also come.  
Every day feels like dream, let’s talk about that possibility,  
Why do we keep thinking about just one day’s result?  
  
Anyway my love is not dependent on a single day,  
I do worship, but I am not a theist,  
Do not think that the yesterday’s moon has gone away,  
Think instead why today’s sun does not agree to rise.  
  
And whom should I go to in this mirage world except you,  
All here are the self-centered or victims of  misunderstanding,  
O life, just come and meet me on an evening like a dream.  
I have been waiting to live you for a long time. \]

  
_## About love, life and waiting. ##_
