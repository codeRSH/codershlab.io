---
public: true
title: "The Suicide Note of a Lonely Girl - Part 1"
date: 2014-07-26
tags:
  - special
  - story
categories: english
---

> **Today I take my life,**  
> **Hopeful, that the pain will now subside,**  
> **It’s just been lonely and dreadful,**  
> **May be I will finally get free tonight.**  
>   
> **I don’t know why I took so much time to do,**  
> **What I should have done so much earlier,**  
> **May be I was afraid to die,**  
> **It’s amusing. Living was so much scarier.**  
>   
> **Sorry mom, I just couldn’t do it,**  
> **No therapy would help me through it,**  
> **My - now burnt- diary, my tear soaked pillow,**  
> **Knew more about me, than you could ever know me,**  
> **I just wanted to feel some emotions mom,**  
> **All that you could provide me were some more sedatives.**  
>   
> **And Sorry your Attagirl is leaving Dad,**  
> **Shattering all the dreams that you ever had,**  
> **I know I was your most precious jewel,**  
> **You would do anything to bring me back,**  
> **But believe me, I am going to a better place Dad,**  
> **If nothing else, at least, I can assure you of that.**  
>   
> **There’s only one regret that I have,**  
> **Wish they didn’t think I was mean,**  
> **I wanted to befriend all of them,**  
> **But the introvert in me came in between.**  
> **So I would sit by the college park, all alone,**  
> **Sometimes crying, sometimes reading.**  
> **Looking at the heavenly sky sometimes,**  
> **Thinking why was I so strange-behaving.**  
>   
> **Nobody liked me, it was a nightmare,**  
> **This exile only got unforgiving glares,**  
> **There was only this guy sitting across,**  
> **Sometimes he would smile at me in class,**  
> **Sometimes he would utter a few words,**  
> **Mostly asking if I was doing alright,**  
> **May be he didn’t want me to feel bad,**  
> **I think he just pitied my sorrow state.**  
>   
> **I just wanted one single person by my side,**  
> **Who understood what I felt inside,**  
> **A friend who could listen to my silence,**  
> **Someone who could deal with my fits,**  
> **Yes, you both tried your best to comfort,**  
> **But you were just incapable of it.**  
>   
> **Please don’t cry, please don’t be sad,**  
> **Be happy, now I won’t have any pain to endure,**  
> **Please do take care of yourselves,**  
> **When I won’t be able to be there for,**  
> **And please forgive me this one last time,**  
> **I promise, I won’t make any mistakes, any more.**
>
> **~RavS**
