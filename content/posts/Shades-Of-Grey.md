---
public: true
title: Shades of Grey
date: 2013-03-20
tags:
  - life
  - people
categories: english
---

> **Do not tell me this is wrong and that is right,  
> That She’s inherently nice and He’s estray,  
> For none of us is purely white or entirely black inside,  
> We all are different hues, different shades of grey.**
>
> **~RavS**
