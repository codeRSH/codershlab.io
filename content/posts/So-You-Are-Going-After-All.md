---
public: true
title: So you are going after all?
date: 2014-10-09
tags:
  - dedication
  - farewell
categories: english
---

![ ](../images/so-you-are-going-after-all.jpg)

> **So you are going after all, right?**  
> **Fine, go away, it is your choice.**  
> **I can’t stop you from going… can I?**  
>   
> **But let this be known to you miss,**  
> **You are going to be missed.**  
> **And not just for a while or a little bit,**  
> **A Lot… A lot more than you can think.**  
> **I’ll miss the carefree laughter, the chunky eyes,**  
> **The incredible innumerous expressions on your face,**  
> **And that you actually believed this laughable BS,**  
> **When someone told you, you aren’t fair!**  
>   
> **I will keep looking back from my seat,**  
> **And I won’t find you there.**  
> **I will keep anticipating a giggle, or a pat,**  
> **But it will never come again, I fear.**  
> **No one is going to ping me now,**  
> **when I would be dead busy,**  
> **Asking… Ravoo what are you doing?**  
> **Come here, I am bored cutie,**  
> **Tell me a joke, entertain me please…**  
>   
> **You know I hated you initially,**  
> **I hated you with all my guts,**  
> **You were too loud for my taste,**  
> **And at that time I just needed silence.**  
> **And then you were gone,**  
> **I heard you were not well,**  
> **And then you came back, strong,**  
> **The ice between us still didn’t melt.**  
>   
> **Then one fine morning of 2012 December,**  
> **After 8 months of we meeting first,**  
> **You and I were stuck in the morning shift,**  
> **I was happy sitting alone,**  
> **you wanted someone to gossip.**  
> **So you initiated the talks,**  
> **I followed suit,**  
> **And then you kept talking,**  
> **I remained on mute.**  
>   
> **And then we talked every now and then,**  
> **I realized you ain’t too bad to be a friend,**  
> **You would always have stories to tell,**  
> **Some were painful, some hilarious,**  
> **But I always learned a little about ya,**  
> **That behind all the glare and chutzpah,**  
> **That behind all the mischief and flutter,**  
> **Is a little girl seeking love and comfort.**  
>   
> **You taught me how to handle the pain,**  
> **And how to seem normal when nothing’s going your way,**  
> **How to keep faith when the chips are down,**  
> **And how to laugh forgetting everything.**  
> **How do be cool just being yourself,**  
> **And how to eat an elephant while dieting.**  
> **How to be a little mad and not care,**  
> **And how to sell a phone in a selfie.**  
>   
> **Some days you would be loud and evil,**  
> **Some days you would be sober and angelic,**  
> **You would always keep me guessing,**  
> **If I should stay away today, or should I be talking.**  
> **I would fear to text you, I would never ever be sure,**  
> **If you’re still driving one handed or if you had safely reached home.**  
>   
> **Well you are gone, it’s a loss for me,**  
> **But I hope your are still in profit.**  
> **I hope, I wish, I pray to Almighty,**  
> **That your Furooh to-be gives you so much love,**  
> **That together we could ill-afford to give.**  
>   
> **Oh and don’t worry about me,**  
> **I am not going anywhere.**  
> **Granted I won’t be nearby now ,**  
> **Ravoo would still hang around somewhere. :)**
>
> **~RavS**


_## One more friend goes away. Screw this life. ##_
