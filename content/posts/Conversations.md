---
public: true
title: Conversations
date: 2014-12-14
tags:
  - free-verse
  - conversation
categories: english
---

![ ](../images/conversations.png)

> **Sometimes I go back and read our old conversations.**  
> **Not yesterday’s or last week’s,**  
> **But at least an year old.**  
> **And, I concentrate only on what you said.**  
> **Just your texts.**  
> **Your words. Your smileys.**  
> **From them, I instinctively know what I would have said.**  
> **I remember the exact words that made you laugh, frown, yawn or frightened.**  
> **It’s always, almost like a pattern.**  
> **As if that conversation was a game of ping pong,**  
> **and I was playing on both sides of the table.**
>
> **It’s amazing how much I learn about myself, through you.**
> 
> **~RavS**
