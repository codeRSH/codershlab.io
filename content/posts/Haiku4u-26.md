---
public: true
title: Haiku4U - 26
date: 2019-04-26
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-26.jpg)

> **I’ve caught fear.  
> Can you hold me  
> as I shiver it away?**
>
> **~RavS**


_## I don’t know about you, but I sometimes start feeling afraid. Afraid of what will happen to our relationship. Afraid something might happen to you. Afraid I might not live up to the expectations._

_It’s at those times I require you to hold my hand and reassure me that everything is gonna be fine. Even if it’s not true. Just lie. ##_
