---
public: true
title: Haiku4U - 13
date: 2019-04-13
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-13.jpg)

> **You’d sleep,  
> And I’d crawl,  
> Beside, behind, all over you.**
>
> **~RavS**


_## That’s The Dream actually. You would sleep blissfully and I would sneak into the bed with you. And then we would sleep cuddling each other. The ultimate ecstasy :) ##_
