---
public: true
title: बोल कौन रंग लगाऊं तुझे...
date: 2014-03-17
tags:
  - holi
categories: hindi
---

> **बोल कौन रंग लगाऊं तुझे…**
> 
> **है एक पीली पीली सुगबुगाहट का रंग, जो तेरे आने से दमक उठता है,**  
> **है एक गुलाबी ख्वाइशों का रंग, जो तुझे देख मचल उठता है,**  
> **है एक लाल रंग मेरे पास, पर वो तो तूने गालों पर कब से ओढ़ रखा है,**  
> **है एक गहरा नीला रंग, जो तेरी आँखों से ही निकला लगता है,**  
> **है एक हरा रंग, क्या कहूँ इसके बारे में, तेरी मेहँदी से शायद बहुत जलता है,**  
> **है तो रंग बहुत से और भी, कत्थई, नारंगी, फिरोज़ी, जामुनी…**  
> **तुझ पर न चढ़े तो फिर इन रंगो में क्या रखा है।**
> 
> **तो बोल कौन रंग लगाऊं तुझे…**
> 
> **~रबी**

  
\[ Tell me which color should I smear on you…

There’s yellowish color of buzz, that shines when you come,  
There’s a pinkish color of wishes, that yearns when it sees your face,  
There’s red color with me too, but your cheeks are already made of it,  
There’s deep blue color, which seems to have been taken from your eyes,  
There’s green color, what shall I say about it, it seems to be jealous of the henna on your hands,  
There are a lot of other colors as well, hazel, amber, azure,lavender…  
But if they don’t touch your skin, what’s their use.

So, tell me which color should I smear on you…\]

  
_## Surrounding yourself with creative people always inspires you to be creative (and makes it that much easier to steal ideas!). Had no plans to write this on occasion of Holi, but the below lines (in comments) sparked an idea to write something of my own. ##_
