---
public: true
title: दमक
date: 2012-02-09
tags:
  - romantic
categories: hindi
---

> **उनके चेहरे पर ही इतनी दमक रहती है,**  
> **हमें रात दीये की जरूरत नहीं पड़ती है,**  
> **उनकी तस्वीरों से ही हम अपना घर रोशन कर लेते हैं…**   
> 
> **~रबी**


\[ There is so much glow on her face,  
That I don’t need to a light a candle at night,  
Just using her pictures I can lighten up my home… \]
