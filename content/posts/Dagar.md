---
public: true
title: डगर
date: 2011-12-24
tags:
  - humor
  - romantic
categories: hindi
---

> **बेशक   लम्बी  है, और  मुश्किल  भी  ये  डगर ,**  
> **लेकिन  तुझसा  हमसफ़र  साथ  हो  अगर ,**  
> **तो  कम्बख़्त  पहुँचना  ही  कौन  चाहता  है  मंजिल  तक ,**  
> **हम  तो  जान  के  जायेंगे  रास्ता  भटक !!** 
> 
> **~रबी**


\[ No doubt, the way is long and difficult,  
But if I have a co-traveler like you,  
Then which idiot wants to reach the destination anyway,  
I would knowingly get lost in the middle!! \]
