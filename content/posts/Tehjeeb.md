---
public: true
title: तहज़ीब
date: 2014-06-23
tags:
  - 30-day-challenge
categories: hindi
---

> **गलतफ़हमियों में तुम भी जीते हो,**  
> **गलतफहमियों में हम भी पलते हैं,**  
> **क्या आवाज उठायें, जो गर तुम हो गलत भी,**  
> **जो हम खुद अक्सर गलतियां करते चलते हैं।**  
> **बदतमीज़ी की नुमाइश करने वालों की दुनिया में फिर भी कमी नहीं,**  
> **तहज़ीब को आहिस्ता से परोसने वाले कम ही मिलते हैं।**
> 
> **~रबी**


\[ You also live in misunderstandings,  
We also thrive in misunderstandings,  
Why raise our voice, even if you are wrong,  
When we often make mistakes ourselves.  
Even then there is no shortage of those eager to exhibit their insolence,  
Dearth is of those who softly serve their mannerism. \]

_## This is the first sher of the #30-day-challenge I am undertaking to see if I can write a sher for 30 days on the trot. Let’s see how it goes. ##_
