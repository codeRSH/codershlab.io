---
public: true
title: तिल
date: 2014-07-09
tags:
  - 30-day-challenge
categories: hindi
---

> **बात करो चाहे गले की या गालों की,**  
> **बात करो चाहे कमर की या हाथों की,**  
> **तुमने हर जगह तिल सजा रखे हैं,**  
> **क्या हमारी नज़र इतनी बुरी है ऐ सनम,**  
> **इनसे बचने को खुद पर दाग लगा रखें हैं ?**
> 
> **पर तुम क्या जानो,**  
> **तुम अभी नादान हो,**  
> **जिसकी खबर पूरी दुनिया को है,**  
> **तुम इस बात से अनजान हो ,**  
> **जो निशान तुमने हमसे बचने को बनाये थे,**  
> **वही निशान तो हमें तुम्हारी ओर खींच लाये थे।**
> 
> **~रबी**

  
\[ Talk about the neck or the cheeks,  
Talk about the waist or the hands,  
You have decorated every place with a mole,  
Are my eyes so evil, Oh Love,  
You have stained yourself just to be safe from them ?

But you know what,  
You are too innocent,  
The thing that the whole world knows,  
You are unaware of this very thing,  
The stains you made prevent yourself from me,  
The same stains pulled me to you  \]
