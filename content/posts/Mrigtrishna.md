---
public: true
title: मृगतृष्णा
date: 2016-11-02
tags:
  - vow
categories: hindi
---

![ ](../images/mrigtrishna.jpg)


> **ज्ञात  है यथार्थ नहीं,  एक  मृगतृष्णा  है,  
> फिर  भी  ये  स्वप्न  सत्य  कर  ही  जगना  है।**
>
> **~रबी**


\[ I know it’s not reality, it’s a mirage,   
Still I’ll wake up only after making this dream true. \]
