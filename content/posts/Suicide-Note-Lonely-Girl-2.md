---
public: true
title: "The Suicide Note of a Lonely Girl - Part 2"
date: 2014-07-27
tags:
  - special
  - story
categories: english
---

> **She’s dead. No more.**  
> **Why she did what she did,**  
> **I wish I could but, I don’t know.**  
> **All I know is that she’s not coming back.**  
> **And she takes with her every question I ever had.**  
>   
> **She was simple, silent, lost in her thoughts,**  
> **As if she didn’t belong to this world,**  
> **She wasn’t like those typical girls her age,**  
> **Doing jib jab yip yap all day,**  
> **She used to say everything through her eyes,**  
> **And almighty knows, I liked her that way.**  
>   
> **I would follow her to the college parks,**  
> **Where she would sit at a secluded place for a million hours,**  
> **And scribble something in her diary,**  
> **But only when she won’t be busy talking her friends, the evening stars,**  
> **That diary… it was so dear to her,**  
> **You could find it everywhere,**  
> **She would always keep it close to her chest,**  
> **As if she planned to take it her death bed.**  
>   
> **My friends made fun of me,**  
> **That I was after a “dead” lifeless girl,**  
> **And I didn’t have guts to date even her,**  
> **And they were probably right as well.**  
> **When she was around, I would get so dumb stuck,**  
> **I would have to psych myself even to mumble a few words,**  
> **A dumb sheepish smile was all I could give,**  
> **And she would smile back at my mercy plea.**  
>   
> **But now I had decided enough was enough,**  
> **Feelings stuck in vocal cords would have to be cough’d,**  
> **Tomorrow when the college was to reopen,**  
> **I was going to ask out the ‘dead’ college girl,**  
> **But destiny had some other evil plans in store,**  
> **May be it was against her, like those college people,**  
> **When the eyes opened today, in front was the newspaper,**  
> **Whose headlines screamed, “SHE IS DEAD.” … for real.**  
>   
> **When the cops found her pale lifeless body,**  
> **She layed on bed covered in the Ashes of her dear diary,**  
> **And clutched in the palms of the wrist she cut,**  
> **Was its last page, soaked in blood,**  
> **Only her parents know what her last words were,**  
> **But the press are calling it,**  
> **“The suicide note of a lonely girl”…**
>
> **~RavS**
