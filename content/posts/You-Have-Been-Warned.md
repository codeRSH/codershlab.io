---
public: true
title: You have been Warned
date: 2013-10-24
tags:
  - anger
categories: english
---

> **You do what you know best,  
> Make me miserable,  
> Keep me tied.  
> Continue stressing me,  
> Till I reach breaking point.  
> Bring out your best weapons,  
> Keep up the torture, until you get tired.  
> It’s my promise, I won’t complain.**
> 
> **But remember this…  
> One day it will be my turn,  
> One day I will just snap,  
> One day this volcano will burst.  
> That day I will vent, spew the lava in me,  
> That day you will burn, and burn severely.  
> That day YOU don’t complain.**
> 
> **You have been warned.**
> 
> **~RavS**

## _Has there ever been a person who  controlled your life  against your wishes, and every moment you felt like you might just lose it anytime. This “Warning” is for that person… ##_
