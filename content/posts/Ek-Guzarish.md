---
public: true
title: एक गुज़ारिश
date: 2012-08-16
tags:
  - request
categories: hindi
---

> **आपसे एक गुज़ारिश थी…  
> यूँ तन्हा, उदास रहा ना करो।  
> खफ़ा है ज़िन्दगी आपसे,  
> आप ज़िन्दगी से ख़फ़ा रहो तो रहो,  
> अपनों से बेपरवाह रहा ना करो।**  
> 
> **एक गुज़ारिश और भी…  
> की थोडा और मुस्कुराया करो,  
> आँखों में चमक ज़रा और लाया करो।  
> सुर्ख होठों को लाल रंग की ज़रुरत नहीं,  
> इन लबों को हँसी से ही सजाया करो।**  
> 
> **और एक गुज़ारिश आख़री…  
> हम प्यार करते हैं आपसे,  
> हमें इतना सताया ना करो।  
> गलतियां तो सभी करते हैं,  
> पर रूठ कर नहीं…  
> हमें प्यार से ही समझाया करो।**
> 
> **~रबी**  


\[ I have a request to you…  
Please don’t remain so lonely and sad.  
Life is angry on you,  
You remain angry with life, fine,  
But please don’t remain nonchalant with friends.  
  
Another request…  
Please smile a little more,  
Sparkle those eyes a little more.  
These lips don’t require a red color,  
They will look lovely just with your laughter.  
  
And one last request…   
You know I love you,   
Please don’t torment me so much.  
Everybody makes a mistake (!),  
but your anger won’t help,  
I can always get better with your love. \]
