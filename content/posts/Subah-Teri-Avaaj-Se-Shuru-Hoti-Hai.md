---
public: true
title: सुबह तेरी आवाज से शरू होती थी
date: 2014-10-05
tags:
  - hope
  - desperation
categories: hindi
---

> **सुबह तेरी आवाज से शरू होती थी ,**  
> **रात ढलती थी तेरे नाम से …**  
> **बहुत कोशिश की, तुझे जेहन से निकालने की,**  
> **तुम फिर भी आ जाते थे सपनो में कितने आराम से।**
> 
> **तुम्हे क्या पता कितनी रातें हमने जाग कर गुजारी हैं,**  
> **तुम्हे क्या पता कितने आंसूं हमने छिपकर हैं बहाए,**  
> **तुम्हे तो लगता होगा तकलीफें सिर्फ तुम्ही को मिली हैं ,**  
> **तुम्हें क्या पता कितनी तकलीफ हम अपने अन्दर हैं दबाये।**
> 
> **जितना पास आने की कोशिश की,**  
> **जितना तुम्हें अपना बनाने की कोशिश की ,**  
> **तुम उतने ही दूर होते चले गए।**  
> **मुझे शक है तुम मेरे कभी थे भी या नहीं,**  
> **जिस लिहाज से तुम हमें छोड़कर चले गए।**
> 
> **एक बार को लगा नफरत ही कर लें,**  
> **तुम्हें भूल जाने की जुर्रत ही कर लें ,**  
> **पर तुम इतने अन्दर तक घुल गए हो,**  
> **कौन बतायेगा मुझमे कितना मैं हूँ, और कितने तुम हो।**  
> **किसे दूर करूँ, किससे दूर जाऊं ,**  
> **पता चलता ही नहीं,**  
> **नफरत है तुमसे, झूठ बोलते बोलते खुद को,**  
> **तुमसे ना कर पाए, हमें खुद से नफरत सी हो गयी।**
> 
> **बहुत मुश्किल है तुम मिल सकोगे कभी,**  
> **बहुत मुश्किल है जो चाहते हैं वो हो सकेगा कभी,**  
> **फिर भी उम्मीद बुझने नहीं दी है ,**  
> **इसी के सहारे तो उठते हैं रोज,**  
> **तुमने मारने में कहाँ कोई कसर रखी है।**
> 
> **अच्छा एक बात बताऊँ, बुरा तो नहीं मानोगे?**  
> **हमने मान लिया है हम मर चुके हैं तुम्हारे वास्ते,**  
> **पर दिल डरता है बहुत, बिन तेरे जीने के ख्याल से,**  
> **सुबह अब भी तेरी आवाज से शुरू होती है,**  
> **रात अभी भी ढलती है तेरे नाम से…**
> 
> **~रबी**


\[ Mornings used to start with your voice,  
Night used to sleep with your name…  
I tried to remove you from the mind,  
Yet you would come in dreams so easily.

Do you know how many nights I have stayed awake,  
Do you know how many tears I have secretly shed,  
You would be thinking that only you’ve got problems,  
Do you know how much pain I have hidden inside.

The more I tried to come near you,  
The more I tried to make you mine,  
So much you would get away,  
I doubt if you were ever mine,  
The way you left me without caring.

I even thought to hate you,  
I even dared to forget you,  
But you have dissolved so much in me,  
Who would explain me how much of me is me and how much is you,  
Whom do I shoo away, from whom should I go away,  
I can not decide,  
I only have hatred for you, telling lies to self,  
I couldn’t hate you, but I started hating myself.

It’s very difficult I would ever get you,  
It’s very difficult it would ever happen like I wish,  
Yet I haven’t let the hope extinguish,  
I get up every day only because of this,  
Otherwise you spared no efforts to kill me.

OK, should I tell something, if you don’t mind?  
I have accepted I am already dead for you,  
But my heart gets too afraid by the thought of living without you,  
Mornings still start with your voice,  
Night still sleeps with your name… \]
