---
public: true
title: ज़ाया
date: 2014-07-21
tags:
  - 30-day-challenge
categories: hindi
---

> **ना ही करे हमें कोई प्यार तो अच्छा है,**  
> **प्यार करने वालों से हमें नफरत हो चली है।**  
> **देखेंगे, फिरेंगे। कुछ दिन बात करेंगे।**  
> **फिर खुद ही खुद को चाहने की फ़रियाद करेंगे।**  
> **क्यों वक्त ज़ाया करना। अपना भी और औरों का भी।**  
> **वो वक्त हम कुछ और ही फ़िज़ूल करने में बर्बाद करेंगे।**
> 
> **~रबी**
  
  
\[ It’s better that no one loves me,  
I have anyway started hating the lovers.  
They would see. They would roam. They would talk for a while.  
Then one fine day, they would ask to love them.  
Why waste time. Mine and theirs.  
I could do something else useless, if only time has to be passed. \]
