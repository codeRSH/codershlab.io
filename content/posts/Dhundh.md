---
public: true
title: धुंध
date: 2011-12-30
tags:
  - romantic
categories: hindi
---

![ ](../images/dhundh.jpg)

> **तू उस धुंध की तरह है,**  
> **जिसके आने से बाकी कुछ दिखाई नहीं देता,**  
> **ऐसे हालात में हम गिरें, चोट खाएँ,**    
> **तो तुझे हैरानी नहीं होनी चाहिए…** 
> 
> **~रबी**


\[ You are like a fog,  
Which when comes, everything else ceases to be visible,  
Now if we start tumbling here and there, and get hurt,  
It shouldn’t surprise you! \]
