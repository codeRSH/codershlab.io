---
public: true
title: "एक हद्द होती है खूबसूरती की"
date: 2014-05-06
tags:
  - beauty
  - dedication
categories: hindi
---

![ ](../images/ek-hadd-hoti-hai.jpg)

> **जो थी नहीं किसी के बस की ,**  
> **वो बात आपने आज कर दी,**  
> **एक हद्द होती है खूबसूरती की,**  
> **आपने जो आज पार कर दी।**
> 
> **नाम की सुध नहीं, काम का ख्याल नहीं,**  
> **लोग जो करने बैठे बातें इधर उधर की,**  
> **भूला भी लौट आये, जिसे याद हो भटक जाए,**  
> **ऐसी हालत है आज संसार भर की,**  
> **एक हद्द होती है खूबसूरती की,**  
> **आपने जो आज पार कर दी।**
> 
> **चढ़े जो एक बार तो वो जाते जाते जाता है,**  
> **क्यों न इंतज़ार करें फिर वो आपके जरा से खुमार भर की,**  
> **कहा था पिछली बार अब कुछ लिखने को बचा नहीं,**  
> **देखो आपने आज बातें और दो चार कर दी,**  
> **एक हद्द होती है खूबसूरती की,**  
> **आपने आज जो पार कर दी।**
> 
> **~रबी**


\[ Something which no one was capable of,  
You did exactly that thing that,  
There’s a limit to being beautiful,  
You crossed that limit today.

They forget their names, they stop caring about their work,  
When people start chit-chating with you,  
The lost come back, others forget their way,  
That’s the situation of the world because of you.  
There’s a limit to being beautiful,  
You crossed that limit today.

If it gets over the head, it takes a while to leave,  
So, why shouldn’t people wait to get intoxicated by you,  
I said last time, I don’t have words left to write,  
See, you gave a few more punchlines,  
There’s a limit to being beautiful,  
You crossed that limit today. \]

  
_## One more, and I will have to get into discussion with her to become my official muse :) ##_
