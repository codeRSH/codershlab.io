---
public: true
title: सोच रहा हूँ... एक गर्लफ्रेंड बना लूं...
date: 2012-01-13
tags:
  - main-blog 
  - humor
categories: hindi
---

> **सोच  रहा  हूँ  बहुत  दिनों  से,  
> एक  गर्लफ्रेंड  बना  लूं।  
> वाट  पहले  से  ही  लगी  पड़ी  है,  
> थोड़ी  और  लगवा  लूं।** 
> 
> **पैसे  खर्च  करने  पड़ेंगे  धो-धो  के,  
> ये  तो  बहुत  हद  तक  पता  है।  
> मगर  कुछ  पाने  के  लिए,  
> बहुत  कुछ  खोना  भी  तो पड़ता  है।** 
> 
> **दिन  रात  फ़ोन  मेरा  कुछ  इस  तरह  चिल्लाएगा,  
> के  खुद  ही  के  फ़ोन  से  डर  लगने  लग  जाएगा।  
> उसकी  चांय चांय  काँय  काँय  से,  
> जो बिल\* अभी देश के विकास की तरह मद्ध्म है,  
> वही  बोल्ट\*\*  की  स्पीड  से  मैराथन  दौड़  लगाएगा।**
> 
> **लेकिन  ये  भी  है, वो  दिमाग  चाटेगी,  
>  तो  भेजा  रिफ्रेश  हो  जाएगा ...  
> जब  उसी की सुनने से  फुर्सत  ना  मिलेगी,  
>  तो  कोई  और  क्या  सुनाएगा!**
> 
> **टेंशन  जिंदगी  में  पहले  से  बहुत  हैं,  
> उसके  आ  जाने  से और  बढ़  ही  जायेंगी।  
> लेकिन  कम  से  कम  इस  बोरिंग  जिंदगी  में,  
> वो कुछ  डिसट्रेक्शन  तो  लाएगी।** 
> 
> **"सही  है  भाई , दोस्ती  सही  तूने  निभाली।  
> साले! हम  घास  छीलते  हैं  तो  भी  तुझे  बता  कर,  
> और  तूने  बिना  बताये , बंदी  पटा  ली,  
> चल  कोई  नहीं  ये  बता, अब  पार्टी  देगा  कहाँ पर,  
> और  कमीने  हमें  'भाभी'  से  कब  मिलवाएगा ?"  
> कुछ  इस  तरह  दोस्तों  में  अपना  भी  रौब  बढ़  जाएगा।** 
> 
> **तो अब  सोच  ही  लिया  है,  
> एक  गर्लफ्रेंड  बना  ही  ली  जाए।  
> अगर  खुदखुशी  करनी  ही  है,  
> तो  खुद ही  की  ख़ुशी  से  क्यों  ना  की  जाए ?**
> 
> **~रबी**

_\*बिल : फोन का बिल, ना की पार्लियामेंट वाला बिल  
\*\*बोल्ट : [उसेन बोल्ट](http://en.wikipedia.org/wiki/Usain_Bolt)_


\[ I've been thinking for a long time,  
I should get a girlfriend.  
Things are already screwed,  
Might as well mess them up a little more.  

I'll have to spend money lavishly,  
This much I know for sure.  
But to gain something,  
You have to lose a lot.  

My phone will ring day and night in such a way,  
That I'll start getting scared of my own phone.  
From her constant chatter (literally, "chirp chirp, caw caw"),  
The bill, which is currently slow like the country's development,  
Will run a marathon at Bolt's speed.  

But there's also this, she'll nag me,  
So my brain will get refreshed...  
When I won't get any free time from listening to her,  
Then what else will anyone tell me!  

Tensions in life are already plenty,  
With her arrival, they'll only increase.  
But at least in this boring life,  
She'll bring some distraction.  

"Great, brother, you've really been a good friend.  
Damn it! We tell you even when we're doing nothing (literally: cutting grass),  
And you, without telling us, got a girlfriend,  
Alright, never mind, tell us now, where will you throw the party,  
And when will you introduce us to our 'sister-in-law'?"  
In this way, my reputation among my friends will also increase.  

So now I've decided,  
I should definitely get a girlfriend.  
If I have to commit 'suicide' (figuratively, meaning facing hardship),  
Why not do it with my own happiness? \]
