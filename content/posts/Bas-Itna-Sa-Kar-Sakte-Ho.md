---
public: true
title: बस  इतना  सा  कर  सकते  हो ?
date: 2016-01-28
tags:
  - romantic
  - wish
categories: hindi
---

> **जब  चलूँ  मैं, तुम  चलो।    
> जब  रुक  पडूँ, तुम  रुके  रहो।  
> क्या  इतना  तुम  कर  सकते  हो ?  
> बस  इतना  सा  कर  सकते  हो ?**
> 
> **जो  ख़ुशी  मिले, तुम  एहसास  हो,  
> जिससे  हँसी मिले, तुम  काश  हो,  
> ना  हो  अफ़सोस  कभी  इस  बात  का,  
> तुम  आस  हो, पर  नहीं  पास  हो।**  
> 
> **जब  डाँटूं, थोड़ा  सुन  लो,  
> बाद  जितना  बुरा  भला  कहो,  
> क्या  इतना  तुम  कर  सकते  हो ?  
> बस  इतना  सा  कर  सकते  हो ?**
> 
> **ज़िन्दगी  सफर, तुम  हमसफ़र  बनो,  
> गिरके  उठता  रहूँ, वो  असर  बनो,  
> ना  आये,  
> पर  आये,  
> ‘गर  ऐसा  मोड़  भी,  
> बिन  तेरे  ज़िन्दगी  हो,  
> तो  पहले ना  मेरी हो।**  
> 
> **जाना  है, तुम  शौक  से  जाओ,  
> बस पहले  जाने  की  ना  ज़िद्द  करो,  
> क्या  इतना  तुम  कर  सकते  हो ?  
> बस  इतना  सा  कर  सकते  हो ?**
> 
> **~रबी**


\[ When I walk, you walk with me,  
When I stop, you stop with me.  
Can you do this much please?  
Only this much please?

When I get happiness, you be the feeling,  
When I laugh, you be there laughing,  
I hope I don’t have this regret,  
That you are my hope, but not nearby.

When I scold, you listen a little,  
Later you may shout out your heart.  
Can you do this much please?  
Only this much please?

If life is a journey, you be my partner,  
I get up after falling, you be that inspiration,  
I hope it never comes,  
But if it comes,  
that point in life,  
That there is life without you,  
then let be not mine first.

If you want to go, you are free,  
Please don’t ask to leave first though,  
Can you do this much please?  
Only this much please? \]
