---
public: true
title: बेरेहमी
date: 2014-11-11
tags:
  - platonic
categories: hindi
---

> **जितनी बेरेहमी से अक्सर रुलाता है तू ऐ हंसाने वाले ,**  
> **मत भूल खुदा के घर फिर मुलकात होगी।**  
>   
> **पर इतना भी ना चाहा कर मुझे ,**  
> **की मरने लगूँ तो जान भी ना निकल पाये।**  
>   
> **~रबी**

  
\[ The way you make me cry, who usually makes me laugh,  
Do not forget, we have to meet at God’s home again.  
  
But do not like me so much,  
That if I start dying even the spirit refuses to leave the body. \]
