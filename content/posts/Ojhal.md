---
public: true
title: ओझल
date: 2014-10-16
tags:
  - love
  - romantic
categories: hindi
---

> **क्या तुम हो सकते हो नज़रों से ओझल हमेशा हमेशा के लिए?**  
> **क्या करूँ, जहां दिख जाते हो कमबख्त प्यार हो जाता है।**
> 
> **~रबी** 

  
\[ Can you get away from sight forever and ever?  
What shall I do, wherever I see you, I fall in love. \]
