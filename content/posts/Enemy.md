---
public: true
title: Enemy
date: 2014-11-18
tags:
  - free-verse
  - enemy
  - death
categories: english
---

![ ](../images/enemy.png)

> **The enemy was stronger,**  
> **And it wasn’t outside,**  
> **It was inside,**  
> **Like a cancer pulping,**  
> **In slow motion.**  
> **Whispering in the ears.**  
> **“Dead.”**  
> **Even before the first breath was finished.**  
> **Haaaaaahhhhh…**
> 
> **~RavS**
