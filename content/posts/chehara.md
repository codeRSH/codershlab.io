---
public: true
title: चेहरा
date: 2025-01-28
tags:
  - sad
categories: hindi
---

> **चेहरा तो भुला देता हूं,  
> क्या करूं, यादें नहीं मिटती,  
> दिन तो कट जाते हैं यूं तो,  
> तुझ बिन रातें नहीं कटतीं।**  
> 
> **वो तकियों को नम करना,  
> और मुंह भींच कर रोना,  
> कोई सुन ना ले मेरी सिसकियां,  
> तो चीख़ों से आवाज़ नहीं निकलती ।**  
> 
> **मैं सोता नहीं चादर तले,  
> इस आस में तुम आके जगाओगे,  
> पर तुम नहीं आते, ना आओगे,  
> ना जाने क्यों, फिर भी,  
> आंखें नही खुलती ।**  
> 
> **काटता है अकेलापन भीड़ में,  
> और अकेले में तेरी आवाजें,  
> खो ना जाऊं अंधेरे में कहीं, इसलिए,  
> आदत बना ली है खुद से लिपटने की ।**  
> 
> **सीखा नहीं है अभी तक,  
> अपना डर छुपा पाना,  
> छलक ही जाता है अक्सर आंखों से,  
> पर मेरी कोशिशें कभी कम नहीं होती ।**  
> 
> **ज़िंदा रहूंगा, कहा था, याद है,  
> पर कोई और उम्मीद मत रखना,  
> ज़िंदगी तो तुम ले गए साथ में,  
> लाशें कभी खुश नहीं हुआ करती ।**  
> 
> **ना होली में रंग नज़र आते हैं,  
> अब ना दिखती है दिवाली में रोशनी ,  
> एक अमावस सी लगती है शक्सियत अपनी,  
> और सहर की कोई आस भी नहीं लगती ।**  
> 
> **बुरा सपना तो घट जाता है,  
> ये कम्बख़्त उम्र क्यों नही घटती?  
> दिन तो कट जाते हैं यूं तो,  
> तुझ बिन रातें नहीं कटतीं।**  
>
>**~रबी**

\[ I can forget the face,  
But I can't, memories don't fade,  
Days pass by somehow,  
But nights without you, don't.  

Those moments of wetting the pillows,  
And weeping with a clenched mouth,  
Lest anyone hear my sobs,  
Even my screams make no sound.  

I don't sleep under the sheets,  
With the hope that you'll come and wake me,  
But you don't come, nor will you,  
But I don't know why, still,  
My eyes don't open.  

Loneliness bites me in crowds,  
And in solitude, your voices,  
Lest I get lost in the darkness, therefore,  
I've made it a habit to embrace myself.  

I haven't yet learned,  
To hide my fear,  
It often spills from my eyes,  
But my efforts never lessen.  

I said I'd stay alive, I remember,  
But don't hold any other hope,  
You took life with you,  
Corpses are never happy.  

Neither do colors appear in Holi,  
Nor is there light visible in Diwali now,  
My personality feels like a new moon night,  
And there's no hope of dawn either.  

A bad dream eventually fades,  
Why doesn't this wretched life shorten?  
Days pass by somehow,  
But nights without you, don't.  \]

_## Not directly related to me, but very personal poem. Publishing it after 3 years now. ##_
