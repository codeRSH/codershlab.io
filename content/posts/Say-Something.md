---
public: true
title: Say Something
date: 2014-02-22
tags:
  - voice
  - tribute
categories: english
---

> **Leave me gasping for breath,**  
> **Bring me down to the knees,**  
> **Move me to tears,**  
> **Rekindle the ache in my guts,**  
> **Let me hear your voice once.**  
> **…Just say something. Anything.**
>
> **~RavS**


_## Some voices, they are so beautiful, so soulful, you are almost moved to tears hearing them. A tribute to all my favorite singers and friends with a sweet little voice. ##_
