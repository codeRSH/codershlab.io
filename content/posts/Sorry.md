---
public: true
title: Sorry
date: 2014-12-12
tags:
  - prose
  - apology
categories: english
---

![ ](../images/sorry.png)

> **Sorry. And that’s all I have to say because I have understood that every excuse I make that saves me from the wrath of my loved ones also slowly takes them away inch by inch until the dear ones no more remain the near ones and vice versa, leaving me with an unbruised ego but no one to show it to and so it’s better I endure a hit of your fury and savor your wounds than risk losing you forever at the cost of something that would ultimately become just a little bubble in the murky waters of long term incoherent memories.**
>
> **So, Sorry.**
>
> **~RavS**
