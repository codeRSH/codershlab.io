---
public: true
title: If only
date: 2011-11-22
tags:
  - romantic
categories: english
---

> **If only I could inhale you,**  
> **with every breath I take.**
> 
> **If only you could run through my veins,**  
> **not the blood instead…** 
> 
> **~RavS**
