---
public: true
title: Haiku4U - 20
date: 2019-04-20
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-20.jpg)

> **I think less about You  
> More about Us.  
> Where most of Us is You.**
>
> **~RavS**


_## It’s true. I confess. I think more about you in terms of “Us”, than only “You”. The “You” where I don’t exist doesn’t excite me that much. May be I am selfish that way._

_But here’s another truth. The “Us” that I think of does and will always contain most of “You” only. ##_
