---
public: true
title: When I go...
date: 2013-10-30
tags:
  - life
  - exit
  - separation
categories: english
---

> **When I go…  
> I hope you remember the moments we spent,  
> And regret the moments we couldn’t.  
> I hope you wish I could stay a little longer,  
> But know in your heart that it’s not possible.  
> I hope your heart aches for the sorrow,  
> Of knowing I won’t be here with you, tomorrow.  
> I hope your eyes shed a tear or two,  
> When I have to finally bid adieu.**
> 
> **… And till that time comes, I promise,  
> I won’t go, I won’t go.**
> 
> **~RavS**

## _When I leave the world, I want to leave a legacy. Something for people to remember me by. And if I am not able to do that, then it would be a life wasted. ##_
