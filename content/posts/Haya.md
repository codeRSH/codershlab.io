---
public: true
title: हया
date: 2013-01-04
tags:
  - romantic
  - fear
categories: hindi
---

> **बेबाक बेफिक्र सी जब हया तू निकलती है,**  
> **बेधड़क बेवजह ही दिल से आह सी निकलती है,**  
> **बेकदर बेरुखी, पर इस दुनिया से क्या कहिये,**  
> **बेरहम बेशरम, फिर भी ये मुझे ही कहती है।**
> 
> **~रबी**


\[ Whenever you get out of home, shyness,  
My hearts skips a beat, it’s not fearless,  
But what can I say about this world, careless,  
It still considers me as the one, the shameless. \]
