---
public: true
title: हद्द से ज्यादा 
date: 2014-05-25
tags:
  - pain
  - life
categories: hindi
---

> **तकलीफ देने लगे ज़िन्दगी हद्द से ज्यादा जो कभी,**  
> **समझ लीजिये आप हमको समझने के काबिल हो गए।**
> 
> **~रबी**


\[ If ever life starts you giving unbearable pain,  
You will become capable of understanding me that day. \]

_## I have said this before, and I will repeat. Intelligent two liners are my favorite to read and write. ##_
