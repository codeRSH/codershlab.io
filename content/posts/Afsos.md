---
public: true
title: अफ़सोस
date: 2011-11-23
tags:
  - sad
  - friendship
  - separation
categories: hindi
---

> **जब  जब  तुझसे  मिलता  हूँ,  
> तो  अफ़सोस  होता  है,  
> कि  तुझसे  पहले  क्यों  ना  मिला।**
> 
> **और  दुःख  इस  बात  का,  
> कि  शायद  फिर  तुझसे  कभी  ना  मिल  पाऊंगा …**
> 
> **~रबी** 

\[ Whenever I meet you,  
I regret,  
not meeting you before,

And I fear,  
may be I won’t meet you ever again…\]
