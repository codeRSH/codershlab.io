---
public: true
title: ज़िन्दगी - एक ट्रेन
date: 2012-07-31
tags: 
  - main-blog
  - musings 
categories: hindi
---

![](../images/zindagi-train.jpg)

> **तेरे बिना ये ज़िन्दगी, वो ट्रेन का डिब्बा है लगता,  
> जिस पर बिन सोचे-समझे मैं चढ़ तो गया,  
> पर अब आगे जाने का मन नहीं करता,  
> आखिर हमसफ़र के बिना हम सफ़र करके करेंगे भी क्या?** 
> 
> **राह में कई मिले अब तक,  
> बहुत से बुरे तो कुछ भले भी,  
> जो कुछ ऐसे मिले भी,  
> जिन्हें दिल ने रोकने की कोशिश की।** 
> 
> **मन किया ले चलें अपने साथ,  
> ये ट्रेन ले जाए जहाँ कहीं भी,  
> पर देखिये किस्मत हमारी ऐसी,  
> उन्हें उनकी मंजिल पहले ही पता थी।** 
> 
> **क्या पता अगले स्टेशन पर कहीं तू मिल जाए,  
> बस यही आस लिए हैं बैठे,  
> वर्ना यहाँ कुछ नहीं है रखा,  
> हम तो कब के कूद चुके होते।** 
> 
> **कभी सोच सहम जाता हूँ,  
> आँखें तुझे खिड़की के बाहर ही ढूंढती रह जाएँ,  
> और तू पिछली सीट पर ही बैठा हो,  
> और ट्रेन अपने मुकाम तक पहुँच जाए,  
> जब तक मुझे इसका एहसास हो।** 
> 
> **~रबी** 


\[ This life without you feels like a train compartment,  
Which I boarded without thinking,  
But now I don't feel like going further,  
After all, what will I do traveling without a companion?  

I've met many on the way so far,  
Many bad, and some good too,  
And some I met like this,  
Whom my heart tried to stop.  

I wished I could take them with me,  
Wherever this train goes,  
But look at my fate,  
They already knew their destination.  

Who knows, maybe I'll find you at the next station,  
I'm just sitting here with this hope,  
Otherwise, there's nothing left here for me,  
I would have jumped off long ago.  

Sometimes the thought makes me shudder,  
My eyes keep searching for you outside the window,  
And you are sitting on the seat behind me,  
And the train reaches its destination,  
By the time I even realize it. \]
