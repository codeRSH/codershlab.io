---
public: true
title: खुद
date: 2013-12-21
tags:
  - life
  - compulsions
categories: hindi
---

> **कुछ तो मजबूरियाँ  रही होंगी,  
> यूँ ही तो कोई खुद ही नहीं आता,  
> खुद ही बुलाने के बाद।**  
> 
> **~रबी**

  

\[ They would definitely have some excuse,  
They wouldn’t have ditched like this,  
After inviting themselves. \]
