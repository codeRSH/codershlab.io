---
public: true
title: अंजाम
date: 2012-12-13
tags:
  - anger
  - separation
categories: hindi
---

> **शुक्र मना जो तू मिलने से पहले बिछड़ गया,  
> मिलकर बिछड़ने को कहता, तो अंजाम बहुत बुरा होता।**
> 
> **~रबी**


\[ Praise the lord,  
that you got separated even before we met,  
If you would have asked for separation afterwards,  
consequences would have been very bad. \]
