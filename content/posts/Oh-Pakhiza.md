---
public: true
title: ओ पाकीज़ा
date: 2012-08-19
tags:
  - romantic
  - poetry
categories: hindi
---

> **ना कहना कभी भूलकर, “मुझ पर तुम शायरी लिखो”,  
> मेरी नज़्में किसी बख़्श तुझे बयाँ ना कर पाएंगी,  
> जा देख कभी आईने में कुछ देर खुद को, ओ पाकीज़ा,  
> ख़ुदा की लिखी शायरी तुझे आप नज़र आ जायेगी।**
> 
> **~रबी**


\[ Don’t ever say, “Write poetry on me”,  
My couplets will never be able to describe you,  
Go see yourself in mirror for a while, Oh pure soul,  
You will get to read the poetry written by the Lord. \]
