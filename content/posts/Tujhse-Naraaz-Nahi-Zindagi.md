---
public: true
title: तुझसे नाराज़ नहीं ज़िन्दगी
date: 2014-09-11
tags:
  - song
  - sweetangel
  - reprise
categories: hindi
---

> **तुझे कभी डर लगे जो अगर, हम छोड़ जाएंगे,**  
> **कभी देना आवाज जोर से, फिर  लौट आएंगे,**  
> **मैं अक्सर वादा निभा पाता तो नहीं ,**  
> **फिर भी तुझसे चलो ये वादा रहा।**  
> **तुझसे नाराज़ नहीं ज़िन्दगी, हैरान हूँ मैं।**  
> **तेरे मासूम सवालों से परेशान हूँ मैं …**
> 
> **तुझे क्या लगा हम जो गए, तुझे भूल जाएंगे ,**  
> **ऐसा तो नहीं मेरे न रहने से, नाते  टूट जाएंगे,**  
> **ऐसी प्यारी चीज को भी भला,**  
> **कोई भूला करता है ? बोलो!**  
> **तुझसे नाराज़ नहीं ज़िन्दगी, हैरान हूँ मैं।**  
> **तेरे मासूम सवालों से परेशान हूँ मैं …**
> 
> **दर्द जो दे ज़िन्दगी अगर, हमें याद कर लेना,**  
> **दर्द मिटा न पाये भी अगर, दर्द बाँट तुम लेना,**  
> **इतनी छोटी सी तो तुम हो ही,**  
> **दर्द कितना ही बड़ा होगा ? बोलो!**  
> **तुझसे नाराज़ नहीं ज़िन्दगी, हैरान हूँ मैं।**  
> **तेरे मासूम सवालों से परेशान हूँ मैं …**
> 
> **~रबी**


\[ If you ever get afraid that, I will leave,  
Just give a loud shout, I will come back,  
I can’t usually keep my promises,  
But still it’s a promise to you.  
Life is not angry at you, I am baffled,  
I am troubled by your innocent questions …

What did you think, that I will forget you so easily,  
It’s not like if I don’t stay, relationship will fall apart,  
And does anyone ever forgets,  
such a sweet little thing? Tell!  
Life is not angry at you, I am baffled,  
I am troubled by your innocent questions …

If life starts giving pain, just remember me,  
Even if I can’t erase your pain, at least share your pain with me,  
You’re already so small,  
how big could the pain be? Tell!  
Life is not angry at you, I am baffled,  
I am troubled by your innocent questions …\]
