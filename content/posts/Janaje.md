---
public: true
title: जनाज़े
date: 2011-12-19
tags:
  - separation
categories: hindi
---

> **क़त्ल  हुए  हमारा  एक  अरसा  बीत  गया ,**  
> **आप  हमें  बस  याद  ही  करते  रह  गए ,**  
> **जनाज़े  में  आये  कुछ  अपने, कई  गैर  भी ,**  
> **आप  ना  आये, बस  घर  का  पता  ही  पूछते  रह  गए …**
> 
> **~रबी**


\[ It’s been ages since I got murdered,  
You just kept on only remembering me,  
Everyone came in my funeral, a few loved ones, a lot of unknowns as well,  
You didn’t come, you just kept on only asking for my address… \]
