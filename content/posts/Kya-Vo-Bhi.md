---
public: true
title: क्या वो भी
date: 2014-05-23
tags:
  - dedication
  - special
categories: hindi
---

![ ](../images/kya-vo-bhi.png)

> **कभी कभी सोचता हूँ,**  
> **ये किसने अँधेरे की ओट में ऐसा सवेरा जलाया होगा,**  
> **ये किसने दिल पिघला कर इस मोम को बनाया होगा,**  
> **वो कौन था जिसे रात-रात भर नींद नहीं आती होगी,**  
> **जो उसने अपने ख़्वाबों को इतने आहिस्ता से सजाया होगा।**
> 
> **कभी कभी सोचता हूँ,**  
> **जब ये बोलती होगी, क्या सच में फ़िज़ा महकती होगी,**  
> **क्या सच में इसकी महफ़िल में चिड़ियाँ आकर चहकती होंगी,**  
> **ये हाथों में चमकते कंगन, ये गले में सफ़ेद हार,**  
> **ये कानो में दमकते झुमके, ये तन पे कत्थई लिबास,**  
> **क्या तब भी दुनिया इसे ही खूबसूरती की हद्द कहती होगी ?**
> 
> **कभी कभी सोचता हूँ,**  
> **जब कोई मुसाफिर पास से गुज़रता होगा,**  
> **क्या उसके जेहन में भी यही सवाल उमड़ता होगा,**  
> **ये हक़ीक़त ही है, या खवाइश किसी की?**  
> **ये अमानत ही है, या साजिश किसी की?**  
> **ये चाहत ही है, या खलिश किसी की?**  
> **क्या वो भी बैठ सामने घंठों यही सब सोचा करता होगा ?**
> 
> **~रबी**

  
\[ Sometimes I think about this…  
Who lightened this morning in the lap of darkness,  
Who melted his heart to create this figure of wax,  
Who was he who couldn’t sleep every night,  
That he decorated his dreams with oh so slightness.

Sometimes I think about this…  
When she would speak, did really air smelled like fragrance,  
Did really sparrows used to come in her court to sing,  
These shining bracelets in hands, this white necklace in neck,  
These flashing rings in ears, this deep brown dress on skin,  
Did the world used to call her the limit of beauty at that time too?

Sometimes I think about this…  
Whenever a passenger would pass by her,  
Does these questions come in his mind too,  
Is she really truth or someone’s dream?  
Is she really someone’s gift or someone’s conspiracy?  
Is she really someone’s wish or someone’s melancholy?  
Does he also sit in front of her and think about this for hours? \]

  
_## And this completes the triocha of ‘dedication’ poems. Really happy that was able to connect them all. Here have tried to compare the picture with a structure who looks like a living breathing human! Referred to the person in third form. Different isn’t it?! ##_
