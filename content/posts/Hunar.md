---
public: true
title: हुनर
date: 2014-07-12
tags:
  - 30-day-challenge
categories: hindi
---

> **क्या हुनर दिखाऊं तुझे मैं अपना,**  
> **अपनी नुमाइश करना तबायफों की फितरत है,**  
> **जिगर काट कर थोड़ा थोड़ा अपना,**  
> **हमें तो पेश करने की आदत है।**
> 
> **~रबी**


\[ What talent shall I show you,  
To show-off is a habit of prostitutes,  
Cutting my liver bit by bit,  
Is how I like to present it. \]
