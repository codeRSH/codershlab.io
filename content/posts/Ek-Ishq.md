---
public: true
title: एक इश्क़
date: 2014-07-25
tags:
  - song
  - love
categories: hindi
---

> **ऐ इश्क़ ये कैसा तेरा इश्क़,**  
> **क्यों घाव दिया यूँ गहरा इश्क़,**  
> **मेरा तो इश्क़ ही मंदिर-मस्जिद था,**  
> **अब कैसे कहूँ तू मेरा इश्क़।**
> 
> **\[ ऐ इश्क़। ऐ इश्क़। ऐ इश्क़। ऐ इश्क़।**  
> **ऐ इश्क़। ऐ इश्क़। ऐ इश्क़। ऐ इश्क़।**  
> **ऐ इश्क़। ऐ इश्क़। ऐ इश्क़। ऐ इश्क़। \]**
> 
> **एक इश्क़ वो था जो तूने मुझसे किया,**  
> **एक इश्क़ वो था जो मैं कर बैठा।**  
> **एक इश्क़ ही था जिससे जीना सीखा,**  
> **एक इश्क़ ही था जिससे मर बैठा।**  
> **एक इश्क़ ने बनाया तिनका तिनका,**  
> **एक इश्क़ ही सब तबाह कर बैठा।**
> 
> **ऐ इश्क़ ये कैसा तेरा इश्क़,**  
> **क्यों घाव दिया यूँ गहरा इश्क़,**  
> **मेरा तो इश्क़ ही काबा-क़िबला था,**  
> **अब कैसे कहूँ तू मेरा इश्क़।**
> 
> **\[ ऐ इश्क़। ऐ इश्क़। ऐ इश्क़। ऐ इश्क़।**  
> **ऐ इश्क़। ऐ इश्क़। ऐ इश्क़। ऐ इश्क़।**  
> **ऐ इश्क़। ऐ इश्क़। ऐ इश्क़। ऐ इश्क़। \]**
> 
> **एक इश्क़ था जो हद्द से बढ़ता चला गया,**  
> **एक इश्क़ था जिससे सब सेहता चला गया।**  
> **एक इश्क़ में पड़कर खुद को भूला,**  
> **एक इश्क़ में डूबकर बेहता चला गया।**  
> **एक इश्क़ से टूटकर जब होश संभाला,**  
> **एक इश्क़ था वो भी दूर करता चला गया।**
> 
> **ऐ इश्क़ ये कैसा तेरा इश्क़,**  
> **क्यों घाव दिया यूँ गहरा इश्क़,**  
> **मेरा तो इश्क़ ही दरगाह-गिरजा था,**  
> **अब कैसे कहूँ तू मेरा इश्क़।**
> 
> **\[ नहीं इश्क़।नहीं इश्क़।नहीं इश्क़।नहीं इश्क़।**  
> **नहीं इश्क़।नहीं इश्क़।नहीं इश्क़।नहीं इश्क़।**  
> **नहीं इश्क़।नहीं इश्क़।नहीं इश्क़।नहीं इश्क़।\]**
> 
> **~रबी**


\[  
Oh Love, how’s this your love,  
Why did you hurt so much, oh love,  
My love was my temple my mosque,  
How do I now say, you are my love.

\[ Oh Love. Oh Love. Oh Love. Oh Love.  
Oh Love. Oh Love. Oh Love. Oh Love.  
Oh Love. Oh Love. Oh Love. Oh Love. \]

There was a love which you did,  
There was a love which I fell into.  
That was love which taught me how to live,  
That was also love which killed me.  
A love made me bit by bit,  
A love is what destroyed everything.

Oh Love, how’s this your love,  
Why did you hurt so much, oh love,  
My love was my temple my mosque,  
How do I now say, you are my love.

\[ Oh Love. Oh Love. Oh Love. Oh Love.  
Oh Love. Oh Love. Oh Love. Oh Love.  
Oh Love. Oh Love. Oh Love. Oh Love. \]

There was a love which grew out of limits,  
There was a love due to which I bore everything,  
There was a love which made me forgot even myself,  
There was a love in which I got swept away,  
When I came to my sense from the grips of the love,  
Just love was what I was left with, but it went away too.

Oh Love, how’s this your love,  
Why did you hurt so much, oh love,  
My love was my tomb, my chapel,  
How do I now say, you are my love.

\[ No Love. No Love. No Love. No Love.  
No Love. No Love. No Love. No Love.  
No Love. No Love. No Love. No Love. \]  
\]
