---
public: true
title: जलपरी
date: 2014-07-13
tags:
  - 30-day-challenge
categories: hindi
---

> **बस एक जलपरी आये कहीं से,**  
> **ले जाए दूर मुझे इस जमी से,**  
> **उस जहां में जहाँ मैं भूल जाऊं सभी को ,**  
> **जो हुआ, जो होगा, जो हो रहा, सभी कुछ।**  
>   
> **बस नीला नीला सा पानी हो या उजली उजली सी बरफ ,**  
> **मैं रहूँ उसमे, वो, और गहरा समंदर हर तरफ,**  
> **ख्वाब ही में सही,**  
> **झूठी आये तो सही ,**  
> **मुझे क्या समझ क्या गलत क्या सही,**  
> **तसल्ली भर ही हो जाए तो सही।**
> 
> **बस एक जलपरी आये कहीं से,**  
> **ले जाए दूर मुझे इस जमीन से।**
> 
> **~रबी**


\[ I hope a mermaid comes from somewhere,  
And takes me with her from this place,  
To a world where I forget everyone,  
What happened, what will happen, what’s happening. Everything.

Let there be just blue water or white ice,  
I get to live in it, her, and all-around deep sea,  
Let it just be a dream,  
Let that liar come at least,  
I don’t know what’s wrong what’s right,  
I can at least console myself.

I hope a mermaid comes from somewhere,  
And takes me with her from this place. \]
