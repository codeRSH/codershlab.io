---
public: true
title: Nib on Paper
date: 2014-11-27
tags:
  - bleed
  - paper
  - ink-on-paper
categories: english
---

![ ](../images/nib-on-paper.png)

> **Before I press the nib,**  
> **to the texture of a vintage paper,**  
> **I think about you.**  
> **I let your words chime,**  
> **in the darkness of my hollow existence,**  
> **I let your cravings rise up,**  
> **to the surface of my skin.**  
> **I let myself crumble down,**  
> **under the weight of your impossible truth.**  
> **A thousand capillaries bursting in my head.**  
> **I let myself cry in your agony.**
>
> **Then I bleed; on your behalf.**
>
> **~RavS**
