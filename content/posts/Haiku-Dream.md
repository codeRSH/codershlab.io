---
public: true
title: Haiku - Dream
date: 2019-02-11
tags:
  - haiku
  - love
categories: english
---

![ ](../images/haiku-dream.jpg)

> **We’re a dream.  
> We’ve to come true.  
> For me and you and us.**  
>   
> **~RavS**

  

_## Right now everything is a blur. We don’t know how we are going to make it._  
  
_But through all these difficulties, there’s a dream. A dream that we have finally found what we both were looking for. That we would be there for each other as we grow old. The Dream that I will take care of you and you will support me, no matter what. This dream is what we live for. This dream is what we have to make true. ##_  
