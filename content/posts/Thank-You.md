---
public: true
title: Thank You
date: 2014-11-28
tags:
  - gratitude
  - prose
categories: english
---

![ ](../images/thank-you.png)

> **Thank you for coming in my life and giving me all the pain that I didn’t ask for in the first place, because that’s what taught me how to be human, to value people close by and reciprocate the care from those who empathize, for life is fickle and it’s essential to experience every little emotion whether good, bad or ugly to know what it means to actually live and not let the life just float by, rolling off on a filtered 65 mm celluloid.**
>
> **Thank you.**
>
> **~RavS**
