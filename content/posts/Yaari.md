---
public: true
title: यारी
date: 2013-02-14
tags:
  - friendship
  - valentine
  - special
categories: hindi
---

> **सुख चैन सब लूटा,**  
> **घर-बार सब छूटा,**  
> **यारा तेरी यारी बड़ी मेहेंगी पड़ी।**  
> **पर सब गया तो गया, तेरी यारी तो रही,**  
> **जीना पड़ा इसी के सारे अब अगर सारी ज़िन्दगी,**  
> **तो भी मुझे अब कोई गर्ज़, कोई मलाल नहीं।**
> 
> **~रबी**


\[ My happiness got destroyed,  
Everything in life, I lost,  
My friend, you friendship proved very costly.  
But let everything be gone, at least I have your friendship,  
Now if I have to live my whole life by only that,  
I won’t ever regret it, even for a minute. \]
