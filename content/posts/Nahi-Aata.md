---
public: true
title: नहीं आता
date: 2014-12-03
tags:
  - past
  - writing
categories: hindi
---

> **लोग पूछते हैं आज कल तेरा संदेशा नहीं आता,**  
> **क्या करूँ उस बिन लिखूं क्या समझ नहीं आता।**  
> **कुछ लिख लेता था उसे सोच-सोच कर पहले तो,**  
> **अब तो उसे याद कर भी दिल में कोई ख़याल नहीं आता।**
> 
> **जो लिखने लगता था, कहीं का शुरू कहीं निकल जाता,**  
> **उसे सामने बैठा, खुद उसका अक्स-इ-बयाँ बन जाता।**  
> **आज भी उन बेनाम नज़्मों से उसकी खुशबू आती है,**  
> **पर उन्हें आँखों से लगाने का जज़्बात अब फिलहाल नहीं आता।**
> 
> **ये आलम भी देखना,**  
> **है किस कदर अजीब सा,**  
> **था अंजुमन भरा हुआ,**  
> **बेशुमार जिसके कायलों का।**  
> **पन्ने सने पड़े थे,**  
> **बेहिसाब जिसके ज़िक्र से,**  
> **वो चला गया जलाकर उन्हें,**  
> **और इस बात का उसे,**  
> **जरा भी मलाल नहीं आता।**
> 
> **~रबी**


\[ People ask why does your message doesn’t reach us nowadays,  
What shall I do, without her I don’t know what to write.  
I used to be able to write something thinking about her, before,  
Now I don’t get any thoughts in my heart, even after remembering her.

Earlier, writing, I would start somewhere and reach somewhere else.  
I would make her sit in front, and become an all revealing mirror myself,  
Even today I can smell her fragrance in those untitled lines,  
But I don’t feel like kissing them with eyes anymore.

You see this situation,  
is just so strange,  
The party was loaded,  
with whose uncountable fans.  
Pages were laced,  
incalculable with whose praise.  
She burned them and left,  
And the fact hurts that she,  
doesn’t have any regrets. \]
