---
public: true
title: मुन्तज़िर 
date: 2012-02-13
tags:
  - friends
  - life
categories: hindi
---

![ ](../images/muntazir.jpg)

> **वो  हमारे  मेसेज़  का  इंतज़ार  करते  रहे ,**  
> **हम  उनके  मेसेज़  के  मुन्तज़िर बने  रहे ,**  
> **किसी  से  ये  ना  हुआ  की  उठ  कर कॉल  करले …**
> 
> **~रबी**


\[ She kept waiting for my message,  
I awaited her message,  
No one could think of getting up and calling the other… \]
