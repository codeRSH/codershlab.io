---
public: true
title: इस्तकबाल
date: 2012-02-27
tags:
  - disappointment
categories: hindi
---

> **इतनी  बेरुखी  से  इस्तकबाल  किया  उन्होंने  हमारा  महफ़िल  में ,**  
> **अपनों  की  ही  बज़्म-ए-दुनिया  में  हम  पराये  बन  गए।**
> 
> **~रबी**  


\[ She greeted me so rudely in the gathering of my owns,  
That I become an outcast in my own world. \]
