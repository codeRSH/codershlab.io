---
public: true
title: Freedom
date: 2013-02-21
tags:
  - life
  - wish
categories: english
---

> **I do not ask for the powers to be bestowed,**  
> **I just want my freedom be given,**  
> **Where I have the choice to decide and not just orders to obey,**  
> **Have time to take some breaths and my share of sunshine,**  
> **If that’s a little too much to ask from them,**  
> **Then I may have to snatch it from someone.**
>
> **~RavS**
