---
public: true
title: Left Uncoveyed
date: 2013-07-31
tags:
  - fear
  - lose
categories: english
---

![ ](../images/left-uncoveyed.jpg)

> **Sometimes I don’t want to talk,**  
> **Or you don’t want to hear me out,**  
> **But I can’t abandon you just like that,**  
> **So, I write what I would have said,**  
> **In the hope that you will read,**  
> **And understand, that behind this egoist,**  
> **Is a fragile person who is afraid,**  
> **That he may not get a chance to say,**  
> **what was left unconveyed.**
> 
> **~RavS**
 

_## You can’t leave some people, no matter what they do to you._   
_They are just your weakness, and they know it too :) ##_
