---
public: true
title: पर तुम नहीं
date: 2013-01-18
tags:
  - alone
  - separation
categories: hindi
---

> **यूं तो बरकत भी है और शोहरत भी,**  
> **साथ कुदरत भी है,  'उसकी’ मेहरत भी ,**  
> **यूं तो है चार पल की फुर्सत भी,**  
> **पर तुम नहीं, पर तुम नहीं…**  
>   
> **यूं तो ज़िन्दगी भी है और साँसे भी,**  
> **कहने को मंजिलें भी हैं और राहें भी,**  
> **यूं तो बारिशें भी हैं और पनाहें भी ,**  
> **पर तुम नहीं, पर तुम नहीं…**  
>   
> **यूं तो ख़ुशी भी है और हंसी भी,**  
> **दिकत्तों की नहीं कोई कमी भी,**  
> **यूं तो मैं कल भी था यहीं, और आज भी,**  
> **पर तुम नहीं, पर तुम नहीं…**
> 
> **~रबी**


\[ Yes I have the success and fame today,  
With me is the nature and His blessings today,  
I have a few seconds to spare,  
But you are not here, but you are not here…  
  
Yes, I have life and breaths,  
The destination and the path is clear,  
I have the rain and there is shelter,  
But you are not here, but you are not here…  
  
Yes, I have the happiness and laughter,  
There is no shortage of problems as well,  
And I stand here just like yesteryears,  
But you are not here, but you are not here…\]
