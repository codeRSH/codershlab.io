---
public: true
title: Elegance on a hospital bed
date: 2014-05-02
tags:
  - hospital
  - dedication
  - elegance
categories: english
---

> **Azure blue eyes, and cyan blue robe.**  
> **Skin pale like banana leaves,**  
> **So tiny, so weak.**  
> **But that smile intact.**
> 
> **Hair scattered, no braids,**  
> **Lips parched, no stick.**  
> **Like a precarious version of self.**  
> **But that smile intact.**
> 
> **WBCs mutilated by infections.**  
> **RBCs disappearing, plummeting haemoglobin.**  
> **Nerves gobling bottles after bottles of glucose.**  
> **But that smile intact.**
> 
> **Needles pierced in arms like a heroine addict.**  
> **Inhaling from a gas mask every now and then,**  
> **A soldier preparing for the imminent chemical warfare.**  
> **But that smile still intact.**
> 
> **Immunity gone, glow left,**  
> **Zeal backtracked,.**  
> **And spirit frailing every minute every second.**
> 
> **But still… That smile intact.**
> 
> **There! Elegance on a hospital bed.**
>
> **~RavS**


_##  A few weeks ago I visited a friend in a hospital._

_Now here is a fact. I have been admitted in hospital for exactly 1 day in hospital (severe dehydration, 1997). But I have had to visit so many of friends and family in hospitals. Some people become almost unrecognizable after just a few hours of hospitalization. The environment and the feeling of being caged takes out a lot mentally._

_But there are some people who refuse to surrender to the adversities. They maintain a certain poise even when looking into the eyes of, Umm.. potential death. And that’s almost a topic of research._  

_The above is a rendition of same kind of zeal that I have seen in that friend and others. ##_
