---
public: true
title: I was right
date: 2014-12-04
tags:
  - free-verse
  - future
  - past
categories: english
---

![ ](../images/I-was-right.png)

> **You might not comprehend it today,**  
> **But one day you will understand,**  
> **Everything I did, or said or advised or cautioned against,**  
> **It all had a common theme.**  
> **And that was to make you win.**  
> **Win battles, win hearts and win life.**  
> **You might hate me right now, because it all seems so selfish.**  
> **Like I did it all for myself.**  
> **But you are young, and self absorbed.**  
> **When you will start seeing life in a more sober light, you will get it.**  
> **And you will get me.**  
> **That I was right.**  
> **For both of us.**
> 
> **~RavS**
