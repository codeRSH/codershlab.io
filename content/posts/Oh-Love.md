---
public: true
title: Oh Love.
date: 2014-11-20
tags:
  - free-verse
  - love
categories: english
---

![ ](../images/oh-love.png)

> **Broken pieces of fallen glasses,**  
> **Let me sweep you from the floor,**  
> **And put the shreds in my pocket.**  
> **Before someone else steps on them,**  
> **And gets romantic about the hurt you cause.**  
> **Oh love.**
>
> **~RavS**
