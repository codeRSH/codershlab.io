---
public: true
title: '... तो अच्छा लगता है'
date: 2012-01-22
tags:
  - main-blog 
  - special 
  - love
categories: hindi
---

> **जब तुम मुंह बनाती हो,  
> तो अच्छा लगता है।  
> जब तुम मुस्कुराती हो,  
> तो अच्छा लगता है।**
> 
> **जब तुम खुद भूल जाती हो  
> तो अच्छा लगता है।  
> और फिर मुझे याद कराती हो  
> तो अच्छा लगता है।**
> 
> **जब तुम समझ नहीं पाती हो,  
> तो अच्छा लगता है।  
> क्योंकि तुम्हें समझाना भी  
> तो अच्छा लगता है।** 
> 
> **जब पढ़ने में दिल लगाती हो,  
> तो अच्छा लगता है।  
> और कभी पढ़ते पढ़ते योंही सो जाती हो,  
> तो अच्छा लगता है।** 
> 
> **जब जाते हुए रोक लेती हो,  
> तो अच्छा लगता है।  
> और जब खुद जाते रुक जाती हो,  
> तो अच्छा लगता है।** 
> 
> **तुम्हारा सॉरी कहना तो नहीं,  
> मगर जब तुम सॉरी कहती हो,  
> तो अच्छा लगता है।** 
> 
> **जब सोते वक्त तुम्हारा ख्याल आता है  
> तो अच्छा लगता है।  
> और जब उठते ही दिल तुम्हारी याद दिलाता है  
> तो अच्छा लगता है।** 
> 
> **लाल, कला, नीला, बैंगनी,  
> किसे परवाह।  
> कुछ भी पहनो, तुम पर सब कुछ ही  
> तो अच्छा लगता है।** 
> 
> **मेरे सिवा कोई और तुमसे बात करे,  
> मुझे पसंद नहीं।  
> लेकिन तुम जिस किसी से बात करती हो,  
> तो, वो भी अच्छा लगता है।** 
> 
> **कई बार कोशिश की पास बैठूं,  
> लेकिन हिम्मत ही ना कर पाया।  
> तो तुम्हें दूर से देख भी लूं,  
> तो अच्छा लगता है।** 
> 
> **जब एक नजर देख भी लेती हो,  
> तो अच्छा लगता है।  
> और जब उन्ही नज़रों को याद कर दिन भर खिलखिलाता हूँ,  
> तो अच्छा लगता है।** 
> 
> **अच्छा तो लगता है,  
> पर डर भी लगता है।  
> बेचैनी होती है,  
> और दर्द बढ़ जाता है।** 
> 
> **कुछ दिनों में तुम नहीं होगी पास,  
> कहीं दूर चली जाओगी।  
> तुम तो ना रहोगी,  
> बस तुम्हारीं यादें रह जायेंगीं।  
> और जो बातें आज अच्छी लगती हैं,  
> कल वही बातें रुलायेंगी।** 
> 
> **~रबी**

\[ The faces that you make,  
makes me feel good.  
And when a smile comes on that very face,  
it feels good.

When you forget something in between,  
it feels good.  
And then you remind me the same thing,  
it feels good.

When there is something to do, that you can't,  
it feels good.  
Because making you understand,  
is what feels good.

When you go bury yourself in studies,  
it feels good.  
And when the same studies puts you to sleep,  
it feels good.

When you make me stop as I depart,  
it feels good.  
And when you stop while getting apart,  
it feels good.

I don't like it   
when you say sorry to me,  
but the way you sorry,  
really feels good.

When you come in thoughts while going to sleep,  
it feels good.  
And when the heart reminds you first thing in morning,  
it feels good.

Red, Black, Blue, Voilet,  
Who cares?  
Whatever you wear, on you  
Everything looks good.

I don't like it   
when someone else talks to you  
But when you talk to someone,  
they also start to feel good.

Thought many times, to sit near and talk,  
couldn't gather the courage to do so,  
But seeing you even from far off,  
feels good too.

When you look at me even once,  
it feels good.  
And when I keep smiling on those looks,  
it feels goood.

Yes, it feels good.  
But it's scary too.  
The increasing anxiety drowns,  
And pain inside continuously growls.

In few days you won't be near,  
would have far gone from here.  
Your memories will all I be left with,  
And the things that feel good now  
Will make me cry, forever. \]
