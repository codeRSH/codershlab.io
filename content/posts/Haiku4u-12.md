---
public: true
title: Haiku4U - 12
date: 2019-04-12
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-12.jpg)

> **My love  
> Needs yours  
> to be alive.**
>
> **~RavS**


_##  We can’t quantify how much we love each other. And I am never going to keep a balance sheet of what all things you did for me, and what all I did for you._

_But this is also a fact, that I can’t take this relationship forward alone. I would need your love, as much and as hard as you possibly can love, to be able to keep the flame of our relationship burning forever. ##_
