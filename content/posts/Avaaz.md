---
public: true
title: आवाज़
date: 2011-11-16
tags:
  - romantic
categories: hindi
---

> **क्या तूने कहा किसे खबर किसे मालूम,**  
> **मैं तो तेरी आवाज़ ही सुनता रह गया…**
> 
> **~रबी**


\[ What you said, who knows, who can tell?  
I was just listening to your voice… \]
