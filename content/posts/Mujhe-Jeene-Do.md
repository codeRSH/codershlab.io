---
public: true
title: मुझे जीने दो
date: 2014-08-16
tags:
  - peace
categories: hindi
---

![ ](../images/mujhse-jeene-do.jpg)

> **ये फ़िज़ा में ज़रा ज़रा सा,**  
> **धुंआ जो घुला घुला सा,**  
> **हवा में कुछ सुगबुगाहट भी है,**  
> **मेरे होने की शायद आहाट ही है,**  
> **बस अब शोर न करो,**  
> **कुछ देर… कुछ देर मुझे जीने दो।**
> 
> **~रबी**


\[ It’s in the air a little bit,  
Fumes which is mixed,  
There are a few murmurs in the air,  
It’s probably the signal of my presence,  
Now, please don’t make any noise,  
Just let me… Let me live for sometime. \]
