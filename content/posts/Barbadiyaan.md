---
public: true
title: बरबादियाँ
date: 2014-03-01
tags:
  - love
  - disaster
categories: hindi
---

> **मैं क्या प्यार करूँगा तुझे, जो मुझे खुद से इश्क़ नहीं ,**  
> **मैं क्या दे पाउँगा तुझे, जो मेरे पास खुद के लिए कुछ नहीं ,**  
> **आना है तो आ, नयी बरबादियाँ दिखाऊं तुझे,**  
> **या पूछ ले जाके उनसे, जो तुझसे पहले तबाह हुए, वो आज तक खुश नहीं।**
> 
> **~रबी**


\[ How will I love you, when I don’t love myself,  
What will I give you, when I don’t have anything for self,  
If you still come in life, let me show you the new limits of disaster,  
Or go ask others, who got destroyed before you, they are still not happy with themselves. \]
