---
public: true
title: Emptiness
date: 2018-10-12
tags:
  - sad
categories: english
---

![ ](../images/emptiness.png)

> **You know what emptiness is?**  
>   
> **Emptiness is sometimes how you define,  
> The longing, the yearning, the endless plight.  
> Emptiness is being numb to the surrounding,  
> Trying to press a thorn in the nerves,  
> To remind yourself of what it means to be alive.**  
>   
> **Emptiness is being never satisfied,  
> with what you have, always craving more,  
> Trying to surround yourself with what’s outside,  
> but not satiating what you feel inside.**  
>   
> **Emptiness is what breaks you down,  
> And keeps you down.  
> Bit by bit, little by little,  
> All the hopes, the will, the spirit imploding,  
> somewhere deep inside your gut.**  
>   
> **Emptiness is being sucked out of your happiness,  
> And someone else’s desires forced upon you,  
> Emptiness is being a slave to your habits,  
> and not really knowing what to do.**  
>   
> **You try hard, really hard to explain,  
> Someone please come and take me away.  
> Please come now and fill me,  
> And free me from my hollowness.**  
>   
> **That is what’s called emptiness.**
> 
> **~RavS**


_## When you feel hollow inside. ##_  
