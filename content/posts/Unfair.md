---
public: true
title: Unfair
date: 2015-02-18
tags:
  - unfair
  - free-verse
categories: english
---

![ ](../images/unfair.jpg)

> **My Friend,**  
>               **I see you everyday,**  
>               **I think about you everyday,**  
>               **After getting up and before sleeping.**  
>               **I care so much about you, but I can’t tell,**  
>               **I fear I would lose you,**  
>               **As Love, but also as a friend.**  
>               **You can’t imagine what it feels,**  
>               **To hear someone else’s name from your lips,**  
>               **And to pretend to be elated about it.**  
>               **I wish I could take you away,**  
>               **And make you fall in love with me.**  
>               **I wish I could make you laugh n’ laugh ,**  
>               **Until you start begging no more please.**  
>               **I wish I didn’t have to eat my words,**  
>               **Every time you asked “who’s she?”**  
>               **I wish I didn’t have to wish so much,**  
>               **I wish it could have all come true.**  
>               **I know it’s unfair to you,**  
>               **I know you would think I deceived you,**  
>               **But try to understand my difficulty,**  
>               **If you could.**  
> **I am sorry, but I love you.**
>
> **~RavS**
