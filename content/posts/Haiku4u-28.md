---
public: true
title: Haiku4U - 28
date: 2019-04-28
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-28.jpg)

> **Madly in love,  
> Or Mads in love.  
> Not sure.**
>
> **~RavS**


_## At times, this Love between us feels maddening.  It takes the extremes I haven’t felt in any other relationship._

_But sometimes, I feel we are just two idiots who have found each other and now claiming we have found that all encompassing love. I don’t know. I guess we can’t find out right now. It will take a lifetime.  ##_
