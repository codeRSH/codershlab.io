---
public: true
title: इतनी शिद्दत
date: 2012-09-17
tags:
  - life
  - happiness
  - jealousy
categories: hindi
---

> **जिए तू ज़िन्दगी इतनी शिद्दत से,  
> ख़ुदा करे, तू इतना ख़ुश रहे,  
> ना माचिस ना तेल की जरूरत पड़े,  
> साली दुनिया तेरी ख़ुशी देख कर ही  जल मरे।**
> 
> **~रबी**

  
\[ I wish you live life in such a way,  
God willing, you remain so happy, so gay,  
No need of matchsticks or oil anyway,  
Let the whole world burn out of jealousy. \]
