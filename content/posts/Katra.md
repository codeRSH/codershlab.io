---
public: true
title: कतरा
date: 2014-08-05
tags:
  - drop
categories: hindi
---

> **एक कतरा तेरी आँखों से बहे तो सही, दुनिया को आग न लगा दूँ तो कहना,**  
> **एक लम्हे की जरूरत है, तू दे तो सही, सारी ज़िन्दगी न बिता दूं तो कहना।**  
> **छोड़ बीतीं बातों को, अब मैं बदल गया हूँ ,**  
> **अगर अब भी फिर बदल जाऊं, तो कहना।**
> 
> **~रबी**

  
\[ Let a single tear drop from your eyes, I swear I will burn the whole world,  
I need just a moment, you just give that to me, I swear I will live my whole life.  
Leave the talks of the past, I have changed,  
And I swear, I will never change again. \]
