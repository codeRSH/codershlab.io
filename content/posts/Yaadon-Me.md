---
public: true
title: यादों में
date: 2014-04-10
tags:
  - separation
  - world
categories: hindi
---

> **ऐ मुझसे दर्द-ए-जुदाई की उम्मीद रखने वालों,**  
> **कभी मेरी रुखसत-ए-दुनिया पर दो आंसूं बहा लेना।**  
> **ऐ मेरी आह पर वाह वाह करने वालों,**  
> **कभी कबर् पर आकर दो जुगनू जला देना।**  
> **मैं आज ज़िंदा हूँ, कल का मुझे होश कहाँ,**  
> **जो न रहूँ, एक याद समझ यादों में जिला लेना।**
> 
> **~रबी**

  
\[ Hey all of you who expect me to be pained by your separation,  
When I separate from the world, please try to shed 2 tears,  
Hey all of you who applause when I scream through my guts,  
On my grave, please do come and burn 2 candles,  
I am alive today, I am not sure about tomorrow,  
If am not there, please do keep me alive in your memories. \]
