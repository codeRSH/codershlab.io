---
public: true
title: खरीद ही लिया!
date: 2013-01-14
tags:
  - romantic
categories: hindi
---

> **लब्ज़ गिरते हैं उनके लबों से,**  
> **शहद टपके है जैसे होटों से।**
>   
> **झिल्मिलातीं हैं आँखें उठती-झुकती पलकों से,**  
> **जुगनू झांकें हैं जैसे झरोखों से।**
> 
> **मचल पड़ती है बालियाँ कभी-कभी कानो में,**  
> **पत्तें लेते हों करवटें जैसे आहट-ए-हवाओं से।**
> 
> **बनते जाते हैं निशाँ गीली मिट्टी में कदमो के,**  
> **कोरे कागज़ पे उडेलता हो कोई अफसाना सिहाइयों से।**
>   
> **… और सबसे बढ़कर, इतराने की उनकी ये अदा,**  
> **कसम से मेहरुल, आज तो तुमने बन्दे को खरीद ही लिया!**
> 
> **~रबी**


\[ The words drop from her lips,  
As if honey is dripping from mouth.  
  
The eyes sparkle under the blinking eyelids,  
As if fireflys are gliding through the shadows.  
  
And her earrings swing sometimes,  
Like leaves dance in the presence of breeze.  
  
Those feet keep making the marks on the wet soil,  
As if on paper, a writer is throwing ink.  
  
… And the most of them all, the way she pouts,  
I swear Mehrul, I am sold to you now. \]
