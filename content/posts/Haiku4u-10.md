---
public: true
title: Haiku4U - 10
date: 2019-04-10
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-10.jpg)

> **Yesterday was regret.  
> Tomorrow is anxiety.  
> Today we breath together.**
>
> **~RavS**


_## It’s gonna remain like this forever. We will regret the mistakes of our pasts. Time lost in fighting, opportunities lost due to misunderstandings._

_And we will always be anxious of our future. Will we be together always? Will we achieve our dreams? Will we keep each other happy?_

_In all this, I hope we don’t forget to live our ‘todays’. Because every tomorrow  slowly keeps becoming today and every today slowly creeps into the oblivion of yesterday. I am afraid we might not be left with ‘today’ one day.  ##_
