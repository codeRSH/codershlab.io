---
public: true
title: NOTHING
date: 2014-12-31
tags:
  - free-verse
  - fear
  - abandonment
categories: english
---

![ ](../images/nothin.png)

> **I fear one day I won’t be left with anything to say or write or express.**  
> **No jokes, no wits, no stories, no compliments, no confessions, NOTHING.**  
> **There will be just silence left.**  
> **A blank expression.**  
> **I fear if you would get bored of me that day.**  
> **And leave me to find someone who could still entertain you or be useful.**  
> **I won’t be angry with you. How could I?**  
> **It’s just that I would be infuriated with self,**   
> **I couldn’t keep you longer than this.**  
> **I thought you would stay forever.**  
> **But forever is always too long to be true…**
>
> **~RavS**
