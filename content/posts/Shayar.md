---
public: true
title: शायर
date: 2013-02-20
tags:
  - poet
  - ridicule
categories: hindi
---

> **मैं शायर हूँ,**  
> **मुझे बदनामी का डर नहीं,**  
> **पर ऐ बदनाम करने वाले,**  
> **मुझे बदनामी की वजह तो बता सही,**  
> **अक्सर बदनाम वो ही करते हैं मुझे,**  
> **जिन्हें मेरी शायरी समझ में आती ही नहीं।**
> 
> **~रबी**


\[ I am a poet,  
I don’t fear being ridiculed,  
But hey you, making fun of me,  
Tell me why you are ridiculing,  
Because mostly only those laugh at me,  
Who don’t understand a word, and that’s the irony. \]
