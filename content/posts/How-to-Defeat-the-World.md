---
public: true
title: How to Defeat the World
date: 2014-11-19
tags:
  - world
  - dedication
  - special
categories: english
---

> **​So you want to defeat the world?**  
> **That’s a heroic endeavour Supergirl.**  
> **But I wonder if that’s what God sent you on Earth for.**  
> **No I don’t doubt your capability even a little.**  
> **Half of you is more than enough stacked against the world.**  
> **The world will try to put all its forces against you precious,**  
> **It will cajole, it will scare, it will change its colors,**  
> **But the blood flowing under your skin is thick and stubborn.**  
> **I know you will stand your ground, take on every weapon in their arsenal,**  
> **Defeat each and everyone of them, and finally win the battle,**  
> **But know this, even if you win the battle, you will lose the war.**  
> **The sole aim of the world is to not let you achieve what you set out for.**  
> **And it would win, if its pettiness gets you entangled,**  
> **Messing with it, is not how you defeat the world.**
> 
> **Believe me, the world does not have any personal agenda against you my popsicle.**  
> **It just does not want anyone to try anything out of ordinary,**  
> **You will be the apple of their eyes till you continue embracing mediocrity.**  
> **But just as you put a single step out of your comfort zone,**  
> **The world gets afraid that you will reach the stars and they will be left gazing the moon.**  
> **So the world gets jealous, angry and vindictive,**  
> **It wants to remould you back into being obedient and submissive.**  
> **Don’t get upset, it’s natural to feel the resistance,**  
> **Every time someone disturbs the status quo trying something different.**  
> **It’s a basic law of Universe, every action has an equal and opposite reaction.**  
> **Still you act, act you must. But first decide the right direction.**
> 
> **You want to conquer the world little girl?**  
> **Then stop caring about this self centred world,**  
> **Because the world cares little about you,**  
> **Concentrate on your goals, the dreams you want to pursue,**  
> **For when you reach there and look down the abyss,**  
> **You will see the world at your feet,**  
> **Conquered, demolished, and defeated.**  
> **And then you will smile, look at the sky and say, “There God, I did it.” :)**
> 
> **~RavS**
