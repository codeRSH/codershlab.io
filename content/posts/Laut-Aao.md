---
public: true
title: लौट आओ...
date: 2014-12-25
tags:
  - plead
  - special
categories: hindi
---

> **वहशियत तो ना कम कभी हुई है, ना कभी होगी।**  
> **सुकून तो है वहां पे, पर ज़िन्दगी तो यहीं इंतज़ार कर रही।**  
> **दर्द जो तुम्हें है, है तकलीफ मुझे भी।**  
> **तुम्हें ज़िल्लत सहने का, मुझे वादा ना निभा पाने की।**  
> **डर है तुम गुमनामी के अंधेरों ना खो जाओ कहीं ,**  
> **और मैं नहीं ढूँढ पाऊँगा तुमको सूरज की रौशनी में भी।**  
> **चाहता तो बहुत था बनना, पर मैं इंसान हूँ तुम्हारा खुदा नहीं।**  
> **पर मैं साथ रहूँगा जितना भी उसने मुझे ताकत है दी।**  
> **कसम है मुझे मेरी खुशियों की, जो तुम्हारे काम आने में कहीं कोई कमी रह गयी ,**  
> **तो मान जाओ, खुद के लिए नहीं तो मेरे लिए ही सही।**
> 
> **लौट आओ … लौट आओ … दिल से कह रहा हूँ।**
> 
> **~रबी**


\[ The cruelty was always there and it will continue to be.  
Yes, you have found peace there, but life is waiting here only.  
If you are in pain, I am also in agony.  
You are enduring the shame, broken promises is what makes me guilty.  
I am afraid you might get lost somewhere in the shadows of oblivion,  
And I won’t be able to find you, even under the brightness of the sun.  
I so wanted to be, but I am only human not your God.  
But I’ll stick with you, with whatever strength I have got.  
I swear on my happiness, if I lack in my efforts to help you,  
So please agree, if not for yourself, at least for me.

Come back… Please come back … I really mean it. \]
