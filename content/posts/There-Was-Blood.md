---
public: true
title: There was blood.
date: 2014-01-12
tags:
  - blood
  - dream
  - death
categories: english
---

> **I was alright, as I laid asleep,**  
> **Then suddenly the wound ripped,**  
> **And it started to bleed,**  
> **First the body dripped in blood,**  
> **Then the pillow, then the sheet.**
> 
> **I was in writhing in pain so much,**  
> **Tried, I couldn’t even scream,**  
> **The blood started spilling; drop-by-drop-by-drop,**  
> **Every ounce wanted to ooze out of my frame.**  
> **I were to drown in my own pool of blood.**  
> **And my bed was to become my own grave.**
> 
> **I struggled like a slayed lamb,**  
> **But I could barely lift my hand,**  
> **In fear I didn’t even dared to cry,**  
> **I knew only blood were to come out of my eyes,**  
> **There was more blood over me, than left inside,**  
> **I had started tasting crimson, by now,**  
> **I knew I wasn’t going to survive.**
> 
> **My will to live, had lost to my dead bodily fluid,**  
> **So I succumbed, accepted my defeat,**  
> **A thought ran through the mind : This Is It.**  
> **If I were to be killed my own blood,**  
> **Then let it be.**  
> **And suddenly it wasn’t so painful anymore,**  
> **I closed my eyes, and it was liberating.**  
> **“Guy Killed By Red”,**  
> **I couldn’t have hoped for a death more exciting.**
> 
> **And then I woke up, and realized I was just dreamin’,**  
> **And instead of blood, it was sweat, I was drownin’ in.**  
> **Relieved I got up and stormed to the basin,**  
> **I splashed water on my face,**  
> **And took a huge sigh of relief,**  
> **Then in the mirror, suddenly I noticed,**  
> **The wound had ripped.**  
> **And it had started to bleed.**
>
> **~RavS**


_##  This poem was written after an actual nightmare. While it wasn’t as dramatic as I have tried to portray here, or even related to blood or dying, it was nightmarish enough to keep me awake and disturbed for several hours. People often wish “sweet dreams”. I only experience the weird or disturbing. ##_
