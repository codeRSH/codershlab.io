---
public: true
title: Haiku4U - 29
date: 2019-04-29
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-29.jpg)

> **Like raindrops  
> on window pane.  
> Stick with me please.**
>
> **~RavS**


_## What else can I ask at the core of it all? Just this… that you stick with me forever. Just like raindrops seem to be going away from the Window Panes, but they never leave. They are there, leaving a trail behind. Sticking, till their last shred of existence. ##_
