---
public: true
title: Loaf of bread
date: 2014-09-08
tags:
  - anger
categories: english
---

> **For each loaf of bread you provide,**  
> **You add another link in the chain on my thighs,**  
> **You made me a cripple, I wonder what was it all worth,**  
> **Wouldn’t it have been better to have starved and died.**
> 
> **~RavS**
