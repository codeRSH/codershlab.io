---
public: true
title: कागज़
date: 2014-06-07
tags:
  - blood
  - tears
categories: hindi
---

> **जो कागज़ हमने रात भर लहू से भरे थे,**  
> **वो सुबह तुम्हारे आंसुओं से भीगे पड़े थे।**
> 
> **~रबी**

\[ The pages that I filled my blood whole night,  
In the morning they were soaked in your tears. \]
