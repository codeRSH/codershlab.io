---
public: true
title: शीशा
date: 2012-07-30
tags:
  - wish
  - betrayal
categories: hindi
---

> **क्या होता गर, इस जहां हर इन्सां शीशा बन जाता,  
> हर शख्स में अक्स बस तेरा ही नजर आता,  
> हर वक्त बस तू ही आँखों में कैद रहता,  
> शीशे से उतार तुझे पलकों में समेट लेता,  
> पर अगर किसी दिन तू मुझे अकेला छोड़ जाता,  
> मैं तेरे साए का हर एक आइना तोड़ देता।**  
> 
> **~रबी**


\[ What would happen, if whole world were to become a mirror,  
I would see your reflection in every person,  
You would remain a captive in my eyes,  
I would have drawn you from the mirror into my eyelids,  
But if you would have left me alone someday,  
I would have broken all those mirrors. \]
