---
public: true
title: Haiku4U - 11
date: 2019-04-11
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-11.jpg)

> **Anger, Ego, Lust, Differences.  
> I’ll kill everything  
> That comes  between us.**
>
> **~RavS**


_## A lot of things come between us at different times. But to me, you are far important than any of those things. I am not gonna let any of them become a cause of separation between us. ##_
