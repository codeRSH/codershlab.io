---
public: true
title: कश-म-कश
date: 2014-07-17
tags:
  - 30-day-challenge
categories: hindi
---

> **कौन रूप लूँ अपना,**  
> **जो सच है या जो दिखावा है ?**  
> **कौन सी बात कहूँ तुझसे,**  
> **जो दिल में है या जो छलावा है ?**  
> **ये जद्द-ओ-जेहद, ये कश-म-कश,**  
> **मेरे लिए पहले मेरी खलिश है,**  
> **या तेरा चैन पहले आता है…**
> 
> **~रबी**

  
\[ Which form should I take,  
The one which’s true or the one which’s a sham?  
Which thing should I say,  
The one which’s in heart, or the one which’s a mirage?  
This struggle, this indecision,  
For me, does my pain come first,  
or your peace takes the first place…\]
