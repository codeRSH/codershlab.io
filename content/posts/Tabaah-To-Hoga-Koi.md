---
public: true
title: तबाह तो होगा कोई
date: 2015-01-06
tags:
  - judgement
categories: hindi
---

> **तबाह तो होगा कोई,**  
> **जो तू ज़िन्दगी में आई है,**  
> **दुनिया फना होगी,**  
> **या बर्बाद रहूँगा मैं,**  
> **ये आगाह होना बाकी है।**
> 
> **~रबी**


\[ Somebody would get destroyed,  
Now that you have you come in life,  
Whether the world gets ruined,  
Or I get devastated,  
Is something still to be determined. \]
