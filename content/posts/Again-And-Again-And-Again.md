---
public: true
title: "Again and again and again (Explicit)"
date: 2014-08-02
tags:
  - explicit
  - life
  - struggle
categories: english
---

> **I hope you find, for which you masturbate everyday.**  
> **But don’t drag me in your fantasies, I’ve my own hell to take care.**  
> **Don’t be an emotional parasite and drain me out.**  
> **Oh no baby, I am not your sex-doll to fuck around.**  
> **Yeah fine your girlfriend jilted you,**  
> **I wonder why your parents screwed up too.**  
> **Why do you want me to clean up every time you spill shit,**  
> **You didn’t listen to me, so that’s what you deserved - bitch,**  
> **So cry your tears of croc or grab your kicked crotch,**  
> **But leave me alone now, I don’t care much,**  
> **Can’t you look through your eye sockets and watch me incinerate in pain?**  
> **So why do you ask again and again.**  
> **And again and again and again …**  
>   
>   
> **I have been fucked up by universe so many times,**  
> **May be I have become a slut practically speaking,**  
> **The only virginity left in me is not being malign,**  
> **The emotional arousal they pump in, might break even that hymn.**  
> **I try hard, but the rage in me refuses to sink,**  
> **Why am I dealt the cards I can not play with?**  
> **Why was I lied, what did they get to cheat,**  
> **Why couldn’t they let me Be, when I could have lived?**  
> **Now I am a jigsaw puzzle whose pieces have been missed.**  
> **It’s too late for a stinky carcass to be fixed,**  
> **HEYYY… Just go live your fucking life and abandon me sulking here,**  
> **Pour some gasoline at the steps and flare up a matchstick.**  
> **Leave me scorching in flames and lock the bloody gates.**  
> **Let me die, shout - Burn and Burn.**  
> **And burn and burn and burn…**  
>   
>   
> **No I don’t need saving any more,**  
> **I think I have started enjoying this hell hole,**  
> **Fate put me in this spot, it’s my time to take my revenge,**  
> **I refuse to make the lemonade out of the life’s served lemons,**  
> **The fate is too strong and may be the time is long gone,**  
> **But I won’t let the destiny write my life’s final pages.**  
> **I will char myself to death if that’s what it’s going to take.**  
> **You also come and join the world as they watch me strip naked,**  
> **And laugh and clap like them as I slowly annihilate,**  
> **And when the show is over and you come to your hungover senses,**  
> **Your guilt trip might send you crawling back to collect my soiled ashes,**  
> **Bawl against it, put it on your face or throw it in the gutter,**  
> **I just don’t care. I don’t care.**  
> **Don’t care. Don’t care. Don’t care.**
>
> **~RavS**


_## My most profane-full creation yet. Wasn’t sure if I should make it public, but it almost accurately describes what I feel sometimes. I hope people who shouldn’t read it, don’t get to read it. ##_
