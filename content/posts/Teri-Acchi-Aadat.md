---
public: true
title: तेरी  हर  अच्छी  आदत  को  मैं  खराब  कर  दूंगा ...
date: 2011-12-08
tags:
  - main-blog
  - special
categories: hindi
---

> **मत  आ  मेरे  पास, तुझे  मैं  बर्बाद  कर  दूंगा।  
> तेरी  हर  अच्छी  आदत  को  मैं  खराब  कर  दूंगा।**  
> 
> **ऐसा  नहीं  के  मैं  चाहता  नहीं  तुझे,  
> बस  खुद  पर  जरा  भी  भरोसा  नहीं  मुझे,  
> डराता   है  मुझे  खुद  का  ही  जुनूँ,  
> तू  खिलौना  है  मेरा, तुझे  ना  तोड़  मैं  बैठूं,  
> खुद  धधक  रहा  हूँ  अन्दर  ही  अन्दर,  
> तुझे  भी  मैं  राख  कर  दूंगा,  
> तेरी  हर  अच्छी  आदत  को  मैं  खराब  कर  दूंगा।**  
> 
> **हंसी  भागती  है  दूर  मुझसे,  
> सुकून  से  कोई  नाता  नहीं,  
> खुद  इतनी  बार  फिसला  हूँ,  
> तुझे  गिराना  मैं  चाहता  नहीं,  
> खुद  नहीं  पाक, रही  तू  गर  साथ,  
> तुझे  भी  मैं  नापाक  कर  दूंगा।  
> तेरी  हर  अच्छी  आदत  को  मैं  खराब  कर  दूंगा।**  
> 
> **पाने  के  लिए  तुझे  तो  उस  रब  से  भी  लड़  सकता  हूँ,  
> मगर  लाकर  तुझे  अपने  दर्द  के  सिवा  कुछ  ना  दे  सकूँगा,  
> खुद  गम  में  ही  जिया  हूँ  जिंदगी,  
> तुझे  भी   ख़ुशी  का  मोहताज  कर  दूंगा।  
> तेरी  हर  अच्छी  आदत  को  मैं  खराब  कर  दूंगा।**  
> 
> **इतना  सा   रहम   कर  मुझपे,  
> अगर हो सके, ना  कर  मोहब्बत  मुझसे,  
> ये  कर  गलती  ना  चख  मुझे,  
> तुझे  मैं  तेज़ाब  ही  मिलूँगा,  
> तेरी  हर  अच्छी  आदत  को  मैं  खराब  कर  दूंगा...**  
> 
> **~रबी**


\[ Don't come near me, I will ruin you.  
I will spoil your every good habit.  

It's not that I don't desire you,  
I just don't trust myself at all,  
My own passion scares me,  
You are my toy, I fear I might break you,  
I am burning inside,  
I will turn you to ashes too,  
I will spoil your every good habit.  

Laughter runs away from me,  
I have no connection with peace,  
I myself have slipped so many times,  
I don't want to make you fall,  
I am not pure myself, if you stay with me,  
I will make you impure too.  
I will spoil your every good habit.  

To have you, I could even fight with God,  
But having you, I would give you nothing but my pain,  
I have lived my life in sorrow,  
I will make you crave happiness too.  
I will spoil your every good habit.  

Have this much mercy on me,  
If possible, don't love me,  
Don't make the mistake of tasting me,  
You will find me to be like acid,  
I will spoil your every good habit... \]
