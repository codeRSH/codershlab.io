---
public: true
title: "तुम मेरी जान ही क्यों नहीं ले लेते?"
date: 2014-06-06
tags:
  - nightmares
categories: hindi
---

> **क्यों सोने नहीं देते,**  
> **क्यों रोने नहीं देते,**  
> **क्यों हंसने नहीं देते,**  
> **क्यों खुश होने नहीं देते।**
> 
> **जो मैंने तुम्हे छोड़ा,**  
> **तुम मुझे छोड़ क्यों नहीं देते,**  
> **जो धुंधली पड़ गयी तुम्हारी यादें,**  
> **आँखों पर से अपना अक्स हटा क्यों नहीं लेते,**  
> **देखो अब तो खुद से भी नफरत हो चली,**  
> **तुम खुद से नफरत करने क्यों नहीं देते?**  
>   
> **राख किये कल को एक अरसा हो गया,**  
> **तुम अस्थियां भिगोने क्यों नहीं देते,**  
> **जो दो कदम जाऊं तुमसे दूर,**  
> **तुम तीसरा कदम लेने क्यों नहीं देते,**  
> **खींच लेते हो सांस हलक से,**  
> **दम घुटता है, पर दम घुटने भी नहीं देते।**
> 
> **क्यों चैन से जीने नहीं देते,**  
> **क्यों सुकून लेने नहीं देते,**  
> **क्या करना ऐसी ज़िन्दगी जी कर फिर,**  
> **तुम मेरी जान ही क्यों नहीं ले लेते?**
> 
> **~रबी**


\[ Why don’t you let me sleep,  
Why don’t you let me cry,  
Why don’t you let me laugh,  
Why don’t you let me be happy.

When I left you,  
Why don’t you leave me too,  
When your memories have become blurred,  
Why don’t you remove your shadow from my eyes,  
See I have started hating even myself now,  
Why don’t you let me hate you?

It’s been a while since I burned our past,  
Why don’t you let me submerge its ashes,  
When I go two steps away from you,  
Why don’t you let me take the third step,  
You take away the breath from wind pipe,  
I agonize, but you don’t let me suffocate too.

Why don’t you let me live in peace,  
Why don’t you let me be relaxed,  
What shall I do of this life then,  
Why don’t you take my life then? \]
