---
public: true
title: चार दिन पहले
date: 2014-02-26
tags:
  - life
  - depression
categories: hindi
---

> **क्या हाल बताएं हम अपना तुम्हें,**  
> **बस साँसें चल रही हैं, ना जाने किसकी दुआएं हैं,**  
> **उठ जाते हैं चिता से हर रोज कुछ लोगों की खातिर आज कल,**  
> **जीना तो वैसे हम चार दिन पहले ही छोड़ चुके हैं।**
> 
> **~रबी**


\[ What shall I tell you about me,  
I am just breathing somehow, don’t know who prays for me,  
I get up from my grave, just for some people nowadays,  
Otherwise I have quit living since four days. \]
