---
public: true
title: गुड़-गोबर
date: 2012-09-30
tags:
  - faaltu
  - bhains
  - humor
  - bihari
categories: hindi
---

> **ऊ  छोड़  गयील  हमका,  हमरी  भैंसिया  के  तरह ,  
> ससुर , सब  गुड़-गोबर  हुई  गवा ,  
> पैहिले - पहेल  हमने  उका  घासिया  नाही  डाली ,  
> बाद  मा  हमरे  पास  कौन  चारा  नये  बचा।**  
> 
> **~रबी**

_Literal Translation:_  
\[ She left me like my buffalo left,  
Father-in-law, all jaggery has become cow-dung,  
At first I didn’t give her grass,   
Later I was not left with any fodder. \]  
  
  
_Meaning:_  
\[ She left me just like my beloved buffalo,  
Damn it, everything is now lost,  
At first I didn’t pay heed to her whims,  
Now there is no chance left  \]
