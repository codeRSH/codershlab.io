---
public: true
title: कोई नहीं
date: 2014-10-18
tags:
  - love
  - desire
categories: hindi
---

> **चाहने वाले तो बहुत हैं यहां,**  
> **‘चाहने’ वाला कोई नहीं।**  
> **तड़पता हूँ मैं तेरे लिए, कहे तो कोई,**  
> **पर अफ़सोस, कहने वाला कोई नहीं।**
> 
> **~रबी**


\[ A lot of people like me here,  
No one to love.  
I desire you so much, I wish someone says,  
But alas, there is no one to say. \]
