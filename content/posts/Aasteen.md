---
public: true
title: आस्तीन
date: 2014-07-19
tags:
  - 30-day-challenge
categories: hindi
---

> **\[ कुछ समय पहले… \]**
> 
> **रखते हैं तुम्हे आस्तीन में छुपाकर,**  
> **पड़ न जाए कहीं फरिश्तों की नज़र,**  
> **ले गए जो अगर तुम्हे हमसे छीनकर,**  
> **कभी सोचा है क्या बीतेगी हम पर।**
> 
> **\[ कुछ समय बाद… \]**
> 
> **नहीं था हमें कोई इल्म, ना थी ऐसी कोई खबर,**  
> **नस नस में घोल दोगे तुम इतना जो जहर,**  
> **तुम को हम छुपाते फिरे, दुनिया भर की नज़रों से,**  
> **डस लिया तुमने हमको ही, आस्तीन का सांप बनकर।**
> 
> **~रबी**


\[ \[ Some time ago…\]

I hide you under my sleeve,  
So that angels don’t get to look at you,  
If they snatch you away from me,  
Have you ever wondered what will I have to go through.

\[ Some time later…\]

I didn’t know, I didn’t have the news,  
You will poison each and every portion of me,  
I kept hiding you from the eyes of the whole world,  
You ended up biting me only, proving a snake under the sleeve. \]
