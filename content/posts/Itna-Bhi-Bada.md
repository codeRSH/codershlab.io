---
public: true
title: इतना भी बड़ा
date: 2012-05-21
tags:
  - humor
  - faaltu
categories: hindi
---

> **तू एक बार हँस दे तो ये दुनिया मैं भुला दूँ …  
> मैं इतना भी बड़ा बावड़ी नहीं हूँ !**
> 
> **~रबी**


\[ You laugh once and I forget this whole world…  
I am not that big an idiot! \]
