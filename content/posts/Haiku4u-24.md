---
public: true
title: Haiku4U - 24
date: 2019-04-24
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-24.jpg)

> **We’ll swim across,  
> Or drown in between.  
> Just be there with me.**
>
> **~RavS**


_## I can’t promise you that everything will turn out to be fine. It’s difficult. Impossible._

_We may end up destroying each other’s lives. But isn’t it worth sacrificing ourselves for each other? Would you be with me even after that? ##_
       