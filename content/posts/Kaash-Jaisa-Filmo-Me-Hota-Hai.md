---
public: true
title: काश जैसा फिल्मों में होता है
date: 2011-11-17
tags:
  - special
  - friends
  - romantic
categories: hindi
---

> **तू ना हो सामने तो बेचैनी इस कदर बढ़ जाती है,  
> सोते हुए जाग जाता हूँ, जागूँ तो नींद नहीं आती है।**
> 
> **और ऐसी कमबख्त हमारी किस्मत भी कहाँ,  
> कि तू सपनों में आये,  
> अब तो बस यही सपना है कि मैं जिंदगी भर सोता ही रहूँ,  
> जो तू एक बार सपने में दिख जाए।**
> 
> **काश जैसा फिल्मों में होता है,  
> तुझे देखते ही ये वक्त थम सा जाता,  
> कुछ पल ही और सही,  
> तुझे आँखों में भरने का वक्त मिल पाता।  
> और ये भी सही पागलपन है,  
> के तेरी हँसी देख पता चलता है कि जिंदगी भी कितनी हसीं है।  
> वर्ना हमें तो बस यही लगता है कि जीते रहो,  
> ये बस जीने के लिए ही बनीं है।**
> 
> **~रबी** 

  
\[ When you are not in front, anxiety overwhelms me so much,  
I wake in the middle of sleep, and can’t go to sleep again.  
  
And this damned luck is not that good,  
that you come in my dreams.  
Now I have only one dream, that I keep sleeping forever,  
once I see you in my dreams.  
  
I wish the life would have been movie-like,  
That time would stop every time I see you,  
So even so just for a few seconds,  
I could fill you in my eyes.  
  
And this also would be called madness only,  
that seeing your laughter I find that the life is so beautiful,  
Otherwise, I used to think, just keep living,  
Life is there just to be lived. \]
