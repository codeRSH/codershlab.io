---
public: true
title: Killed the Humanity
date: 2012-11-10
tags:
  - anger
  - hatred
  - frustration
categories: english
---

> **All I seeked everywhere was love,  
> But it seems it has ceased to exist.  
> All I got was hatred, agony and indifferences,  
> Expect no affection in human, it’s a myth.**  
>   
> **I do not know what to believe in anymore,  
> Ideas, principles, relationships, people,  
> Everything seems to have become a blur,  
> For man might wear fancy clothes, but inside he’s still an animal.**  
>   
> **I embraced people but they injected pain in me,  
> So I embraced the pain instead and killed the humanity within.**
> 
> **~RavS**
