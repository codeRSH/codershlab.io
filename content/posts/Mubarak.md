---
public: true 
title: मुबारक़
date: 2020-09-11
tags: 
 - special
 - life
 - love
categories: hindi
---

> **ज़िन्दगी का नाम ही परेशानी रख देना चाहिए,  
> और परेशानी को ही मुझे अपना फलसफा कहना चाहिए।  
> मगर इस पथरीले इकतरफा फ़साने में एक मोड़ आता है,  
> जहाँ से रास्ता आसान तो नहीं होता, पर चलने का हौसला मिल जाता है।  
> जब बाँटने वाला मिलता है ना, तो परेशानी आधी और ज़िन्दगी पूरी लगने लगती हैं।  
> जब हौसला मिलता है ना, तो रास्ता छोटा और मंज़िल पास लगने लगती है।**  
>   
> **जो तुम मिले इस जनम में, तो ये जनम मुबारक हो मुझे।  
> जो तुम हो इस दिन में, तो ये दिन मुबारक हो मुझे।  
> और अगर तुम साथ हो,  
> तो ये ज़िन्दगी,  
> ये परेशानी,  
> ये रास्ता,  
> ये मंज़िल,  
> सब मुबारक हो मुझे …**  
>   
> **~रबी**  


\[ Life should be renamed as trouble,  
And I should call trouble as my life’s philosophy.  
But sometimes a twist comes in this thorny one-sided story,  
Where the path doesn’t become easy, but we find the courage to walk further.  
When we find someone to share, then troubles seem half and life seems full.  
When you get the encouragement, then the path seems short and the destination nearby.  
  
That I found you in this birth, so this birth is happy for me.  
That you are here with me today, so today is happy for me.  
And if you are there,  
Then this life,  
This problem,  
This path,  
This destination,  
All are happy for me…  \]  
