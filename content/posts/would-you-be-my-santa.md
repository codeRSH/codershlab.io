---
public: true
title: Would You Be My Santa?
date: 2025-01-14
tags:
  - romantic
categories: english
---

![](../images/would-you-be-my-santa.jpeg)

> **Of course you are going to blame me.  
> But do remember I asked you so many times.  
> Had you given a few more pictures I would have been happy,  
> But you made me think as if asking your photos is an unpardonable crime.**  
> 
> **Disappointed, I had to obsess on just one picture, clearly you left me no choice ,  
> And I ended up postmorteming it like a curious  kid tearing up his toy,  
> Now, if you are going to curse me, please be my guest,  
> But you didn't give me your time today  and an idle mind is devil's nest.**  
> 
>  **I always find something mysterious in your pics,  
> I fail to name what's so mystic, so cryptic about it,  
> They unfailingly strum a few strings of incomprehensible emotions,  
> And it's so frustrating when I can't perform its root cause analysis.**  
> 
> **So today determined, and with a lot of time in hand,  
> I pondered for hours on your imagery,  
> I had to find out why I need to see it again and again,  
> So I closely gazed each and every part of you, that I could possibly see .**  
> 
> **First and foremost I went to the most glorious part of your pic,  
> Baby, you shouldn't have smiled.  
> You seldom smile in your photos, you should have again kept your rose petals zipped.  
> But of course you showed your pearls in this one, and they are my restlessness's culprit.**  
> 
> **And that chin, Ahh.. My God.  
> Why is it so... delicate,  
> I just wanted to feel its crests and troughs,  
> But screw this phone's flat surface.**  
> 
> **And I guess you were quite happy,  
> Your  eyes, they are so uncharacteristically half closed,  
> Your last one's were so large, they kept twinkling at me,  
> as if asking me for something,  
> I would try staring back at them, but would always lose.**  
>  
> **And tell me about your eyebrows girl,  
> Those long lines over your eyes' kajal,  
> If I were to describe them, I would call them the protector,  
> Eyes safe under someone's 'Anchal'.**  
> 
> **And please keep your cheeks and nose safe from me,  
> One day they will definitely get pinched or squeezed,  
> Don't roll your eyes, have you ever seen a plum or a red cherry,  
> Don't you desire to touch and press it a little?  
> Then why judge me,  
> If I yearn to do exactly.**  
> 
> **In the end, a few words about your hair,  
> I tried counting each and every strand,  
> But I lost count somewhere,  
> between fifty eight and fifty nine.  
> Ok so, they are long, they are illustrious,  
> But I won't say a word more, let's just say they make me envious.**  
> 
> **See, I don't believe in Santa Claus,  
> So I don't wish for anything,  
> But, this right there, trust me baby,  
> is the cutest Santa I have ever seen,  
> And if you were to be my Santa every year,  
> Right here right now, I would change my belief forever.**  
> 
> **So would you be my Santa? PLEASE.**  
>
> **~RavS**  


_## A poem describing a girl in an old photo. The photo above is (obviously) AI generated (Credit Meta AI) and acts as placeholder for the real photo. ##_
