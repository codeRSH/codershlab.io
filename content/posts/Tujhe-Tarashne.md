---
public: true
title: तुझे तराशने में उसको बहुत वक्त लगा होगा...
date: 2012-02-18
tags:
  - main-blog
  - romantic 
categories: hindi
---

> **काजल के घेरे में महफूज़ दो आँखें,  
> ठिठुरती सर्दी में सुकून देती वो साँसे,**
> 
> **झुककर उठतीं, उठकर झुकतीं पलकें,  
> उँगलियों से पूछतीं, घर का पता भूल बैठी लटें,**
> 
> **नर्म होटों पर पनाह लिए कुछ अलफ़ाज़,  
> कानों में मिसरी बन घुलती एक आवाज़,**
> 
> **चेहरे पर रुक रुक कर आती एक मासूम मुस्कान,  
> गालों पर पड़ते गड्ढे, झूमती बालियों से सजे कान,**
> 
> **केशों में फँसी ओस की बूंदे,   
> तेरी बाहों पर झूलने को बेचैन नज़र आती हैं,  
> पाओं में पड़ी पायलें,  
> इक इक कदम पर तेरे होने का एहसास कराती हैं,**
> 
> **बेशक तुझे तराशने में उसको बहुत वक्त लगा होगा.  
> आज तुझसे ज्यादा उस बनाने वाले पर प्यार आता है।** 
> 
> **~रबी** 


\[ Two eyes safe within circles of kohl,  
Those breaths, giving solace in the shivering cold,  

Eyelids that droop and rise, rise and droop,  
Strands of hair seem asking for their way home from fingertips,  

Some words taking refuge on soft lips,  
A voice dissolving like rock candy in the ears,  

A naive smile appearing intermittently on the face,  
Dimples forming on the cheeks, ears adorned with swaying earrings,  

Dewdrops caught in the hair,  
Seem restless to swing on your arms,  
Anklets adorning your feet,  
Make me feel your presence with every single step,  

Undoubtedly, it must have taken Him a lot of time to sculpt you,  
Today, I feel more love for the Creator than for you. \]
