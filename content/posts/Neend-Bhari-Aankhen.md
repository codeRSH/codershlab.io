---
public: true
title: "नींद भरी आँखें "
date: 2014-12-19
tags:
  - agony
  - special
categories: hindi
---

> **रोने को ना आंसूं दिए, ना कांधा दिया तुमने ,**   
> **मैं सो नहीं पाता हूँ अक्सर, बस अपनी नींद भरी आँखें दे दे।**
> 
>   
> **पूछो ना भगवान से अपने, मुझे मेरी खता तो बता दे ,**  
> **जिन्हे मैं थोड़ा सा चाहने लगता हूँ, भगवान को बस उन्ही से क्यों दिक्कत हो जाती है।**  
> **उन्हें पता है मुझसे अपनों की तकलीफ बर्दाश्त नहीं होती, फिर बस उन्हें ही क्यों दर्द देते।**  
> **मेरे बदले माफ़ी मांग लेना ना दोस्त, मेरी तो वो माफ़ी भी नहीं सुनते …**  
> **रोने को ना आंसूं दिए, ना कांधा दिया तुमने ,**  
> **मैं सो नहीं पाता हूँ अक्सर, बस अपनी नींद भरी आँखें दे दे।**
> 
>   
> **जितना दिल दुखाया है ना तुमने, आज तक किसी ने नहीं किया ,**  
> **जितना ज्यादा सताया है ना तुमने, किसी को मैंने सताने नहीं दिया ,**  
> **तुम तो छोड़ कर ही चल दिए, एक बार तुम पर गुस्सा क्या किया।**  
> **माफ़ कर दो ना दोस्त, अब फिर कुछ नहीं कहूँगा तुम्हे ,**  
> **थोड़ा सा गुस्सा कर लो तुम भी, बस छोड़ कर मत जाओ मुझे …**  
> **रोने को ना आंसूं दिए, ना कांधा दिया तुमने ,**  
> **मैं सो नहीं पाता हूँ अक्सर, बस अपनी नींद भरी आँखें दे दे।**
> 
>   
> **कभी -कभी उदास कभी हँसता देख लेता था तुम्हे, अक्सर आते जाते ,**  
> **खुद खुश रहने का ज़रिया ढूंढ लिया था तुम्हे हँसाते हँसाते।**  
> **अब बोलो किसे जाके हँसाऊँ, तुम्हारी खाली कुर्सी को या खाली बोतल को ,**  
> **अब बोलो कैसे मैं खुश रहूँ, तुम जो अगर अकेले ही तन्हा रोती रहोगी तो।**  
> **मुझे पता था तुम ऐसा ही करोगी मेरे साथ दोस्त, तुम्हारी फितरत पता थी ,**  
> **जब रुकना ही नहीं था ज़िन्दगी में तो आने की जरूरत भी क्या थी।**  
> **दिल के पास किसी को इसलिए ही आने नहीं देता, उनके तोड़ जाने के डर से …**  
> **रोने को ना आंसूं दिए, ना कांधा दिया तुमने ,**  
> **मैं सो नहीं पाता हूँ अक्सर, बस अपनी नींद भरी आँखें दे दे।**
> 
> **~रबी**


\[ You didn’t give the tears, neither the shoulders to cry,  
I am unable to sleep nowadays, at least give me your sleepy eyes.

Please ask your God, to tell me what’s my fault,  
If I start liking someone a little, why does he have problems with only them.  
He knows I can’t bear to see my own in troubles, then why does he hurt them only.

Please apologize to him on my behalf friend, he doesn’t even her my pleas…  
You didn’t give the tears, neither the shoulders to cry,  
I am unable to sleep nowadays, at least give me your sleepy eyes.

  
The hurt that you have given, no one else ever did,  
The way you have tortured, I never let anyone do to me,  
You left me just like that, just because I got angry once,  
I am Sorry my friend, I won’t scold you ever again,  
You also get a little bit angry, but just don’t leave me …  
You didn’t give the tears, neither the shoulders to cry,  
I am unable to sleep nowadays, at least give me your sleepy eyes.

  
I would see you laughing sometimes, sometimes I would see you sad,  
I had found my reason to be happy, by making you laugh again and again.  
Now tell me, whom should I go and make laugh, your empty chair or empty bottle,  
Now tell me, how should I be happy, if you keep crying alone, all by yourself.  
I knew you would do this only my friend, I knew your nature beforehand,  
If weren’t going to stop then what was the need to come in my life.  
That’s why I don’t let anyone come to close to the heart, for fear of them breaking it…  
You didn’t give the tears, neither the shoulders to cry,  
I am unable to sleep nowadays, at least give me your sleepy eyes. \]
