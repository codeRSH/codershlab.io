---
public: true
title: अपना
date: 2012-09-29
tags:
  - wish
  - friendship
  - confinement
categories: hindi
---

> **मेरे बस में होता तो तुझे इस लम्हे में कैद कर लेता,  
> तू लाख मिनत्तें करता, पर तुझे ना जाने देता।    
> मगर मालूम है मुझे, अब तू ‘वो’ नहीं रहा,  
> कैद में तू 'मेरा’ तो हो जाता, पर तुझे 'अपना’ कैसे कहता?**
> 
> **~रबी**  


\[ If it were possible, I would have caged you in this moment,  
You would have pleaded a million times, I wouldn’t have let you be gone,  
But I know, that you are not the same anymore,  
In my confinement, you would have been my 'mine’, but how could have I called you 'own’? \]
