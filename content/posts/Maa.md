---
public: true
title: माँ
date: 2014-08-25
tags:
  - song
  - mother
categories: hindi
---

> **मत दो रोटी मुझको माँ, चाहे भूखा ही रहने दो,**  
> **मत सुलाओ गोद में अपनी माँ, सारी रात चाहे जागने दो,**  
> **मत सहलाओ माथा मेरा माँ, भूत से मुझको डरने दो,**  
> **कर दो बंद कमरे में माँ, अँधेरे में चाहे सिसकने दो।**
> 
> **पर मत नाराज हो माँ,**  
> **मुझे रोना आता है…**  
> **मुझसे तुम नाराज़ मत हो।**  
> **जब तुम नाराज होती हो माँ,**  
> **बहुत बुरा लगता है…**  
> **तुम जब नाराज होती हो तो।**
> 
> **फिर कभी नहीं करूँगा ऐसा मैं माँ, अपनी कसम खाता हूँ,**  
> **तेरा छोटा सा बच्चा हूँ ना मैं माँ, इस बार गलती माफ़ कर दो।**  
> **मुझसे कहा तो नहीं जाता है माँ, तेरे बिन नहीं रह सकता हूँ,**  
> **मारो … सब सह लूंगा मैं माँ, पर हाथ छिटक के ना जाओ।**
> 
> **पर मत नाराज हो माँ,**  
> **मुझे रोना आता है…**  
> **मुझसे तुम नाराज़ मत हो।**  
> **जब तुम नाराज होती हो माँ,**  
> **बहुत बुरा लगता है…**  
> **तुम जब नाराज होती हो तो।**
> 
> **~रबी**


\[ Do not give me food Maa, just let me starve,  
Don’t put me on your lap Maa, let me remain awake whole night,  
Don’t tame my forehead Maa, let the ghosts scare me,  
Put me in a room and close the doors Maa, let me weep silently.

But do not be angry mother,  
It makes me cry …  
Don’t be angry with me please.  
When you are angry mother,  
I feel very bad …  
When you get angry with me.

I will never do it again Maa, I swear,  
I’m your little kid na Maa, forgive my mistakes this time.  
I am not able to say this Maa, but I can not live without you,  
Beat me … I will bear all Maa, just don’t shirk your hand and leave.

But do not be angry mother,  
It makes me cry …  
Don’t be angry with me please.  
When you are angry mother,  
I feel very bad …  
When you get angry with me. \]
