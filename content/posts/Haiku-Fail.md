---
public: true
title: Haiku - Fail
date: 2016-10-26
tags:
  - haiku
categories: english
---

![ ](../images/haiku-fail.jpg)

> **You’re sure you’ll fail.  
> But I’m sure too.  
> I won’t let you.**
>
> **~RavS**


_## I know you are short on confidence. We all are sometimes, and we feel like a failure. We feel we have failed even before we have barely started. But let me assure you even against your belief - till the time I am there, I won’t let you fail come what may. ##_
