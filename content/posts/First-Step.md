---
public: true
title: First Step
date: 2015-02-22
tags:
  - rap
  - special
categories: hindi
---

> **Sitting around, I try to make sense of this life,  
> Sometimes it feels lovely sometimes it feels like a knife,  
> Has been put around my neck,  
> And someone is choking me from inside, as I gasp for breadth.**
>
> **I get up everyday, all pumped up to conquer,  
> The world, but then the external and internal forces conspire and whisper,  
> to each other. They make me lose the battle even before it starts,  
> And that’s why I hate it so much, because I don’t even  get a chance,  
> To fight for the glory, To fight for respect,  
> To fight these demons, Hiding inside my head.**
>
> **I write these stupid lyrics and I feel like burning them,  
> I feel I’ve lost my mojo, my art and I am not getting them,  
> back, I push hard for something of substance to come out,  
> There’s a lot inside but I struggle to express it out loud.  
> I wish I was a mortician and I could execute my vocab,  
> And then riff new lines, invent my own alphabets.**
>
> **I heard someone say her songs kept her alive when it wasn’t all sound.  
> I wonder if it was a lie or her words really kept her from going down.  
> If the words have so much power why don’t they impart  me courage.  
> Why do I always have to search for the perfect metaphor for this furry n’ rage.  
> Why is that I come at a stand still in between the two stanzas,  
> May be it’s a beautiful analogy of my state while deciding under pressure.**
>
> **I am afraid I have lost the will to fight anymore,  
> A defeated soul I walk with my head heavy n’ low.  
> There was a time I had something to look up for,  
> Now wherever I look I see the history merging into the future.  
> But the problem with the world is they can’t even let you live discomfited,  
> They want business, so you must keep slogging for their happiness.  
> But I am done. It’s now or never.  
> These words would be the first for the rest of my life,  
> Or they would be the last, forever.  
> These sentences  were the step towards the visible dim light,  
> It’s now up to me if I want to keep walking or still sit tight.**
> 
> **~RavS**
