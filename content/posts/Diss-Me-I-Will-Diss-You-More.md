---
public: true
title: ..diss me, I will diss you more..
date: 2015-06-02
tags:
  - rap
categories: english
---

> **A time came when I thought  
> I am not going to grow up,  
> And become the man I deserved,  
> To be, with the  amount of pain I withstood.  
> But then these words came in my life,  
> and they saved me from dying,  
> They gave me the feathers to fly,  
> And touch the sky, with a thousand nautical mile.**  
>   
> **No, I was never dumb or mute,  
> I just didn’t wanna talk to you,  
> Not because we had any unresolved issues,  
> But you just wouldn’t have understood it through.  
> What it means to be a loner in an overcrowded world,  
> And no one being there share it to.  
> But now I am back,  
> With all the  aggression that I earlier lacked,  
> If you diss me, I will diss you more, get that,  
> And If you behave, you’ll surely gain my respect.  
> I don’t care what you think of me,  
> I don’t have time to spend on thee,  
> I ain’t saying make me an idol and start worshiping,  
> But I deserve my space and Now I demanding it.**  
>   
> **Yeah, I tried and probably it was good what I did,  
> But don’t  you yet call me a frigging emcee,  
> The Masters are the Eminems and the Rakims,  
> I  am just finding my feet like a newly minted baby.  
> Of course I stand nowhere near their legacy.  
> They got the flow, the  content, the rhyme,  
> The style to spit stuffs in a melodic chime,  
> Look I can’t reach them touching  the sunshine,  
> But to try and emulate them isn’t really a crime.**
>
> **~RavS**
