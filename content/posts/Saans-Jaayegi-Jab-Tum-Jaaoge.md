---
public: true
title: सॉंस  जायेगी  जब  तुम  जाओगे।
date: 2015-10-02
tags:
  - fight
  - romantic
categories: hindi
---

> **हम  जाएंगे  तो  तुम  आओगे, हम  आएंगे  तो  उठके  चले  जाओगे,  
> यूँ  आने  से  जाने  का  सिलसिला  कबसे  कब  तक  निभाओगे ?  
> चलो  क्यों  न  ऐसा  करें,  
> तुम  चाहो  तो  ये  फैसला  करें,  
> साँस  आएगी  जब  तुम   आओगे।    
> सॉंस  जायेगी  जब  तुम  जाओगे।**
>
> **~रबी**


\[ When I will go, then only you will come; when I will enter, you will walk away,  
Till when are you going to continue like this?  
OK, let’s do one thing,  
You be the judge and the jury for this.  
My breath will come with you,  
My breath will go with you. \]
