---
public: true
title: एक बेरोजगार की आह...
date: 2011-09-18
tags:
- main-blog
- special
- unemployment
categories: hindi
---

![ ](../images/berozgaar-ki-aah.jpg)

> **क्यूँ पूछते हो मुझसे तुम ?  
> वही सवाल बार बार।**  
> 
> **बस थोडा थक गया हूँ,  
> नहीं मानी है पूरी हार।**  
> 
> **कल तक कहा 'चैम्प' मुझे,  
> अब कहते हो, हूँ मैं बेकार।**  
> 
> **अब तकल्लुफ बस करो,  
> मत पूछो ये मुझसे यार।**  
> 
> **लो मैं ये खुद कहता हूँ,  
> हाँ ! हाँ ! हूँ मैं 'बेरोज़गार' !**  
> 
> **_--कात्यायन पाण्डेय "घायल"_**  
> 
> **~रबी**

\[ Why do you keep asking me?  
The same question over and over. 

I'm just a little tired,  
I haven't accepted complete defeat. 

Until yesterday you called me "Champ",  
Now you say I'm useless. 

Now just stop the pretense,  
Don't ask me this, my friend. 

Fine, I'll say it myself,  
Yes! Yes! I'm "unemployed"! \]
