---
public: true
title: Haiku - Unfair
date: 2016-03-26
tags:
  - haiku
  - love
categories: english
---

![ ](../images/haiku-unfair.jpg)

> **You’re my weakness,  
> I’m your strength.  
> How beautifully unfair is that.**
>
> **~RavS**


_## You consider me your strength. Your bedrock. But I consider you my greatest weakness. It’s unfair, but it is what it is. Accepted. ##_
