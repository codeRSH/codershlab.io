---
public: true
title: तकलीफों
date: 2014-01-08
tags:
  - problems
categories: hindi
---

> **क्यों मुस्कुरा कर मेरा वक्त ज़ाया करते हो,**  
> **तुम्हारी तकलीफों से मेरी रोज बातचीत होती है।**
> 
> **~रबी**


\[ Why do you waste my time by smiling,  
I chat with your troubles everyday. \]
