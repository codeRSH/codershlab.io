---
public: true
title: चिल्लाहटें
date: 2013-04-21
tags:
  - screams
  - pain
  - alone
categories: hindi
---

> **कुछ खामोश चीखें हैं मेरे पास,**  
> **पर किसी को वो सुनाई नहीं देतीं ,**  
> **रोज चिल्लाता हूँ मैं तकिये में सर डाल कर ,**  
> **अँधेरे में चिल्लाहटें दिखाई नहीं देतीं।**  
>   
> **क्यों लगता है दलदल में फंसा हुआ,**  
> **अब आजादी से रुसवाई सी लगती,**  
> **क्यों आता है गुस्सा इतना,**  
> **जब उलझाने कमाई  सी लगती।**   
>   
> **दिन के रात, रात के दिन निकल जाते हैं,**  
> **बंद मुट्ठियों में रेत  सुखाई नहीं रूकती,**   
> **कोई नहीं गौर करता जिंदा लाशों पर,**  
> **जब तक मुखोटों की परत मुरझाई नहीं उतरती।**  
>   
> **फिर दुनिया को क्यों सुनाऊं कहानी अपनी,**  
> **जब हर बार है जग-हंसाई ही होती,**  
> **सोचता हूँ चीखें खामोश ही रखूँ ,**  
> **खामोश चीखें किसी को आवाज नहीं देतीं।** 
> 
> **~रबी**


\[ I have a few silent screams,  
But no one can really hear them,  
Every night, I shriek hiding under the pillow,  
But in dark, nobody can see them.  
  
Why do I find myself stuck,  
Why can’t I get back my freedom,  
Why do I get angry so much,  
When the dilemmas are so dearly earned.  
  
Days become nights, nights turn to days,  
You can’t stop sand in your palms,  
Nobody really cares about the living  corpses,  
Until you take off the peeling masks over them.  
  
So why do I tell my story to the world,  
When all they can do is laugh over them,  
I think I should keep my screams silent,  
Silent screams never cause any disturbance. \]
