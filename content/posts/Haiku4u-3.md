---
public: true
title: Haiku4U - 3
date: 2019-04-03
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-3.jpg)

> **When you’ll be happy,  
> When you’ll be sad,  
> I’ll be there.**
>
> **~RavS**


_## If I can’t be there when you celebrate your happiness. Or, if I can’t be there by your side while you are feeling sad and lonely; then what’s the use of me being in your life? ##_
