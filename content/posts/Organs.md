---
public: true
title: Organs
date: 2019-01-30
tags:
  - hurt
  - fight
categories: english
---

> **When you hurt me,  
> The first organs to stop working fine  
> are not my eyes, I still see you,  
> nor my ears, I still hear you,  
> but my vocal cords, for I can’t speak,  
> and my heart, as I can’t feel.**
>
> **~RavS**

_## True story! ##_
