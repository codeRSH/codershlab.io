---
public: true
title: "निगाहें"
date: 2014-05-07
tags:
  - eyes
  - dedication
categories: hindi
---

# "निगाहें"

![ ](../images/nigaahen.jpg)

> **इन्ही निगाहों में तो दुनिया बस्ती है,**  
> **इन्ही निगाहों में रोज सोती, रोज जगती है,**  
> **ये निगाहें जो बोलती कहा सुनती है,**  
> **इन्ही निगाहों से तो ज़रा रोती ज़रा हंसती है।**
> 
> **ये निगाहें जिन्हे देख लें वो राना हो जाए,**  
> **जो महरूम हो इन निगाहों से फना हो जाए,**  
> **गर्द में भी उसे जन्नतें पनाह हो जाएँ,**  
> **जो ये निगाहें किसी की आशना हो जाएँ।**
> 
> **कोई समझा नहीं इन निगाहों की नवाज़िशें, अजब हैं,**  
> **मगर लोग कहते हैं, यही निगाहें बा-खुदाई का सबब हैं,**  
> **इन्ही निगाहों की तो वो निगाह-ए-क़वायद करते हैं,**  
> **झूठ नहीं, इन्ही निगाहों को निगाह-ए-इनायत कहते हैं।**
> 
> **जो ये निगाहें थोड़ा भी परेशान होती हैं,**  
> **सुकून उजड़ता है, खुशियाँ वीरान होती हैं।**  
> **इन निगाहों में मुख़्तसर भी जो शिकन होती है,**  
> **ऐसे अज़ाब ऐसे हालत दुनिया को कफ़न होती है।**
> 
> **तो जो ये निगाहें इस दुनिया को इतनी जरूरी हैं,**  
> **न लगे दुनिया की नज़र इन निगाहों को ये जरूरी है।**
> 
> **~रबी**


\[ The world lives in these eyes only,  
It sleeps, it wakes in these eyes only,  
Whatever the eyes say, it listens closely,  
It laughs, it cries in these eyes only.

If these eyes look at someone, he becomes beautiful,  
If someone remains obscure from them, he gets destroyed,  
He will find heaven even in middle of desert,  
If these eyes become friends with someone.

No one could understand the favors of these eyes, it’s astonishing,  
But people say, these eyes only are the reason to be with God,  
So, they try desperately to see these eyes,  
No lies, these eyes only are called the God’s favor to the world.

If these eyes get even a little tensed,  
Peace evaporates, happiness leaves,  
If these eyes get sad even for a little while,  
These tortures are synonomous to death for the world.

So, if these eyes are so important to the world,  
I hope no evil touches these eyes. \]

_## And with this, I complete my desire to write something solely on eyes. The eyes, forehead and hair belong to a girl who is a parallel fashion show in herself. God bless her. ##_
