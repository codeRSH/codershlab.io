---
public: true
title: काबिल-ए-फिरदौस
date: 2013-02-17
tags:
  - efforts
  - pride
  - special
categories: hindi
---

> **नाज़ है अगर तुझे अपनी कोशिशों पर,**  
> **तेरी किस्मत तेरे हाथों के आगोश है,**  
> **मेरी नज़रों में एक तू ही ताहिर,**  
> **बस एक तू ही काबिल-ए-फिरदौस है।**
> 
> **~रबी**


\[ If you are proud of your efforts,  
Your luck is in the embrace of your palms,  
In my eyes, you are the only pure soul,  
Only you are entitled for heaven seventh. \]
