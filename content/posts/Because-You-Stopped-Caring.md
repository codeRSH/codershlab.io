---
public: true
title: '.... Because you stopped caring'
date: 2015-09-11
tags:
  - muse
  - writing
  - writers-block
categories: english
---

> **I don’t know what the heck happened to my writing. Every time I put my fingers to the keypads, the words don’t flow out. Not anymore. The consonants and vowels don’t mingle and resonate as they used to. Every passage feels like a worthless piece written halfheartedly, to be force fed to someone later.**
>
> **Am I angry?  
>    On whom?  
> Am I frustrated?  
>     With what?  
> Am I anxious?  
>     But why?  
> Why am I writing?  
>       For whom?  
> What’s the point?**
>
> **If I had a Muse somewhere, I would have summoned her today and asked her…  What the hell is wrong with you? Why don’t you inspire me anymore? Why have you stopped living and breathing in my craft?**
>
> **But I think I know her answer :  
> “…. Because you stopped caring”.**
>
> **~RavS**
