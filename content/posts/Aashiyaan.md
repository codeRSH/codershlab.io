---
public: true
title: आशियां
date: 2017-09-09
tags:
  - video
  - clouds
  - romantic
  - special
  - first
categories: hindi
---

<video controls width="640" height="360">
  <source src="../images/aashiyaan.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

> **मैं कल शाम एक मकां देख आया,  
> आसमां पे अपना जहां देख आया।  
> जैसा तुमने कहा था, कुछ वैसा ही था,  
> थोड़ा सा धुआं, कुछ धुंधला ही था।  
> वहां दिन भी था, और थी रात भी,  
> तुम्हारी खुशबू भी थी,  बस तुम नहीं।  
> अगली बार चलना, दिखाऊंगा तुम्हें,  
> मैं तुम्हारे ख्वाब कहां देख आया।  
> खरीद लूंगा, \`गर पसंद आ जाए तुम्हें,  
> तुम्हारे रहने का जो आशियां देख आया।**
>
> **~रबी**


\[ I saw a house yesterday evening,  
I saw my world in the sky.  
The way you told, it was quite similar,  
There was a little smoke, a little blurred.  
There was day there, and night as well,  
Your fragrance was there too, but only you weren’t.  
Come with me next time, I will show you,  
Where I saw your dreams from my eyes.  
I will buy it, if you like it,  
The home I saw for you to live. \]

_## First Video Post. Cheers! ##_
