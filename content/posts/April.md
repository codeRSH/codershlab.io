---
public: true
title: "April"
date: 2014-04-07
tags:
  - superwoman
categories: english
---

> **Morning. She opened her eyes,**  
> **Glistening with the moisture. Last night.**  
> **She smiled. Parted with the blanket hugging her.**  
> **Slowly kept her feet. The cold concrete floor.**  
> **Tip toing her way. Towards the moor.**  
> **The sounds from her anklet. Following her,**  
> **Shrill Shrill Shrull, Shrill Shrull…**
> 
> **She was about to cross the door,**  
> **She remembered something.**  
> **One thing. She had forgotten from her morning routine,**  
> **She ran back. The other end of the room,**  
> **Shrill Shrill Shrill, Shrulll Shrill…**  
> **And opened the windows. The sunshine came in.**
> 
> **That sunshine.**  
> **It was waiting patiently. Only for her.**  
> **Lightning her dress. Pure white fabric.**  
> **Kissing her face. Touching her neck.**  
> **The cold waves. Disappearing in her frivolous hair,**  
> **They had travelled for ages. To reach their sire.**
> 
> **She closed her eyes. Smelled the breeze ,**  
> **Took a moment. Savoured the aroma in it,**  
> **Then she kneeled down. Looked at the glorious sky ,**  
> **“God tell me, today which life needs saving” ,**  
> **She waited for an answer. From deafening silence.**  
> **Nobody said anything. Nobody listened.**  
> **But she smiled. She knew what had to be done.**
> 
> **She got up. The world at standstill.**  
> **Then quickly barged out of her room,**  
> **The anklets. Trying to follow her foot steps,**  
> **Shrill Shrull Shrill Shrull Shrill…**  
> **The destiny’s child on her mission.**  
> **Her name… APRIL.**
> 
> **~RavS**


_## April. Liked the name. Loved the concept of girl getting up in the morning and asking God to tell whom should she save today. Plus tried to write it in a little different style than normal. Short sentences. Probably not the best that I wrote but this is something different. ##_
