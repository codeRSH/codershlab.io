---
public: true
title: नफ़रत की वजह
date: 2025-02-15
tags:
- philosophy
categories: hindi
---

> **मैं भी चाहता हूँ करना,  
> नफ़रत... औरों की तरह।  
> मगर कोई ठोस वजह तो हो।  
> तुम तुम हो, मैं नहीं।  
> भला ये भी कोई वजह हुई?**
>
> **क्या पता तुम गलत ही हो,  
> ना मालूम मुझे, पता नहीं।  
> और फिर मुझे क्या ही लेना देना।  
> मुझे अपनी गलतियों से फुरसत कहाँ।  
> मगर 'वो' कहता है तुम गलत हो,  
> नफ़रत करो! नफ़रत करो!  
> भला ये भी कोई वजह हुई?**
>
> **चलो क्यों ना ऐसा करें...  
> ना तुम मेरी मुश्किलें बढ़ाओ,  
> ना मैं तेरी परेशानी का सबब बनूँ।  
> मैं जियूँ अपनी जिंदगी, जैसे तैसे,  
> और तुम्हें अपनी जीने दूँ।  
> प्यार नहीं है तुमसे...  
> सिर्फ इसलिए नफ़रत करूँ?!  
> भला ये भी कोई वजह हुई?**
>
> **~रबी**

\[ I also want to feel,  
Hatred.. like others.  
But there should be some solid reason.  
You are you, and not me.  
Is this even a reason?

Who knows, maybe you are wrong,  
I don't know, really don't know.  
And what's it to me anyway?  
I'm too busy dealing with my own issues.  
But 'he' says you are wrong,  
So, I should Hate you! Hate you!  
Is this even a reason?

Okay, why don't we do this...  
Neither you increase my troubles,  
Nor I become the cause of your problems.  
I'll live my life, somehow or other,  
And will let you live yours.  
There is no love between us...  
Just for that, should I hate you?!  
Is this even a reason? \]

_## A little controversial. But also denoting my value
systems. I believe in this poem. ##_
