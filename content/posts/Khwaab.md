---
public: true
title: ख्वाब
date: 2014-07-29
tags:
  - eyes
  - dream
categories: hindi
---

> **कुछ तो ऐसा रखते हो निगाहों में छुपाकर,**  
> **जिसका इल्म हमको नहीं लगता है।**  
> **की जिस भी ख्वाब पर तुम्हारी निगाहें पड़ जाती हैं,**  
> **वो ख्वाब खुद को मुकम्मल समझने लगता है।**
> 
> **~रबी**

  
\[ You definitely keep something hidden in your eyes,   
That we don’t come to know about,   
That whichever dream you fix your eyes upon,  
That dream starts to think yourself as complete. \]
