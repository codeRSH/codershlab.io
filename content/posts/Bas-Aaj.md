---
public: true
title: बस आज मेरा ये कहा मान लो
date: 2012-07-15
tags:
  - main-blog 
  - sad 
categories: hindi
---

> **नहीं कुछ कहने की जरूरत नहीं,  
> नहीं कुछ करने की जरूरत नहीं,  
> एक घुटन सी महसूस होती है,  
> बस यों ही पास बैठे रहो।** 
> 
> **नहीं मुझे चाँद नहीं चाहिए,  
> आसमाँ के सितारों की चाहत नहीं,  
> बहुत अकेला लग रहा है,  
> बस मेरा हाथ थामे रहो।** 
> 
> **नहीं मेरा हँसने का मन नहीं,  
> आज मुस्कुराने का मन नहीं,  
> आज दिल है बस रोती रहूँ,  
> अपनी बाहों में छुपा लो।** 
> 
> **नहीं कहीं घूमने नहीं जाना,  
> बाहरी नज़ारों से इश्क नहीं,  
> आज चाहत है बस सोती रहूँ,  
> अपने सीने में पनाह दो।** 
> 
> **सच कहती हूँ,  
> फिर कभी कुछ नहीं माँगुंगी मोहसिन,  
> बस आज मेरा ये कहा मान लो।** 
> 
> **~रबी**

\[ There's no need to say anything,  
There's no need to do anything,  
I feel a sense of suffocation,  
Please just sit close by. 

I don't need the moon,  
I don't desire the stars in the sky,  
I feel very lonely,  
Please just hold my hand. 

I don't feel like laughing,  
I don't feel like smiling today,  
Today my heart just wants to keep crying,  
Please hide me in your arms. 

I don't want to go anywhere,  
I'm not in love with outside views,  
Today I just want to keep sleeping,  
Please give me shelter in your chest. 

I swear,  
I won't ask for anything ever again, dear (Mohsin),  
Please just listen to what I say today. \]
