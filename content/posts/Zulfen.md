---
public: true
title: जुल्फें
date: 2012-06-15
tags:
  - faaltu
  - witty
categories: hindi
---

> **उसकी लहराती जुल्फें देखीं तो याद आया मुझे…  
> लटें मेरी भी कभी लम्बी हुआ करती थीं !**
> 
> **~रबी**


\[ When I saw her hair flowing like a river, I remembered…  
I too used to have long silky hairs! \]
