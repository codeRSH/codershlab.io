---
public: true
title: Happy Endings
date: 2014-11-22
tags:
  - free-verse
  - dark
  - romance
categories: english
---

![ ](../images/happy-endings.png)

> **I left a little space at the end of the story ,**  
> **Not to suggest he might get reborn,**  
> **But in the off-chance that you return,**  
> **And put a leaf over his grave.**  
> **So that he dies happily ever after.**
>
> **I like happy endings.**
> 
> **~RavS**
