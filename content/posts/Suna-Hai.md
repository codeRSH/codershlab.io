---
public: true
title:  सुना है
date: 2019-08-19
tags: 
 - romance
categories: hindi
---
 
> **सुना  है  बरबादियाँ  ही  मिलती  हैं  उसके  आशिक़ों  को  वफ़ा  में।  
> तो  चलो  हम   भी  उसकी  गली  में  चलके  प्यार  करते  हैं।**  
>   
> **सुना  है  औरों  की  हंसी  उड़ने  से  आती  है  हंसी  उसे।  
> तो  चलो  उसके  मोहल्ले  में  खुद  को  सरे-बाज़ार  करते  हैं।**    
>   
> **सुना  है  जन्नत  भी  गुज़रती  है  उसके  कदमो  से  होके।  
> तो  चलो  उसके  रस्ते  में  खुद  को  तार-तार  करते  हैं।**  
>   
> **सुना  है, ग़मों  का  रुख  तक  नहीं  देखा कभी  उसने।  
> तो  चलो  उसका  रुख  देख  कर हम ख़ुशी  इख़्तियार  करते  हैं।**  
>   
> **सुना  है  फूल  भी  इत्र  मांगने  आते  हैं  उसके  बदन से।  
> तो  चलो  हम  भी क्यों ना अपनी  शामें  गुलज़ार  करते  हैं।**  
>   
> **सुना  है  जगमगाती  है  वो  कभी-कभी  अमावस  की  रात  में।  
> तो  चलो  हम  भी  मुद्दतों  नूर  का  इंतज़ार  करते  हैं।**  
>   
> **सुना  है  चाँद  भी  तकता  है  उसे  एक आह  भर  के।  
> तो  चलो  हम  भी  चलके  अपनी  साँसें  उधार  करते  हैं।**  
>   
> **सुना  है  जाहिल  भी  शायर  हो  जाते  हैं  उसके  आगे।  
> तो  चलो  हम  भी  कुछ  नज़्में  उसपे  वार  करते  हैं।**  
>   
> **सुना  है, मरते  भी  आते  हैं  उससे  ज़िन्दगी  की  चाहत  में।  
> तो  चलो  हम पहले  खुद   को  बेहद  बीमार  करते  हैं।**  
>   
> **सुना  है  रबी, दोस्तों  को  कुंद  कलम  की  शिकायत  करते।  
> तो  चलो  उसकी  आँखों  से  लेकर  इसमें  धार  करते  हैं।**  
>   
> **~रबी**   


\[  Have heard that her lovers get only devastation in return.  
So let’s also move in her street and love.  
  
Have heard she laughs when others get ridiculed,  
So let’s ridicule ourselves in her neighborhood.  
  
Have heard that the heaven also passes through her feet.  
So let’s slaughter ourselves on her path.  
  
Have heard, she hasn’t ever faced sadness yet.  
So let’s also see her face and feel some happiness.  
  
Have heard even flowers come to beg some fragrance from her body.  
So let’s go to her and make our evenings fragrant.  
  
Have heard that she glows on the night of new moon,  
So let’s also wait forever to see her shine.  
  
Have heard the moon glances at her with bated breath,  
So let’s also lend our breaths to hers.  
  
Have heard even illiterates become poets in front of her.  
So let’s gift some of our verses to her.  
  
Have heard, death also comes to her for the want of life.  
So let’s first make ourselves very ill.  
  
Have heard Rabi, friends complain of the blunt pen.  
So let’s ask for sharpness from her eyes.  \]  

  

_## A romantic gift for my wife :) ##_ 
