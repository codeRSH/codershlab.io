---
public: true
title: ज़मानत
date: 2012-12-11
tags:
  - friendship
  - separation
categories: hindi
---

> **तो क्या हुआ, जो तुम आज इतने बदल गए,  
> तो क्या हुआ, जो तुम बहुत दूर निकल गए,  
> मेरी यादों की सलाखें बहुत मजबूत है, परख़ लो,  
> नामुमकिन है, जो तुम्हें यहाँ से ज़मानत मिल पाए।**
> 
> **~रबी**

  

\[ So what, if you have changed a lot,  
So what, if you have gone so far,  
The bars of my memories are very strong,  
It’s impossible, that you can bail out from here. \]
