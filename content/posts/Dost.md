---
public: true
title: दोस्त
date: 2013-03-21
tags:
  - friends
categories: hindi
---

> **जो बहुतों के दोस्त\* होते हैं ,**  
> **दोस्त\* उन्ही के बहुत होते हैं।**  
>   
> **\*दोस्त: सच्चा दोस्त**  
> 
> **~रबी**


\[ Those who are friends\* with many,  
Only for them, the friends\* are many.  
  
\*friends : true friend \]
