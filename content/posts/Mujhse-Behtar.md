---
public: true
title: मुझसे बेहतर
date: 2014-04-06
tags:
  - jealousy
categories: hindi
---

> **मैं इस फरेब में नहीं रहता की मुझसे बेहतर तुम्हे कोई मिल नहीं सकता,**  
> **शायद यही वजह है की मुझसे बेहतर तुम्हे कोई मिल नहीं पायेगा।**
> 
> **~रबी**


\[ I don’t live in the ignorance that you can’t get someone better than me,  
Probably that’s the reason you won’t ever get someone better than me. \]
