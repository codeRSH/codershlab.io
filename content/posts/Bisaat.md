---
public: true
title: बिसात
date: 2015-12-02
tags:
  - writers-block
categories: hindi
---

> **खुद ही लिख लिया कर मुख़्तसर ऐ कलम।  
> तुझ पे उँगलियाँ फेरना मेरे बस की अब बात नहीं।  
> रूह काँप जाती है तुझमें स्याहियाँ उड़ेलते,  
> रबी। अब रबी कहलाने की मेरी कोई बिसात नहीं।**
> 
> **~रबी**


\[ You please write yourself a little bit, my pen,  
I don’t have the capability to handle you anymore,  
My soul shivers to ink your inside,  
Rabi. I don’t deserve to be called Rabi anymore. \]
