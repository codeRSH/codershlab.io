---
public: true
title: इतना भी क्यों तुम समझ नहीं पाती हो?
date: 2012-04-23
tags:
  - main-blog
  - romantic
categories: hindi
---

> **मैंने चाहा है तुम्हें माहरुख\*,  
> क्यों तुम मुझे इतना सताती हो?**
> 
> **मेरी क्या है गलती, बता दो मुझे,  
> मुझे क्यों तुम बेवजह रुलाती हो?**
> 
> **काश खता पता होती मुझे अपनी, काश कोई बता पाता,  
> आज कल क्यों तुम खफा-खफा नज़र आती हो?**
> 
> **भुला दो जो भी गिला है मुझसे,  
> मुझे सजा देने की फ़िराक में क्यों तुम खुद ही को जलाती हो?**
> 
> **कल मैं ना रहूँगा तो मुझे याद कर रोया करोगी,  
> तो आज क्यों नहीं तुम मुझे देख कर मुस्कुराती हो?**
> 
> **जिंदगी छोटी है और साथ भी, आज है, ना हो कल कहीं,  
> इतना भी क्यों तुम समझ नहीं पाती हो?**
> 
> **~रबी**  

_\*माहरुख: जिसका चेहरा चाँद सा हो_

\[ I have loved you, Mahrukh*  
Why do you torment me so much?  

What is my fault, tell me,  
Why do you make me cry for no reason?  

I wish I knew my mistake, I wish someone could tell me,  
Why do you always seem so upset these days?  

Forget whatever grievance you have against me,  
Why do you burn yourself while trying to punish me?  

If I am not here tomorrow, you will remember me and cry,  
So why don't you smile when you see me today?  

Life is short, and so is our time together, we are here today, might not be tomorrow,  
Why can't you even understand this much?  

*Mahrukh: (a term of endearment, literally meaning "moon-faced") \]
