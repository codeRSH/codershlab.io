---
public: true
title: जब तुम आओगी
date: 2018-04-30
tags: 
 - special
 - wife
categories: hindi
---

![ ](../images/jab-tum-aaogi.jpg)

> **खुशियाँ तलाशते बहुत दुख दिए हैं तुम्हे,  
> तुम्हारा आँचल भरूंगा अब इन्हीं खुशियों से।  
> मैं जीना थोड़ा भूल सा गया हूँ,  
> जब तुम आओगी न तो अब से जीने लगूँगा।**  
>   
> **कई रातें काट दी खुली आँखों मे,  
> जगाये रखा मुझे तुम्हारे सपनों ने।  
> तुम्हारे ना मिलने का डर सोने नही देता मुझे,  
> जब तुम आओगी न तो चैन से सोया करूँगा।**  
>   
> **शिकायत तुम्हें फ़िज़ूल ही रहती है मुझसे,  
> मैं अब भी वही हूँ, जैसा मिला था तुम्हें।  
> सच है, मैं निकला नहीं एक अरसे से बारिश में,  
> जब तुम आओगी न तो जी भरकर भीगा करूँगा।**  
>   
> **मैं काबिल नहीं था जो मैंने कर दिया है,  
> लड़खड़ाए कदम तो तुमने हौसला दिया है।  
> हिम्मत टूट गई है वैसे तो आगे बढ़ने की,  
> जब तुम आओगी न तो खुद ही चल पड़ूँगा।**  
>   
> **नाराज़ ना हुआ करो जो बहुत कम हूँ लिखता,  
> लिखना भूला नहीं हूँ, बस याद नहीं रहता।  
> मेरी कई ग़ज़लें अधूरी पड़ी हैं ऐसी ही,  
> जब तुम आओगी न तो उन्हें पूरा करूँगा।**  
>   
> **~रबी**  

  

\[ In search of happiness, I have given you a lot of grief,  
I will fill your lap with these happiness now.  
I have forgotten to live a little bit,  
But when you will come, I will start living again.  
  
I have spent so many nights with open eyes,  
Your dreams kept me awake.  
Fear of not meeting you does not let me sleep,  
But when you will come, I will not sleep peacefully now.  
  
You keep complaining to me without reasons,  
I am still the same as you met me the first time.  
True, I have not been in the rain for a long time,  
But when you will come, I will get drenched to my heart’s content.  
  
I didn’t have the ability to do what I did,  
You have encouraged when my steps stumbled,  
Although my courage is broken to move forward,  
But when you will come, I will starting walking by my own.  
  
Don’t be angry that I write very little,  
I have not forgotten to write, just do not remember.  
Many of my poems are incomplete like this,  
But when you will come, I will complete them. \]  
  
  

_## Wedding Special :) ##_
