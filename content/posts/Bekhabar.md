---
public: true
title: बेख़बर
date: 2012-03-01
tags:
  - romantic
  - searching
  - cafeteria
categories: hindi
---

> **वो  हमें  हर  गली,  हर  मंज़र, हर  जहान  ढूँढ  आये ,**  
> **इस  ज़मीन  से  आसमान  के  दरम्यान  ढूँढ  आये ,**  
> **पर  ऐसी  बेख़याली  में  लोग  अक्सर  होश  खो  ही  बैठते  हैं ,**  
> **हम  तो  उनके  पास  ही  थे  हमेशा ,**  
> **वो  बेख़बर  दिल  पर  हाथ ना रख पाए।**  
> 
> **~रबी**


\[ They searched for me in every lane, every scenery, every world,  
They searched for me everywhere between land and sky,  
But in this mindlessness, people often lose all their senses,  
I was with them only, always,  
they forgot to keep their hands over their heart. \]
