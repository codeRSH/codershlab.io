---
public: true
title: रुनझुन
date: 2014-07-04
tags:
  - 30-day-challenge
categories: hindi
---

> **शोर में भीड़ के,**  
> **हम आपको ना सुन सके.**  
> **चार पल सुकून के,**  
> **जो बात करने को मिले ,**  
> **तो आप उठ कर चल दिए।**  
> **रुनझुन, रुनझुन, रुनझुन …**
> 
> **~रबी**


\[ In the noise of crowd,  
I could not hear you.  
Four moments of peace,  
I got to talk to you,  
But then you got up and went away.  
Shrill, Shrill, Shrill…\]
