---
public: true
title: दरिया
date: 2012-03-03
tags:
  - friendship
  - confusion
  - life
  - witty
categories: hindi
---

![ ](../images/dariya.jpg)

> **ये  ज़िन्दगी  का  दरिया,  
> बहा  ले  जा  रहा  है  कहाँ,  
> ना  मुझे  रास्ते  का इल्म,  
> ना  तुम्हें  मंजिल  का  पता।**
> 
> **‘गर  कुछ  भी  न  किया  तो,  
> दूर  बहुत  दूर  निकल  जायेंगे,  
> गुमनामी  के  भंवर  में  गुम हो,  
> लौट  वापस  न  पायेंगे।**   
> 
> **चलो  इस  कश -म -कश  से  अब बाहर निकलते  हैं ,   
> तुम संभालो एक पतवार, दूसरी हम पकड़ते हैं।**  
> 
> **\[ TIME सही  है, CAT की  preparation शुरू  करते  हैं। \]**   
> 
> **~रबी**


\[ This stream of life,   
Don’t know where it’s taking us,   
Neither do I know the route,   
Nor do you know the destination.

If we don’t do anything now,   
Then we will get far, very far,   
We will get lost in this pool of anonymity,   
And will never be able to come back.

OK, let’s get out of this dilemma now,   
You handle one paddle, I will take the other. \]
