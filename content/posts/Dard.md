---
public: true
title: दर्द
date: 2014-03-31
tags:
  - pain
  - agony
categories: hindi
---

> **दर्द इतना है, शायद अब दर्द की तलब सी हो गयी,**  
> **जो ना हो दर्द तो लगता है, मुझे कुछ ना-मुकम्मल कहीं,**  
> **कभी कभी दर्द खुद चिल्ला उठता है,**  
> **बस कर फैज़, अब और नहीं, अब और नहीं।**
> 
> **~रबी**


\[ There’s so much pain, may be now I am addicted to it,  
If I don’t feel pain, I feel there’s something missing in me,  
Sometimes the pain itself cries out loud,  
Stop it Faiz. It’s enough. It’s enough. \]

_## Have been towing with the idea of getting a pen name for writing Hindi poetry. Faiz sounds pretty good. But it’s already used by a few. ##_
