---
public: true
title: आदत  तुम्हारी
date: 2012-09-25
tags:
  - romantic
  - sad
categories: hindi
---

> **ना  तुम  मिल  सके, ना  तुम्हारा  साया  मयस्सर  हुआ,  
> जाते  जाते  फिर  भी  ये  ख़ुमारी  गयी  नहीं।**    
>   
> **कई  रातों   से  चाँद  भी  आधा  ही  निकला   है,  
> रुख  पर  पर्दा  करने  की  आदत  तुम्हारी  गयी  नहीं।**  
> 
> **~रबी**

  

\[ Couldn’t get you, neither your shadow,  
But still your intoxication hasn’t gone yet.  

For so many days I have seen only half of moon,  
Your habit of covering your face hasn’t gone yet. \]
