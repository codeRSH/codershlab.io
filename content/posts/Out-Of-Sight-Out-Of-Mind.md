---
public: true
title: Out of sight. Out of mind.
date: 2014-11-30
tags:
  - anger
  - disappointment
  - love
categories: english
---

> **I thought you were an angel,**  
> **But you turned out to be a fucking human just like everyone else,**  
> **I thought I was your saviour,**  
> **But now I realize you already had what you needed.**  
>   
> **I hope one day the world burns and you burn with it,**  
> **I hope your skin gets roasted and I get to pour a little more gasoline,**  
> **You know why you were able to hurt me,**  
> **More than anybody else could possibly hurt?**  
> **It’s because I cared about you more than anyone else,**  
> **And more than anyone else ever could,**  
> **It’s because I considered you one of my own.**  
> **I thought the world was a bad bad place,**  
> **which you needed to be saved from,**  
> **Now I realize you are a part of it,**  
> **You yourself have come out of this very heinous world’s womb.**  
>   
> **I tell you a little secret.**  
> **I really don’t need a single person, to survive this unbearable curse called life,**  
> **But if I like someone, I let them decide,**  
> **If they want to stay,**  
> **Be a part of it, or go away.**  
> **I trust them not to break my trust,**  
> **But you did exactly what I feared.**  
> **Gained my trust and then so brutally shattered it.**  
>   
> **But I learned my lessons and now I am done,**  
> **Next time you are in danger, I am not coming to save you,**  
> **Oh, In fact I might come,**  
> **As you drown in swamp,**  
> **I will stand by and watch you.**  
>   
> **Please RavS, please save me.**  
> **One last time,**  
> **You will cry for help yet again.**  
> **A part of me, out of habit, will want to catch hold of your hand.**  
> **But I will control my instincts,**  
> **And let your body dissolve,**  
> **Knees first, then waist, chest, throat, lips and nose,**  
> **Finally those eyes, as they plead for life,**  
> **They will meet mine,**  
> **And know,**  
> **I don’t adore,**  
> **you anymore.**  
>   
> **I might even smirk a little,**  
> **To see the end of your ugliness.**  
> **Yeah that makes me an animal,**  
> **but I really don’t care,**  
> **You are either on my side,**  
> **or you were never there.**  
> **Out of sight. Out of mind.**  
>
> **~RavS**
