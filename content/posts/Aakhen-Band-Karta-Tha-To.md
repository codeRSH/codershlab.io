---
public: true
title: आँखें बंद करता था तो...
date: 2014-10-06
tags:
  - memories
  - romantic
categories: hindi
---

> **आँखें बंद करता था,**  
> **तो नजर आता था,**  
> **एक लहराता आँचल,**  
> **एक ठहरा दरिया,**  
> **टप टप करता झरना,**  
> **तुम्हारे आने की आहट में,**  
> **भीनी भीनी सी खामोशी,**  
> **एक सुबह सोई सोई सी।**
> 
> **एक बहती खुशबू हवा में,**  
> **जो तुझसे पहले तेरे आने का पैगाम दे जाती थी ,**  
> **एक सिली सी नमी फिजा में ,**  
> **तेरे बालों से होकर मेरे चहरे को जो छू जाती थी।**
> 
> **एक रास्ता संकरा सा,**  
> **जिसपे हम चला करते थे,**  
> **धीरे धीरे होले होले, बेसुध से,**  
> **घंटों बातें किया करते थे।**  
> **एक पीली पीली धूप चलती थी आगे आगे,**  
> **जैसे हरे गलीचे पे रास्ता दिखाती हो।**  
> **या कहीं ऐसा तो नही था, तेरे पैरों से लिपटने को,**  
> **हर सुबह सब छोड़ कर चली आती हो।**  
> **अब क्या पता,**  
> **उसे भी तो तेरे साथ रहना पसंद था।**
> 
> **एक चिड़िया मिलती थी कभी कभी,**  
> **शायद वो भी तेरी दीवानी थी।**  
> **जैसे मैं तेरा दीवाना था।**  
> **वो कहती थी कुछ तुझसे,**  
> **तेरे  सिवा  कोई और उसे न समझ सकता था।**  
> **दूर कहीं से उड़ती हुई अक्सर आकर गले लग जाती थी,**  
> **जैसे तेरा कान्धा ही उसे अपना घराना लगता था।**
> 
> **कभी कभी खो जाते थे जंगल में,**  
> **अँधेरे में कभी, कभी धुंध में।**  
> **पर तुझे कभी डर नहीं लगा करता था,**  
> **शायद इसलिये तो नहीं, क्योंकि,**  
> **मैं तेरे साथ जो हुआ करता था।**  
> **हर पत्ते हर टहनी से रास्ता पूछते ,**  
> **हर पोधे हर फूल का कहा सुनते,**  
> **तुम जाने कैसे समझ लेती थी सब कुछ,**  
> **मैं कुछ कहता तो नहीं था,**  
> **पर मैं तो सिर्फ तेरे भरोसे चला करता था।**
> 
> **वक्त बदला बदला सा लगता था,**  
> **जब तुम होते थे पास में,**  
> **मैं कुछ और ही हुआ करता था,**  
> **जब तुम होते थे पास में,**  
> **धड़कने थम सी जाती थी,**  
> **जब तुम होते थे पास में,**  
> **नींद बहुत अच्छी आती थी, कसम से,**  
> **जब तुम होते थे पास में।**
> 
> **वो वक्त कुछ और था,**  
> **वो मंजर कुछ और था।**  
> **अब आँखें खोल कर भी कुछ नहीं देख पाता मैं,**  
> **तब आँखें बंद करके भी सब देख लिया करता था।**
> 
> **~रबी**


\[ When I used to close my eyes,  
I would see,  
A flowing scarf,  
A still pond,  
Slowly dripping waterfall,  
Waiting for your arrival,  
A slight fragrance of silence,  
A morning feeling sleepy.

A drifting scent in the air,  
Which used to bring the message of your arrival even before you,  
A little moisture in the breeze,  
That would come through your hair and touch my face.

A narrow path,  
On which we used to walk,  
Slowly, softly, being insensible,  
We used to talk for hours.  
A pale yellow sunlight would move ahead,  
As if it was trying to show the way on the green carpet.  
Or, was it because wanting to touch your feet,  
It would come every morning leaving everything,  
Now who knows,  
It also used to like being with you.

We used a find a sparrow sometimes,  
May be she was also crazy for you,  
Like I was crazy about you.  
She used to say something to you,  
No one else could ever understand her except you,  
It would come flying from far away and hug you,  
Like your shoulders was her home.

We used to get lost in the woods sometimes,  
In the dark sometimes, in mist sometimes.  
But you never felt afraid,  
Perhaps, may be, because,  
I used to be with you.  
We would ask the way from every leaf, every twig,  
We used to hear every flower, every plant on the path,  
Don’t know how you used to understand everything,  
I would never say word,  
I used to walk just on your confidence.

The time used to seem different,  
When you used to be close,  
I used to be something else,  
When you used to be close,  
My heartbeats used to stop,  
When you used to be close,  
I used to get sound sleeps,  
When you used to be close.

That time was different,  
That scene was different,  
Now, I can not see anything with open eyes,  
Then I used to be able to see everything even with eyes shut. \]
