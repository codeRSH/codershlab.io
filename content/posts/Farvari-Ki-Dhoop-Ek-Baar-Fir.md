---
public: true
title: फरवरी की धूप... एक बार फिर
date: 2015-02-21
tags:
  - song
  - reprise
categories: hindi
---

> **मुरझाई  थी  फूल  की  कली ,**  
> **थी  बेजान  डाल  बेल  की ,**  
> **तेरे  बसंत  से  खिल  गयीं ,**  
> **जो  फिर  आ  गयी...**  
> **तू  फरवरी  की  धूप  सी।**  
> **गुनगुनी  खिली  खिली।**
>
> **शिकायत  तुझे  इस  बात  की ,**  
> **तेरी   प्यास  से  दिल  बुझता   नहीं ,**  
> **मगर   ज़रा  से  में   जो  बुझ   गयी ,**  
> **तेरी   प्यास  फिर   किस  काम   की।**  
>
> **तुझ  बिन  है   रहती    उदासी ,**  
> **तुझ  संग  बंध  गयी  ख़ुशी।**  
> **अब  तू  है   सब  कुछ  तभी ,**  
> **जो   तू   नहीं, तो  कुछ  नहीं।**
>
> **सरसों   पीले   पीले  खेत   की ,**  
> **या   सुनार  की  कारीगिरी   ?**  
> **नहीं … तू   फरवरी  की  धूप  ही।**  
> **इस   बार   मिली,  अब  जाना  नहीं।**
>
> **~रबी**  


\[ The buds were scorched,  
The twigs were lifeless,  
They started blooming with your spring,  
When you came back…  
Like February’s sunshine.  
Warm and Fresh.

You have a complaint that,  
The heart doesn’t get quenched from your thirst,  
But if your thirst gets quenched so easily,  
What use is your thirst then.

Without you there is sadness,  
The happiness is now bound with you.  
Now if you are there, only then there’s everything,  
If you are not, then there’s nothing.

Are you Mustard of yellow farm,  
Or a Goldsmith’s work?  
No… You are actually February’s sunshine.  
Please don’t go away, now that you are mine. \]
