---
public: true
title: यादें
date: 2014-10-14
tags:
  - faaltu
categories: hindi
---

> **उड़ता है धुंआ,**  
> **जलता मैं यहां,**  
> **तेरी यादें जलती हैं,**  
> **बता जाऊं मैं कहाँ ?**
> 
> **जाऊं मैं कहाँ,**  
> **पता मुझे कहाँ,**  
> **तुझे भी नहीं पता,**  
> **तो जा पूछ के आ।**
> 
> **साले बता दे,**  
> **आग लगी है,**  
> **तन बदन में,**  
> **किलस रही है।**
> 
> **जो यादें जल रही थीं,**  
> **पागल तेरी ही थीं,**  
> **अब क्या फायदा,**  
> **पहले बता देता,**  
> **बच भी सकती थीं।**
> 
> **इतना धुंआ, इतना धुंआ,**  
> **खांस खांस के परेशां,**  
> **साला जिसने आग लगाई,**  
> **उसके पड़ोसी की भैंस की आँख।**
> 
> **~रबी**

  
\[ Smoke is flying,  
I am burning,  
Your memories are burning,  
Tell me where should I uld go?

Where should I go,  
I don’t know,  
If you also don’t know,  
Then go and find out.

Brother-in-law tell me,  
I am on fire,  
The whole body,  
Is paining.

The memories that were burning,  
Idiot it was yours,  
Now what’s the use,  
You should have told earlier,  
It could have been saved.

So much smoke, so much smoke,  
I am exhausted of caughing,  
Whoever in the hell, started the fire,  
Buffalo’s eye of his neighbour. \]
