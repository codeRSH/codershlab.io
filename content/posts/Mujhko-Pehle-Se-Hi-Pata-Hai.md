---
public: true
title: मुझको पहले से ही पता है
date: 2014-04-20
tags:
  - talk
categories: hindi
---

> **जो बात तुम कहना चाहते हो,  
> वो मुझको पहले से ही पता है,  
> जो बात है, मुझे मालूम है,  
> तुमको कहते दर्द तो होता है।**  
>   
> **बेचैनियां दिखती हैं तुम्हारी आँखों में,  
> तुम्हारे चेहरे पे, तुम्हारी साँसों में,  
> तुम्हारे सीने में, धड़कनो में,  
> तुम्हारे कांपते हाथों में।**  
>   
> **तुम चाहते हो सब पहले सा हो जाए,  
> पर तुम जानते हो अब ऐसा हो नहीं सकता,  
> तुम चाहते हो जो खोया वो लौट आये,  
> पर इस बार जो गया वो वापस  आ नहीं सकता।**  
>   
> **अब जो भी है, यही है ,  
> चाहे गलत है या सही है ,  
> दुनिया जिससे अनजान है ,  
> वो बात धीरे धीरे जो काटती है।**  
>   
> **पर तुम फिर भी कुछ मत कहना,  
> वो बात अंदर ही दफनाए रखना,  
> इस बात का तो तुमको भी पता है ,  
> जो बात तुम कहना चाहते हो ,  
> वो मुझको पहले से ही पता है।**
> 
> **~रबी**

  
\[ The thing that you want to say,  
That I already know about,  
That thing, I know,  
It would hurt you to say out loud.  
  
I can see the restlessness in your eyes,  
On your face, in your breaths,  
In your chest, in your heartbeats,  
In the trembles of your hands.  
  
You want that everything gets just like old times,  
But you also know, it’s not possible now,  
You want that whatever you have lost, comes back again,  
But this time what’s gone can never be brought back.  
  
So this is what it is,  
Whether it’s right or wrong,  
The world which seems to be oblivious to,  
That thing that eats you from inside.  
  
But still please don’t say anything,  
Keep it buried inside you,  
Because even you know this,  
The thing that you want to say,  
That I already know about. \]

_## Below is my attempt to recite this poem. First ever. Not too great but good enough for the very first attempt I guess :). The Soundtrack is Oliver’s Lullaby. ##_

[ Jo Baat Tum Kehna Chahte Ho by RavS on Grooveshark](http://grooveshark.com/search/song?q=RavS%20Jo%20Baat%20Tum%20Kehna%20Chahte%20Ho "Jo Baat Tum Kehna Chahte Ho by RavS")
