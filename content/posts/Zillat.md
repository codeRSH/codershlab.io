---
public: true
title: ज़िल्लत
date: 2013-02-19
tags:
  - poet
categories: hindi
---

> **क्यों शायर को बेवजह बदनाम करते हो, रबी,**  
> **प्यार के सिवा उसने न कुछ दिया है,**  
> **इन्तेकाम के सिवा उसने न कुछ लिया है,**  
> **और ज़िल्लत के सिवा उसे न कुछ मिला है।**
> 
> **~रबी**


\[ Why do you defame a poet without any reason?  
He has never given anything but love,  
He has never taken anything but revenge,  
And he never got anything but ridicule. \]
