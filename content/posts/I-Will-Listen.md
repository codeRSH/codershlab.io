---
public: true
title: I Will Listen
date: 2012-01-20
tags: 
  - main-blog 
categories: english
---

> **The next time you talk,  
> I will listen.  
> Not just from ears,  
> But from the heart.  
> And process it.  
> Not just from the mind,  
> But from the soul.**
> 
> **Till the time you keep talking...  
> I will control the disturbance outside,  
> and shut down the turbulence inside.  
> That impeding noise of hatred,  
> Those heart-wrenching memories of failures,  
> Will all be gone!   
> ...till the time you keep talking.**
> 
> **When you will speak,   
> There will be only you in the moment,  
> And no one else will be allowed to enter it.  
> All other works will cease to exist,  
> Like they were never there to begin with.**
> 
> **You will be the only person in the universe  
> Who will matter in those precious seconds.  
> On you will I focus,  
> In you will I search my zen.**
> 
> **Your words will become the lyrics,  
> Your voice will be music to my ears,  
> And so your little talk will become a song,  
> A song worth listening for years.**
> 
> **The modulation in your speech,  
> The glow in your eyes,  
> Those waving hands,  
> And that smiling face,  
> I will absorb them in my core,  
> And imprint them in my memory, forever.**
> 
> **For a change, I will concentrate on you,  
> And not on me, myself, my pride,  
> For a change, I will hear what You want to say,  
> And not what I want to hear.**
> 
> **And there will be nothing artificial,  
> My attempts will not be fake,  
> Because I will listen, because I want to listen,  
> You will not have to make any efforts,  
> To see my eagerness, the excitement, the intent,**
> 
> **And also that little apology,  
> For the last time, I didn't pay attention.  
> And also the gratitude,  
> For letting me listen to you, again.**
> 
> **The next time you talk,  
> I promise you this,  
> With all the honesty in my heart,  
> I. Will. Listen...**
> 
> **~RavS**


_## This was written as a self-reminder. ##_
