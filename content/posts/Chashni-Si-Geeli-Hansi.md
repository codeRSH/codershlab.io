---
public: true
title: चाशनी  सी, गीली  हंसी
date: 2015-02-17
tags:
  - laughter
categories: hindi
---

> **उसके  क़दमों   से  होकर  राहें  निकला  करती  थीं ,**  
> **उसकी  आँखों  को  छूकर  रौशनी  दिखा  करती  थी ,**  
> **उसकी  साँसों  में  घुलकर  खुशबू  बहा  करती  थी ,**  
> **उसकी  ऊँगली  पकड़कर  खुशियाँ  चला  करती  थीं।**  
> **ऐ  खुदा, वो  समा, फिर  ज़रा, ले  आइये।**  
> **मुझे  उसकी, चाशनी  सी, गीली  हंसी, फिर  चाहिए।**
>
> **उसकी  लटों  में  घुसके,  ओस  नहा  लेती  थी ,**  
> **उसके  कानों  में  हौले  से, लोरियाँ  गा  लेती   थीं ,**  
> **उसके  आँचल  के  तले  शाम  रहा करती  थी।**  
> **उसके   आँचल  के  नीचे  ही  नींद  सोया  करती   थी।**  
> **ऐ   खुदा, सुन   ज़रा, मेरी  फरमाइश  ये ,**  
> **मुझे  उसकी,  चाशनी  सी, गीली  हंसी, फिर  चाहिए।**  
>
> **उसके  लिए  तो  पल  रूक कर  इंतज़ार  किया  करता   था ,**  
> **उसका  आसमान  झुक  झुककर  दीदार  किया  करता  था ,**  
> **उसके  तलवों  से  लिपटकर   धूल  खिला  करती  थी ,**  
> **उसके  भवों   की  अदा  से   कलियाँ  जला  करती  थीं ,**  
> **ऐ  खुदा, होता  ज़रा, भी  असर, मेरी  दुआओं  से।**  
> **मुझे  उसकी,  चाशनी  सी, गीली  हंसी, फिर  चाहिए।**  
>  
> **~रबी** 

\[ The paths used to start through her feet,  
The light used to show touching her eyes,  
The fragrance would flow dissolved with her breath,  
Happiness would walk clutching his fingers.  
O God, please make this happen again,  
I want her warm, wet, syrupy smile again.

The dew would shower in her streak,  
Lullabies would sing gently in her ears,  
The evenings would stay in her lap,  
In her lap, the sleep itself would have slept,  
O God, hear me, my requests again,  
I want her warm, wet, syrupy smile again.

The time would stop and wait for her,  
The sky would also bow down to get a glimpse of her,  
Dust would glow embraced under her sole,  
The buds would get jealous from her eyebrows’ curves,  
O God, if there is any effect of my prayers.  
I want her warm, wet, syrupy smile again.  \]
