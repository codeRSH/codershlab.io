---
public: true
title: The Last Words
date: 2014-11-24
tags:
  - free-verse
  - end
categories: english
---

![ ](../images/the-last-words.png)

> **It wasn’t my fault,**  
> **But I so wish it was,**  
> **At least you could have hated me for the right reasons.**
>
> **(The last words.)**
>
> **~RavS**
