---
public: true
title: She's an IT girl
date: 2013-11-10
tags:
  - girl
  - musings
categories: english
---

> **She reads bits and bytes like Latin-English alphabets.**   
> **She deals with IP addresses like elementary school mathematics.**   
> **She brushes her teeth with 7 layers of OSI.**   
> **And, combs her hair with broken passwords of her neighbors' WiFi.**
> 
> **Workstation in one hand, GPS in her locket,**   
> **Not forgetting the rooted droid in her pocket,**  
> **She doesn’t need to drink n’ smoke to get her daily fix,**  
> **Work is her alcohol, she’s a workaholic.**
> 
> **She thinks crying like feminists is now passé,**  
> **She instead burns men's** **egos to ashes,**  
> **Even though she looks cute and harmless in her rimless glasses.**  
> **Make no mistake, she knows how to put boots to dysfunctional asses.**
> 
> **She likes to grab every bull by its slimy horns,**  
> **Small talking around the issue is not her point, strong,**  
> **She grew up hearing about stolen identities and Nigerian scams,**  
> **Her childhood wasn’t full of fairies and unicorns,**  
> **So she doesn’t loss sleep over dead relationships with boyfriends,**  
> **The dead pixels on her devices are the only things she mourns.**
> 
> **She keeps a spreadsheet of the minutes wasted,**  
> **Everyday, having to deal with other people’s bullshit.**  
> **Most of the times she has problems digesting it,**  
> **Sometimes she spits it back on their faces, sometimes she just vomits.**
> 
> **She knows she’s right more often than she’s not,**  
> **Yet she puts a zipper to keep her mouth shut,**  
> **She would rather spend time with tedious problems needing diagnosis,**  
> **Than getting into futile verbal duels with idiots n’ obnoxious.**
> 
> **You won’t find her dwelling in shopping complexes or restaurants,**  
> **She’s a hippy who camps in hilly terrains under torrents,**  
> **She opens her arms to hug cyclones and wind whirls.**  
> **And dances around tornadoes with a little twirl,**  
> **Her life is more twisted than her own hair curls,**  
> **She’s truly one of a kind, she’s an IT girl.**
> 
> **~RavS**
 

## _Had originally thought to post this with some illustrations. Unfortunately that couldn’t happen. Nevertheless, here I have tried to imagine an “IT girl”, a nerdy girl with a fierce personality. Wish she does exist :)_

_Inspired from “Last of the American Girls ” By Green Day. ##_
