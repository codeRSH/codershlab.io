---
public: true
title: रज़ा
date: 2012-02-23
tags:
  - romantic
categories: hindi
---

> **अब  इसे  इत्तेफाक  कहें  या  रज़ा  खुदा  की …**  
> **घर  से  निकले  थे  इश्क  करने ,**   
> **और  अगली  ही  गली  में  आप  दिख  गए …**
> 
> **~रबी**


\[ Now should I call this coincidence, or God’s will…  
That I started from my home to find love,  
And in the very next lane I found you… \]
