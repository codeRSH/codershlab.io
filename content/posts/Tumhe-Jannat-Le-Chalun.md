---
public: true
title: तुम्हे ज़न्नत ले चलूँ 
date: 2014-10-01
tags:
  - wait
categories: hindi
---

> **मुझे परवाह नहीं कोई मुझ पर लिखे ,**  
> **मैं मुन्तजिर हूँ उसका जिस पर मैं लिख सकूं,**  
> **जिसपे हक हो मेरा, सिर्फ मेरा, कोई और नहीं,**  
> **वो शायरी बने तो मैं खुद को शायर कह सकूं।**
> 
> **कश्तियाँ आती तो बहुत हैं, लेकिन कोई रूकती नहीं,**  
> **मैं फिर भी साहिलों के किनारे बैठा रहता हूँ।**  
> **इस उम्मीद में कि कोई तो रुकेगा, कभी,**  
> **कहेगा, चलो… तुम्हे ज़न्नत ले चलूँ।**
> 
> **~रबी**


\[ I do not care if someone writes for me,  
I await the one for whom I can write,  
Who is rightfully mine, only mine, no one else’s,  
If she becomes the ​​poetry, I can call myself poet.

So many rafts come, but none stops,  
I still keep sitting by the shores,  
In the hope that someone will stop, someday,  
And will say, come on … let me take you to the heaven. \]
