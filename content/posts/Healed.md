---
public: true
title: Healed
date: 2013-02-22
tags:
  - heal
  - pain
  - adversity
categories: english
---

> **When you can tell your story,  
> Without that pain, piercing in your belly,**  
>   
> **What happened with you and what could have been,  
> When the thoughts do not make you angry,**  
>   
> **When you can smile at your adversity,  
> And forgive the ones who have caused it,**  
>   
> **When no more do you need to search a place, dark & lonely,  
> Every now and then, to scream. Silently.**  
>   
> **That day you come back to me,  
> And I will tell you, that you have healed. Finally.**
>
> **~RavS**
