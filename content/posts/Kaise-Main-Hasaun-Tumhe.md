---
public: true
title: कैसे मैं हँसाऊं तुम्हें
date: 2014-12-23
tags:
  - helplessness
  - special
categories: hindi
---

> **बिन छुए, बिन कुछ कहे ,**  
> **दूर बहुत दूर तुमसे बैठे ,**  
> **कैसे मैं हँसाऊं तुम्हें।**
> 
> **बिना तुम्हें गुस्सा किये ,**  
> **बिना तुम्हें दर्द दिए ,**  
> **कैसे अपनी बेबसी का एहसास कराऊँ तुम्हें।**
> 
> **तुम्हारी हर छोटी छोटी बात याद है मुझे ,**  
> **तुम्हारी आँखों से गिरा हर कतरा जेहेन में अब तक साफ़ है मेरे ,**  
> **पर कैसे मैं दिखाऊं तुम्हें।**
> 
> **ये शर्मिंदगी, ये लाचारी ,**  
> **ये गुस्सा, ये उदासी ,**  
> **ये सब दिखावा तो नहीं।**  
> **मुझे सच में बहुत लगता है ,**  
> **जब तुम्हें ठेस है लगती।**  
> **अब कैसे विश्वास दिलाऊँ तुम्हें।**
> 
> **अरे नादान गुड़िया तुम भार नहीं हो ,**  
> **तुम तो मेरा ऊपर वाले से तार आखिरी हो ,**  
> **तुम्हें देख कर ही तो लगता है ,**  
> **वो शायद अभी भी हैं, जो तुम्हें भेजा है ,**  
> **ये सब कैसे मैं समझाऊँ तुम्हें।**
> 
> **तुम अगर दूर चली जाओगी ना तो ,**  
> **इस बार उनपे विश्वाश उठ जाएगा हमेशा को ,**  
> **फिर चाहे जितना मर्जी गुस्सा करती रहो।**  
> **और तुम मुझ पे क्यों इतना गुस्सा करती हो ?**  
> **अब बताऊँ, तो कैसे मैं बताऊँ तुम्हें।**
> 
> **मैं मजबूर हूँ बच्चा ,**  
> **इस बार तुम खुद ही हंस दो ना ,**  
> **बिन छुए, बिन कुछ कहे ,**  
> **दूर बहुत दूर तुमसे बैठे ,**  
> **कैसे मैं हँसाऊं तुम्हें।**
> 
> **~रबी**


\[ Without touching, without saying anything,  
Sitting far, too far away from you,  
How do I make you laugh.

Without getting you angry,  
Without giving you pain,  
How should I make you realize my helplessness.

I remember every little thing you do,  
Every drop of tears fallen from your eyes is still clear in my conscience,  
But how should I show them to you.

The embarrassment, the helplessness,  
The anger, the sadness,  
It’s not a show off.  
I really feel the hurt,  
When you get hurt.  
Now how can I convince you of this.

Oh my silly doll, you’re not a liability,  
You’re my last connection with the almighty,  
Only because of you do I believe,  
He probably exists, because he sent you to me,  
How do I explain you all this.

If you go away,  
This time the belief on him would be shattered forever,  
Then you can get angry howsoever much you want,  
And why do you so angry with me by the way?  
If I need to tell you, how should I tell you anything.

I am helpless baby,  
Please laugh by yourself this time,  
Without touching, without saying anything,  
Sitting far, too far away from you,  
How do I make you laugh. \]
