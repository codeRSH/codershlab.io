---
public: true
title: आ बरस
date: 2012-06-19
tags:
  - sad
  - anger
categories: hindi
---

> **ना रहा अब विश्वास, रब्बा तेरे भरोसों  पे ,  
> ना ही तुझसे कोई आस, तू करेगा कोई तरस,   
> गम के बादल तो छायें हैं बरसों से,  
> है हिम्मत,  तो  अब आ बरस।**  
> 
> **~रबी**


\[ Belief in your trust is broken forever God,  
No longer do I hope, you will show any pity on me,  
The clouds of sadness are overhead since ages,  
Let it rain now, if you have the guts. \]
