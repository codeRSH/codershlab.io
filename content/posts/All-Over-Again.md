---
public: true
title: All Over Again
date: 2013-01-03
tags:
  - work
  - life
categories: english
---

> **What I do daily is not work, it’s a self inflicted war with self,**  
> **Between the part which is lazy and the part gone insane,**  
> **Let me lick my wounds tonight and take some rest,**  
> **‘cause tomorrow morning it starts all over again!**
>
> **~RavS**
