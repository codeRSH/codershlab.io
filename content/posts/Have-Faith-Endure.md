---
public: true
title: Have Faith. Endure.
date: 2014-04-17
tags:
  - special
  - dedication
  - rap
categories: english
---

> **It sucks to know what you are going through,**  
> **It sucks that I can’t come ‘n meet you,**  
> **It sucks that I am far far-away,**  
> **It sucks that no-one is there to cheer you ,**  
> **It sucks that you are suffering so much,**  
> **It sucks that there’s nothing that I can do ,**  
> **And I would have done it earlier if only I knew,**  
> **But from today, I will pray, that you get well soon.**  
> **Have Faith. Endure.**
>
> **~RavS**
