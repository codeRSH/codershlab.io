---
public: true
title: हंस के दो गल्लाँ
date: 2014-09-12
tags:
  - punjabi
  - birthday
categories: hindi
---

> **जे ओ भुल्ले साड्डा जन्मदिन कदी भुल्ल के वि, लोकी उदास हो जांदे ने,**  
> **ऐत्थे कदी कोई कर लेन्दा हंस के दो गल्लाँ वि, अस्सी ओउनु ही जन्मदिन सुम्मझ मना लेन्दे ने।**
> 
> **~रबी**


\[ Even if they forget my birthday by mistake, people feel guilty and sad,  
And here, if someone smiles and talks to me for a while, I celebreate that as my birthday. \]

![ ](../images/hans-ke-do-gallan.jpg)

_## Thanks office team for doing this. Means a lot. :) ##_
