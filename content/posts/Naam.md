---
public: true
title: नाम
date: 2012-07-18
tags:
  - life
  - separation
categories: hindi
---

> **जो नाम मुझमें शामिल था,  
> वो नाम कहीं दूर निकल गया।**    
>   
> **जिस नाम से साँसे चलती थी,  
> वो नाम अब कातिल बन गया।**    
>   
> **जिस नाम पर लगते थे जलसे मेरे शहर में,  
> उस नाम पर कोलाहल मच गया।**    
>   
> **जो नाम पहुंचा था साहिल से शिखर तक,  
> वो नाम आज धू-मिल गया।**  
> 
> **~रबी**

  

\[ The name which was inside me,  
That name has gone far away.  
  
The name for which I used to breathe,  
That name has become a killer today.  
  
The name for which there used to be celebration in my town.  
That name has created chaos today.  
  
The name that went from bottom to top,  
That name has soiled forever today. \]
