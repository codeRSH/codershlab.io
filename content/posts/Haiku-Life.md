---
public: true
title: Haiku - Life 
date: 2013-05-15
tags:
  - haiku
  - life
categories: english
---

> **In chaos, I looked inside.  
> Found myself filled,  
> … with emptiness.**
>
> **~RavS**
