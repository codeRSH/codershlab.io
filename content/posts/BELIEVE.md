---
public: true
title: BELIEVE
date: 2014-12-24
tags:
  - prose
  - believe
categories: english
---

![ ](../images/believe.png)

> **Next time you tell me I can’t do something, I am going to shove a rag through your mouth and choke your neck to the point of suffocation.**  
>   
> **When you are deprived of oxygen, and are jostling for life, eyes coming out of your ducts, face getting pale; you would do anything and everything to survive. You would scratch, claw, kick, scuffle till your last moment.**  
>   
> **Please don’t make me asphyxiate you, for you to realize the kind of struggle I want you to devote to succeed.**  
>   
> **Anything is possible. Anything.**  
> **BELIEVE.**  
> **In me. And yourself.**
> 
> **~RavS**
