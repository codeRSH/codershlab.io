---
public: true
title: देहलीज़
date: 2014-06-24
tags:
  - 30-day-challenge
categories: hindi
---

> **मौत की देहलीज़ पर पहुँच कर याद आया मुझे,**  
> **जो ज़िंदा पीछे छूट गए, अब उनसे मुलाकात ना होगी,**  
> **ये देहलीज़ भी बेवफा ही लगती है बशर,**  
> **जो पार बैठे हैं, उन्हें भी मेरी सूरत याद ना होगी।**
> 
> **~रबी**

  
\[ When I reached the doorsteps of death, I remembered,  
Those who are left behind, I won’t be able to meet them anymore,  
And these doorsteps also seem disloyal to me, my friend,  
Those who are sitting across, they also won’t remember my face. \]
