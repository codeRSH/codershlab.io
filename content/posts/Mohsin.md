---
public: true
title: मोहसिन
date: 2012-07-04
tags:
  - friendship
  - life
  - problems
  - philosophical
categories: hindi
---

> **मेरी दिक्कत और उसकी परेशानी में अंतर सिर्फ़ इतना रहा मोहसिन,  
> मेरे पास वक्त ना रहा और वो वक्त का मोहताज़ हो गया…**
> 
> **~रबी**

  

\[ The only difference between my dilemma and his problem was,  
That I didn’t have time left with me and he had to beg for time… \]
