---
public: true
title: फुर्सत
date: 2012-02-28
tags:
  - romantic
categories: hindi
---

> **तड़प-तड़प  के  रुख़सत-ए-दुनिया  हुए  उनके  चाहने  वाले,**  
> **हाय!  उन्हें  अब  साँस लेने  तक  की  फुर्सत  ना  रही।**  
> 
> **~रबी**


\[ Her lovers left the world tossing in agony,  
Oh! Now she doesn’t even have time to catch a breath. \]
