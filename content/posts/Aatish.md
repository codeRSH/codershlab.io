---
public: true
title: आतिश
date: 2014-07-23
tags:
  - life
  - irony
categories: hindi
---

> **तुम करती थी इंतज़ार शरारों की छाँव में,**  
> **मैं आतिशों की धूप में मुन्तज़िर रहता था।**  
> **तुम ढूँढा करती थी मुझे शोर के समंदर में,**  
> **मैं ख़ामोशी के दरिया में बहा करता था।**
> 
> **मुझे तुम न मिली, मैं तुम्हे न मिल सका,**  
> **सब कुछ छूट गया, बहुत कुछ मिलकर।**  
> **जब गलतफहमी पलती रही यूँ दोपहर से रात भर,**  
> **और कटती गई ज़िन्दगी पहर दो पहर।**
> 
> **~रबी**


\[ You used to wait in the shades of sparks,  
I used to look for you under the sunshine.  
You used to search me in the sea of noise,  
I used to swim in the river of silence.

You couldn’t get me, I could not get you,  
Everything got left, after getting a lot.  
When misunderstanding got fed day and night,  
And life passed by, clock by clock. \]
