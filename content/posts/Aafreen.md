---
public: true
title: आफरीन
date: 2014-07-07
tags:
  - 30-day-challenge
categories: hindi
---

> **चाहूँ चाहे जितना भी खुद को रोकना,**  
> **पर दिल दिमाग के बस में अब कहाँ,**  
> **धक् सी होती है सीने में हर बार,**  
> **पर तुझे देखे बिना दिल को ठंडक भी कहाँ ,**  
> **होगी तू नर्गिस, मह्रूख, नवाजिश कहीं की,**  
> **तेरी तस्वीर ने तो मेरे दिल से बस यही कहा…**  
> **आफरीन। आफरीन। आफरीन।**
> 
> **~रबी**

  
\[ However much I want to stop myself,  
But heart isn’t in control of brain now,  
It skips a beat inside the chest every time,  
But without seeing you, the heart doesn’t get contend too,  
You might get called a flower, a moon, or a favor somewhere,  
But your picture says just one word to my heart…  
Beautiful. Beautiful. Beautiful. \]
