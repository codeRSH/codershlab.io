---
public: true
title: जिंदगी भी सही मजाक है!
date: 2011-08-12
tags:
  - main-blog
  - humor
  - life
categories: hindi
---

> **जितना चाहे पढ़ लो इसे,  
> मगर पूछती ऐसे सवाल है, जिनका होता नहीं जवाब है,  
> बिना इंडेक्स की थकेली किताब है,  
> ज़िंदगी भी सही मजाक है।**  
> 
> **जब छोटा था तो बड़े होने की जल्दी थी,  
> अब बड़ा हूँ, तो बचपन याद आता है,  
> वो टाइम मशीन बनाने वालोँ का क्या हिसाब है?  
> ज़िंदगी भी सही मजाक है।**  
> 
> **जब चीटिंग करो तो पकड़े जाओ,  
> जब चीटिंग ना करो तो भी पकड़े जाओ,  
> भैंस की आँख, अपनी तो तो किस्मत ही खराब है,  
> ज़िंदगी भी सही मजाक है।**  
> 
> **कुत्ते के तरह पढो, तो नंबर नहीं आते,  
> खयाली पुलाव पकाने पे रिकॉर्ड टूट जाते हैं,  
> अपना तो एजुकेशन सिस्टम ही लाजवाब है,  
> ज़िंदगी भी सही मजाक है।**  
> 
> **जब भी मैंने किसी चीज़ को बड़ी ही शिद्दत से चाहा,  
> तो पूरी कायनात मेरी वाट लगाने में लग गयी,  
> कभी-कभी तो पूरी दुनिया ही लगती खिलाफ है,  
> ज़िंदगी भी सही मजाक है।**  
> 
> **दोस्त अक्सर पूछते हैं,  
> के मेरी कंकाली का क्या राज है,  
> बावड़ी पून्छो, जो तुम खा जाते थे,  
> वो मेरी दिन-भर की खुराक है,  
> ज़िंदगी भी सही मजाक है।**  
> 
> **कह-कह के "फेसबुक झकास है, झकास है",  
> बच्चे की जान ले ली,  
> और अगर मैं ट्विट्टर यूज़ करूँ,  
> तो वो लगता सबको बकवास है,  
> ज़िंदगी भी सही मजाक है।**  
> 
> **इत्ती मेहनत से ये ब्लॉग बनाया, दर्जनो पोस्ट लिख मारे,  
> तो लोग कहते हैं, "यार एक बात बताओ,  
> तुम्हारे पास नहीं कोई काम-काज है?",  
> सही कहा दोस्तोँ, बेरोजगारी के दिन है,  
> आजकल तो साली  ज़िंदगी भी लगती मजाक है।**  
> 
> **~रबी**

\[ Read it as much as you want,  
But it asks such questions, which have no answers,  
It's like a worn-out book without an index,  
Life is truly a joke.  

When I was little, I was in a hurry to grow up,  
Now that I'm grown, I miss childhood,  
What's the deal with those time machine makers?  
Life is truly a joke.  

When you cheat, you get caught,  
When you don't cheat, you still get caught,  
Damn it! My luck is just bad,  
Life is truly a joke.  

If you study like a dog, you don't get good grades,  
But if you daydream, records get broken,  
Our education system is simply amazing,  
Life is truly a joke.  

Whenever I've desired something intensely,  
The entire universe conspires to mess me up,  
Sometimes the whole world seems to be against me,  
Life is truly a joke.  

Friends often ask,  
What's the secret to my thin frame?  
Ask those gluttons, what you guys used to eat,  
That was my entire day's meal,  
Life is truly a joke.  

By constantly saying "Facebook is awesome, awesome,"  
They've taken the life out of the kid in me,  
And if I use Twitter,  
Everyone thinks it's nonsense,  
Life is truly a joke.  

I created this blog with so much effort, wrote dozens of posts,  
And people say, "Hey, tell me one thing,  
Don't you have any work to do?"  
You're right, friends, these are days of unemployment,  
Nowadays, life itself seems like a joke. \]

_## This is NOT Poem. This is Life. ##_
