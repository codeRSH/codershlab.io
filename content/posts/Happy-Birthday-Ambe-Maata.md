---
public: true
title: "हैप्पी बर्थडे अम्बे माता"
date: 2014-04-21
tags:
  - birthday
  - special
  - little-angel
categories: hindi
---

![ ](../images/happy-birthday-ambe-maata.jpg)

> **हैप्पी बर्थडे अम्बे माता,**  
> **बोलो हैप्पी बर्थडे अम्बे माता,**  
> **जो कोई तुम को विश देवत,**  
> **जो कोई तुम को विश देवत,**  
> **ऑलवेज समथिंग गुड ही पाता,**  
> **बोलो हैप्पी बर्थडे अम्बे माता।**
> 
> **कद काठी छोटो बच्चन सी,**  
> **बातें जैसे दादी माता ,**  
> **हाँ बातें जैसे दादी माता।**   
> **जो कोई भक्तजन बोर होवत,**  
> **जो कोई भक्तजन बोर होवत,**  
> **गॉसिप करन नाल त्वाडे कोल ही आता,**  
> **बोलो हैप्पी बर्थडे अम्बे माता।**
> 
> **रोटी सब्जी न तुम्हे भावत,**  
> **रोज ब्रेड और जैम ही भाता,**  
> **हाँ रोज ब्रेड और जैम ही भाता,**  
> **ओहकी खुराक भी इतनन सी,**  
> **ओहकी खुराक भी इतनन सी,**  
> **जैसा कोई चूज़ा हो खाता,**  
> **बोलो हैप्पी बर्थडे अम्बे माता।**
> 
> **कोड डिबग करन में माहिर,**  
> **कोई बग नहीं बच पाता,**  
> **हाँ कोई बग नहीं बच पाता,**  
> **कस्टम की तो बात ही छोडो,**  
> **कस्टम की तो बात ही छोडो,**  
> **स्वयम स्टैण्डर्ड का बग भी डर जाता,**  
> **बोलो हैप्पी बर्थडे अम्बे माता।**
> 
> **पहले भक्तजन सिया-राम ही ध्यावत,**  
> **अब हर कोई सी.आर.एम. के गुण गाता,**   
> **हाँ अब हर कोई सी.आर.एम. के गुण गाता,**  
> **हरदम बालन मा हाथ फेरत,**  
> **हरदम बालन मा हाथ फेरत,**  
> **बाल्कन का कौन इत्ता ख्याल रख पाता,**   
> **बोलो हैप्पी बर्थडे अम्बे माता।**
> 
> **जो कोई अम्बे माँ की लेटेस्ट आरती,**  
> **टुडे, एव्री ईयर सुमिरन करता,**  
> **हाँ एव्री ईयर सुमिरन करता,**  
> **कहत रेव्स, सी.आर.एम. के जूनियर,**   
> **कहत रेव्स, सी.आर.एम. के जूनियर,**   
> **अप्रैज़ल उसी का अच्छा हो पाता,**  
> **बोलो… हैप्पी… बर्थडे… अम्बे… माता…**
> 
> **~रबी**


\[ Happy Birthday Mother Amba,  
Happy Birthday Mother Amba,  
Whoever wishes you,  
Whoever wishes you,  
Always gets something good.  
Say, Happy Birthday Mother Amba.

Structure and Height is like a little child,  
But you talk like my Grandmaa,  
Yes, you talk like my Grandmaa,  
Whoever desciple gets bored,  
Whoever desciple gets bored,  
To gossip, they always come to you,  
Say, Happy Birthday Mother Amba.

You don’t like vegetable and chappatis,  
Every day you live on bread and jam,  
Yes, Every day you live on bread and jam,  
That too you eat so little,  
That too you eat so little,  
May be little chickens eat more than you.  
Say, Happy Birthday Mother Amba.

An expert in code debugging,  
No bug is safe from you,  
Yes, no bug is safe from you.  
Let’s not even talk about Custom,  
Let’s not even talk about Custom,  
Even the standard code’s bug fears you.  
Say, Happy Birthday Mother Amba.

Previously, humans used to remember Siya-Raam Gods,  
Now everyone sings praises of CRM,  
Now everyone sings praises of CRM,  
Always hands up in the hair,  
Always hands up in the hair,  
Who can take care so much of their heir,  
Say, Happy Birthday Mother Amba.

Whoever sings the latest prayer of Mother Amba,  
And remembers Today, every year, forever,  
Yes, remembers Today, every year, forever,  
Says, RavS, junior in CRM,  
Says, RavS, junior in CRM,  
They only get a great appraisal.  
Say… Happy… Birthday… Mother… Amba…\]
