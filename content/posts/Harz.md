---
public: true
title: हर्ज़
date: 2023-06-25
tags:
  - special
  - mother
categories: hindi
---

> **वैसे तो नहीं मानता मैं,  
> ये रिवाज़ नए-नए त्योहारों के,  
> जहां दुनिया बताती है, कहां,  
> कब और क्या खरीदना है बाजारों से।  
> मगर मां को एक दिन के लिए खुश देखा,  
> तो लगा, नए रिवाज अपनाने में,  
> इतना हर्ज़ भी क्या है?  
> यूं बाजारी त्योहार मनाने में,  
> इतना हर्ज़ भी क्या है?**
>
> **कौन सा साल भर मैं इतनी,  
> कद्र करता ही हूं मां की,  
> बाकी दिन वैसे भी मुझे खिलाकर,  
> वो अकेले आधी रात को खाती है।  
> घुटने का दर्द ना मैं याद रखता हूं,  
> ना वो कभी अपने से याद दिलाती है।  
> काश बाकी दिन भी पूछ लिया करूं,  
> मां ये तेरा मर्ज़ भी क्या है?  
> आज ड्यूटी निभाने को पूछा तो,  
> यूं बाजारी त्योहार मनाने में,  
> इतना हर्ज़ भी क्या है?**
>
> **मां इक दिन के दिखावे पे मेरे,  
> सब कुछ जानते हुए भी,  
> मेरे खुशी के लिए मुस्कुराती है।  
> आज मेरी जली सब्जी खा के भी,  
> वो मजेदार पकवान बताती है।  
> हां दिखावा है, पर सच कहूं तो,  
> दिखावे में इतना हर्ज भी क्या है?  
> यूं बाजारी त्योहार मनाने में,  
> इतना हर्ज़ भी क्या है ?**
>
> **~रबी**

\[ I don't believe in these new-age festivals,  
Where the world dictates what, when, and where to buy.  
But seeing my mother happy just for a day,  
I thought, what's the harm in adopting new ways?  
So what if we celebrate these commercial festivals?  

I don't cherish my mother enough throughout the year,  
While she eats alone late at night, after feeding me.  
I always forget her aching knees, and she never reminds me.  
I wish I'd ask her every day, "Mother, what ails you?"  
Today, when I asked her to all this as a 'duty',  
I thought, so what if we celebrate these commercial festivals?  

My mother, knowing everything, puts up a show for me,  
Smiling for my happiness, even today.  
Even after eating my burnt food, she praises it.  
Yes, it's a show, but honestly, what's the harm in a show?  
So what if we celebrate these commercial festivals? \]