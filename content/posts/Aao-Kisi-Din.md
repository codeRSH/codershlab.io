---
public: true
title: आओ किसी दिन
date: 2012-05-16
tags:
  - romantic
categories: hindi
---

> **आओ किसी दिन मेरे आँगन में तुम धूप बन कर,**  
> **मेहको कभी मेरे बाग़ों में एक फूल बन कर,**  
> 
> **लहराओ किसी दिन मेरी छत पर तुम पतंग बन कर,**  
> **बसो कभी मेरे दिल में एक उमंग बन कर,**  
> 
> **होने दो किसी दिन यकीन मुझे भी अपनी किस्मत पर,**  
> **रखो कभी अपने कदमो को मेरी चौखट पर,**  
> 
> **क्या करूँ बहक जाता हूँ, तुम्हें देख कर,**  
> **बता देना, कहीं मैंने कुछ ज़्यादा तो नहीं मांग लिया।** 
> 
> **~रबी** 

\[ Come someday, like sunshine in my courtyard,  
Smell sometime, like a flower in my garden.  
  
Wave someday, like a kite on my terrace,  
Settle sometime, like enthusiasm in my heart.  
  
Let me believe someday, that I can also have good luck,  
Step sometime, in my little home,  
  
What can I do, I get distracted every time I see you,  
Do tell, if I have asked for a little too much. \]
