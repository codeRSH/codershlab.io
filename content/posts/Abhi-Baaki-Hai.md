---
public: true
title: अभी बाकी है
date: 2012-05-27
tags:
  - hurt
  - pain
  - special
categories: hindi
---

> **खुदा, जो तू ना पूरी कर सका,  
> मेरी वो फ़रियाद अभी बाकी है।**
> 
> **डर लगता है कोई देख ना ले,  
> आँखों में आँसुओं कि प्यास अभी बाकी है।**
> 
> **घाव भरे तो वक्त हो गया बहुत,  
> पर दर्द का एहसास अभी बाकी है।**
> 
> **मैं कुरेदता नहीं जानकर,    
> पर दिल के किसी कोने में दफ़न,  
> हाँ… उसकी याद अभी बाकी है।**
> 
> **~रबी**

  

\[ The one you couldn’t fulfill,  
God, that wish of mine, still remains.  
  
I fear someone might see,  
The thirst of tears in my eyes, still remains,  
  
It’s been a while that wound got healed,  
But the feeling of pain still remains,  
  
I knowingly don’t dig them,  
but somewhere buried in the corner of my heart,  
her memory still remains. \]
