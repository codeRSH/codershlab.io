---
public: true
title:  दो लफ्ज़
date: 2014-07-30
tags:
  - life
  - value
categories: hindi
---

> **वो सोचता था कि अब मेरी दुनिया में कोई औकात नहीं,**  
> **उसे क्या मालूम था, उसके दो लफ्ज़ किसी की जान बचा सकने को काफी थे।**
> 
> **~रबी**


\[ He used to think that he isn’t needed in the world anymore,  
But what did he know, his two words were enough to save someone’s life. \]
