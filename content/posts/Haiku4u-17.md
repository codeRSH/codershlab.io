---
public: true
title: Haiku4U - 17
date: 2019-04-17
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-17.jpg)

> **I wish we competed  
> in loving each other.  
> And I’d win everytime.**
>
> **~RavS**


_## I sometimes kinda wish, that we would be in a fierce competition to prove that we love each other, more than each other._

_And then if we were to compete, I would make sure, I always always win. ##_
