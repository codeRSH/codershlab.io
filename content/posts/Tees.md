---
public: true
title: टीस
date: 2014-07-03
tags:
  - 30-day-challenge
categories: hindi
---

> **गैरों के लिए जी गए चुप चाप सारी जिंदगी,**  
> **टीस रगों में रह गयी बस छोटी सी बात की,**  
> **किसी अपने के लिए मरने का मौका ना मिला।**
> 
> **~रबी**


\[ I lived my whole life for others quietly,  
Just one little twinge got left in the veins,  
Didn’t get a chance to die for someone my own. \]
