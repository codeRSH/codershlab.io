---
public: true
title: Haiku
date: 2012-09-12
tags:
  - haiku
  - special
  - crush
categories: english
---

> **Crisp, composed, clear,  
> that lady,  
> nothing but Haiku.**
> 
> **~RavS**
