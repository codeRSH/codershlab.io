---
public: true
title: क़तरा - क़तरा
date: 2012-05-06
tags:
  - romantic
categories: hindi
---

> **रेशा - रेशा, क़तरा - क़तरा,  महक  उठे  मेरा।**   
> **कुछ  इस  तरह  आज  तुम  मुझे  गले  लगा  लो।**
> 
> **~रबी**  

\[ Every fiber, every strand of mine, engulfs in your fragrance,  
I need you to hug me today in that fashion. \]
