---
public: true
title: Poetry
date: 2015-01-01
tags:
  - free-verse
  - poetry
categories: english
---

![ ](../images/poetry.jpg)

> **The beautiful thing**  
> **about poetry**  
> **is that**  
> **you**  
> **and i**  
> **and he**  
> **and she**  
> **will read exactly the same words**  
> **but**  
> **they would never mean the same**  
> **to any of us.**
>
> **~RavS**
