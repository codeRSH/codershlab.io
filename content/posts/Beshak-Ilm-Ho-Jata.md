---
public: true
title: बेशक  इल्म  हो  जाता
date: 2012-04-29
tags:
  - sarcasm
  - friendship
categories: hindi
---

> **हँसता  हूँ, खेलता  हूँ, बातें  बहुत  सी  करता  हूँ,  
> पर  तुझसे  रिश्ता  क्या  है, ये  मालूम  नहीं  चल  पाता।**
>
> **लेकिन  जो  भी  है, इसे  इश्क  ना  समझ  लेना।  
> अगर  इश्क  होता, मुझे  ना  सही,  
> अब  तक  मेरे  यारों  को  बेशक  इल्म  हो  जाता।**
>
> **~रबी**


\[ I laugh, I play, I talk a lot with you,  
But I still don’t know what’s the relationship between us.

Well, whatever it is, don’t consider it love.  
Had it been love, then if not me,  
My friends would have definitely found out about it. \]
