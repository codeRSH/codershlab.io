---
public: true
title: सिरहाना
date: 2014-07-02
tags:
  - 30-day-challenge
categories: hindi
---

> **उसे नींद नहीं आती थी अक्सर आलम-ए-सरगोशियों में,**  
> **डर जाता था वो अक्सर मेरी गैरमौजूदगी में,**  
> **थक हार कर उसने मेरी धडकनों को अपना सिरहाना बना लिया।**
> 
> **~रबी**


\[ He often could not sleep in the the presence of whispers,  
He would often get afraid in my absence,  
So he gave up, and made a pillow out of my heartbeats. \]
