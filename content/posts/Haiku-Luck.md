---
public: true
title: Haiku - Luck
date: 2016-04-26
tags:
  - haiku
  - love
categories: english
---

![ ](../images/haiku-luck.jpg)

> **Don’t need luck my way.  
> I’m enough  
> ‘cause you’re there.**
>
> **~RavS**


_##  I know some things are out of our control. There does exist something called ‘luck’ even if we try our hardest._

_But I don’t want to believe in that. At least, not in your case.  ##_
