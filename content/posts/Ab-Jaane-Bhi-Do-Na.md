---
public: true
title: अब जाने भी दो ना।
date: 2014-11-10
tags:
  - life
  - song
categories: hindi
---

> **\[ क्यों खफा हो ज़िन्दगी से,**  
> **जाने भी दो ना।**  
> **अब जाने भी दो ना।**  
> **क्यों लड़ते हो बीते लम्हों से,**  
> **रहने भी दो ना।**  
> **अब रहने भी दो ना। \]**
> 
> **जिनके पास हो बहुत कुछ,**  
> **उनके पास भी सब नहीं होता,**  
> **खुश रहने को फिर भी,**  
> **किसी के पास कम नहीं होता।**  
> **ज़िन्दगी तो अभी शुरू हुई है यार,**  
> **तुम जीकर तो देखो ना।**  
> **जीकर देखो तो ना।**
> 
> **\[ क्यों खफा हो ज़िन्दगी से,**  
> **जाने भी दो ना।**  
> **अब जाने भी दो ना।**  
> **क्यों लड़ते हो बीते लम्हों से,**  
> **रहने भी दो ना।**  
> **अब रहने भी दो ना। \]**
> 
> **दिल दुखा दुखा सा है,**  
> **मत कहो मुझको पता है।**  
> **जिसपे हक़ था, वो नहीं मिला,**  
> **तो ऐसा भी क्या हुआ है।**  
> **तुम हो, हम हैं, फिर क्या कमी है,**  
> **अब मुस्कुरा भी दो ना।**  
> **मुस्कुराओ ना। मुस्कुराओ ना।**
> 
> **\[ क्यों खफा हो ज़िन्दगी से,**  
> **जाने भी दो ना।**  
> **अब जाने भी दो ना।**  
> **क्यों लड़ते हो बीते लम्हों से,**  
> **रहने भी दो ना।**  
> **अब रहने भी दो ना। \]**
> 
> **~रबी**

  
\[ \[ Why are you upset with life,  
Let it go,  
Just let it go.  
Why are you fighting with the past moments,  
Let it be.  
Just Let it be as it is. \]

Those who have so much;  
They also don’t have everything,  
Still to be happy,  
No one has dearth of things,  
Your life has just begun my friend,  
Live and see where it takes you,  
Just live and see.

\[ Why are you upset with life,  
Let it go,  
Just let it go.  
Why are you fighting with the past moments,  
Let it be.  
Just Let it be as it is. \]

Your heart is hurt,  
Do not tell, I already know.  
You had the right, but you didn’t get them,  
So what has happened.  
You are here, so are we, what is lacking,  
Come on, now smile.  
Smile. Smile please.

\[ Why are you upset with life,  
Let it go,  
Just let it go.  
Why are you fighting with the past moments,  
Let it be.  
Just Let it be as it is. \] \]
