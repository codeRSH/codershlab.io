---
public: true
title: मन होता है
date: 2014-10-07
tags:
  - love
  - death
  - desire
categories: hindi
---

> **तेरी आगोश में सोने को मन होता है,**  
> **तुझसे लिपट कर कुछ कहने को मन होता है,**  
> **तेरी आँखों में आँखें डाल कर,**  
> **तेरी उँगलियों में उंगलियां कसा कर,**  
> **तुझे गौर से देखने को मन होता है।**
> 
> **जो सीने में मेरे जल रही है अर्सों से,**  
> **वो आग तुझसे भिगोने को मन होता है,**  
> **लगता है गरम चाशनी सा,**  
> **तेरा बदन छूने को मन होता है।**  
> **तेरे होटों से निकली उजली भाप में,**  
> **आँखें सेंक लेने को मन होता है।**
> 
> **दामन तेरा, तुझसे छीनकर,**  
> **ओढ़ लेने को मन होता है।**  
> **किसी सन्नाटे में मिल जाए कभी, एक बार,**  
> **तुझमे मिल जाने को मन होता है,**  
> **तुझे चख लेने को मन होता है,**  
> **तुझे अपना कर लेने को मन होता है।**
> 
> **ऐ मौत, अब आ भी जा…**  
> **और कितना करूँ इंतज़ार तेरा,**  
> **उतना किसी से नहीं हुआ मुझे कभी,**  
> **जितना तुझसे मिलने को मन होता है।**
> 
> **~रबी**


\[ I want to sleep in your arms,  
I want to cling to you and say something,  
I want to look you in your eyes,  
Put my fingers in your fingers tight,  
I want to observe you carefully.

The one which is burning in my chest,  
I want to soak that fire with you,  
Looks like a hot sweet syrup,  
I want to touch your body.  
From the warm steam coming out of your mouth,  
I want to relax my eyes.

I want to snatch your scarf from you,  
And wrap it over myself.  
I wish I find you somewhere in silence, once,  
I want to dissolve in you,  
I want to taste you,  
I want to make you mine forever.

O Death, please come now …  
How much more should I wait for you,  
I never felt this for anyone else,  
The way I want to meet you. \]
