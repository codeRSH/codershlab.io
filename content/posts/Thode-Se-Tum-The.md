---
public: true
title: थोड़े  से  तुम  थे
date: 2019-09-11
tags:
  - togetherness
  - love
categories: hindi
---

> **थोड़े  से  तुम  थे, थोड़े  हम  थे,  
> थोड़ी  खुशियाँ, थोड़े  गम  थे।**
>
> **दर्द  ज़िन्दगी  में  तो  ना  कभी  कम  थे,  
> पर  वजह  सहने  की  फिर  भी,  तुम  सनम  थे।    
> थोड़े  से  तुम  थे, थोड़े  हम  थे,  
> थोड़ी  खुशियाँ  थोड़े  गम  थे।**
>
> **जो  मैं  तेरा  हो  ना सका,  
> क्या  मुझे  हक़  है  तुझे  अपना  कहने  का ?  
> ख्वाइशें  जो  मैं  पूरी  कर  ना  सका,  
> क्या  मुझे  हक़  है  कुछ  माँग सकने  का ?  
> फिर  क्या  कहें  और  किसके  दम  पे  ,  
> जो  मेरे  आँसूं  तेरे  गालों  पे  नम  थे।  
> थोड़े  से  तुम  थे, थोड़े  हम  थे,  
> थोड़ी  खुशियाँ,  थोड़े  गम  थे।**
>
> **मेरे  सिवा  तेरा   कौन  होगा  वहाँ,  
> तेरे  सिवा  मेरा  कौन  था  ही  यहाँ,  
> बस  ये  जो  डर  की  खाई  थी गहरी  दरमियाँ,  
> पार  करने  का  हौसला,  था  ना जाने  कहाँ।    
> पर  जब  मुड़ने  लगे तुम  उस  और,  कसम  से,  
> साँसे  उखड़ती  चली  गई  इसी  भरम   से।    
> दर्द  ज़िन्दगी  में  तो  ना  कभी  कम  थे,  
> वजह  सहने  की  आखिरी, तुम   सनम  थे।**
>
> **थोड़े  से  तुम  थे, थोड़े  हम  थे,  
> थोड़ी  खुशियाँ,  थोड़े  गम  थे …**
>
> **~रबी**


\[ There was a little you and a little me,  
There was some happiness, and some sadness.

Pain was never less in life,  
But even then, you were there as a cause to bear it all.  
There was a little you and a little me,  
There was some happiness, and some sadness.

When I could not be yours,  
Do I have the right to call you my own?  
Desires that i could not fulfill,  
Do I have the right to ask for something?  
Then what to say and on what basis,  
When my tears were moist on your cheeks.  
There was a little you and a little me,  
There was some happiness, and some sadness.

Who will be there for you, except me?  
Who was here except me anyways?  
Just this deep gap of fear in between us,  
The courage to cross it was never there.  
But when you started to turn and go, I swear,  
I become lifeless with this thought,  
Pain was never less in life,  
You were the last reason to bear it all.
 
There was a little you and a little me,  
There was some happiness, and some sadness. \]


_## About you and me. And also about you and them. ##_
