---
public: true
title: आसमान में सुराग
date: 2013-04-12
tags:
  - philosophical
categories: hindi
---

> **सर चढ़े आसमान को सबक सिखाने को,**  
> **एक पत्थर बड़ी तबियत से उछाला था यारों,**  
> **मेरे सर पर पड़े उस सबक के निशान देख,**  
> **वो आसमान आज भी मुझ पर हँसता है।**  
>   
> **एक नसीहत देता हूँ**  
> **ना मानो किसी शायर की नसीहत को,**  
> **आसमान में सुराग ना कभी हुआ है,**  
> **ना कभी हो ही सकता है।**
> 
> **~रबी**


\[ To teach a lesson to that sky above,  
I took a stone and threw upwards,  
That lesson on my forehead,  
Is still a topic of amusement for it.  
  
I give you an advice,  
Never take advice from a poet,  
Nobody has ever been able to make a hole in sky,  
with a stone, nobody ever will. \]  
