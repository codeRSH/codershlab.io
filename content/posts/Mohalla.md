---
public: true
title: मोहल्ला
date: 2017-04-11
tags:
  - philosophical
categories: hindi
---

> **जान  देके  भी  मुकम्मल  कहाँ  रबी।    
> इंतेक़ाल  पे  भी  मोहल्ला  गिला  करता  है,  
> जनाजे  की  इतिल्ला  पहले  से  ना  थी।**
>
> **~रबी**


\[ What’s the use of even giving life,  
When even after death the society is going to crib,  
they should have been informed earlier about the funeral. \]

_## Society is Society. No point in crying about it. ##_
