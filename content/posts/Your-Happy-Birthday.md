---
public: true
title: Your Happy Birthday
date: 2014-02-17
tags:
  - friend
  - birthday
categories: english
---

![ ](../images/your-happy-birthday.png)

> **Long ago there was no existence of you,**  
> **People of the world were happy then too,**  
> **But their happiness used to be far and few.**  
> **So God decided to pay his dues,**  
> **And take the world by storm,**   
> **He sent you from heaven, you were born.**
> 
> **You opened your eyes, you saw the world in pain.**   
> **It made you sad, you tried again and again,**  
> **To make the world a better place,**  
> **It took efforts, lots of it, several decades,**  
> **But bit by bit, straw by straw you changed lonely faces ,**  
> **Wherever you went you spread happiness,**  
> **People got happier,**  
> **The world got better,**
> 
> **Hence, there was no choice left with the world,**  
> **But to celebrate that particular girl,**  
> **And that one particular day,**  
> **Which is now fondly known as Your Happy Birthday.**
>
> **~RavS**


_## A poem on a friend’s birthday. Probably the first attempt! ##_
