---
public: true
title: इत्मिनान
date: 2014-06-29
tags:
  - 30-day-challenge
categories: hindi
---

> **तुम पूछते हो इतने इत्मिनान से कभी कभी ऐसे,**  
> **वो भी हम भूल से कह जाते हैं,**  
> **जिन्हें हम कबके भुला बैठे थे …**
> 
> **~रबी**

  
\[ Sometimes you so ask things so leisurely,  
I go on to divulge even all that, by mistake,  
Which I had forgotten a long time ago…\]
