---
public: true
title: How to Write a Poem
date: 2014-01-20
tags:
  - how-to
  - special
categories: english
---

> **As you walk down the memory lanes,**  
> **Suddenly you hit an epiphany,**  
> **Brimming, you take out a blank sheet, or a blank screen,**  
> **And start to scribble little somethings.**
> 
> **You write a stanza or three,**  
> **And then the mind goes blank,**  
> **You had decided to pour your heart into it,**  
> **Now, you don’t know what else to yank.**
> 
> **You fiddle with the nibble, change ink,**  
> **change editor, finally get tired of changing,**  
> **You stare at the blank canvas with rue,**  
> **And it keeps staring blankly back at you.**
> 
> **You try desperately to bring your thoughts out,**  
> **You even try reciting what you wrote, out loud,**  
> **You know it’s there somewhere,**  
> **Lurking in the corner of your head,**  
> **Childishly, playing hide and seek,**  
> **And you just can not find it.**
> 
> **So you tear up the paper, or shut down the screen,**  
> **But you don’t cremate the fragmented remains,**  
> **In the hope that you will return to it soon ,**  
> **You haplessly go back to your normal routine.**
> 
> **Then life takes over,** **You get busy with other stuffs,**  
> **But subconsciously the thought keeps brewing up,**  
> **One day you unexpectedly stumble on your uncompleted masterpiece,**  
> **And suddenly you can see the puzzle’s last piece.**
> 
> **Delighted, you bring out the wea(pen) of your choice,**  
> **Determined this time you are going to nail it,**  
> **Obstacles return with vengeance on the course, of course,**  
> **But now you have the inspiration, the motivation, the resilience to fail it.**
> 
> **Finished… you pick it up and take a last look at it again,**  
> **And wipe the sweat off your face, grinning.**  
> **Proud and satisfied, you share it with you friends,**  
> **Some read it, most ignore it,**  
> **A few praise it, while others call it boring.**  
>   
> **“It would have been great,” A friend wishes,**  
> **“If only I also knew how to write like this.”**  
> **You look into their eyes,**  
> **and you know they truly meant it.**  
> **So, you take out another blank sheet,**  
> **Only this time for them,**  
> **And start writing another piece,**  
> **Titled… “How to Write a Poem.”**
> 
> **~RavS**

_## This is the process that I actually follow most of the time, while I try to write something. In fact this poem was written in exactly the same way as described above! ##_
