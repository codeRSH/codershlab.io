---
public: true
title: You Are The Pain
date: 2018-07-16
tags:
  - love
  - wife
  - determination
  - special
categories: english
---

![ ](../images/you-are-the-pain.png)

> **You are the pain, you are the suffering.  
> You will be end now, you were the beginning.  
> The path that leads to you… It’s excruciating,  
> But I chose it myself, I am not complaining.**
>
> **Not just Love, it’s my existence I am fighting for.  
> Everything I am now, it’s you at the core.  
> So when I say, it hurts all the time, you’d know,  
> But the fact it hurts you, hurts me even more.**
>
> **Just you, that’s all that I asked from nature.  
> I was naive then, didn’t anticipate failure.  
> But fail I must, fail I will, if that’s what it takes,  
> I swear to God, I won’t have it another way.**
>
> **So let the world bring out their daggers & knives,  
> I am not stopping until you are my lawful wife,  
> The sensibilities of rights & wrongs, I have lost.  
> If I were to rest in peace now, without you I will Not.**
>
> **~RavS**


_## Written for my wife! Yay!! ##_
