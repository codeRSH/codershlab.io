---
public: true
title: Blank Page
date: 2018-01-17
tags:
  - prose
categories: english
---

![ ](../images/blank-page.jpg)

> **I feel a blank page is a person’s best friend.**  
>   
> **It’s non-judgmental, it’s patient and it’s always there for you. You don’t need to pretend in front of it. You can yell on it, you can cry or you can express your joy. It’s not “busy” or “dealing with its own issues”. It won’t ‘avoid’ you if you cling a bit too much. And it will always have your back (assuming you don’t run out of papers). It won’t hurt you (although paper cuts are a real thing).**  
>   
> **You can pour your deepest darkest secrets on it and it promises to keep them buried ( unless you let them leak). It will take you in the right direction if you are willing to share the journey.**  
> ‎  
> **All it asks in return is honesty. Complete, naked honesty. And a willingness to face it, when it stares back at you.**  
>   
> **Friends with flesh and blood are irreplaceable. But sometimes all you need is someone to listen. Not to advise or judge. Just listen. When that happens next time with you, try a blank piece of paper.**  
>   
> **~RavS**  

_## Getting into prose writing. It’s easier to write and can explain some thoughts a lot better :) ##_
