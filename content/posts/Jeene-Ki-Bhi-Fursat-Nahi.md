---
public: true
title: जीने की भी फुर्सत नहीं
date: 2014-05-08
tags:
  - life
categories: hindi
---

> **सोचेंगे फुर्सत में कभी तुझे किस हद्द तक मैंने जाना है ,**  
> **था कभी जो अक्स सा मेरा, क्यों आज वो मुझसे अंजाना है,**  
> **शायद इल्म भी है मुझे इस बात की,**  
> **पर मैं जान कर गौर करता नहीं,**  
> **देते रहते हैं रह-रहकर खुद को तसल्ली,**  
> **सोचें कैसे, आज कल जीने की भी फुर्सत नहीं।**
> 
> **~रबी**


\[ Will sit down and think someday, to what extent have I known you,  
He who used to be my shadow, why is he a stranger to me now,  
May be I know the reason too,  
But I knowingly don’t think about it,  
I try to solace myself saying,  
How can I think, when I don’t have even time to live. \]

  
_## Another ‘challenge’ given by a friend to complete. I hate them, because they are tough. But they also bring a fresh perspective and hence are more satisfying to complete than something written completely by own. ##_
