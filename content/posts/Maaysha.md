---
public: true
title: मायशा
date: 2016-12-13
tags:
  - happiness
  - mysha
  - philosophical
categories: hindi
---

> **कहने  को  तो  जिस  चिराग  से  उजेरा  था,  
> कहने  को  तो  उसी  चिराग  तले  अँधेरा  था।    
> जिससे  उम्मीद  लगाए  बैठे  थे  हम  दोनों  ही,  
> कहने  को  तो  ना वो  तेरा  ना  मेरा  था।**
>
> **सुनी  है  मुसाफिरों  से  अफवाह  ये  उड़ते-उड़ते,  
> वो  मायशा दिखी  थी  उन्हें   शायद  कहीं  चलते-चलते,  
> जो  चहक  के  कभी  आँगन  में  उड़  आती  थी,  
> उस  चिड़िया  का  अब  कहीं  और  ही  बसेरा  था।**
>
> **चल  पड़ें  या  यूँ  ही  बैठे  रहे  उसके  इंतज़ार  में,  
> जो  छोड़  गया  मुझे  बे-रस्ता  अपने  ऐतबार  पे।    
> गम-ए-हिज़्र  ना  कर  रबी, बस  ये  रात  निकल  जाने  दे,  
> पिछली  मर्तबा  स्याह  रात  के  बाद  ही  हसीं  सवेरा  था।**
>
> **~रबी**


\[ You can say that the lamp had brightness,  
You can say it was most dark under the lamp itself,  
From whom we both were expecting a lot,  
You can say, she was neither yours nor mine.

I have heard this rumor from passengers,  
They probably saw that ‘all-happiness’ walking somewhere,  
One who used to come in the courtyard tweeting,  
Now that bird has a home elsewhere.

Should I walk or keep sitting here waiting,  
For the one who left me here on her trust,  
Do not be sad Rabi due to separation, just let the night pass,  
Last time after the dark night only came the beautiful morning. \]
