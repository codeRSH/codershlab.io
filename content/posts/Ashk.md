---
public: true
title: अश्क़
date: 2014-05-17
tags:
  - special
  - song
  - tears
categories: hindi
---

> **अश्क़ों का क्या है,**  
> **ना समय ना बहाना,**  
> **अश्क़ तो निकल ही आते हैं,**  
> **साथ छोड़ने से पहले,**  
> **साथ छूटने के बाद,**  
> **दिल तोड़ने से पहले,**  
> **दिल टूटने के बाद।**
> 
> **अश्क़ सामने होते दांट देता,**  
> **जो दोस्ती होती इनसे तो गम बाँट लेता,**  
> **और जो होती दुश्मनी कसम से,**  
> **बहुत लड़ता, बहुत झगड़ता।**
> 
> **पर अश्क़ों का क्या है,**  
> **ना अपना ना बेगाना,**  
> **अश्क़ तो निकल ही आते हैं,**  
> **दर्द देने से पहले,**  
> **दर्द मिलने के बाद,**  
> **सांस लेने से पहले,**  
> **सांस लेने के बाद।**
> 
> **अश्क़ खुद की आँखों में न होते ,**  
> **तो दूजे की आँखों में देख पाता,**  
> **जो आँखों में ये छिपे ना रहते,**  
> **तो हर जगह साथ लेके न जाता।**
> 
> **पर अश्क़ों का क्या है,**  
> **ना पता ना ठिकाना,**  
> **अश्क़ तो निकल ही आते हैं,**  
> **किसी के जाने से पहले,**  
> **किसी के आने के बाद,**  
> **मर जाने से पहले,**  
> **मार देने के बाद।**
> 
> **~रबी**

  
\[ What’s with the tears,  
They don’t have a time or reason,  
They come whenever they want,  
Before leaving someone,  
After getting left behind,  
Before breaking someone’s heart,  
After getting heartbroken.

If tears would have been in front, I would have scolded them,  
If we would have been friends, I would have shared my sorrows with them,  
If we would have been enemies, I swear,  
I would have kept fighting with them.

But what’s with the tears,  
They don’t have friends or enemies,  
They come whenever they want,  
Before giving pain,  
After getting pained,  
Before taking a breath,  
After taking a breath.

If tears wouldn’t have been in my own eyes,  
I would have seen tears in others’ eyes,  
If they wouldn’t have taken refuge in eyes,  
I wouldn’t have taken them everywhere.

But what’s with the tears,  
They don’t have an address or home,  
They come whenever they want,  
Before someone goes away,  
After someone comes back,  
Before dieing alone,  
After killing someone known. \]

_## Again, tried something a little different than my usual style. Have tried to adopt it in a song format. Let’s see if we can make a song out of it. ##_
