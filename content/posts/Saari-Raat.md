---
public: true
title: सारी  रात
date: 2012-02-20
tags:
  - romantic
categories: hindi
---

> **कोई  कह  दो  उनसे,  कि  हमारी  याद  में  सारी  रात  जागा  ना  करें,**  
> **हमने  उनके  सपनों  को  ही  अपना  घर  कर  लिया  है।**  
> 
> **~रबी**

  

\[ Go tell her, not to stay awake whole night remembering me,  
I have made her dreams only as my home now. \]
