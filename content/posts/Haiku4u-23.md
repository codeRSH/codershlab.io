---
public: true
title: Haiku4U - 23
date: 2019-04-23
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-23.jpg)

> **You hurt yourself,  
> I start bleeding.  
> What’s wrong with us?**
>
> **~RavS**


_## I can’t tell you exactly why. But all the blows you give yourself hurt me more than anything I can hurt myself with. I hope no one but us gets to know this fact. And I hope you never use this against me. Ever._

_Otherwise I will be destroyed. ##_
