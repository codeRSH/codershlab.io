---
public: true
title: Toast of My Life
date: 2013-08-29
tags:
  - friendship
  - separation
categories: english
---

> **At one point you were the toast of my life,  
> We were to live together, together we were to die.  
> But as it happens, life took you away from life,  
> It felt my arteries were getting sliced off by a knife.**  
>   
> **And then you moved on somewhere,  
> Only sometimes I called your number.  
> You had made new pals there,  
> Me, You forgot to remember.  
> But I can’t really blame you,  
> You were scaling new highs.  
> And I felt jealous and lonely,  
> As I fought just so as to survive.**  
>   
> **But now, you want me back in your life,  
> But now, I have a few problems.  
> But now may not be the right time,  
> But now that you insist, I will tell them.**   
>   
> **You see, I had to find someone else,  
> When you were not present here.  
> Who could never replace you of course,  
> But at least they were near.  
> At least, they could see my eyes shining in happiness,   
> At least, they could feel when they were moist with tears.   
> At least, they wished me good nights,  
> At least, they comforted me during my nightmares.**  
>   
> **So when you came back,  
> There was nothing left vacant.  
> There’s only so much space in my heart,  
> You got a little late, now it’s already been taken.**  
>   
> **I don’t know what went wrong and when,  
> Ours was a friendship which no one could rip.  
> We always had each others back then,  
> They said, we were joint from hip.**  
>   
> **So many times I remember those days,  
> When we used to spend every moment together.  
> I wish I knew it that time,  
> These fleeting moments won’t last forever.**
> 
> **~RavS**


_## For friendships and relationships that were never meant break, but unfortunately do. ##_
