---
public: true
title: A friend in need...
date: 2016-08-07
tags:
  - friend
categories: english
---

> **A friend in need is a friend indeed,  
> Time and again, they told me.  
> And I took it as gospel truth,  
> Selfish, until I found you.  
> Or rather, the Universe,  
> Conspired to find us.**
>
> **I learned and You taught me,  
> To look through others’ sufferings.  
> Be mediocre; but at the least be.  
> So, now that’s what I always seek,  
> Strive to be that friend in need.  
> In my thoughts and in my deeds.**
>
> **~RavS**
