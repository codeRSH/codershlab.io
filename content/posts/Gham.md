---
public: true
title: ग़म
date: 2012-06-28
tags:
  - friendship
categories: hindi
---

> **तेरे ग़म मिटा नहीं सकता तो क्या, तेरे ग़म भुला तो सकता हूँ,  
> तेरे आँसू हटा नहीं सकता तो क्या, कुछ देर हँसा तो सकता हूँ …**
> 
> **~रबी**


\[ So what if I can’t remove your sadness, I can make you forget them,  
So what, if I can’t remove your tears, I can make you laugh for a while. \]
