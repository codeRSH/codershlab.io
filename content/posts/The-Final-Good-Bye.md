---
public: true
title: "The Final Good Bye"
date: 2014-02-01
tags:
  - friend
  - separation
categories: english
---

![ ](../images/the-final-good-bye.jpg)

> **I dismissed, I prayed, I pleaded,**   
> **For this day, this time to never come.**  
> **But it had to eventually, today it finally arrived,**  
> **It’s time to say the words and be done with,**  
> **“My friend… For now… Good Bye. ”**
> 
> **We can’t hug and cry, of course,**  
> **That would such a sissy thing to do,**  
> **(Not to mention ‘gayish’ and awkward too.)**  
> **So we mask our feelings with a big muffler of smile,**  
> **But the naked eyes would describe a different sight.**
> 
> **We shake each other’s trembling hands,**  
> **(Trembling from cold, of course)**  
> **But mutually decide not to utter a single sentimental word.**  
> **It’s against Brotocol to say “I’ll miss you bro”,**  
> **So I shout, “If you forgot me, I’ll come there and screw you”.**
> 
> **We stand there, with nothing to say,**  
> **For the first time since we met,**  
> **Awkward silence in between,**  
> **We wait for the meeting to get over,**  
> **While desperately hoping it doesn’t end.**
> 
> **We fist bump each other one last time,**  
> **Then you quickly turn around and walk away with your bag,**  
> **I stand there for a long time,**  
> **Hoping for you not to look back,**  
> **Waiting for you to slowly disappear,**  
> **Clouded by the memories of all the good, and the bad,**  
> **We braved together over the years.**
> 
> **I text Good Bye again,**  
> **And wipe off a dust speck from my left eye,**  
> **Then swiftly take my own direction,**  
> **Hoping it won’t be “The Final Good Bye”.**
>
> **~RavS**
