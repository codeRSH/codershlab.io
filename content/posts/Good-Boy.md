---
public: true
title: 'Good Boy'
date: 2014-11-23
tags:
  - free-verse
  - life
categories: english
---

![ ](../images/good-boy.jpg)

> **Basically I am done with life,**  
> **But life is not done with me yet,**  
> **It yearns to unthread me,**  
> **Then knit me again.**  
> **And once it’s done,**  
> **a kiss on the cheeks…**  
> **“Good boy.”**
> 
> **~RavS**
