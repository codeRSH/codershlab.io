---
public: true
title: ...हो जैसे
date: 2020-04-29
tags: 
 - death
 - tribute
 - special
categories: hindi
---

> **आज वो नहीं, कुछ अंदर-अंदर मरा हो जैसे।  
> काश बह जाए, मुझमें एक समंदर भरा हो जैसे।  
> वो चला गया बेवक़्त ऐसे ही एकदम से,  
> तक़लीफ़ हो रही है, घाव बेहद हरा हो जैसे।**  
>   
> **आज आसमान देखने की चाहत नहीं रही,  
> मेरा चहीता तारा टूटकर गिरा हो जैसे।  
> कुछ लोग अपने होने से ही ख़ुशी देते हैं,  
> छटपटाहट है, वो सुकून छिन गया हो जैसे।**  
>   
> **याद करने वाला मैं अकेला तो नहीं, फिर भी,  
> परेशान हूँ, वो हमें भूल गया हो जैसे।  
> मैं उसे जानता नहीं था, बस देखा था अक्स उसका,  
> अक्सर याद करूँगा फिर भी, मेरा कोई अपना गया हो जैसे।**  
>   
> **~रबी**  


\[ Today not he, a part of me has died,  
I wish it sweeps me away, whatever ocean is locked inside.  
He went away all of a sudden,  
I am in pain, this wound is very new like.  
  
Don’t want to see the sky today,  
My favorite star has fallen out of it.  
Some people make you happy just by being there,  
I am restless, as if someone has snatched my peace.  
  
I am not the only one grieving today, yet,  
I am anxious, as if he has forgotten us.  
I didn’t know him, just saw his image on films,  
But I will still remember him often, as if I have lost someone mine. \]  


_## My tribute to Irrfan Khan. Really felt pained today. He was one of my favorite actors. Feels like someone my own has gone away. Don’t have words to express. ##_
