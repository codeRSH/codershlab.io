---
public: true
title: शगुन
date: 2014-06-28
tags:
  - 30-day-challenge
  - special
categories: hindi
---

> **भले कुछ न दिखाई दे,**  
> **तेरी आवाज हमेशा सुनाई दे,**  
> **जो साँझ हो और आये शगुन मौत का,**  
> **मैं ढलूँ धीरे धीरे… तेरी आवाज की परछाई में।**
> 
> **~रबी**

  
\[ Even if I don’t see anything,  
I hope to hear your voice,  
When the evening comes and brings the auspicious time of death,  
I will go down slowly, in the shadow of your voice. \]

_## This little piece has been in my draft for almost an year. I just couldn’t get my head around to completing the first two lines. I initially envisioned it as a full poem then tried to write a 4 liner and then asked friends for ideas. Nothing helped._

_But this seems just fine. Shagun - the auspicious word. ##_
