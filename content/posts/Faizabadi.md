---
public: true
title: फैज़ाबादी
date: 2012-04-23
tags:
  - philosophical
categories: hindi
---

> **कौन  सी  दुनिया  में  जीते  हो  फैज़ाबादी ! 
> क्या  तुम्हें  इतना  भी  नहीं  पता ?  
> अब  प्यार^ मांगने  से  नहीं  मिलता ,  
> छीनना  पड़ता  है।** 
> 
> **~रबी** 

\[ In which world do you live, Oh Faizabadi!  
Don’t you know even this much?  
Now, you don’t ask for Love^,  
you need to snatch it.  

_^ also applies for jobs, treats, seat in metro, comments on posts, and all things good in life._ \]
