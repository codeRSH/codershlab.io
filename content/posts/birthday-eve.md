---
public: true
title: birthday-eve
date: 2023-03-03
tags:
  - friend
  - witty
categories: english
---

> **Sir Happy First Birthday Eve,
> After finally meeting your Eve,
> Sure you'll have plans tomorrow Eve,
> But how about the Eve of that Eve?**
> 
> **Even if Putin invades the Kyiv,
> Modi can ask his fighting Peeps,
> To stop their army tanks and Jeeps,
> So his subjects can peacefully Leave .**
> 
> **My point is not to write lyrics so Cheap,
> But can you make a promise you can Keep,
> Hope tomorrow you will be on Leave,
> Will you find some time for us Jeeves?**
> 
> **~RavS**


_## A witty poem written on the eve of my friend Katy's birthday taking a dig at his busyness after getting married. ;) ##_
