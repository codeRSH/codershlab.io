---
public: true
title: "उन्हें क्यों कहता फिरूँ ?"
date: 2014-06-08
tags:
  - muse
  - song
categories: hindi
---

> **पागल कहती है दुनिया मुझे,**  
> **मैं उन्हें पागल कहता हूँ,**  
> **सच का पता है मुझे और तुझे,**  
> **तो फिर उन्हें क्यों कहता फिरूँ?**
> 
> **दुनिया हंसती है मुझ पे,**  
> **जो करता हूँ तारीफें तेरी,**  
> **कहती है क्यों लिखता हूँ उस पे,**  
> **जो किसी को दिखती ही नहीं,**  
> **तू नहीं इस दुनिया की, ये दुनिया समझती नहीं,**  
> **तो फिर क्यों उन्हें समझाने बैठूं ?**
> 
> **काफ़िर कहती है दुनिया मुझे,**  
> **मैं उन्हें काफ़िर कहते हूँ,**  
> **रब का पता है मुझे और तुझे,**  
> **तो फिर उन्हें क्यों कहता फिरूँ ?**  
>   
> **कहती रहे पागल दुनिया मुझे,**  
> **चाहे कहे काफ़िर सही,**  
> **लिखता रहूँगा तुझ पे मैं,**  
> **मुझे कुछ और आता ही नहीं,**  
> **दुनिया से तनहा सही, पर जो मिली तेरी तन्हाई,**  
> **तो कहीं सच में पागल न हो जाऊं।**
> 
> **नासमझ कहती है दुनिया मुझे,**  
> **मैं उन्हें नासमझ कहता हूँ,**  
> **समझता हूँ मैं तुझे तू मुझे,**  
> **तो फिर उन्हें क्यों कहता फिरूँ ?**
> 
> **~रबी**


\[ The world calls me crazy,  
I say the world is crazy,  
I know the truth, and you,  
Then why should I go and explain them?

The world laughs at me,  
When I praise you,  
They ask, why do I write on someone,  
No one has seen whom,  
You aren’t of this world, this world does not understand this,  
Why then should I go and explain them?

The world calls me atheist,  
I say the world is atheist,  
I know about the Lord and you,  
Then why should I go and explain them?  
   
Let the world call me crazy,  
Let it call me atheist,  
I’ll keep on writing on you,  
I do not know anything else;  
Let me be alone in the world, But if you leave me too,  
I am afraid, I might really go crazy.

The world calls me stupid,  
I say the world is stupid,  
I understand you and you understand me,  
Then why should I go and explain them? \]

_## Another attempt to write a song. Here, the writer talks to his muse about how the  world doesn’t understand his affection  for her and the feelings he pours in his writing. But he decides to still keep writing because that’s what he can do. ##_
