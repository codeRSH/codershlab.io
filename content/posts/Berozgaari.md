---
public: true
title: बेरोजगारी
date: 2014-04-20
tags:
  - expecations
  - disappointment
categories: hindi
---

> **पता नहीं इसे उन्हें गलती से मिले फुर्सत के पल,**  
> **या हमारी बेरोजगारी का आलम कहते हैं,**  
> **की लिखते तो हैं हम सुबह-सुबह,**  
> **लोग पढ़ते-पढ़ते शाम कर देते हैं।**  
> 
> **~रबी**

  
\[ Don’t know if it’s called a moment’s breather given to them,  
Or the fact that we don’t have any work to do,  
That we write it very early in the morning,  
But by the time they read it, it gets late in the evening. \]

_## I have gotten into this really bad habit to expect immediate response on anything that I create. The response (good, bad or ugly) if any, is not in my control. My job should be to just create whatever the heck I can within my limited capabilities and then share it with the universe. Let the universe take its time to experience it, if at all it wants. ##_
