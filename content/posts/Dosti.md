---
public: true
title: दोस्ती
date: 2013-03-03
tags:
  - friendship
  - betrayal
categories: hindi
---

> **धोखे इस कदर दोस्ती में खाए मेरे दोस्त,  
> दोस्त तो रह गए, पर दोस्ती न रही।**
> 
> **~रबी**


\[ I got betrayed in friendship, to such an extent, my friend,  
We remained friends, but there wasn’t friendship anymore. \]
