---
public: true
title: I Am Alive
date: 2012-07-03
tags:
  - death
  - separation
  - friends
categories: english
---

> **And tomorrow, if you hear that your friend has died,  
> Don’t believe what the world says, they are true lies,**
> 
> **I may not be with you tonight,  
> but…**
> 
> **Believe in me and my 9 words…  
> _“I am Alive, I am Alive, I am Alive”_.**
> 
> **~RavS**
