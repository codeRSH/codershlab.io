---
public: true
title: "फरवरी की धूप"
date: 2014-02-06
tags:
  - romantic
  - sufi
categories: hindi
---

![ ](../images/farvari-ki-dhoop.jpg)

> **तू फरवरी की धूप सी,**  
> **गुनगुनी सी खिली खिली ,**  
> **तू ओस गहरी रात की,**  
> **जैसे घास पे बिछी सो रही,**
> 
> **तू हाथ की लकीर सी,**  
> **किसी के नसीब में किसी के नहीं,**  
> **तू दुआ किसी फ़कीर की,**  
> **वो खुशनसीब जिसे मिल गयी।**
> 
> **जिसकी तलब तो है सबको ही ,**  
> **तू चीनी में घुली ऐसी मिठास सी,**  
> **मगर जो चाह कर भी ना मिल सकी,**  
> **जैसे हया हो किसी हिजाब की।**
> 
> **तुझे देखता तो हूँ मैं रोज ही,**  
> **गिला है फकत इस बात की,**  
> **तू फरवरी की धूप सी,**  
> **बस आज है, फिर कल नहीं।**
> 
> **~रबी**


\[ You are like Feb’s sunshine,  
A little warm, a little shine,  
You are like dew from a dark night,  
sleeping on the edge of grass lines.

You are like streaks on hand,  
Not everyone can get it,  
You are a blessings from a begger,  
He will be lucky to have you.

Everyone’s addicted to you,  
You are like sweetness of sugar,  
But no-one can get you,  
You are an innocence in disguise.

I see you everyday,  
But I have just one complain,  
You are like Feb’s sunshine,  
You will be gone, when Feb’s gone. \]
