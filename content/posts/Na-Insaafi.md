---
public: true
title: ना-इंसाफी
date: 2013-07-24
tags:
  - faaltu
  - lizard
categories: hindi
---

> **तू पास रहे न रहे,  
> तेरे पास होने का एहसास काफ़ी है,  
> जाना है तो दिल-ओ-दिमाग से जा,  
> बस नज़रों से दूर जाना नाकाफ़ी है।**  
>   
> **जब तक जागूं, तब तक सब्र,  
> जब सोऊं तो तेरी फ़िक्र सताती है,  
> मेरा क्या कसूर फिर जो,  
> तेरे ज़िक्र भर से रूह काँप जाती है।**  
>   
> **जाऊं चाहे किसी भी कोने में,  
> साथ मेरे होती तेरी परछाई है।    
> बिन बताये सर पर कूद जाना,  
> ऐ घर दी छिपकली, ये तो बहुत ही ना-इंसाफी है!**
> 
> **~रबी** 

\[ Whether you are near or not,  
Your feelings are always nearby,  
If you want to go, go from mind & heart,  
Only going away from eyes is not enough.  
  
It’s alright, till the time I am awake,  
But your fear engulfs me when I sleep,  
So tell me what’s my fault,  
If I start trembling just by your thoughts.  
  
No matter where I go,  
Your shadows is always near,  
Without telling, you jump on my head,  
This is really not fair, dear home lizard! \]
