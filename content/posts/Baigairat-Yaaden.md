---
public: true
title: बेगैरत यादें
date: 2014-09-17
tags:
  - misunderstanding
categories: hindi
---

> **मैं चुरा लाया था उसे अपने साथ बहुत दूर तलक,**  
> **सोचा था उसे उसकी यादों से बचा लूंगा,**  
> **मुझे क्या पता था बेगैरत यादें अपनी जेब में रख कर निकला था।**
> 
> **~रबी**


\[ I stole her and took her very far away with me,  
I thought I would save her from her memories,  
I didn’t know the heedless had kept her memories in her pocket when she started. \]
