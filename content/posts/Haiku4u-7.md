---
public: true
title: Haiku4U - 7
date: 2019-04-07
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-7.jpg)

> **Profit, ‘cause you’ll be  
> worth more than  
> sum of all the troubles.**
>
> **~RavS**


_## And.. I don’t care about the kind of troubles I will have to go through to get you. Ultimately you will be worth much more than anything and everything I do for you. I bet. ##_
