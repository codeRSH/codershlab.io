---
public: true
title: Haiku4U - 21
date: 2019-04-21
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-21.jpg)

> **I need skin,  
> I need warmth.  
> Words are not enough sometimes.**
>
> **~RavS**


_## To be far away from each other for long, it’s not easy. It gets frustrating. We might talk on phone for hours. We might text throughout the day. And we might assure each other we are close._

_But sometimes a touch is what’s needed, more than the thousand words of assurance. ##_
