---
public: true
title: Why?
date: 2015-07-02
tags:
  - love
  - frustration
categories: english
---

![ ](../images/why.jpg)

> **Yeah just do one thing, take this dagger and pierce it right through the ribs into my lungs.  
> Loving you is probably the simplest thing a man can ever get to do.  
> And yet, at times, it feels so overwhelming, only death seems to be a better alternative.**
>
> **Why?**
>
> **~RavS**
