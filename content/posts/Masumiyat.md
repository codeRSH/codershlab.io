---
public: true
title: मासूमियत
date: 2011-11-26
tags:
  - romantic
categories: hindi
---

> **माना  तू  इतनी  हसीं  है  जिसका  कोई  बयान  नहीं,  
> वैसे  ही  तुझे  भुला  पाना  आसान  नहीं,  
> पर  हद   तो  तब  हो  जाती  है,  
> जब  दूसरों  की  मासूमियत  देख  कर  भी  तू  ही  याद  आती  है।**  
> 
> **~रबी**


\[ I agree, that you are so beautiful, it’s impossible to describe,  
Anyway, it’s difficult to forget you,  
But it gets a little too much for me,  
When I see you in others’ innocence too. \]
