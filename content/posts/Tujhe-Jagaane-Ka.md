---
public: true
title: तुझे जगाने का मन नहीं करता...
date: 2012-02-04
tags:
  - main-blog 
  - special
  - romantic 
categories: hindi
---

> **सर रक्खा है मेरे कांधे पर,  
> जाना था कहीं,  
> पर तुझे जगाने का मन नहीं करता।** 
> 
> **सहा नहीं जाता,  
> इतना सुकूं है तेरे चेहरे पर,  
> पर इसे हटाने का मन नहीं करता।** 
> 
> **तेरे जुल्फें खेलती हैं मेरे गालों से,  
> हारना है पसंद,  
> पर इन्हें हराने का मन नहीं करता।** 
> 
> **दुआ है रब से,  
> ये पल कभी खत्म ना हो,  
> इस पल से आगे जाने का मन नहीं करता।** 
> 
> **अब ऐसा होना तो मुमकिन नहीं,  
> की हर बार मेरा कंधा तेरा तकिया बन सके,  
> पर ऐसा ना चाहने का भी मन नहीं करता।** 
> 
> **~रबी**

\[ Your head rests on my shoulder,  
I had to go somewhere,  
But I don't feel like waking you.  

I can't bear it,  
There's so much peace on your face,  
But I don't feel like moving it.  

Your hair plays with my cheeks,  
I would like to lose (to them),  
But I don't feel like stopping them.  

My prayer to God is,  
May this moment never end,  
I don't feel like moving beyond this moment.  

Now, it's not possible,  
That every time my shoulder can become your pillow,  
But I also don't feel like not wishing for it. \]
