---
public: true
title: Haiku4U - 9
date: 2019-04-09
tags:
  - haiku4u
  - love
categories: english
---

![ ](../images/h4u-9.jpg)

> **As the dawn creeps in,  
> I kiss the petals  
> on your eyes.**
>
> **~RavS**


_## Wouldn’t you want to be awoken in the morning by a little kiss on your sleeping eyes ? Don’t lie. Everyone loves that. :)  ##_
