---
public: true
title: मीठा
date: 2012-02-08
tags:
  - romantic
categories: hindi
---

> **शिकायत  उन्हें  ये  रहती  है ,**   
> **कि  हम  उनसे  बहुत  कम  मिलते  हैं ,**  
> **ना  ज़्यादा  उनकी  सुनते  ही  हैं।**    
>   
> **वो  ये  मगर  समझ  पाते  नहीं ,**  
> **हमें  बहुत  ज्यादा  मीठा  देखने - सुनने  की  आदत  नहीं …**
> 
> **~रबी**


\[ She complaints, that I seldom see her,  
Nor do I listen to her frequently.  
  
But she is not able to understand,  
That I am not used to seeing and hearing that much sweetness together… \]
