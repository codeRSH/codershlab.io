---
public: true
title: बिखरी-बिखरी
date: 2012-02-02
tags:
  - girl-view
  - special
  - sad
categories: hindi
---

> **बिखरी-बिखरी  सी  थी  ज़िन्दगी  मेरी ,  
> तुम  जो  आये  तो  लगा  ये  संवर  सी  गयी।**    
>   
> **अकेली  समझ  डराती-धमकाती  थी  लोगों  की  नज़रें ,  
> तुम्हें  देख  मेरे  साथ , वो  भी  सिहर  सी  गयीं।**    
>   
> **बुझी - बुझी  मुरझाई  सी  थी  काया  जिसकी ,  
> तुम्हारे  हाथों  में  पड़  वो  भी  निखर  सी  गयी।**  
> 
> **शिकायत  हमें  बस  तुमसे  थी  इतनी  सी,  
> तुम्हें  ज़िन्दगी  में  आने  में  सालों  लग  गए,  
> पर  तुम्हारे  जाते   हुए  क़दमों  में  एक  पल  की  हिचक  ना  थी।**   
>   
> **और  जो  ज़िन्दगी तिनका-तिनका  कर  बस  अभी  संवारी  थी,  
> वो  पल  भर  में  फिर  से  बिखर  सी  गयी।**  
> 
> **~रबी**


\[ My life was a scattered mess,  
When you came, I thought it got transformed.

People used to scare me with their looks,  
When they saw you with me, they also got scared.

Faded, almost extinguished had become, my body,  
When you took me in your arms, it also got transformed.

Now, I only have a little complaint from you,  
That you took ages to come in my life,  
But you didn’t take a second to go away.

And so the life that got transformed piece by piece,  
It got scattered again in a second. \]
