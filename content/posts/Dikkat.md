---
public: true
title: दिक्कत
date: 2014-10-15
tags:
  - problem
categories: hindi
---

> **तू कुछ कहे तो दिक्कत है ,**  
> **तू कुछ ना कहे तो दिक्कत है ,**  
> **तू मिल गयी तो रब ही जाने क्या करूँगा तेरा,**  
> **तू ना मिले तो दिक्कत है।**  
>   
> **~रबी**

  
\[ It’s a problem if you say something,  
It’s a problem if you say nothing,  
Only God knows what will I do with you, if I get you,  
I am concerned about the problem if I don’t get you. \]
