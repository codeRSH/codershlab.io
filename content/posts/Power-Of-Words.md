---
public: true
title: Power of Words
date: 2015-04-02
tags:
  - poet
categories: english
---

![ ](../images/power-of-words.png)

> **They said I am a poet.  
> I used to take that as a compliment.  
> But now I understand, it’s a responsibility.  
> A responsibility to tell the story that was left  unsaid,  
> To unearth the emotions that were left unfelt,  
> To force the nature to do things that were left undone.**
>
> **Up until now I never quite understood how a pen could be mightier than a sword.  
> But now I know.  
> A sword can only kill,  
> Words can both kill and save someone.**
>
> **~RavS**
