---
public: true
title: समझ  पातीं
date: 2012-01-29
tags:
  - romantic
  - hope
categories: hindi
---

> **तुम्हें  जो  बताने  की  चाहत  थी ,**  
> **काश  तुम  मेरी  वो  बात  समझ  पातीं।**  
>   
> **अलफ़ाज़  तो  वो  ही  हैं  सीधे  से, जो  तुमने  पढ़े  अभी-अभी ,**  
> **काश  उनके  पीछे  की  आवाज़  समझ  पातीं।**  
>   
> **लिखा  भी  तुम्हारी  बोली  में  ही  था, बस  बात  बेबाक  नहीं  लिखी ,**  
> **काश  मेरे  लिखने  का  अंदाज़  समझ  पातीं।**
> 
> **~रबी** 

\[ The ones I wanted to convey,  
I wish you could have understood those feelings,  
  
The words were simple, that you read just now,  
I wish you could have understood the voice behind them.  
  
I wrote them in your language only, just that I didn’t write frankly,  
I wish you could have understood my style of writing. \]
