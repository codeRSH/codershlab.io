---
public: true
title: If it were possible
date: 2015-01-07
tags:
  - free-verse
  - reasoning
categories: english
---

![ ](../images/if-it-were-possible.png)

> **If it were possible; long ago,**  
> **She would have given you an affirmation of your dreams,**  
> **Or handed over a negation of your desires.**
>
> **If it were possible; long ago,**  
> **She would have committed to the tasks in hand,**  
> **Or rolled back the efforts made till now.**
>
> **Unfortunately life is not lived in Binary.**  
> **There’s a huge flux of reasoning in between.**
> 
> **~RavS**
