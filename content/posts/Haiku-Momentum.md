---
public: true
title: Haiku - Momentum
date: 2017-02-14
tags:
  - haiku
  - love
categories: english
---

![ ](../images/haiku-momentum.jpg)

> **Be the momentum to my inertia,  
> The force to my resistance.  
> The antidote to my disease.**  
> 
> **~RavS**


_##  It’s every thinking man’s dream, that his lady is his ultimate inspiration and source of motivation. Time and again, I get into this ugly web of mind where I am not able to move forward in life. I just feel sick not being able to understand how to get out the mess._  
  
_And these times, I need a helping hand to take me out of ditch. So I can start moving again. That’s where I need you the most. ##_
