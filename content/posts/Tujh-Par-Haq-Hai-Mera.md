---
public: true
title: तुझ पर  हक़  है  मेरा
date: 2015-01-17
tags:
  - song
categories: hindi
---

> **\[ तुझ पर  हक़  है  मेरा ,**  
> **पर  जताऊं  कैसे।**  
> **मैं  जताऊं कैसे।**  
> **करता  हूँ  प्यार  तुझसे ,**  
> **पर  दिखाऊं  कैसे।**  
> **मैं  दिखाऊं  कैसे... \]**
>
> **तू  ही  छुपा  सकता  है  मुझे**  
> **अंधेरो में  खो  जाने   से।**  
> **तू  ही  बचा  सकता  है  मुझे ,**  
> **गर्त  में  गिर  जाने  से ,**  
> **तू   ही  ले  जा   सकता   है  मुझे ,**  
> **दूर  दुश्मन  जमाने   से।**  
> **जमाने   से...**
>
> **तूने  अकेला  जो  छोड़ा  हमें ,**  
> **तन्हां  मर  ही  जाएंगे।**  
> **तू   जिस  पल  हुआ  जुदा  हमसे ,**  
> **वो  लम्हा  कैसे  बिताएंगे।**  
> **मत  जाना  इतना   दूर  हमसे  ,**  
> **की  वापस  ला  भी  ना  पाएंगे।**  
> **ला  ना  पाएंगे...**
>
> **~रबी**

  

\[ \[ I have right on you,  
But how do I claim it.  
How do I claim it.  
I love you.  
But how do I show it.  
How do I show it. \]

Only you can hide me,  
from getting lost in the shadows.  
Only You can save me,  
from falling in the troughs.  
Only You can take me,  
away from the enemy world.  
from the enemy world ….

If you left me,  
I would die alone.  
The moment you part from me,  
How would I spend that moment.  
Do not go away so far,  
That I can’t even bring you back.  
Can’t bring you back…\]
