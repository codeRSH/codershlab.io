---
public: true
title: If I were you...
date: 2016-07-02
tags:
  - hypocrisy
  - wish
categories: english
---

![ ](../images/if-i-were-you.jpg)

> **If I were you,  
> I would have done that,  
> Made that, and loved that.  
> But I am I.  
> So I do this,  
> Make this and love this.**  
> 
> **Easier said this, than done that.**
> 
> **~RavS**
