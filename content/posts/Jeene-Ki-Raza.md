---
public: true
title: जीने की रज़ा
date: 2014-03-13
tags:
  - separation
  - hurt
categories: hindi
---

> **तुम गयी, सांस गयी,**  
> **सांस लेने कि वजह गयी,**  
> **जिस फ़िज़ा कि सोहबत में जीता था,**  
> **तेरे संग संग वो फ़िज़ा गयी.**  
> **कुछ दीवाने कहते थे, बहार है बहार है,**  
> **तुम गयी, तो लगता है आ खिज़ा गयी,**  
> **तुमको देखा तो आते आते,**  
> **एक पल को रुक क़ज़ा गयी.**  
> **अब तो तुम ही बताओ किस के लिए जियें,**  
> **जो थोड़ी थी बची हुई,**  
> **तुम गयी, तो वो जीने की भी रज़ा गई।**
> 
> **~रबी**


\[ You went away, you took the breath away,  
You took the reason to breathe everyday,  
The breeze, I used to walk with,  
With you, that breeze went away.  
There were a few lunatics, shouting it’s spring, it’s spring!  
You went, it feels the autumn has come again.  
When it saw you the first time,  
Even the death stopped in the middle somewhere.  
So, you tell me, for whom should I live now,  
That little bit that was left in me,  
With you, that spirit of living has also gone away. \]
