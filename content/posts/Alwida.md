---
public: true
title: अलविदा
date: 2023-06-28
tags:
  - good-bye
categories: hindi
---

> **कभी हम भी चले जाएंगे ऐसे ही एक दिन,  
> तुम्हें अलविदा कहना है, तुम आज ही कह दो।  
> कहीं तुम तो ना चले जाओगे ऐसे ही एक दिन?  
> कुछ समय तो दोगे मुझे अलविदा कहने को ?  
> क्योंकि वो भी चले गए यों ही बिन कहे,  
> हमें अलविदा कहना था, ना कह सके।**  
>
> **अलविदा...**  
>
> **~रबी**  

\[ One day, we too shall depart just like this,  
Say your goodbyes to me today, if you wish.  
What if you were to leave me, suddenly one day?  
Would you give me a moment to say goodbye, I pray?  
For they too left without a word, it's true,  
We wanted to say goodbye, but couldn't do.  

Good Bye... \]
