---
public: true
title: सरकार
date: 2025-02-04
tags:
  - desperation
  - frustration
categories: hindi
---

> **दुआ भींच के मुट्ठी में, दवा की कतारें लगती हैं।  
> बोलियां सांसों की भी यहाँ बीमारों की लगती हैं।  
> जो चिल्लाते हैं, बचे कुछ फरिश्ते इधर-उधर हैं,  
> वरना सन्नाटा पसरा रहता है, ये मुर्दों का शहर है।**  
> 
> **जहाँ देखो मातम सा छाया लगता है,  
> जैसे किसी बुरे सपने का साया लगता है।  
> बंद कर लो, आँखें बंद करने से क्या होगा?  
> कराहती चीखें, बिलबिलाती सिसकियों का,  
> जैसे कोहराम सा आया लगता है।**  
> 
> **इतना असहाय, इतनी बेबसी, इतनी लाचारी,  
> बीमारी से भी बढ़कर लगती है ये बीमारी।  
> हर खांसी में जान हलक तक आ जाती है बाप की,  
> उसके जाने बाद कैसे रहेगी उसकी बेटी बेचारी।**  
> 
> **हर वक्त एक बुरी खबर सुनने की आदत  बना लेनी  चाहिए,  
> बेचारगी महसूस करने को अब फितरत बना लेनी चाहिए।  
> करो खुदा से  गुहार, बाकी सब  अपने  में लगे हैं,  
> मगर उसे भी ना-उम्मीदगी की मिन्नत मान लेनी चाहिए।**  
> 
> **सरकार! सुनो तो हमारी, हम बस गिनती नहीं हैं।  
> मत मानो आदेश हमारा, हमारी तो बस बिनती ही है।  
> पट्टी मुंह पर बांधनी थी, आपने तो आँख पर बाँध ली है।  
> खुद भूल कर हमें याद दिलाया,  दूरी का फरमान "गज दो",  
> अब हमारे मरने वालों के लिए कफन का इंतजाम कर दो,  
> और जा सके वो चैन से, "दो गज" मिट्टी का एहसान कर दो।**  
> 
> **~रबी**


\[ With fists clenched in prayer, people queue for medicines.  
(Even) the whispers of breaths here sound like those of the sick.  
Those who cry out, a few angels remain scattered here and there,  
Otherwise, silence prevails; this is a city of the dead.  

Wherever you look, a sense of mourning seems to loom,  
As if it’s the shadow of a nightmare.  
Close your eyes, but what will closing them achieve?  
(The sounds of) moaning cries, whimpering sobs,  
It seems as if a great chaos has descended.  

Such helplessness, such powerlessness, such destitution,  
This feels like a disease worse than the disease itself.  
With every cough, a father feels his life reach his throat,  
How will his poor daughter survive after he is gone?  

One should get used to hearing bad news all the time,  
One should now make helplessness one's nature.  
Plead only with God, everyone else is preoccupied with themselves,  
But one should even accept despair as a request from Him.  

Government! Listen to us, we are not just numbers.  
Don't consider it our command, it is only our humble plea.  
You were supposed to tie a cloth over your mouth, but you have tied it over your eyes.  
Forgetting yourself, you reminded us of the "two yards" decree of distance,  
Now arrange for shrouds for those who are dying among us,  
And so that they may depart in peace, bestow the favor of "two yards" of earth. \]


_## An emotional poem written out of despair during Covid time. I pray to god that nightmarish phase never comes back in our lives. ##_
