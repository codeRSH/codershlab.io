---
public: true
title: ज़िन्दगी   बस   कुछ   इस  तरह  से गुज़र  गयी...
date: 2015-02-19
tags:
  - life
  - her
categories: hindi
---

> **ज़िन्दगी  कुछ  इस  तरह  से  गुज़रती  गयी ,**  
> **कुछ  उसकी  याद  में, कुछ  उसकी  आस  में ,**  
> **कुछ  उससे  , कुछ  उसी की  बात  में ,**  
> **मुट्ठियाँ  बंद  करता  रहा, वो   फिसलती  गयी।**
>
> **कुछ  लड़ते  लड़ते, अंहम  को  लगी ठेस  में।**  
> **कुछ  नाराज़गी, कुछ  अफ़सोस  में ,**  
> **मैं  चीखता  रहा, वो  दूर  होती  गयी।**
>
> **कुछ  सोचने  में, कुछ  समझने  में ,**  
> **कुछ  गलतफहमियों  से  निकालने, निकलने  में ,**  
> **बेबस  खड़ा  रहा, वो  धीरे  धीरे  कहीं  खोती  गयी।**
>
> **कुछ  बेकरारी, कुछ  लाचारी  में ,**  
> **कुछ  अल्ल्हड़पन,  कुछ  खुमारी  में ,**  
> **कभी  खुद  को  कभी  उसको  ढूंढता  रहा ,**  
> **जब  मैं  मिलता,  तो   वो   धुंधला जाती ,**  
> **जब  वो  संभलती, तो  मैं  नहीं।**  
> **ज़िन्दगी   बस   कुछ   इस  तरह  से गुज़र  गयी…**
>
> **~रबी**


\[ Life went on a little like this…  
A little in her memories, a little in her hope,  
A little in talking with her, a little in talking about her,  
I kept clinching my fists, and she kept slipping by.

Some in fighting, some in feeling the hurt on ego,  
Some in resentment,some in regrets,  
I kept yelling, she kept becoming distant.

Some in thinking, some in understanding,  
Some in getting and pulling out of misunderstandings,  
I stood there helpless, she faded away slowly.

Some in discomfort, some in helplessness,  
Some in innocent stupidity, some in hangover,  
Sometimes I would search for myself or for her,  
When I would find myself, she would fade away,  
When she would come in her sense, then I won’t be.  
Life went by just like this…\]
