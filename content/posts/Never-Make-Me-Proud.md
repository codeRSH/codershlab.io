---
public: true
title: You Will Never Make Me Proud
date: 2019-05-12
tags:
  - pain
  - helpless
  - heartbreak
categories: english
---

![ ](../images/you-will-never-make-me-proud.png)

> **I’ve tried to give you everything I’ve ever had,  
> My time, my love, my advice - good and bad.  
> But all I have always got from you in return,  
> Is disgrace, disappointment and heartburn.  
> You’ve been a liar, an addict, a thug & a thief,  
> And I always had to pay for your mischieves.  
> I have forgotten and forgiven your every crime,  
> In the delusion that this might be the last time.**
>
> **Now I don’t know how to handle even myself,  
> What’s the point of even trying to help?  
> I feel like a cocoon holding a black hole,  
> I want to leave, but I know you will implode.  
> But if I don’t leave, you will eat me alive.  
> So either I perish with you or watch you die.  
> You have broken my heart and filled it with void,  
> And it’s so painful to know that i have no choice.**
>
> **I never expected you to share my pains,  
> I just wanted my efforts to not go in vain.  
> You have shattered my confidence, to such an extent,  
> Why am I related to you, every single day I repent.  
> I have often wondered if it’s just my fate,  
> Other times I have questioned if it’s my mistake?  
> I have hoped against hope, but am convinced now.  
> I’ll never see the day when you’ll ever make me proud.**
>
> **~RavS**


_## Some things can only be expressed indirectly. ##_
