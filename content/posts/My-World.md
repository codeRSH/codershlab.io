---
public: true
title: My World
date: 2015-02-20
tags:
  - free-verse
  - world
categories: english 
---

![ ](../images/my-world.png)

> **I feel I am surrounded by vultures who are hungry for my flesh and skin.**  
> **And the only way to save myself is to become a flesh-eater myself.**  
> **I am tired of having to compete for things that don’t matter to me.**  
> **And I am tired of trying to explain myself to the ones who would never understand me.**  
> **All this rationality and logics of the world makes me sick through my bones…**  
> **I want to go live in a world where irrationality rules.**  
> **Where people are mad, so that even if they don’t understand my insanity, they won’t judge me at least.**  
> **Where no one would  laugh when I say I can achieve anything I want to.**  
> **Where no one would lecture me about the rules, because none exists.**  
> **Where I would be free to create and break my own customs,  tread my own path, and decide what, when and how to love.**  
> **That’s the world I dream to live in one day.**  
> **And take my own world there, with me.**
>
> **~RavS**
