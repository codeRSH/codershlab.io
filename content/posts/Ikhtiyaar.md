---
public: true
title: इख्तियार
date: 2014-07-14
tags:
  - 30-day-challenge
categories: hindi
---

> **इंतज़ार ही तो किया है ज़िन्दगी भर, उसमे क्या है,**  
> **लौटने के लिए तुम्हे चाहिए ज़िन्दगी भर, तो उसमे क्या है,**  
> **तुम्हे सब्र का इम्तेहान लेना है तो शौक से लो,**  
> **इख्तियार करना हमें भी खूब आता है, उसमे क्या है।**
> 
> **~रबी**

  
\[ I’ve only been waiting all my life, what’s in it,  
To come back, if you need your whole life, so what’s in it,  
If you would like to test my patience, be my guest,  
I know how to control myself, what’s in it. \]
