---
public: true
title: "उसमे थोड़ा सच भी था"
date: 2014-04-27
tags:
  - special
  - dedication
  - friend
  - eyes
categories: hindi
---

![ ](../images/usme-thoda-sach-bhi-tha.jpg)

> **उसने कहा, मेरी आँखों में देखो और बताओ क्या दिखता है?**  
> **मैंने कहा, निहायत ही घटिया ये सवाल कैसा है?**  
> **उसने कहा बकवास बंद करो मुझे जानना है!**  
> **मैंने कहा, अच्छा फिर सुनो, तुम्हारी आँखों पर मेरा क्या मानना है।**
> 
> **वो सामने बैठी और गौर से सुनने लगी,**  
> **मैंने गाला साफ़ कर दास्तान-ए-अँखियाँ शुरू की …**
> 
> **क्या तुमने किसी बच्चे की आँखों में देखा है ?**  
> **मासूमियत के सिवा उनमे कुछ और कभी मिलता है ?**  
> **वो ही मासूमियत दिखती है तुम्हारी आँखों में।**  
> **एक मासूमियत जो कभी घटती नहीं,**  
> **एक मासूमियत जो कभी मिटटी नहीं।**
> 
> **क्या तुमने कभी किसी कैफ़ी की आँखों में देखा है ?**  
> **एक अजीब सी मसर्रत दिखती है उसकी आँखों में।**  
> **वो ही मसर्रत नजर आती है तुम्हारी आँखों में।**  
> **एक मसर्रत जो छिपाने से भी छिपती नहीं,**  
> **एक मसर्रत जो ढूंढने पर भी मिलती नहीं।**
> 
> **कुछ देर वो चुप बैठी, फिर बोली, “हैं… सच्ची?!”**  
> **मैं जोर से हंसा, बोला, “चल पागल लड़की!”**  
> **ऐसा भी कभी कहीं होता है?**  
> **आँखों में भी कभी किसी ने मासूमियत और मसर्रत को देखा है ?**  
> **वो बोली, भाग जाओ यहाँ से, अब बात मत करना।**  
> **दोस्त बन गए तुम किसी मनहूस घड़ी में,**  
> **मैं कहती हूँ, दूर हो जाओ नज़रों से, वर्ना…**
> 
> **मैं मुस्कुराया, बोला, अरे दोस्त नाराज़ नहीं हो,**  
> **लेकिन मैं वो नहीं कह सकता जो तुम सुनना चाहती हो।**  
> **मासूमियत, मसर्रत कहाँ रह गई है किसी की आँखों में अब ,**  
> **ये लफ्ज़ बस नज़र आते हैं कविताओं-किताबों में अब।**
> 
> **सुनो मैं बताता हूँ, मुझे तुम्हारी आँखों में क्या दिखता है …**  
> **तुम्हारी दाहिनी आँख में किसी बात को लेकर बहुत गुस्सा है,**  
> **जो तुम्हारी बाईं आँख तक भी थोड़ा थोड़ा पहुँचता है।**  
> **एक मंशा, एक आग है तुम्हारी आँखों में ,**  
> **जिसे पलकों ने बड़े हौले से छुपा रखा है।**  
> **कुछ सपने तैरते हैं इन पुतलियों में ,**  
> **जैसे तैरती नाव हो एक सफ़ेद सागर में।**
> 
> **कभी अपनी आँखों में झांकना, तुम्हे कुछ सूखे आँसूं नज़र आएंगे,**  
> **जिन्होंने आँखों में ही दम तोड़ दिया, वो बेजार जुगुनू नज़र आएंगे।**  
> **जिसको मैं कब से समझने की कोशिश में हूँ, एक कहानी नज़र आएगी ,**  
> **शायद तुम भी न जिसे बूझ सको, एक पहेली नज़र आएगी।**  
> **एक दर्द नज़र आएगा, एक मरहम नज़र आएगा,**  
> **एक कोशिश नज़र आएगी, एक भरहम नज़र आएगा।**
> 
> **उसने हाथ जोड़ लिए, बोली बस मियाँ, बस,**  
> **मैंने क्यों ही आपसे कुछ पूछा।**  
> **जाने किस कीड़े न काट लिया था,**  
> **जो मुझे ये सवाल करना ही सूझा।**  
> **मैं बोला, निहायत ही घटिया सवाल है,**  
> **मैंने तो पहले ही कहा था।**  
> **पर अब तुम मुझसे मजे ले रही थी,**  
> **तो मैं भी तबसे मजे ही ले रहा था !**
> 
> **तुनक पड़ी, तुम्हारे बस की कुछ नहीं है “रेवो”,**  
> **क्या पूछो तो न जाने क्या क्या बतातो हो।**  
> **इतना बोला, फिर वो उठ कर चल दी।**  
> **मैं मुस्कुराया, मन में सोचा, शेर-दिल लड़की…**
> 
> **कभी इसकी ज़िन्दगी में भी कोई खुशियाँ लेके आये ,**  
> **जो ये चाहती है, कम-स-कम आधी बातें तो सच हो जाएँ।**  
> **क्योंकि बोला तो मैंने सब मजाक मजाक में ही था,**  
> **लेकिन वो सब मजाक, मजाक नहीं, उसमे थोड़ा सच भी था।**
> 
> **~रबी**


\[ She asked, Look into my eyes and tell, what can you see?  
I said, What an utterly ridiculous question is this?  
She said, Stop your non-sense, I want to know!!  
I said, Alright, then listen, what I feel about your eyes.

She sat before me, and started listening carefully,  
I cleared my throat, and started the ‘Tail Of The Eyes’…

Have you ever gazed into the eyes of a baby?  
Do you find anything but innocence in them?  
Your eyes show the same innocence,  
An innocence that never diminishes,  
An innocence that never vanishes.

Have you ever looked into the eyes of a drunk?,  
There’s a strange kind of happiness in his eyes.  
I see that same happiness in your eyes.  
A happiness that can’t be hidden even if tried,  
A happiness that can’t be found even if looked for.

She sat quiet for a while, then said, “Umm… Really?!”  
I laughed aloud, said, “Shooo, you silly girl!”  
Does this ever happens anywhere?  
Has anyone ever seen innocence and happiness in eyes?  
She said, Go away from here, never talk to me again now.  
Don’t know in what fateful moment did you become my friend.  
I say, just go away from my sight, or else…

I smiled, said, Oh my Friend!! Don’t be angry now,  
But I can’t say what you want to hear.  
Where is any innocence and happiness left in anyone’s eyes now?  
These words are just found in poetry and novels now.

Listen, I will tell you, what I see in your eyes,  
In your right eye, there is extreme anger about something,  
That somewhat reaches your left eye too.  
A desire, a fire is there in your eyes,  
That your lashes have kept hidden gently.  
Some dreams float in your pupils,  
Like a boat swimming in a white sea.

Peep into your eyes someday, you’d find some dry tears;,  
The ones who died in the eyes itself, you’d find such fireflies,  
The one that I am still trying to understand, you’d find that story there,  
Even you might not be able to decipher it, you’d find a riddle inside.  
You’d find pain, You’d find a salve,  
You’d find efforts, You’d find an illusion.

She folded her hands, said, Enough Mister, Please Stop!!  
Why did I even bother to ask you.  
God knows which bug bit me,  
That it struck me to ask this question.  
I said, Utterly ridiculous question it is,  
I told you earlier too.  
But then you were kidding with me,  
So I was also just fooling around too!

She got furious, You can’t do anything Ravoo,  
What I ask you, and what you reply.  
She finished, got up and left.  
I smiled, and thought in my mind, Girl with a Lion’s Heart….

Someone should bring happiness in her life as well,  
What she desires, I pray she gets at least half of it.  
Because I did say all that as a joke,  
But not all of it was, that humor had a bit of truth too. \]

_## Written for a very special friend :) And translated by another great friend. I will keep both of them unnamed._ _This becomes my longest poem yet. And I have probably for the first time tried to tell a little story through a Hindi poem. ##_
