---
public: true
title: All of it?
date: 2014-10-13
tags:
  - music
categories: english
---

> **Can you be the music to my words,**  
> **Can you be the rhyme in my lyrics,**  
> **Can you be the rhythm in my phrases,**  
> **Or the tune for me to sing?**  
>   
> **Can you be the melody in my chords,**  
> **Can you be the echo in my vocals,**  
> **Can you be the sharpness in my scales,**  
> **Or the harmony in my octaves?**  
>   
> **Can you be any of this?**  
> **Or may be… all of it?**
>
> **~RavS**
