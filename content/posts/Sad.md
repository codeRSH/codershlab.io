---
public: true
title: Sad
date: 2014-12-08
tags:
  - sadness
  - free-verse
categories: english
---

![ ](../images/sad.png)

> **Why do I need to be happy all the time?**  
> **All this running around after happiness is so tiring.**  
> **So sometimes I just sit down and let myself be sad.**  
> **I allow myself to accept that some things are not meant to be mine,**  
> **howsoever hard I try to get them.**  
> **I permit myself to feel my failures,**  
> **I permit myself to feel my losses.**  
> **And once in a while I let myself be sad without any reason at all.**
>
> **Sadness is not being depressed.**  
> **Sadness doesn’t mean pitying on yourself.**  
> **Sadness is being sad just because you are.**  
> **You are human right?**  
> **You have the right to be sad when you feel so.**  
> **Let yourself be sometimes.**  
> **It’s not so bad.**  
> **I promise.**
>
> **~RavS**
