---
public: true
title: कोड  लिखते  लिखते 
date: 2011-12-01
tags:
  - work
  - witty
  - humor
categories: hindi
---

![ ](../images/code-likhte-likhte.jpg)

> **कौन  कमबख्त गम  भुलाने  के  लिए  पीता  है,  
> हमें  तो  बस  कोड  लिखते  लिखते  प्यास  लग  जाती  है …**
> 
> **~रबी**


\[ Who idiot drinks to forget the sorrows,  
We just get thirsty writing code… \]
