---
public: true
title: रश्क़
date: 2015-01-04
tags:
  - hopes
  - disappointment.
categories: hindi
---

> **बहुत आरज़ू लेके आया था मैं उसकी गलियों में,**  
> **उसने दीद की भी जेहमत न होने दी।**  
> **अब क्या करूँगा मैं इश्क़ में डूबकर उसके,**  
> **मुझे तो उसके रश्क़ में ही दफ़न कर दो।** 
> 
> **~रबी**

  
\[ I came in her lane with a lot of hopes,  
But she didn’t even let me have a look at her.  
Now, what will I do drowning in her love,  
Just let me get buried in her envy. \]
