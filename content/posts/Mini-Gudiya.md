---
public: true
title: मिनी  गुड़िया
date: 2017-01-07
tags:
  - father
  - promise
  - lullaby
  - special
  - dedication
categories: hindi
---

![ ](../images/mini-gudiya.jpg)

> **लोरी  नहीं  आती  गुड़िया  मुझे,  
> पर  यूँही  गोदी  में  सोया  करना।  
> अच्छा  कुछ  कहूँ  कान  में  ,  
> पर  मम्मा  से  मत  शेयर  करना।**
>
> **तू  रोयेगी  तो  सारी  रात  जगूँगा,  
> तू  मुस्कुराएगी  तो  मैं  साथ  हसूँगा,  
> खिलोने  कि  तरह  तेरे  साथ  खेलूँगा,  
> मम्मा से  भी  ज्यादा  तुझे  प्यार  करूँगा।  
> तेरे  पहले  कदम  पे  तेरा  हाथ  पकडूँगा,  
> ना  जाने  कब  यकीन  ये  बात  करूँगा।**
>
> **मेरी  प्यारी  मिनी  गुड़िया  ,  
> क्या  तू  सच  में  है  आयी ?….  
> समेट  ना  सकूँ  इतनी  खुशियाँ  ,  
> क्या  तू  सच  में  है  लायी ?…**
>
> **हर  मौसम  में  तेरा  साया  बनूँगा  ,  
> गलतियों  पे  तुझको  समझाया  करूँगा।  
> जब  शैतानी  करेगी  तो  डाँट  लगाया  भी  करूँगा  ,  
> मम्मा  से  तेरे  बदले  डाँट  खाया  भी  करूँगा।  
> यूँ  तो  उम्र  भर  तुझे  संवारा  ही  करूँगा  ,  
> कभी  कभी  मगर  तुझे  बिगाड़ा  भी   करूँगा।**
>
> **मेरी  प्यारी  मिनी  गुड़िया  ,  
> क्या  तू  सच  में  है  आयी ?….  
> समेट  ना  सकूँ  इतनी  खुशियाँ  ,  
> क्या  तू  सच  में  है  लायी ?…**
>
> **~रबी**


\[ I don’t know any lullaby doll,  
But you always sleep in my lap like this,  
Should I whisper something in your ear,  
But don’t share with mommy please.

When you will cry, I will remain awake all night,  
While you will smile, I will laugh with you,  
I will play with you like a toy,  
I will love you more than mommy.  
When you will take your first step, I will there to catch you,  
I don’t know when I am going to believe this.

My sweet mini doll,  
Have you really come? ….  
I can not absorb this joy,  
Have you really brought so much?….

I’ll be your shadow in every season,  
I will guide you through your mistakes,  
If you do something evil, I will scold you,  
But will also shield you from the scolds of mommy.  
Although I will always make you,  
Sometimes some mischiefs, I will also teach you.

My sweet mini doll,  
Have you really come? ….  
I can not absorb this joy,  
Have you really brought so much?…. \]

_## For a friend who recently became father of a lovely ‘Mini doll’. My best wishes to the parents and the little one :) ##_
