---
public: true
title: तू कुछ ऐसे बरसी
date: 2012-09-10
tags:
  - faaltu
  - monsoon
  - humor
categories: hindi
---

> **ना आसमान इतना नीला लगा,  
> ना धूप इतनी पीली,  
> ना पत्तियाँ इतनी हरी लगीं,  
> ना कलियाँ ऐसी गीली,  
> अबके सावन में तू कुछ ऐसे बरसी,  
> ओवरफ्लो कर गयी, मेरी गली की नाली!**
> 
> **~रबी**


\[ Never felt the sky so blue,  
Never found sunshine so yellow,  
Never saw leaves so green,  
Never felt buds so wet,  
This monsoon the way you rained,  
The drains of my street started overflowing! \]
