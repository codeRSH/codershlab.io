---
public: true
title: घर से निकलना
date: 2014-03-11
tags:
  - weather
  - play
categories: hindi
---

> **क्यों मौसम बदला बदला है देखो,**  
> **क्या इसने भी तुम्हारी अंगड़ाइयों की अदा सीख ली।**
> 
> **क्यों चल रही है सर्द हवा बेवजह रुक रुक कर,**  
> **क्या इन्होने भी तुम्हारी धड़कनो की संगत है कर ली।**
> 
> **निकले तो खुशनुमा धूप थी, पहुँचते पहुँचते बरसात हो गयी ,**  
> **क्या तुमने ही आसमान को खिले चेहरे से गीली लटें छटकने की तामील दी।**
> 
> **पेड़ हैं झूमते, पत्तियां हैं कांपती,**  
> **बेलें थर्राती हैं, टहनियाँ हैं अलापती,**  
> **क्या वजह हो सकती है, कुदरत से रिश्ता तोड़कर,**  
> **अब तो बस सब तुम्हारी पायल का ही कहा सुनती।**
> 
> **और ये जो बादल में रह रह कर चिंगारी है उठती,**  
> **डर जाते हैं हम, तुम्हारी ही कोई शरारत है लगती।**  
> **माना कि अक्सर तंग करना फितरत है आपकी,**  
> **यों तंग करने को मगर कम-स-कम कोई वजह तो दी होती।**
> 
> **तुम्हारी किलकारियों को हमने मेघों का गरजना समझ लिया ,**  
> **मौसम तो बस खेल रहा था संग आपके, हमने मौसम को ही गलत समझ लिया,**  
> **आप जो निकले आज आशियाने से अपने, आह… हमने घर से निकलना छोड़ दिया।**
> 
> **~रबी**


\[ Why this weather keeps on changing, see,  
Has it learnt your style of stretching the body,

Why is this cold breeze blowing, intermittently,  
Has it also started enjoying your heartbeat’s companionship.

When we started there was bright sunshine, by the time we reached we got drenched in rain,  
Have you taught the sky how to twitch wet hair from blooming face.

Trees vacillate, leaves vibrate,  
Vines tremble, twigs hum something,  
What could be the reason, having broken up with nature,  
They have started listening to your anklet.

And the sparks that fly in these clouds,  
They scare us, seems to be the product of your mischievousness,  
Agreed it’s your habit to annoy every now and then,  
But to annoy like this, at least you should have found a reason.

We mistook your giggles as the growls of clouds,  
The weather was just playing with you, we misunderstood the weather itself,  
You had come out of your nest after a long time, aahh… and we stopped coming out of our house. \]

_## City’s weather is changing like anything nowadays. It has also gotten me down with cough and fever. But it’s also kind of fun. Cold in morning, warm in afternoon and then rain by evening. Here, I have compared the changing weather to a girl’s feature and nature. Always, enjoy this kind of stretch of imagination :) ##_
