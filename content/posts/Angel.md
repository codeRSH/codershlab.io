---
public: true
title: Angel
date: 2015-02-07
tags:
  - free-verse
  - angel
categories: english
---

> **I am torn.  
> Between loving you and hating you.  
> Between wanting to harm you and trying to shield you.  
> Between deciding what’s more important,  
> You or my ego.  
> My yin and yang have left me lacerated.**
>
> **I don’t believe in  angels but please…  
> Just this one time,  
> Prove me wrong.  
> Be an angel.  
> And save me.  
> From myself.**
>
> **~RavS**
