---
public: true
title: Love by Brute Force
date: 2018-01-02
tags:
  - love
  - hate
categories: english
---

![ ](../images/love-by-brute-force.jpg)

> **She had springs in her steps, her eyes had glimmer,  
> She wanted to stay back in the present but jump into the future.  
> They called her July, the month she was born.  
> But she loved January, waiting for the page to turn.  
> She knew the next year would be more similar than different,  
> The disappointments would be as high as her expectations.  
> But her heart didn’t accept what her mind believed.  
> She had darkness all around, so hope’s flame she lit.  
> And so she continued, battered and bruised on the Love’s course.  
> Of all the hate of past, she just wanted to Love by Brute Force.**  
>   
> **~RavS**


_## Want of Love against hate. ##_  
