---
public: true
title: I Will Write
date: 2018-01-12
tags:
  - for-her
categories: english
---

![ ](../images/i-will-write.jpg)

> **I will write when you’d asleep,  
> And I will write when you’d be alone.  
> I will write when you’d need me,  
> And I will write when you’d feel bored.**  
>   
> **I will write when you’d be angry,  
> Or sad,  
> Or low…  
> But I will also write when you’d be playful,  
> Or ecstatic,  
> Or glow…**  
>   
> **I will write when it’d be difficult to (write),  
> I will write when I won’t know (what to write).  
> I will write when you’d refuse to (read that I write).  
> I will write when I’d miss you (while I write).**  
>   
> **For what will I be to me if not your writer that I imagined myself.  
> And what will I be to you if not the writer you claimed to love.**  
>   
> **Writing is a lonely exercise.  
> But it doesn’t feel so,  
> when I write for me and you,  
> my lovely audience of just two.**  
>   
> **~RavS**


_## Written on a sleepless night. Just in time for the morning :) ##_  
