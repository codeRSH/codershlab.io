---
public: true
title: Winter
date: 2014-11-25
tags:
  - melancholic
categories: english
---

![ ](../images/winter.png)

> **She was the chill that he felt in his spine,**  
> **Everytime the first drop of morning dew fell on his earlobes.**  
> **The glacial breeze that would permeate through his chest,**  
> **and tremble him through his entire core,**  
> **She was the frost on his lips that he tried to conceal with a balm.**  
> **More than the ache in the bare calves ran,**  
> **She was the frozen blood under the nails.**  
> **As cold as stones beside abandoned railway tracks,**  
> **She made him feel sad with her loneliness.**  
> **But her embrace felt assuring.**  
> **Like warmth of a torn blanket on a coal black cold night,**  
> **He wanted her,**  
> **Every bit of her,**  
> **He forgot her name deliberately,**  
> **So that he could call her WINTER.**
>
> **~RavS**
