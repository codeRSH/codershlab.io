---
public: true
title: छोटी सी लड़की की बातें इतनी बड़ी
date: 2014-08-27
tags:
  - dedication
categories: hindi
---

> **एक छोटी सी पागल सी लड़की थी,**  
> **बातें बड़ी बड़ी मगर करती थी।**   
> **बड़े अजीब से सपने थे उसके,**  
> **कुछ पराय कुछ अपने थे उसके,**  
> **अभी उड़ना कहाँ सीखा था उनने,**  
> **पंख लगने बाकी थे उनमे,**  
> **अपने सपने अपने में ही कैद रखती थी,**  
> **कोई छीन न ले, थोड़ा सा तो डरती थी।**   
> **लोग लड़ा करते थे उससे हर बात पे,**  
> **वो बेबात लोगों से लड़ा करती थी।**
> 
> **कभी मुह सिकोड़ती, कभी मुह बनाती थी,**  
> **वजन न बढे, तो थोड़ा कम ही खाती थी।**   
> **खैर ये तो हर लड़की की कहानी है,**  
> **उसमे क्या अलग था, ये बात कइयों से अंजानी थी,**  
> **कभी नाखुनो को तिरंगे में रंगती,**  
> **कभी हाथों को मेहँदी से ढंकती,**  
> **कभी कुछ पहनती, कभी कुछ पहनती,**  
> **कभी कभी कुछ और ही ढंग से सजती।**
> 
> **घरवाले-बाहर वाले, दोस्त-दुश्मन,**  
> **जाने-अनजाने, इंसान-जानवर,**  
> **सब थक गए थे भाग भाग कर,**  
> **जो मिल जाय जहां मिल जाय पकड़ लाती,**  
> **“चल आ, फोटो खिंचा ”,**  
> **फिर उनके साथ या उन्ही से फोटो खिंचवाती,**  
> **उसने हर रोज फोटो बदलने के शौक़ पाल रखे थे,**  
> **लोग उससे काम उसकी फोटो से ज्यादा बतियाते थे।**
> 
> **पर अब क्या ही कहोगे उसे,**  
> **अपनी दुनिया में खुश रहने दो उसे,**  
> **एक नदी है, ना रोको उसे,**  
> **अपनी ही रफ़्तार से बहने दो उसे,**  
> **एक दिन बहते बहते किसी समंदर में मिल जाएगी,**  
> **फिर बस दो ही बातें याद रह जाएंगी,**  
> **एक छोटी सी पागल सी लड़की थी,**  
> **बातें बड़ी बड़ी मगर करती थी।**
> 
> **~रबी**


\[ There was this mad little girl,   
But she used to talk about big big stuffs,   
She had strange dreams,   
Few were hers and few of others,   
But they hadn’t learned to fly yet,   
They were yet to get wings,   
She used to keep her dreams imprisoned in herself,   
Someone might snatch them from her, she used to be scared a little.   
People fought her on every point,   
She used to fight with people without a point.

Sometimes she would grump, sometimes she would pout,   
Being weight concious, she used to eat only a little bit,   
Well, but that’s the story of every girl,   
So what was different in her, it was a thing unknown to many,   
Sometimes she would paint her nails in tricolor,   
Sometimes she would cover her hands in henna,   
Sometimes she would wear X, Sometimes it would be Y and Z,   
Sometimes she would dress up in totally different style.

Family-outsiders, friends-enemies,   
Known-unknowns, humans-animals,   
All were tired of running from her,   
Whenever, wherever she would find someone, she would grab them,   
“Come on, let’s get clicked”,  
Then she would get her pictures done with or from them,   
She had decided to change her DP everyday,   
And people also used to talk to her less and converse more with her pictures.

But what to say to her now,   
Let her be happy in her world,   
She’s a river, please don’t stop her,   
Let her flow with her own pace,   
She would flow and meet an ocean one day,   
And then only two things will remain in memories,   
There was this mad little girl,   
But she used to talk about big big stuffs. \]
