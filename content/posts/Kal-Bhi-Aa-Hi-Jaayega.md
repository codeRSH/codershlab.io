---
public: true
title: कल  भी  आ  ही  जाएगा
date: 2012-02-07
tags:
  - life
  - philosophical
categories: hindi
---

> **कल - कल  करके  बहुत  जी  लिए ,**  
> **चलो  आज , आज  की  बात  करते  हैं ,**  
> **कल  का  क्या  है ,**  
> **कल  तो  कल  आया  था, कल  भी  आ  ही  जाएगा …**
> 
> **~रबी**


\[ Enough of living in the hope of ‘someday’,  
Today, let’s talk about today,  
What’s there in 'someday’,  
Someday came yesterday, it’s gonna come tomorrow as well. \]
