---
public: true
title: मेरे यार
date: 2014-07-24
tags:
  - song
  - friend
  - separation
categories: hindi
---

> **ऐ मेरे यार, कहाँ गया तू,**  
> **कौन गाँव किस देश,**  
> **आ देख तुझ बिन क्या हालत मेरी,**  
> **देख कैसा हुआ मेरा भेष।**
> 
> **देख अब सुबह ने भी उठना छोड़ दिया,**  
> **देख शाम भी बैठी रहती है गम सुम,**  
> **देख आहटें भी वीरान पड़ी हैं,**  
> **देख सरगोशियाँ भी रहती हैं चुप चुप।**
> 
> **लौट आ, याद। याद तेरी बहुत सी आती है,**  
> **पत्थर ऑंखें भी, जिनमे डूब। डूब पिघल सी जाती हैं ….**
> 
> **ऐ मेरे यार, कहाँ गया तू,**  
> **कौन गाँव किस देश,**  
> **आ देख तुझ बिन क्या हालत मेरी,**  
> **देख कैसा हुआ मेरा भेष।**
> 
> **क्या रूतबा तेरा आज इतना बढ़ गया,**  
> **क्या तू दोस्त के घर का रस्ता बिसर गया,**  
> **क्या सच में तू इतना खुदगर्ज़ हो गया,**  
> **क्या जरूरत नहीं थी मेरी, फिर क्यों अकेला छोड़ गया।**
> 
> **खुशियाँ थी जब तक तो साथ। साथ बांटता था हर वक्त,**  
> **जब ज़िन्दगी ने दर्द दिया, तो सब खुद क्यों ले गया ….**
> 
> **ऐ मेरे यार, कहाँ गया तू,**  
> **कौन गाँव किस देश,**  
> **आ देख तुझ बिन क्या हालत मेरी,**  
> **देख कैसा हुआ मेरा भेष।**
> 
> **~रबी**


\[ My friend, where did you go,  
Which country, which village,  
Come see what’s my condition without you,  
See what form have I taken.

See, the mornings have left waking up now,  
See, the evenings have stopped talking now,  
See, all the sounds have gone silent,  
See, the whispers also keep quiet now.

Come back, I miss you. I miss you a lot.  
These cold eyes melt in your thoughts, always ….

My friend, where did you go,  
Which country, which village,  
Come see what’s my condition without you,  
See what form have I taken.

Has you status increased so much today,  
Did you forget the way to your friend’s place,  
Did you really get so selfish;  
Didn’t you need me, then why did you leave me alone.

Whenever there was happiness, you would share with me all the time,  
When life started giving pain, why take it all yourself ….

My friend, where did you go,  
Which country, which village,  
Come see what’s my condition without you,  
See what form have I taken. \]
