---
public: true
title: रूह  तक  बेच  दी
date: 2012-04-26
tags:
  - hurt
  - pain
  - sad
categories: hindi
---

> **अब  तक  बस  हँसना  ही  आता  था  हर  घड़ी ,**  
> **तुमने  हर  पल  ग़म  सहना  सिखा  दिया।**
>
> **अब  तक  पहुँचता  था  मंज़िल  तक  वक्त - बेवक्त कभी ,**  
> **तुमने  मुश्तों  इंतज़ार  करना  सिखा  दिया।**
>
> **अब  तक  लगता  था  तुम  हो  बस  प्यार  के  ही  काबिल ,**  
> **तुमने  तुमसे  बेइन्तेहाँ  नफरत  करना  भी  सिखा  दिया।**
>
> **मैंने  तो  रूह  तक  बेच  दी  थी  तुम्हारे  नाम  से ,**  
> **बताओ   मेहरुल  तुमने  मुझे  क्या  दिया ?**
>
> **~रबी**


\[ Till now, I only knew how to laugh,  
Now, you have taught me how to bear the pain all the time.

Till now, I used to reach my destination somehow,  
Now, you have taught me to stand and wait forever.

Till now, I used to think you are made only to be loved,  
Now, you have taught me to hate you without limits.

Hey, I even sold my soul in your name,  
Tell me Mehrul, what did you give me back? \]
