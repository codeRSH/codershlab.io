---
public: true
title: जीना छोड़ दिया
date: 2014-05-14
tags:
  - end
  - nightmares
categories: hindi
---

> **तुम मिलते थे जिस राह पर, हमने जाना छोड़ दिया,**  
> **जो नाम तुम्हारा लेना पड़े, वो हमने कहना छोड़ दिया,**  
> **तुम आते थे जिन ख्यालो में, हमने सोचना छोड़ दिया,**  
> **आ ना जाओ सपनो में, डर से हमने सोना छोड़ दिया,**  
> **फिर भी जो तुम्हारी याद ने पीछा न छोड़ा,**  
> **थक हार कर हमने जीना छोड़ दिया।**
> 
> **~रबी**

  
\[ You used to meet me there, I left going through those ways,  
If your name has to come in it, I left saying those things,  
You used to come in some thoughts, I stopped thinking such stuffs,  
I fear you would come in dreams, so I stopped sleeping,  
But still when your memories didn’t stop chasing me,  
I was left with no choice but to stop living. \]
